package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "apicatalog")
public class ApiCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_api_catalog")
	private Long idApiCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "deployment_id")
    private String deploymentId;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "version")
    private String version;
    
    @Column(name = "env_id")
    private String envId;
    
    @Column(name = "deployment_state")
    private String deploymentState;
    
    @Column(name = "dev_org_id")
    private String devOrgId;
    
    @Column(name = "dev_org_name")
    private String devOrgName;
    
    @Column(name = "base_path")
    private String basePath;

    @Column(name = "removed_at")
    @DateTimeFormat(style = "S-")
    private Date removedAt;

    @Column(name = "created_at")
    @DateTimeFormat(style = "S-")
    private Date createdAt;
    
    @Column(name = "created_by")
    private String createdBy;

    @NotNull
    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;
    
    @Column(name = "updated_by")
    private String updatedBy;
    
    @Column(name = "url")
    private String url;
    
    @Column(name = "swagger_url")
    private String swaggerUrl;
    
    @Column(name = "x_ibm_name")
    private String xIbmName;
    
    @Column(name = "protocol")
    private String protocol;
    
    @Column(name = "contact_name")
    private String contactName;
    
    @Column(name = "contact_email")
    private String contactEmail;
    
    @Column(name = "description", length=3000)
    private String description;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_product_catalog", nullable = true, updatable = true, insertable = true)
    private ProductCatalog productCatalog;
    
    @Column(name = "azure_host")
    private String azureHost;
    
    @Column(name = "security")
    private String security;
    
    @Column(name = "is_case_enabled")
    private boolean isCaseEnabled;
    
    @Column(name = "confluence_page_id")
    private Long confluencePageId;

	public Long getIdApiCatalog() {
		return idApiCatalog;
	}

	public void setIdApiCatalog(Long idApiCatalog) {
		this.idApiCatalog = idApiCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getDevOrgId() {
		return devOrgId;
	}

	public void setDevOrgId(String devOrgId) {
		this.devOrgId = devOrgId;
	}

	public String getDevOrgName() {
		return devOrgName;
	}

	public void setDevOrgName(String devOrgName) {
		this.devOrgName = devOrgName;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Date getRemovedAt() {
		return removedAt;
	}

	public void setRemovedAt(Date removedAt) {
		this.removedAt = removedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getxIbmName() {
		return xIbmName;
	}

	public void setxIbmName(String xIbmName) {
		this.xIbmName = xIbmName;
	}

	public String getProtocol() {
		return protocol;
	}

	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactEmail() {
		return contactEmail;
	}

	public void setContactEmail(String contactEmail) {
		this.contactEmail = contactEmail;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getEnvId() {
		return envId;
	}

	public void setEnvId(String envId) {
		this.envId = envId;
	}

	public String getDeploymentState() {
		return deploymentState;
	}

	public void setDeploymentState(String deploymentState) {
		this.deploymentState = deploymentState;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getSwaggerUrl() {
		return swaggerUrl;
	}

	public void setSwaggerUrl(String swaggerUrl) {
		this.swaggerUrl = swaggerUrl;
	}

	public String getDeploymentId() {
		return deploymentId;
	}

	public void setDeploymentId(String deploymentId) {
		this.deploymentId = deploymentId;
	}

	public ProductCatalog getProductCatalog() {
		return productCatalog;
	}

	public void setProductCatalog(ProductCatalog productCatalog) {
		this.productCatalog = productCatalog;
	}

	public String getAzureHost() {
		return azureHost;
	}

	public void setAzureHost(String azureHost) {
		this.azureHost = azureHost;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public boolean isCaseEnabled() {
		return isCaseEnabled;
	}

	public void setCaseEnabled(boolean isCaseEnabled) {
		this.isCaseEnabled = isCaseEnabled;
	}

	public Long getConfluencePageId() {
		return confluencePageId;
	}

	public void setConfluencePageId(Long confluencePageId) {
		this.confluencePageId = confluencePageId;
	}
    
    
}
