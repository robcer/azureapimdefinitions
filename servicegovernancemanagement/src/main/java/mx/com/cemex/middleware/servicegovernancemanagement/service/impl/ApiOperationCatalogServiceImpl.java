package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlProperties;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProductCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlPropertiesDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiOperationCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;


/**
 * 
 * @author RogelioReyo
 *
 */

@Service("apiOperationCatalogService")
public class ApiOperationCatalogServiceImpl implements ApiOperationCatalogService {
	
	private static final Logger LOG  = Logger.getLogger(ApiOperationCatalogServiceImpl.class);

	@Autowired
	ApiOperationCatalogDAO apiOperationCatalogDAO;

	@Autowired
	ProductCatalogDAO productCatalogDAO;
	
	@Autowired
	YamlPropertiesDAO yamlPropertiesDAO;
	
	@Autowired
	ApiCatalogDAO apiCatalogDAO;
	
	@Autowired
	UtilitiesService utilitiesService;
	
	public void mantainApiOperationCatalog(ApiCatalog apiCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException {

		if(apiCatalog.getProductCatalog() != null) {
			List<YamlProperties> yamlPropertiesList = yamlPropertiesDAO.findAllYamlPropertiesByFullPathAndProductNameAndXIbmName(apiCatalog.getProductCatalog().getProductName(), apiCatalog.getxIbmName(), apiCatalog.getBasePath(), apiCatalog.getDevOrgId());
			List<ApiOperationCatalog> apiOperationCatalogs = getApiOperationCatalogs(yamlPropertiesList, apiCatalog);
			for(ApiOperationCatalog apiOperationCatalog: apiOperationCatalogs) {
				try {
					apiOperationCatalogDAO.findApiOperationCatalogByPathAndMethodAndEnvId(apiCatalog.getEnvId(), apiOperationCatalog.getPath(), apiOperationCatalog.getMethod(), apiCatalog.getBasePath());
				} catch (NoResultException e) {
					apiOperationCatalog.setRegisteredAt(new Date());
					apiOperationCatalogDAO.create(apiOperationCatalog);
				} catch (NonUniqueResultException e) {
//					e.printStackTrace();
				}
			}
		}
	}


	private List<ApiOperationCatalog> getApiOperationCatalogs(List<YamlProperties> yamlPropertiesList, ApiCatalog apiCatalog) {
		List<ApiOperationCatalog> apiOperationCatalogs = new ArrayList<ApiOperationCatalog>();
		for(YamlProperties yamlProperties: yamlPropertiesList) {
			ApiOperationCatalog apiOperationCatalog = new ApiOperationCatalog();
			apiOperationCatalog.setApiId(apiCatalog.getDeploymentId());
			apiOperationCatalog.setDevOrgId(apiCatalog.getDevOrgId());
			apiOperationCatalog.setDevOrgName(apiCatalog.getDevOrgName());
			apiOperationCatalog.setEnvId(apiCatalog.getEnvId());
			apiOperationCatalog.setPath(yamlProperties.getPath());
			apiOperationCatalog.setDescription(yamlProperties.getDescription());
			apiOperationCatalog.setBasePath(yamlProperties.getBasePath());
			apiOperationCatalog.setSecurity(yamlProperties.getSecurity());
			apiOperationCatalog.setMethod(yamlProperties.getMethod());
			apiOperationCatalog.setOperationId(yamlProperties.getOperationId());
			apiOperationCatalog.setDescription(yamlProperties.getOperationDescription());
			if(yamlProperties.getIsCasePrepared().equals("NO")) {
				apiOperationCatalog.setCaseEnabled(false);
			} else {
				apiOperationCatalog.setCaseOperations(yamlProperties.getIsCasePrepared());
				apiOperationCatalog.setCaseEnabled(true);
			}
			if(yamlProperties.getIsIntegratedWithNET().equals("NO")) {
				apiOperationCatalog.setIntegratedWithNet(false);
			} else if(yamlProperties.getIsIntegratedWithNET().equals("MISSINGXIBMCONFIGURATION")){
				apiOperationCatalog.setIntegratedWithNet(false);
				apiOperationCatalog.setObservation("MISSINGXIBMCONFIGURATION");
			} else {
				apiOperationCatalog.setIntegratedWithNet(true);
				apiOperationCatalog.setInvokeTitle(yamlProperties.getIsIntegratedWithNET());
			}
			apiOperationCatalog.setApiCatalog(apiCatalog);
			apiOperationCatalog.setInvokeTargetURL(yamlProperties.getInvokeTargetURL());
			apiOperationCatalogs.add(apiOperationCatalog);
			apiCatalog.setSecurity(yamlProperties.getSecurity());
			apiCatalog.setAzureHost(yamlProperties.getAzureHost());
			apiCatalogDAO.merge(apiCatalog);
		}
		return apiOperationCatalogs;
	}
	
}
