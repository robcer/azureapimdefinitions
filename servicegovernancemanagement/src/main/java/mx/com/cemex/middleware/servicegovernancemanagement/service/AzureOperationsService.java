package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.json.JSONObject;
import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.AzureTenantStatusInformation;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.AzureTenantSyncState;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzurePropertyCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface AzureOperationsService {
	
	String generateSharedAccessSignature(EnvironmentCatalog environmentCatalog) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException;
	String getApiPolicy(EnvironmentCatalog environmentCatalog, String apiId) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException;
	String getTenantPolicy(EnvironmentCatalog environmentCatalog) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException;
	String getOperationPolicy(EnvironmentCatalog environmentCatalog, String operationId) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException;
	List<AzureApiCatalog> getApiList(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	List<AzureApiCatalog> getFilteredApi(EnvironmentCatalog environmentCatalog, String filter, String operator, String value) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	int updateAzurePolicy(EnvironmentCatalog environmentCatalog, String uriOperation, String xml) throws MalformedURLException, IOException, ParseException;
	String syncTenantGitRepository(EnvironmentCatalog environmentCatalog, String branch, boolean force) throws MalformedURLException, IOException, ParseException;
	AzureTenantStatusInformation getTentantOperationStatusInformation(EnvironmentCatalog environmentCatalog, String id) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	AzureTenantSyncState getTentantSyncState(EnvironmentCatalog environmentCatalog, String id) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	List<AzureApiOperationCatalog> getApiOperations(AzureApiCatalog azureApiCatalog, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	AzureApiOperationCatalog getOperationById(String operationId, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	List<AzurePropertyCatalog> getAzureProperties(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException;
	AzureApiCatalog createOrUpdateApi(EnvironmentCatalog environmentCatalog, AzureApiCatalog azureApiCatalog) throws MalformedURLException, IOException, ParseException;
	AzureApiOperationCatalog createNewOperation(EnvironmentCatalog environmentCatalog, String operationId, JSONObject jsonObject) throws MalformedURLException, IOException, ParseException;
	AzureApiOperationCatalog migrateOperationIntoAPITargetTenant(EnvironmentCatalog targetEnvironmentCatalog, AzureApiOperationCatalog sourceAzureApiOperationCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException, InterruptedException;
	AzureApiCatalog migrateAPIIntoTargetTenant(EnvironmentCatalog targetEnvironmentCatalog, AzureApiCatalog sourceAzureApiCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException, InterruptedException;
	void commitToBitbucketIntegration(String policy, String fileName, String branch, String repository, String author, String pathToFile, String message, String user) throws IOException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException;
}
