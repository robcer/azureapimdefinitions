package mx.com.cemex.middleware.servicegovernancemanagement.service;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;

public interface TenantCompareSummaryService extends CrudService<TenantCompareSummary, Long> {

}
