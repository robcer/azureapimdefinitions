package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProductCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProductCatalogDAO;

@Service("productCatalogDAO")
public class ProductCatalogDAOImpl implements ProductCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ProductCatalogDAOImpl()
    {
    }

    ProductCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ProductCatalog create(ProductCatalog productCatalog) {
        em.persist(productCatalog);
        return productCatalog;
    }

    @Transactional
    public ProductCatalog merge(ProductCatalog productCatalog) {
        em.merge(productCatalog);
        return productCatalog;
    }

    public long countProductCatalog() {
        return (Long) em.createQuery("select count(o) from ProductCatalog o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<ProductCatalog> findProductCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from ProductCatalog o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ProductCatalog findProductCatalog(Long id) {
        if (id == null) return null;
        return em.find(ProductCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ProductCatalog> findAll() {
        return em.createQuery("select o from ProductCatalog o").getResultList();
    }
    
    public List<ProductCatalog> findAllByDevOrgId(String devOrgId) {
        if (devOrgId == null) return null;
        return em.createQuery("select o from ProductCatalog o where o.devOrgId = :devOrgId", ProductCatalog.class).setParameter("devOrgId", devOrgId).getResultList();
    }
    
    public List<ProductCatalog> findAllByDevOrgIdNotRemoved(String devOrgId) {
        if (devOrgId == null) return null;
        return em.createQuery("select o from ProductCatalog o where o.devOrgId = :devOrgId and deploymentState = 'published'", ProductCatalog.class).setParameter("devOrgId", devOrgId).getResultList();
    }
    
    public ProductCatalog findProductCatalogById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from ProductCatalog o where o.id = :id", ProductCatalog.class)
        		.setParameter("id", id).getSingleResult();
    }

}