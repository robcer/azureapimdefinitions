package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareDetail;

public interface TenantCompareDetailDAO {

	TenantCompareDetail create(TenantCompareDetail tenantCompareDetail);
	TenantCompareDetail merge(TenantCompareDetail tenantCompareDetail);
    long countCompareDetail();
    List<TenantCompareDetail> findTenantCompareDetail(int firstResult, int maxResults);
    List<TenantCompareDetail> findAll();
    TenantCompareDetail findTenantCompareDetail(Long id);
    void deleteAll();
}
