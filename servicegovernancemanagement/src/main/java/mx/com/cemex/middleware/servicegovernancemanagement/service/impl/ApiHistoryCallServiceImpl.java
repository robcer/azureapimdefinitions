package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;

import java.sql.Timestamp;
import java.text.MessageFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.cli.CommandLine;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiHistoryCall;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiHistoryCallDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiHistoryCallService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

@Service("apiHistoryCallService")
public class ApiHistoryCallServiceImpl implements ApiHistoryCallService {
	
	private static final Logger LOG  = Logger.getLogger(ApiHistoryCallServiceImpl.class);

	@Autowired
	ApiHistoryCallDAO apiHistoryCallDAO;
	
	@Autowired
	UtilitiesService utilitiesService;

	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;

	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	private SimpleDateFormat dateFormatterFile = new SimpleDateFormat("ddMMyyyyHHmmssZ");
	private SimpleDateFormat dateSQLLoaderFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private File plainAPILoader = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + dateFormatterFile.format(new Date()) + "_PlainAPILoader.txt");
	
	private static String[] attributes = { "datetime", "apiId", "apiName", "apiVersion", "appName", "catalogName",
			"planId", "planName", "planVersion", "productName", "productVersion", "devOrgId", "devOrgName",
			"resourceId", "timeToServeRequest", "bytesSent", "requestProtocol", "requestMethod", "uriPath",
			"queryString", "statusCode", "latency", "orgId", "orgName", "resourcePath", "productTitle" };

	private static List<String> attributesList = new ArrayList<String>(Arrays.asList(attributes));
	
	public void callAndStoreNextApiHistoryCall(EnvironmentCatalog environmentCatalog, CommandLine commandLine) throws IOException, ParseException, java.text.ParseException {
		LOG.info("Fetching Analytics getting latest API calls on Environment: " + commandLine.getOptionValue("organization") + "...");
		if(environmentCatalog.getCatalogEnv().equals(commandLine.getOptionValue("catalog"))) {
			String stringUrl = "https://{0}/v1/orgs/{1}/environments/{2}/events?after={3}&before={4}&limit{5}&fields={6}";
			String cUrl = MessageFormat.format(stringUrl, commandLine.getOptionValue("region"), commandLine.getOptionValue("organization"), commandLine.getOptionValue("catalog"), commandLine.getOptionValue("after"), commandLine.getOptionValue("before"), commandLine.getOptionValue("limit"), String.join(",", attributesList));
			jsonCalls(cUrl, environmentCatalog, null);
		}
	}
	
	
	public void callAndStoreNextApiHistoryCall(EnvironmentCatalog environmentCatalog) throws IOException, ParseException, java.text.ParseException {
		Timestamp timestamp = environmentCatalog.getLatestOperationCall();
	    if(IYamlBeansMasterConstants.INCREMENTAL_LOAD == false) {
	    	timestamp = apiHistoryCallDAO.getFirstDateTime(environmentCatalog.getDevOrgNameEnv(), environmentCatalog.getCatalogEnv(), environmentCatalog.getId());
		}
	    LOG.info("Fetching Analytics getting latest API calls on Environment: " + environmentCatalog.getRegion() + " - " + environmentCatalog.getDevOrgNameEnv() + "...");
		Date lastDateTime = new Date();
		if(timestamp == null) {
			lastDateTime = DateUtils.addMonths(new Date(), -6);
			lastDateTime = DateUtils.addMilliseconds(lastDateTime, 1);
		} else {
			lastDateTime = new Date(timestamp.getTime());
			lastDateTime = DateUtils.addMilliseconds(lastDateTime, 1);
		}
		Date processingDate = lastDateTime;
		processingDate = DateUtils.addMinutes(processingDate, 30);
		Date lastProcessedDate = processingDate;
		while(processingDate.before(new Date())) {
//		if(environmentCatalog.getId().equals("59b1cb240cf22aa4792673fe")) {
//			String stringUrl = "https://"+MessageFormat.format(IYamlBeansMasterConstants.CLOUD_BLUEMIX_HOSTNAME, environmentCatalog.getRegionHostAcronym())+"/v1/orgs/{0}/environments/{1}/events?before=2018-01-30T23:59:59.999&after=2018-01-30T00:00:00.000&limit={3}&fields={4}";
//			String stringUrl = "https://us2.apiconnect.ibmcloud.com/v1/orgs/cnx-gbl-org-qa2/environments/qa2/events?before=2018-02-04T23:00:00.000&after=2018-02-04T22:30:00.001&limit=2000&fields=datetime,apiId,apiName,apiVersion,appName,catalogName,planId,planName,planVersion,productName,productVersion,devOrgId,devOrgName,resourceId,timeToServeRequest,bytesSent,requestProtocol,requestMethod,uriPath,queryString,statusCode,latency,orgId,orgName,resourcePath,productTitle";
			try {
				String stringUrl = "https://"+MessageFormat.format(IYamlBeansMasterConstants.CLOUD_BLUEMIX_HOSTNAME, environmentCatalog.getRegionHostAcronym())+"/v1/orgs/{0}/environments/{1}/events?" + (IYamlBeansMasterConstants.INCREMENTAL_LOAD?"after":"before") + "={2,date,yyyy-MM-dd'T'HH:mm:ss.SSS}&" + (IYamlBeansMasterConstants.INCREMENTAL_LOAD?"before={3,date,yyyy-MM-dd'T'HH:mm:ss.SSS}&":"") + "limit={4}&fields={5}";
				String limit = "2000";
				String cUrl = MessageFormat.format(stringUrl, environmentCatalog.getDevOrgNameEnv(), environmentCatalog.getCatalogEnv(), lastProcessedDate, processingDate, limit, String.join(",", attributesList));
				jsonCalls(cUrl, environmentCatalog, processingDate);
				environmentCatalogDAO.merge(environmentCatalog);
				lastProcessedDate = processingDate;
				processingDate = DateUtils.addMinutes(processingDate, 30);
			} catch (Exception e) {
				e.printStackTrace(System.out);
			}
//			}
		}
	}
	
	private boolean jsonCalls(String cUrl, EnvironmentCatalog environmentCatalog, Date processingDate) throws MalformedURLException, java.text.ParseException, IOException, ParseException {
		org.json.JSONObject object = utilitiesService.getJSONObject(cUrl, utilitiesService.getEncodedAuthorization(IYamlBeansMasterConstants.BLUEMIX_CREDENTIALS));
	    JSONArray jsonCalls = object.optJSONArray("calls");
	    if(jsonCalls.toList().isEmpty()) {
	    	LOG.info("Not API calls on Environment: " + environmentCatalog.getRegion() + " - " + environmentCatalog.getDevOrgNameEnv() + " " + dateFormatter.format(processingDate) + "..." + cUrl);
		    environmentCatalog.setLatestOperationCall(new Timestamp(processingDate.getTime()));
		    return false;
	    } else {
	    	LOG.info("Last Date we are going to register on the database: " + dateFormatter.parse(jsonCalls.getJSONObject(0).optString("datetime")));
		    environmentCatalog.setLatestOperationCall(new Timestamp(dateFormatter.parse(jsonCalls.getJSONObject(0).optString("datetime")).getTime()));
		    if(IYamlBeansMasterConstants.INITIAL_LOAD) {
		    	initialLoadFlow(object, jsonCalls, cUrl);
		    } else {
		    	appendNewRequests(object, jsonCalls, cUrl);
		    }
	    }
	    return true;
	}
	

	private void appendNewRequests(org.json.JSONObject object, JSONArray jsonCalls, String cUrl) throws MalformedURLException, java.text.ParseException, IOException, ParseException {
		LOG.info("A total of " + object.optLong("totalCalls") + " new API calls would be added to the database.");
		int count = jsonCalls.length();
		do {
			cUrl = object.optString("nextHref");
		    for (int i = 0, size = jsonCalls.length(); i < size; i++) {
		    	if(jsonCalls.getJSONObject(i).optString("requestMethod") != null) {
			    	if(!jsonCalls.getJSONObject(i).optString("requestMethod").equals("OPTIONS")) {
				    	ApiHistoryCall apiHistoryCall = fillApiHistoryCall(jsonCalls.getJSONObject(i));
				    	if(apiHistoryCall != null) {
				    		apiHistoryCallDAO.create(apiHistoryCall);
				    	}
			    	}
		    	}
		    }
		    LOG.info(count + " new entries are being processed out of " + object.optLong("totalCalls") + ".");
			object = utilitiesService.getJSONObject(cUrl, utilitiesService.getEncodedAuthorization(IYamlBeansMasterConstants.BLUEMIX_CREDENTIALS));
		    jsonCalls = object.optJSONArray("calls");
		    count += jsonCalls.length();
		} while (jsonCalls.length() > 0);
	}


	private void initialLoadFlow(org.json.JSONObject object, JSONArray jsonCalls, String cUrl) throws JSONException, java.text.ParseException, MalformedURLException, IOException, ParseException {
		LOG.info(object.optLong("totalCalls"));
		do {
			cUrl = object.optString("nextHref");
		    for (int i = 0, size = jsonCalls.length(); i < size; i++) {
		    	if(jsonCalls.getJSONObject(i).optString("requestMethod") != null) {
			    	if(!jsonCalls.getJSONObject(i).optString("requestMethod").equals("OPTIONS")) {
			    		addDataToSQLLoaderFile(jsonCalls.getJSONObject(i));
			    	}
		    	}
		    }
			object = utilitiesService.getJSONObject(cUrl, utilitiesService.getEncodedAuthorization(IYamlBeansMasterConstants.BLUEMIX_CREDENTIALS));
		    jsonCalls = object.optJSONArray("calls");
		} while (jsonCalls.length() > 0);
	}


	private ApiHistoryCall fillApiHistoryCall(JSONObject jsonObject) throws java.text.ParseException {
    	ApiHistoryCall apiHistoryCall = new ApiHistoryCall();
    	apiHistoryCall.setApiId(jsonObject.optString("apiId"));
    	if(jsonObject.optString("devOrgId") == null && jsonObject.optString("apiName").equals("")) {
    		return null;
    	}
    	apiHistoryCall.setApiName(jsonObject.optString("apiName"));
    	apiHistoryCall.setApiVersion(jsonObject.optString("apiVersion"));
    	apiHistoryCall.setAppName(jsonObject.optString("appName"));
    	apiHistoryCall.setBytesSent(jsonObject.optLong("bytesSent"));
    	apiHistoryCall.setCatalogName(jsonObject.optString("catalogName"));
    	apiHistoryCall.setDatetime(dateFormatter.parse(jsonObject.optString("datetime")));
    	apiHistoryCall.setRegisteredAt(new Date());
    	if(jsonObject.optString("devOrgId") != null && !jsonObject.optString("devOrgId").equals("")) {
    		apiHistoryCall.setDevOrgId(jsonObject.optString("devOrgId"));
    	} else {
    		apiHistoryCall.setDevOrgId(jsonObject.optString("orgId"));
    	}
    	if(jsonObject.optString("devOrgName") != null && !jsonObject.optString("devOrgName").equals("")) {
        	apiHistoryCall.setDevOrgName(jsonObject.optString("devOrgName"));
    	} else {
    		apiHistoryCall.setDevOrgName(jsonObject.optString("orgName"));
    	}
    	apiHistoryCall.setLatency(jsonObject.optString("latency"));
    	apiHistoryCall.setOrgId(jsonObject.optString("orgId"));
    	apiHistoryCall.setOrgName(jsonObject.optString("orgName"));
    	apiHistoryCall.setPlanId(jsonObject.optString("planId"));
    	apiHistoryCall.setPlanName(jsonObject.optString("planName"));
    	apiHistoryCall.setPlanVersion(jsonObject.optString("planVersion"));
    	apiHistoryCall.setProductName(jsonObject.optString("productName"));
    	apiHistoryCall.setProductTitle(jsonObject.optString("productTitle"));
    	apiHistoryCall.setProductVersion(jsonObject.optString("productVersion"));
    	apiHistoryCall.setQueryString(jsonObject.optString("queryString"));
    	apiHistoryCall.setRequestMethod(jsonObject.optString("requestMethod"));
    	apiHistoryCall.setRequestProtocol(jsonObject.optString("requestProtocol"));
    	apiHistoryCall.setResourceId(jsonObject.optString("resourceId"));
    	apiHistoryCall.setResourcePath(jsonObject.optString("resourcePath"));
    	apiHistoryCall.setStatusCode(jsonObject.optString("statusCode"));
    	apiHistoryCall.setTimeToServeRequest(jsonObject.optLong("timeToServeRequest"));
    	apiHistoryCall.setUriPath(jsonObject.optString("uriPath"));
    	try {
    		Pattern pattern = Pattern.compile("\\/[^\\/]+\\/[^\\/]+(.*?)(\\+\\/)?+(.*?)");
    		Matcher matcher = pattern.matcher(jsonObject.optString("uriPath"));
    		int count = 0;
    		String basePath = "";
    		while (matcher.find()) {
    			count++;
    			if(count == matcher.groupCount()-1){
    				basePath = matcher.group(0);
    				break;
    			}
    		}
    		apiHistoryCall.setPath(basePath + jsonObject.optString("resourceId"));
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	if(apiHistoryCall.getUriPath().length() > 4000) {
        	apiHistoryCall.setUriPath(apiHistoryCall.getUriPath().substring(0, 4000));
    	}
	    return apiHistoryCall;
	}
	
	@SuppressWarnings("deprecation")
	private void addDataToSQLLoaderFile(JSONObject jsonObject) throws java.text.ParseException {
		String strSQLLoader = jsonObject.optString("apiId") + "|" +
				jsonObject.optString("apiName") + "|" +
				jsonObject.optString("apiVersion") + "|" +
				jsonObject.optString("appName") + "|" +
				jsonObject.optLong("bytesSent") + "|" +
				jsonObject.optString("catalogName") + "|" +
				dateSQLLoaderFormat.format(dateFormatter.parse(jsonObject.optString("datetime"))) + "|" +
				jsonObject.optString("devOrgId") + "|" +
				jsonObject.optString("devOrgName") + "|" +
				jsonObject.optString("latency") + "|" +
				jsonObject.optString("orgId") + "|" +
				jsonObject.optString("orgName") + "|" +
				jsonObject.optString("planId") + "|" +
				jsonObject.optString("planName") + "|" +
				jsonObject.optString("planVersion") + "|" +
				jsonObject.optString("productName") + "|" +
				jsonObject.optString("productTitle") + "|" +
				jsonObject.optString("productVersion") + "|" +
				jsonObject.optString("queryString") + "|" +
				jsonObject.optString("requestMethod") + "|" +
				jsonObject.optString("requestProtocol") + "|" +
				jsonObject.optString("resourceId") + "|" +
				jsonObject.optString("resourcePath") + "|" +
				jsonObject.optString("statusCode") + "|" +
				jsonObject.optLong("timeToServeRequest") + "|" +
				jsonObject.optString("uriPath") + "|\n";
		try {
			FileUtils.writeStringToFile(plainAPILoader, strSQLLoader, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
