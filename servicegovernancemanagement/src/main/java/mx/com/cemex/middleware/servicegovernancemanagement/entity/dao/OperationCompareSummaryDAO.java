package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareSummary;

public interface OperationCompareSummaryDAO {

	OperationCompareSummary create(OperationCompareSummary operationCompareSummary);
	OperationCompareSummary merge(OperationCompareSummary operationCompareSummary);
    long countOperationCompareSummary();
    List<OperationCompareSummary> findOperationCompareSummary(int firstResult, int maxResults);
    List<OperationCompareSummary> findAll();
    OperationCompareSummary findOperationCompareSummary(Long id);
    List<OperationCompareSummary> findAllByReportGUID(String reportGuid);
    List<Object> findTargetFileNameByReportGUID(String reportGuid);
    void deleteAll();

}
