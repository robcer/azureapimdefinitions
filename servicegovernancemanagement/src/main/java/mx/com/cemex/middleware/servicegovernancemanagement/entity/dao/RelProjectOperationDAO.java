package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProjectCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelProjectOperation;

public interface RelProjectOperationDAO {

	RelProjectOperation create(RelProjectOperation relProjectOperation);
	RelProjectOperation merge(RelProjectOperation relProjectOperation);
    long countRelProjectOperation();
    List<RelProjectOperation> findRelProjectOperation(int firstResult, int maxResults);
    List<RelProjectOperation> findAll();
    RelProjectOperation findRelProjectOperation(Long id);
    List<RelProjectOperation> findAllByProjectAndOperation(ProjectCatalog projectCatalog, ApiOperationCatalog apiOperationCatalog);

}
