package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;

public interface ApiGovernanceService {

	void runMaitainanceProcess() throws IOException, InterruptedException;
	
}
