package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

public interface CompareFilesService {
	
	void prepareAndCompareVersions() throws IOException, ParseException, java.text.ParseException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException;
	void prepareAndCompareVersions(String guid) throws IOException, ParseException, java.text.ParseException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException;
}
