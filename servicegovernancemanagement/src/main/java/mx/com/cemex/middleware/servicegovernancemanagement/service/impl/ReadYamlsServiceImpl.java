package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.service.ReadYamlPortfolioService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ReadYamlPropertiesValuesService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ReadYamlsService;

@Service("readYamlsService")
public class ReadYamlsServiceImpl implements ReadYamlsService {

	@Autowired
	ReadYamlPortfolioService readYamlPortfolioService;

	@Autowired
	ReadYamlPropertiesValuesService readYamlPropertiesValuesService;

	public void readYamlInformation() throws FileNotFoundException, IllegalAccessException, InvocationTargetException {
		
		readYamlPortfolioService.readYamlPorfolio(readYamlPropertiesValuesService.readPropertiesValues());
	}
}
