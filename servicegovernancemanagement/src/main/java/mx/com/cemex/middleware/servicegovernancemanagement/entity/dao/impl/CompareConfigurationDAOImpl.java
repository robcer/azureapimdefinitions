package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareConfigurationDAO;

@Service("compareConfigurationDAO")
public class CompareConfigurationDAOImpl implements CompareConfigurationDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public CompareConfigurationDAOImpl()
    {
    }

    CompareConfigurationDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public CompareConfiguration create(CompareConfiguration compareConfiguration) {
        em.persist(compareConfiguration);
        return compareConfiguration;
    }

    @Transactional
    public CompareConfiguration merge(CompareConfiguration compareConfiguration) {
        em.merge(compareConfiguration);
        return compareConfiguration;
    }

    public long countCompareConfiguration() {
        return (Long) em.createQuery("select count(o) from CompareConfiguration o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<CompareConfiguration> findCompareConfiguration(int firstResult, int maxResults) {
        return em.createQuery("select o from CompareConfiguration o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public CompareConfiguration findCompareConfiguration(Long id) {
        if (id == null) return null;
        return em.find(CompareConfiguration.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<CompareConfiguration> findAll() {
        return em.createQuery("select o from CompareConfiguration o where o.isEnable = 1 order by o.idCompareConfiguration DESC").getResultList();
    }

}