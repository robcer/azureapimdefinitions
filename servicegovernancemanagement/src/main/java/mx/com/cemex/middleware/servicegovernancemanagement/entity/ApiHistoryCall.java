package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "apihistorycall")
public class ApiHistoryCall {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_api_history_call")
	private Long idApiHistoryCall;

    @NotNull
    @Column(name = "datetime")
    @DateTimeFormat(style = "S-")
    private Date datetime;

    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "api_id")
    private String apiId;
    
    @Column(name = "api_name")
    private String apiName;
    
    @Column(name = "api_version")
    private String apiVersion;
    
    @Column(name = "app_name")
    private String appName;
    
    @Column(name = "catalog_name")
    private String catalogName;
    
    @Column(name = "plan_id")
    private String planId;
    
    @Column(name = "plan_name")
    private String planName;
    
    @Column(name = "plan_version")
    private String planVersion;
   
    @Column(name = "product_name")
    private String productName;
    
    @Column(name = "product_version")
    private String productVersion;
    
    @Column(name = "dev_org_id")
    private String devOrgId;
    
    @Column(name = "dev_org_name")
    private String devOrgName;
    
    @Column(name = "resource_id")
    private String resourceId;
    
    @Column(name = "time_to_serve_request")
    private Long timeToServeRequest;
    
    @Column(name = "bytes_sent")
    private Long bytesSent;
    
    @Column(name = "request_protocol")
    private String requestProtocol;
    
    @Column(name = "request_method")
    private String requestMethod;
    
    @Column(name = "uri_path")
    private String uriPath;
    
    @Column(name = "query_string")
	@Lob
    private String queryString;
    
    @Column(name = "status_code")
    private String statusCode;
    
    @Column(name = "latency")
    private String latency;
    
    @Column(name = "org_id")
    private String orgId;
    
    @Column(name = "org_name")
    private String orgName;
    
    @Column(name = "resource_path")
    private String resourcePath;
    
    @Column(name = "product_title")
    private String productTitle;
    
    @Column(name = "path")
    private String path;

	public Long getIdApiHistoryCall() {
		return idApiHistoryCall;
	}

	public void setIdApiHistoryCall(Long idApiHistoryCall) {
		this.idApiHistoryCall = idApiHistoryCall;
	}

	public Date getDatetime() {
		return datetime;
	}

	public void setDatetime(Date datetime) {
		this.datetime = datetime;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getCatalogName() {
		return catalogName;
	}

	public void setCatalogName(String catalogName) {
		this.catalogName = catalogName;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getPlanVersion() {
		return planVersion;
	}

	public void setPlanVersion(String planVersion) {
		this.planVersion = planVersion;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getDevOrgId() {
		return devOrgId;
	}

	public void setDevOrgId(String devOrgId) {
		this.devOrgId = devOrgId;
	}

	public String getDevOrgName() {
		return devOrgName;
	}

	public void setDevOrgName(String devOrgName) {
		this.devOrgName = devOrgName;
	}

	public String getResourceId() {
		return resourceId;
	}

	public void setResourceId(String resourceId) {
		this.resourceId = resourceId;
	}

	public Long getTimeToServeRequest() {
		return timeToServeRequest;
	}

	public void setTimeToServeRequest(Long timeToServeRequest) {
		this.timeToServeRequest = timeToServeRequest;
	}

	public Long getBytesSent() {
		return bytesSent;
	}

	public void setBytesSent(Long bytesSent) {
		this.bytesSent = bytesSent;
	}

	public String getRequestProtocol() {
		return requestProtocol;
	}

	public void setRequestProtocol(String requestProtocol) {
		this.requestProtocol = requestProtocol;
	}

	public String getRequestMethod() {
		return requestMethod;
	}

	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}

	public String getUriPath() {
		return uriPath;
	}

	public void setUriPath(String uriPath) {
		this.uriPath = uriPath;
	}

	public String getQueryString() {
		return queryString;
	}

	public void setQueryString(String queryString) {
		this.queryString = queryString;
	}

	public String getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(String statusCode) {
		this.statusCode = statusCode;
	}

	public String getLatency() {
		return latency;
	}

	public void setLatency(String latency) {
		this.latency = latency;
	}

	public String getOrgId() {
		return orgId;
	}

	public void setOrgId(String orgId) {
		this.orgId = orgId;
	}

	public String getOrgName() {
		return orgName;
	}

	public void setOrgName(String orgName) {
		this.orgName = orgName;
	}

	public String getResourcePath() {
		return resourcePath;
	}

	public void setResourcePath(String resourcePath) {
		this.resourcePath = resourcePath;
	}

	public String getProductTitle() {
		return productTitle;
	}

	public void setProductTitle(String productTitle) {
		this.productTitle = productTitle;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}
    
    
}
