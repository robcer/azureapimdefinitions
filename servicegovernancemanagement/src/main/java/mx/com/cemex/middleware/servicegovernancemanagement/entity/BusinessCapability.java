package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "businesscapability")
public class BusinessCapability {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_business_capability")
	private Long idBusinessCapability;
    
    @Column(name = "business_capability")
    private String businessCapability;
    
    @Column(name = "acronym")
    private String acronym;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "businessCapability")
    @JsonIgnore
    private Set<ApiOperation> apiOperations = new HashSet<ApiOperation>();

	public Long getIdBusinessCapability() {
		return idBusinessCapability;
	}

	public void setIdBusinessCapability(Long idBusinessCapability) {
		this.idBusinessCapability = idBusinessCapability;
	}

	public String getBusinessCapability() {
		return businessCapability;
	}

	public void setBusinessCapability(String businessCapability) {
		this.businessCapability = businessCapability;
	}

	public String getAcronym() {
		return acronym;
	}

	public void setAcronym(String acronym) {
		this.acronym = acronym;
	}

	public Set<ApiOperation> getApiOperations() {
		return apiOperations;
	}

	public void setApiOperations(Set<ApiOperation> apiOperations) {
		this.apiOperations = apiOperations;
	}
    
}
