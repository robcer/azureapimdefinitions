package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.BluemixAzureCompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.BluemixAzureCompareConfigurationDAO;

@Service("bluemixAzureCompareConfigurationDAO")
public class BluemixAzureCompareConfigurationDAOImpl implements BluemixAzureCompareConfigurationDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public BluemixAzureCompareConfigurationDAOImpl()
    {
    }

    BluemixAzureCompareConfigurationDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public BluemixAzureCompareConfiguration create(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration) {
        em.persist(bluemixAzureCompareConfiguration);
        return bluemixAzureCompareConfiguration;
    }

    @Transactional
    public BluemixAzureCompareConfiguration merge(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration) {
        em.merge(bluemixAzureCompareConfiguration);
        return bluemixAzureCompareConfiguration;
    }

    public long countBluemixAzureCompareConfiguration() {
        return (Long) em.createQuery("select count(o) from BluemixAzureCompareConfiguration o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<BluemixAzureCompareConfiguration> findBluemixAzureCompareConfiguration(int firstResult, int maxResults) {
        return em.createQuery("select o from BluemixAzureCompareConfiguration o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public BluemixAzureCompareConfiguration findBluemixAzureCompareConfiguration(Long id) {
        if (id == null) return null;
        return em.find(BluemixAzureCompareConfiguration.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<BluemixAzureCompareConfiguration> findAll() {
        return em.createQuery("select o from BluemixAzureCompareConfiguration o where o.isEnable = true order by o.idBluemixAzureCompareConfiguration DESC").getResultList();
    }

    public List<Object[]> findDifferencesBetweenAzureAndBluemix(Long environmentId, String devOrgId) {
    	@SuppressWarnings("unchecked")
    	List<Object[]> results = em.createNativeQuery("SELECT * FROM   (SELECT    AC.NAME, '/' || AC.PATH || NVL(SUBSTR(AO.URL_TEMPLATE, 0, INSTR(AO.URL_TEMPLATE, '?')-1), AO.URL_TEMPLATE) AS PATH, AC.SERVICE_URL, AO.METHOD AS METHOD, AO.METHOD || '/' || AC.PATH || NVL(SUBSTR(AO.URL_TEMPLATE, 0, INSTR(AO.URL_TEMPLATE, '?')-1), AO.URL_TEMPLATE) AS FULLPATH FROM AZUREAPIOPERATIONCATALOG AO    INNER JOIN AZUREAPICATALOG AC ON AC.ID_AZURE_API_CATALOG =  AO.ID_AZURE_API_CATALOG WHERE   AO.ID_ENVIRONMENT_CATALOG = ?1   ORDER BY 2, 4) AZURE    RIGHT JOIN  (SELECT API_NAME, API_VERSION, BASE_PATH || PATH AS PATH, METHOD || BASE_PATH || PATH AS FULLPATH, AZURE_HOST, METHOD METHOD FROM YAMLPROPERTIES WHERE DEV_ORG_ID = ?2 AND DELETED_AT IS NULL)     BLUEMIX      ON BLUEMIX.FULLPATH = AZURE.FULLPATH  UNION   SELECT * FROM   (SELECT    AC.NAME, '/' || AC.PATH || NVL(SUBSTR(AO.URL_TEMPLATE, 0, INSTR(AO.URL_TEMPLATE, '?')-1), AO.URL_TEMPLATE) AS PATH, AC.SERVICE_URL, AO.METHOD AS METHOD, AO.METHOD || '/' || AC.PATH || NVL(SUBSTR(AO.URL_TEMPLATE, 0, INSTR(AO.URL_TEMPLATE, '?')-1), AO.URL_TEMPLATE) AS FULLPATH FROM AZUREAPIOPERATIONCATALOG AO    INNER JOIN AZUREAPICATALOG AC ON AC.ID_AZURE_API_CATALOG =  AO.ID_AZURE_API_CATALOG WHERE   AO.ID_ENVIRONMENT_CATALOG = ?3   ORDER BY 2, 4) AZURE    LEFT JOIN  (SELECT API_NAME, API_VERSION, BASE_PATH || PATH AS PATH, METHOD || BASE_PATH || PATH AS FULLPATH, AZURE_HOST, METHOD METHOD FROM YAMLPROPERTIES WHERE DEV_ORG_ID = ?4 AND DELETED_AT IS NULL)     BLUEMIX      ON BLUEMIX.FULLPATH = AZURE.FULLPATH WHERE BLUEMIX.FULLPATH IS NULL ORDER BY 6,8,11")
    		.setParameter(1, environmentId).setParameter(2, devOrgId).setParameter(3, environmentId).setParameter(4, devOrgId).getResultList();
    	return results;
    }
}