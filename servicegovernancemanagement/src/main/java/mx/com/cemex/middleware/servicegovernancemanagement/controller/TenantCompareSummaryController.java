package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.resthub.web.controller.ServiceBasedRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.TenantCompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.TenantCompareSummaryService;

@Controller
@RequestMapping(value = "/v1/its/tenantcomparesummaries", produces = "application/json")
public class TenantCompareSummaryController extends ServiceBasedRestController<TenantCompareSummary, Long, TenantCompareSummaryService> {
	
	private static final Logger LOG  = Logger.getLogger(TenantCompareSummaryController.class);
	
	@Autowired
	TenantCompareSummaryDAO tenantCompareSummaryDAO;

    @Inject
    @Named("tenantCompareSummaryService")
    @Override
    public void setService(TenantCompareSummaryService service) {
        this.service = service;
    }

//    @RequestMapping(value = "/{guid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//    @ResponseBody
//    public Iterable<TenantCompareSummary> searchBySkillLines(@PathVariable String guid) {
//        return tenantCompareSummaryDAO.findAllByReportGUID(guid);
//    }
    
}