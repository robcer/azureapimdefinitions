package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.List;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface AzureApiCatalogService extends CrudService<AzureApiCatalog, Long> {

	void mantainAzureApiCatalog(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException;
	List<AzureApiCatalog> findAllByEnvironmentCatalog(EnvironmentCatalog environmentCatalog);

}
