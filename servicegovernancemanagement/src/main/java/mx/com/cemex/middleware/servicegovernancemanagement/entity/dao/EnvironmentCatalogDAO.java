package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface EnvironmentCatalogDAO {

	EnvironmentCatalog create(EnvironmentCatalog environmentCatalog);
	EnvironmentCatalog merge(EnvironmentCatalog environmentCatalog);
    long countEnvironmentCatalog();
    List<EnvironmentCatalog> findEnvironmentCatalog(int firstResult, int maxResults);
    List<EnvironmentCatalog> findAll();
    EnvironmentCatalog findEnvironmentCatalog(Long id);
    EnvironmentCatalog findEnvironmentCatalogById(String id);
    List<EnvironmentCatalog> findAllByDevOrgId(String devOrgId);
    List<EnvironmentCatalog> findAllIncludingAzure();

}
