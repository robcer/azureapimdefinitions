package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.AzureTenantStatusInformation;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.AzureTenantSyncState;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.BitbucketSrcHeadersBean;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.EmailBean;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzurePropertyCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.BitbucketOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.HipchatOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.MailManager;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

@Service("azureOperationsService")
public class AzureOperationsServiceImpl implements AzureOperationsService {
	
	private static final Logger LOG  = Logger.getLogger(AzureOperationsServiceImpl.class);
	
	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	HipchatOperationsService hipchatOperationsService;
	
	@Autowired
	BitbucketOperationsService bitbucketOperationsService;
	
	@Autowired
	MailManager mailManager;
	
	public String generateSharedAccessSignature(EnvironmentCatalog environmentCatalog) throws UnsupportedEncodingException, NoSuchAlgorithmException, InvalidKeyException {
		final byte[] primaryKey = environmentCatalog.getPrimaryKey().getBytes("UTF-8");
		final String expiry = LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'0000Z'"));
		Mac hmacSha512 = Mac.getInstance("HmacSHA512");
		hmacSha512.init(new SecretKeySpec(primaryKey, "HmacSHA512"));
		String dataToSign = environmentCatalog.getIdentifier() + "\n" + expiry;
		String signature = new String(Base64.getEncoder().encode(hmacSha512.doFinal(dataToSign.getBytes("UTF-8"))));
		return "SharedAccessSignature uid="+environmentCatalog.getIdentifier()+"&ex=" + expiry + "&sn="+signature;
	}
	
	public void commitToBitbucketIntegration(String policy, String fileName, String branch, 
			String repository, String author, String pathToFile, String message, String user) throws IOException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException {
		if(!policy.isEmpty()) {
			BitbucketSrcHeadersBean bitbucketSrcHeadersBean = new BitbucketSrcHeadersBean();
			bitbucketSrcHeadersBean.setAuthor(author);
			bitbucketSrcHeadersBean.setDestinationBranch(branch);
			bitbucketSrcHeadersBean.setFileName(fileName);
			bitbucketSrcHeadersBean.setMessage(message);
			bitbucketSrcHeadersBean.setPathToFile(pathToFile);
			bitbucketSrcHeadersBean.setRepository(repository);
			bitbucketSrcHeadersBean.setUser(user);
			bitbucketSrcHeadersBean.setXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(policy.replace("&#xD;", ""))));
			bitbucketOperationsService.commitUpdateCreateSourceFile(bitbucketSrcHeadersBean);
		}
	}
	
	public String getApiPolicy(EnvironmentCatalog environmentCatalog, String apiId) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		String azureUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv())  + apiId +  "/policy?api-version=2017-03-01";
		LOG.info("Fetching policy from Azure Tenant: " + environmentCatalog.getCatalogEnv() + "... on URL: " + azureUrl);
		return utilitiesService.getXMLString(azureUrl, generateSharedAccessSignature(environmentCatalog));
	}
	
	public String getTenantPolicy(EnvironmentCatalog environmentCatalog) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		String azureUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv())  + "/tenant/policy?api-version=2017-03-01";
		LOG.info("Fetching policy from Azure Tenant: " + environmentCatalog.getCatalogEnv() + "... on URL: " + azureUrl);
		return utilitiesService.getXMLString(azureUrl, generateSharedAccessSignature(environmentCatalog));
	}
	
	public String getOperationPolicy(EnvironmentCatalog environmentCatalog, String operationId) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		String azureUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + operationId +  "/policy?api-version=2017-03-01";
		LOG.info("Fetching policy from Azure Tenant: " + environmentCatalog.getCatalogEnv() + "... on URL: " + azureUrl);
		return utilitiesService.getXMLString(azureUrl, generateSharedAccessSignature(environmentCatalog));
	}
	
	public List<AzureApiCatalog> getApiList(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/apis?api-version=2017-03-01";
		LOG.info("Fetching information from Azure APIs on: " + environmentCatalog.getName() + " Environment... on URL: " + apiUrl);
		JSONObject jsonObjectApis = utilitiesService.getJSONObject(apiUrl, null);
		return fillAzureApiCatalogDeployedBean(jsonObjectApis);
	}
	
	public List<AzureApiCatalog> getFilteredApi(EnvironmentCatalog environmentCatalog, String filter, String operator, String value) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException {
		String encodedFilter = URLEncoder.encode("$filter", "UTF-8");
		String encodedFilter2 = URLEncoder.encode(filter + " " + operator + " '" + value + "'", "UTF-8");
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/apis?" + encodedFilter + "=" + encodedFilter2 + "&api-version=2017-03-01";
		LOG.info("Fetching information from Azure APIs on: " + environmentCatalog.getName() + " Environment... on URL: " + apiUrl);
		return fillAzureApiCatalogDeployedBean(utilitiesService.getJSONObject(apiUrl, null));
	}

	/**
	 * 
	 * @param environmentCatalog
	 * @param uriOperation 
	 * 
	 * 		Tenant: /tenant
	 * 		API:	/apis/customermanagement_2.0.0 (api:id)
	 * 		API:	/apis/customermanagement_2.0.0/operations/getCustomers (operation:id)
	 * 
	 * @param xml
	 * @return
	 * @throws MalformedURLException
	 * @throws IOException
	 * @throws ParseException
	 */
	public int updateAzurePolicy(EnvironmentCatalog environmentCatalog, String uriOperation, String xml)
			throws MalformedURLException, IOException, ParseException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + uriOperation + "/policy?api-version=2017-03-01&import=true";
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			OutputStream outputStream;
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/vnd.ms-azure-apim.policy.raw+xml");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			outputStream = connection.getOutputStream();
			BufferedWriter bufferedWriter = new BufferedWriter(new OutputStreamWriter(outputStream));
			bufferedWriter.write(StringEscapeUtils.unescapeHtml3(xml));
			bufferedWriter.flush();
			bufferedWriter.close();
			outputStream.close();
			return connection.getResponseCode();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return HttpURLConnection.HTTP_INTERNAL_ERROR;
	}

	public String syncTenantGitRepository(EnvironmentCatalog environmentCatalog, String branch, boolean force)
			throws MalformedURLException, IOException, ParseException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/tenant/configuration/save?api-version=2017-03-01&import=false";
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("branch", branch);
			jsonObject.put("name", force);
			outputStreamWriter.write(jsonObject.toString());
			outputStreamWriter.close();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonObjectResponse = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return jsonObjectResponse.get("Id").toString();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}
	
	public AzureTenantStatusInformation getTentantOperationStatusInformation(EnvironmentCatalog environmentCatalog, String id) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException  {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/tenant/configuration/operationResults/" + id + "?api-version=2017-03-01";
		LOG.info("Fetching Tenant Operation Status Information: " + environmentCatalog.getCatalogEnv() + "... on URL: " + apiUrl);
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/xml");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return fillAzureTenantStatusInformationObject(new org.json.JSONObject(jsonObject.toJSONString()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}
	
	public AzureTenantSyncState getTentantSyncState(EnvironmentCatalog environmentCatalog, String id) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException  {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/tenant/configuration/syncState?api-version=2017-03-01";
		LOG.info("Fetching Tenant Sync State: " + environmentCatalog.getCatalogEnv() + "... on URL: " + apiUrl);
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/xml");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return fillAzureTenantSyncStateObject(new org.json.JSONObject(jsonObject.toJSONString()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}
	
	private AzureTenantSyncState fillAzureTenantSyncStateObject(JSONObject jsonObject) {
		AzureTenantSyncState azureTenantSyncState = new AzureTenantSyncState();
		azureTenantSyncState.setBranch(jsonObject.optString("branch"));
		azureTenantSyncState.setCommitId(jsonObject.optString("commitId"));
		azureTenantSyncState.setExport(new Boolean(jsonObject.optString("isExport")));
		azureTenantSyncState.setSynced(new Boolean(jsonObject.optString("isSynced")));
		azureTenantSyncState.setGitEnabled(new Boolean(jsonObject.optString("isGitEnabled")));
		azureTenantSyncState.setSyncDate(jsonObject.optString("syncDate"));
		azureTenantSyncState.setConfigurationChangeDate(jsonObject.optString("configurationChangeDate"));
		return azureTenantSyncState;
	}
	
	private AzureTenantStatusInformation fillAzureTenantStatusInformationObject(JSONObject jsonObject) {
		AzureTenantStatusInformation azureTenantStatusInformation = new AzureTenantStatusInformation();
		azureTenantStatusInformation.setId(jsonObject.optString("id"));
		azureTenantStatusInformation.setStatus(jsonObject.optString("name"));
		azureTenantStatusInformation.setStarted(jsonObject.optString("method"));
		azureTenantStatusInformation.setUpdated(jsonObject.optString("urlTemplate"));
		azureTenantStatusInformation.setResultInfo(jsonObject.optString("description"));
		azureTenantStatusInformation.setError(jsonObject.optString("description"));
		JSONArray actionLogJsonArray = jsonObject.getJSONArray("actionLog");
		StringBuilder actionLog = new StringBuilder();
		for(int j = 0; j < actionLogJsonArray.length(); j++) {
			actionLog.append(actionLogJsonArray.getString(j)).append("',");
		}
		actionLog.deleteCharAt(actionLog.length() - 1);
		azureTenantStatusInformation.setActionLog(actionLog.toString());
		return azureTenantStatusInformation;
	}

	public AzureApiOperationCatalog createNewOperation(EnvironmentCatalog environmentCatalog, String operationId, JSONObject jsonObject)
			throws MalformedURLException, IOException, ParseException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + operationId + "?api-version=2017-03-01";
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			removeRepresentations(jsonObject);
			outputStreamWriter.write(jsonObject.toString());
			outputStreamWriter.close();
			LOG.info("Trying to create a new Operation URL: " + jsonObject.toString());
			LOG.info("Trying to create a new Operation JSON: " + apiUrl);
			LOG.info("Trying to create a new Operation Response Code: " + connection.getResponseCode());
	        switch (connection.getResponseCode()) {
            case 201:
    			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
    			org.json.simple.JSONObject jsonOperationObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
    			return fillApiOperationObject(new org.json.JSONObject(jsonOperationObject.toJSONString()));
            default:
            	return null;
	        }
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}
	
	private static void removeRepresentations(Object object) throws JSONException {
	    if (object instanceof JSONArray) {
	        JSONArray array = (JSONArray) object;
	        for (int i = 0; i < array.length(); ++i) removeRepresentations(array.get(i));
	    } else if (object instanceof JSONObject) {
	        JSONObject json = (JSONObject) object;
	        JSONArray names = json.names();
	        if (names == null) return;
	        for (int i = 0; i < names.length(); ++i) {
	            String key = names.getString(i);
	            if (key.equals("schemaId") || key.equals("typeName")) {
	                json.remove(key);
	            } else {
	            	removeRepresentations(json.get(key));
	            }
	        }
	    }
	}

	public JSONObject addApiToProduct(EnvironmentCatalog environmentCatalog, AzureApiCatalog azureApiCatalog, String productId)
			throws MalformedURLException, IOException, ParseException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + productId + azureApiCatalog.getId() + "?api-version=2017-03-01";
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			outputStreamWriter.write("");
			outputStreamWriter.close();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return new org.json.JSONObject(jsonObject.toJSONString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}

	public AzureApiCatalog createOrUpdateApi(EnvironmentCatalog environmentCatalog, AzureApiCatalog azureApiCatalog)
			throws MalformedURLException, IOException, ParseException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + azureApiCatalog.getId() + "?api-version=2017-03-01&import=false";
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("PUT");
			connection.setRequestProperty("Authorization", generateSharedAccessSignature(environmentCatalog));
			connection.setRequestProperty("If-Match", "*");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			outputStreamWriter.write(buildAzureApiCatalogJSON(azureApiCatalog).toString());
			LOG.info(buildAzureApiCatalogJSON(azureApiCatalog).toString());
			outputStreamWriter.close();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return fillApiCatalogObject(new org.json.JSONObject(jsonObject.toJSONString()));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}

	private JSONObject buildAzureApiCatalogJSON(AzureApiCatalog azureApiCatalog) {
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("id", azureApiCatalog.getId());
		jsonObject.put("name", azureApiCatalog.getName());
		jsonObject.put("description", azureApiCatalog.getDescription());
		jsonObject.put("serviceUrl", azureApiCatalog.getServiceUrl());
		jsonObject.put("path", azureApiCatalog.getPath());
		jsonObject.put("protocols", azureApiCatalog.getProtocols().split(","));
		return jsonObject;
	}
	
	private List<AzureApiCatalog> fillAzureApiCatalogDeployedBean(JSONObject jsonObjectApis) throws ParseException {
		List<AzureApiCatalog> azureApiCatalogs = new ArrayList<AzureApiCatalog>();
		if(jsonObjectApis.optJSONArray("value") != null &&
				jsonObjectApis.optJSONArray("value").length() > 0) {
			JSONArray jsonArray = jsonObjectApis.optJSONArray("value");
		    for (int i = 0, size = jsonArray.length(); i < size; i++) {
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
	    		if(new Boolean(jsonObject.optString("isCurrent"))) {
		    		AzureApiCatalog azureApiCatalog = fillApiCatalogObject(jsonObject);
		    		azureApiCatalogs.add(azureApiCatalog);
		    	}
		    }
		}
		return azureApiCatalogs;
	}
	
	private AzureApiCatalog fillApiCatalogObject(JSONObject jsonObject) {
		AzureApiCatalog azureApiCatalog = new AzureApiCatalog();
		azureApiCatalog.setId(jsonObject.optString("id"));
		azureApiCatalog.setName(jsonObject.optString("name"));
		azureApiCatalog.setApiRevision(jsonObject.optString("apiRevision"));
		azureApiCatalog.setDescription(jsonObject.optString("description"));
		azureApiCatalog.setServiceUrl(jsonObject.optString("serviceUrl"));
		azureApiCatalog.setPath(jsonObject.optString("path"));
		JSONArray protocolsJsonArray = jsonObject.getJSONArray("protocols");
		StringBuilder protocols = new StringBuilder();
		for(int j = 0; j < protocolsJsonArray.length(); j++) {
			protocols.append(protocolsJsonArray.getString(j)).append(",");
		}
		protocols.deleteCharAt(protocols.length() - 1);
		azureApiCatalog.setProtocols(protocols.toString());
		azureApiCatalog.setCurrent(new Boolean(jsonObject.optString("isCurrent")));
		return azureApiCatalog;
	}
	
	public List<AzureApiOperationCatalog> getApiOperations(AzureApiCatalog azureApiCatalog, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException  {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + azureApiCatalog.getId() + "?api-version=2017-03-01&export=true";
		LOG.info("Fetching operations from Azure API: " + azureApiCatalog.getId()  + " Environment: " + environmentCatalog.getCatalogEnv() + "... on URL: " + apiUrl);
		JSONObject jsonObjectApi = utilitiesService.getJSONObject(apiUrl, null);
		return fillAzureApiOperationCatalogDeployedBean(jsonObjectApi);
	}
	
	public JSONObject getAllProducts(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException  {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/products?api-version=2017-03-01&export=true";
		LOG.info("Fetching All Products Environment: " + environmentCatalog.getCatalogEnv() + "... on URL: " + apiUrl);
		JSONObject jsonObjectApi = utilitiesService.getJSONObject(apiUrl, generateSharedAccessSignature(environmentCatalog));
		LOG.info(jsonObjectApi);
		if(jsonObjectApi != null) {
			return jsonObjectApi;
		}
		return null;
	}
	
	public JSONObject getOperationJSONById(AzureApiOperationCatalog azureApiOperationCatalog, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException  {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + azureApiOperationCatalog.getId() + "?api-version=2017-03-01&export=true";
		LOG.info("Fetching Operation: " + azureApiOperationCatalog.getId()  + " Environment: " + environmentCatalog.getCatalogEnv() + "... on URL: " + apiUrl);
		JSONObject jsonObjectApi = utilitiesService.getJSONObject(apiUrl, null);
		LOG.info(jsonObjectApi);
		if(jsonObjectApi != null) {
			return jsonObjectApi;
		}
		return null;
	}
	
	public AzureApiOperationCatalog getOperationById(String operationId, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException  {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + operationId + "?api-version=2017-03-01";
		LOG.info("Fetching Operation: " + operationId  + " Environment: " + environmentCatalog.getCatalogEnv() + "... on URL: " + apiUrl);
		JSONObject jsonObjectApi = utilitiesService.getJSONObject(apiUrl, null);
		if(jsonObjectApi == null) {
			return null;
		} else {
			return fillApiOperationObject(jsonObjectApi);
		}
	}
	
	private List<AzureApiOperationCatalog> fillAzureApiOperationCatalogDeployedBean(JSONObject jsonObjectApi) throws ParseException {
		List<AzureApiOperationCatalog> azureApiOperationCatalogs = new ArrayList<AzureApiOperationCatalog>();
		if(jsonObjectApi.optJSONObject("operations") != null && jsonObjectApi.optJSONObject("operations").optJSONArray("value") != null &&
				jsonObjectApi.optJSONObject("operations").optJSONArray("value").length() > 0) {
			JSONArray jsonArrayOperations = jsonObjectApi.optJSONObject("operations").optJSONArray("value");
		    for (int i = 0, size = jsonArrayOperations.length(); i < size; i++) {
		    	JSONObject jsonObject = jsonArrayOperations.getJSONObject(i);
				AzureApiOperationCatalog azureApiOperationCatalog = fillApiOperationObject(jsonObject);
	    		azureApiOperationCatalogs.add(azureApiOperationCatalog);
		    }
		}
		return azureApiOperationCatalogs;
	}
	
	private AzureApiOperationCatalog fillApiOperationObject(JSONObject jsonObject) {
		AzureApiOperationCatalog azureApiOperationCatalog = new AzureApiOperationCatalog();
		azureApiOperationCatalog.setId(jsonObject.optString("id"));
		azureApiOperationCatalog.setName(jsonObject.optString("name"));
		azureApiOperationCatalog.setMethod(jsonObject.optString("method"));
		azureApiOperationCatalog.setUrlTemplate(jsonObject.optString("urlTemplate"));
		azureApiOperationCatalog.setDescription(jsonObject.optString("description"));
		return azureApiOperationCatalog;
	}
	
	public List<AzurePropertyCatalog> getAzureProperties(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException {
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_AZURE_HOSTNAME, environmentCatalog.getCatalogEnv()) + "/properties?api-version=2017-03-01";
		LOG.info("Fetching information from Azure APIs on: " + environmentCatalog.getName() + " Environment... on URL: " + apiUrl);
		JSONObject jsonObjectApis = utilitiesService.getJSONObject(apiUrl, generateSharedAccessSignature(environmentCatalog));
		return fillAzurePropertyCatalogDeployedBean(jsonObjectApis);
	}
	
	private List<AzurePropertyCatalog> fillAzurePropertyCatalogDeployedBean(JSONObject jsonObjectApis) throws ParseException {
		List<AzurePropertyCatalog> azurePropertyCatalogs = new ArrayList<AzurePropertyCatalog>();
		if(jsonObjectApis.optJSONArray("value") != null &&
				jsonObjectApis.optJSONArray("value").length() > 0) {
			JSONArray jsonArray = jsonObjectApis.optJSONArray("value");
		    for (int i = 0, size = jsonArray.length(); i < size; i++) {
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	AzurePropertyCatalog azurePropertyCatalog = new AzurePropertyCatalog();
		    	azurePropertyCatalog.setId(jsonObject.optString("id"));
		    	azurePropertyCatalog.setName(jsonObject.optString("name"));
		    	azurePropertyCatalog.setValue(jsonObject.optString("value"));
	    		azurePropertyCatalog.setSecret(new Boolean(jsonObject.optString("secret")));
	    		azurePropertyCatalogs.add(azurePropertyCatalog);
		    }
		}
		return azurePropertyCatalogs;
	}
	
	private AzureApiOperationCatalog createNewOperationOnTargetAPI(EnvironmentCatalog targetEnvironmentCatalog, AzureApiOperationCatalog sourceAzureApiOperationCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException, InterruptedException {
		JSONObject jsonObject = getOperationJSONById(sourceAzureApiOperationCatalog, sourceAzureApiOperationCatalog.getEnvironmentCatalog());
		if(jsonObject != null) {
			AzureApiOperationCatalog apiOperationCatalog = createNewOperation(targetEnvironmentCatalog, sourceAzureApiOperationCatalog.getId(), jsonObject);
			if(apiOperationCatalog != null) {
				String xml = getOperationPolicy(sourceAzureApiOperationCatalog.getEnvironmentCatalog(), sourceAzureApiOperationCatalog.getId());
				if(!xml.isEmpty()) {
					updateAzurePolicy(targetEnvironmentCatalog, sourceAzureApiOperationCatalog.getId(), xml);
				}
				return apiOperationCatalog;
			}
		}
		return null;
	}
	
	public AzureApiCatalog migrateAPIIntoTargetTenant(EnvironmentCatalog targetEnvironmentCatalog, AzureApiCatalog sourceAzureApiCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException, InterruptedException {
		List<AzureApiCatalog> azureApiCatalogs = getFilteredApi(targetEnvironmentCatalog, "path", "eq", sourceAzureApiCatalog.getPath());
		if(!azureApiCatalogs.isEmpty()) {
			for(AzureApiCatalog apiCatalog: azureApiCatalogs) {
				if(apiCatalog.getPath().equals(sourceAzureApiCatalog.getPath())) {
					String xml = getApiPolicy(sourceAzureApiCatalog.getEnvironmentCatalog(), sourceAzureApiCatalog.getId());
					if(!xml.isEmpty()) {
						updateAzurePolicy(targetEnvironmentCatalog, apiCatalog.getId(), xml);
						hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all " + apiCatalog.getName() + " API Policy has being updated successfully on " + targetEnvironmentCatalog.getName() + " Environment.");
					}
				}
				return apiCatalog;
			}
		} else {
			AzureApiCatalog azureApiCatalog = createOrUpdateApi(targetEnvironmentCatalog, sourceAzureApiCatalog);
			hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all The API " + sourceAzureApiCatalog.getId() + " has being created on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + azureApiCatalog.getName());
			if(azureApiCatalog != null) {
				addApiToProduct(targetEnvironmentCatalog, azureApiCatalog, getProductByKey(targetEnvironmentCatalog, "Unlimited"));
				addApiToProduct(targetEnvironmentCatalog, azureApiCatalog, getProductByKey(targetEnvironmentCatalog, "CEMEXGO"));
				addApiToProduct(targetEnvironmentCatalog, azureApiCatalog, getProductByKey(targetEnvironmentCatalog, "Integration"));
				String xml = getApiPolicy(sourceAzureApiCatalog.getEnvironmentCatalog(), sourceAzureApiCatalog.getId());
				if(!xml.isEmpty()) {
					updateAzurePolicy(targetEnvironmentCatalog, azureApiCatalog.getId(), xml);
					hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all " + azureApiCatalog.getName() + " API Policy has being updated successfully on " + targetEnvironmentCatalog.getName() + " Environment.");
				}
				return azureApiCatalog;
			}
		}
		return null;
	}
	
	public AzureApiOperationCatalog migrateOperationIntoAPITargetTenant(EnvironmentCatalog targetEnvironmentCatalog, AzureApiOperationCatalog sourceAzureApiOperationCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, InvalidKeyException, NoSuchAlgorithmException, InterruptedException {
		List<AzureApiCatalog> azureApiCatalogs = getFilteredApi(targetEnvironmentCatalog, "path", "eq", sourceAzureApiOperationCatalog.getAzureApiCatalog().getPath());

		EmailBean emailBean = new EmailBean();
		emailBean.setEmails(new String[] {"rogelio.reyocachu@ext.cemex.com"});
		emailBean.setCcEmails(new String[] {"rogelio.reyo@gmail.com","rogelio.reyo@hotmail.com"});
		emailBean.setExecutedProcess("Azure API Management Publishing Notifications");
		emailBean.setFrom("middlewareservices@cemex.com");
		emailBean.setPersona("Middleware Services Team");
		emailBean.setSubject("Azure API Management Publishing Notifications.");
		
		if(!azureApiCatalogs.isEmpty()) {
			for(AzureApiCatalog apiCatalog: azureApiCatalogs) {
				if(apiCatalog.getPath().equals(sourceAzureApiOperationCatalog.getAzureApiCatalog().getPath())) {
					AzureApiOperationCatalog apiOperationCatalog = getOperationById(apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")), targetEnvironmentCatalog);
					if(apiOperationCatalog == null) {
						sourceAzureApiOperationCatalog.setId(apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")));
						AzureApiOperationCatalog apiOperationCatalog2 = createNewOperationOnTargetAPI(targetEnvironmentCatalog, sourceAzureApiOperationCatalog);
						if(apiOperationCatalog2 != null) {
							hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all The Operation " + apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")) + " has being deployed on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + apiCatalog.getName());
							emailBean.setMessage("@all The Operation " + apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")) + " has being deployed on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + apiCatalog.getName());
//							mailManager.publishingNotification(emailBean);
							return apiOperationCatalog2;
						}
					} else {
						String xml = getOperationPolicy(sourceAzureApiOperationCatalog.getEnvironmentCatalog(), apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")));
						if(!xml.isEmpty()) {
							hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all The Operation " + apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")) + " has being updated successfully on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + apiCatalog.getName());
							emailBean.setMessage("@all The Operation " + apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")) + " has being updated successfully on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + apiCatalog.getName());
//							mailManager.publishingNotification(emailBean);
							updateAzurePolicy(targetEnvironmentCatalog, apiCatalog.getId() + sourceAzureApiOperationCatalog.getId().substring(sourceAzureApiOperationCatalog.getId().indexOf("/operations")), xml);
						}
						return apiOperationCatalog;
					}
				}
			}
		} else {
			AzureApiCatalog azureApiCatalog = createOrUpdateApi(targetEnvironmentCatalog, sourceAzureApiOperationCatalog.getAzureApiCatalog());
			hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all The API " + sourceAzureApiOperationCatalog.getAzureApiCatalog().getId() + " has being created on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + azureApiCatalog.getName());
			emailBean.setMessage("@all The API " + sourceAzureApiOperationCatalog.getAzureApiCatalog().getId() + " has being created on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + azureApiCatalog.getName());
//			mailManager.publishingNotification(emailBean);
			if(azureApiCatalog != null) {
				addApiToProduct(targetEnvironmentCatalog, azureApiCatalog, getProductByKey(targetEnvironmentCatalog, "Unlimited"));
				addApiToProduct(targetEnvironmentCatalog, azureApiCatalog, getProductByKey(targetEnvironmentCatalog, "CEMEXGO"));
				addApiToProduct(targetEnvironmentCatalog, azureApiCatalog, getProductByKey(targetEnvironmentCatalog, "Integration"));
				AzureApiOperationCatalog apiOperationCatalog = createNewOperationOnTargetAPI(targetEnvironmentCatalog, sourceAzureApiOperationCatalog);
				if(apiOperationCatalog != null) {
					emailBean.setMessage("@all The Operation " + sourceAzureApiOperationCatalog.getId() + " has being deployed on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + azureApiCatalog.getName());
//					mailManager.publishingNotification(emailBean);
					hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "@all The Operation " + sourceAzureApiOperationCatalog.getId() + " has being deployed on " + targetEnvironmentCatalog.getName() + " Environment. Inside the API: " + azureApiCatalog.getName());
				}
				return apiOperationCatalog;
			}
		}
		return null;
	}

	private String getProductByKey(EnvironmentCatalog environmentCatalog, String productKey) throws InvalidKeyException, MalformedURLException, NoSuchAlgorithmException, ParseException, IOException, org.json.simple.parser.ParseException {
		JSONObject jsonObject = getAllProducts(environmentCatalog);
		if(jsonObject.optJSONArray("value") != null &&
				jsonObject.optJSONArray("value").length() > 0) {
			JSONArray jsonArray = jsonObject.optJSONArray("value");
		    for (int i = 0, size = jsonArray.length(); i < size; i++) {
		    	JSONObject jsonObjectProduct = jsonArray.getJSONObject(i);
		    	if(jsonObjectProduct.optString("name").equals(productKey)) {
		    		return jsonObjectProduct.optString("id");
		    	}
		    }
		}
		return null;
	}
}
