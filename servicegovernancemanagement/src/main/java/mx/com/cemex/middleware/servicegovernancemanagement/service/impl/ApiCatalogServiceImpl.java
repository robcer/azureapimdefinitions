package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProductCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlPropertiesDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiOperationCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;


/**
 * 
 * If we want to get all the created APIs we can use the following EndPoints:
 * 
 * https://us2.apiconnect.ibmcloud.com/apim/proxy/apimanager/orgs/58e2aa5f0cf26aa41002a447/apis?fields=DEFAULT,tags.name,tags.url
 * https://us2.apiconnect.ibmcloud.com/apim/proxy/apimanager/orgs/58f04bc90cf26aa41002f745/apis?fields=DEFAULT,tags.name,tags.url
 * https://us2.apiconnect.ibmcloud.com/apim/proxy/apimanager/orgs/58e506570cf26aa41002a7fc/apis?fields=DEFAULT,tags.name,tags.url
 * https://us2.apiconnect.ibmcloud.com/apim/proxy/apimanager/orgs/59b1cb240cf22aa4792673fe/apis?fields=DEFAULT,tags.name,tags.url
 * 
 * Those EndPoints are not accessible by curl Basic Authentication
 * 
 * @author RogelioReyo
 *
 */

@Service("apiCatalogService")
public class ApiCatalogServiceImpl implements ApiCatalogService {
	
	private static final Logger LOG  = Logger.getLogger(ApiCatalogServiceImpl.class);

	@Autowired
	ApiCatalogDAO apiCatalogDAO;

	@Autowired
	ApiOperationCatalogService apiOperationCatalogService;

	@Autowired
	ProductCatalogDAO productCatalogDAO;
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
	@Autowired
	YamlPropertiesDAO yamlPropertiesDAO;
	
	@Autowired
	UtilitiesService utilitiesService;
	
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	
	public void mantainApiCatalog(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException {
		
		String apiUrl = "https://" + MessageFormat.format(IYamlBeansMasterConstants.CLOUD_BLUEMIX_HOSTNAME, environmentCatalog.getRegionHostAcronym()) + "/v1/orgs/{0}/environments/{1}/apis?fields=DEFAULT,tags.name,tags.url";
		ApiCatalog persistedApiCatalog = null;
			String cUrl = MessageFormat.format(apiUrl, environmentCatalog.getId(), environmentCatalog.getCatalogEnv());
			LOG.info("Fetching information from Bluemix APIs on: " + environmentCatalog.getName() + " Environment... on URL: " + cUrl);
			JSONArray jsonArrayDeployedApis = utilitiesService.getJSONArray(cUrl, utilitiesService.getEncodedAuthorization(IYamlBeansMasterConstants.BLUEMIX_CREDENTIALS));
			List<ApiCatalog> deployedApiCatalogs = fillApiCatalogDeployedBean(jsonArrayDeployedApis, environmentCatalog.getCatalogEnv());
			for(ApiCatalog deployedApiCatalog: deployedApiCatalogs) {
				try {
					ApiCatalog apiCatalog = apiCatalogDAO.findApiCatalogByBasePathAndNameAndEnvIdAndDevOrgId(deployedApiCatalog.getBasePath(),
							deployedApiCatalog.getName(), deployedApiCatalog.getEnvId(), deployedApiCatalog.getDevOrgId(), "running");
					if(apiCatalog != null) {
						apiCatalog.setDeploymentState("retired");
						apiCatalog.setUpdatedAt(new Date());
				    	apiCatalogDAO.merge(apiCatalog);
				    	deployedApiCatalog.setProtocol(apiCatalog.getProtocol());
				    	deployedApiCatalog.setConfluencePageId(apiCatalog.getConfluencePageId());
				    	deployedApiCatalog.setContactName(apiCatalog.getContactName());
    					deployedApiCatalog.setSwaggerUrl("NOTNULL");
    					deployedApiCatalog.setRegisteredAt(new Date());
    					deployedApiCatalog.setAzureHost(yamlPropertiesDAO.findOneAzureHostByDevOrgIdAndApiNameAndApiVersion(deployedApiCatalog.getDevOrgId(), deployedApiCatalog.getName(), deployedApiCatalog.getVersion()));
    					persistedApiCatalog = apiCatalogDAO.create(deployedApiCatalog);
    					apiOperationCatalogService.mantainApiOperationCatalog(persistedApiCatalog);
					} else {
    					deployedApiCatalog.setSwaggerUrl("NULL");
    					deployedApiCatalog.setRegisteredAt(new Date());
    					persistedApiCatalog = apiCatalogDAO.create(deployedApiCatalog);
    					apiOperationCatalogService.mantainApiOperationCatalog(persistedApiCatalog);
					}
				} catch (NoResultException e) {
					deployedApiCatalog.setSwaggerUrl("NORESULT");
					deployedApiCatalog.setRegisteredAt(new Date());
					persistedApiCatalog = apiCatalogDAO.create(deployedApiCatalog);
					apiOperationCatalogService.mantainApiOperationCatalog(persistedApiCatalog);
				}
			}
	}
	
	private List<ApiCatalog> fillApiCatalogDeployedBean(JSONArray jsonArray, String environment) throws ParseException {
		List<ApiCatalog> apiCatalogs = new ArrayList<ApiCatalog>();
	    for (int i = 0, size = jsonArray.length(); i < size; i++) {
	    	JSONObject jsonObject = jsonArray.getJSONObject(i);
    		ApiCatalog apiCatalog = new ApiCatalog();
    		apiCatalog.setDeploymentId(jsonObject.optString("apiId"));
    		if(jsonObject.optJSONArray("apiEndpoints") != null && jsonObject.optJSONArray("apiEndpoints").getJSONObject(0) != null && jsonObject.optJSONArray("apiEndpoints").getJSONObject(0).optString("endpointUrl") != null) {
        		String endpointUrl = jsonObject.optJSONArray("apiEndpoints").getJSONObject(0).optString("endpointUrl");
        		apiCatalog.setBasePath(endpointUrl.substring(endpointUrl.lastIndexOf(environment) + environment.length()));
    		}
    		try {
	    		if(jsonObject.optJSONObject("dependents") != null && jsonObject.optJSONObject("dependents").optJSONArray("PRODUCTVERSION") != null &&
	    				jsonObject.optJSONObject("dependents").optJSONArray("PRODUCTVERSION").getJSONObject(0) != null && 
	    				jsonObject.optJSONObject("dependents").optJSONArray("PRODUCTVERSION").getJSONObject(0).optString("id") != null) {
	        		apiCatalog.setProductCatalog(productCatalogDAO.findProductCatalogById(jsonObject.optJSONObject("dependents").optJSONArray("PRODUCTVERSION").getJSONObject(0).optString("id")));
	    		}
			} catch (NoResultException e) {
				LOG.info("Product not found: " + jsonObject.optJSONObject("dependents").optJSONArray("PRODUCTVERSION").getJSONObject(0).optString("id"));
			}
    		apiCatalog.setUrl(jsonObject.optString("url"));
    		apiCatalog.setUpdatedAt(dateFormatter.parse(jsonObject.optString("updatedAt")));
    		apiCatalog.setUpdatedBy(jsonObject.optString("updatedBy"));
    		apiCatalog.setContactEmail(jsonObject.optString("updatedBy"));
    		apiCatalog.setName(jsonObject.optString("apiTitle"));
    		apiCatalog.setVersion(jsonObject.optString("apiVersion"));
    		apiCatalog.setxIbmName(jsonObject.optString("apiName"));
    		apiCatalog.setDescription(jsonObject.optString("apiDescription"));
    		apiCatalog.setDevOrgId(jsonObject.optString("orgId"));
    		apiCatalog.setCreatedAt(dateFormatter.parse(jsonObject.optString("createdAt")));
    		apiCatalog.setCreatedBy(jsonObject.optString("createdBy"));
    		apiCatalog.setEnvId(jsonObject.optString("envId"));
    		apiCatalog.setDeploymentState(jsonObject.optString("deploymentState"));
    		apiCatalog.setRegisteredAt(new Date());
    		apiCatalog.setId(jsonObject.optString("id"));
    		apiCatalogs.add(apiCatalog);
	    }
		return apiCatalogs;
	}
}
