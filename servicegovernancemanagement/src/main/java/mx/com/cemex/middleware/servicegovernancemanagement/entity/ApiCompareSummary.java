package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "apicomparesummary")
public class ApiCompareSummary {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_api_compare_summary")
	private Long idApiCompareSummary;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "report_guid")
    private String reportGuid;
    
    @Column(name = "is_source_exist")
    private boolean isSourceExist;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
    @JsonIgnore
    @JoinColumn(name = "id_tenant_compare_summary", nullable = false, updatable = true, insertable = true)
    private TenantCompareSummary tenantCompareSummary;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "apiCompareSummary")
    private Set<ApiCompareDetail> apiCompareDetails = new HashSet<ApiCompareDetail>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "apiCompareSummary")
    private Set<OperationCompareSummary> operationCompareSummaries = new HashSet<OperationCompareSummary>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_azure_api_catalog_target", nullable = true, updatable = true, insertable = true)
    private AzureApiCatalog azureApiCatalogTarget;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_azure_api_catalog_source", nullable = true, updatable = true, insertable = true)
    @OrderBy("name ASC")
    private AzureApiCatalog azureApiCatalogSource;
    
    @Column(name = "is_target_exist")
    private boolean isTargetExist;

    private boolean isSafePatch;

	@Lob
    @Column(name = "source_policy_xml")
    private String sourcePolicyXml;

	@Lob
    @Column(name = "target_policy_xml")
    private String targetPolicyXml;

	public String getSourcePolicyXml() {
		return sourcePolicyXml;
	}

	public void setSourcePolicyXml(String sourcePolicyXml) {
		this.sourcePolicyXml = sourcePolicyXml;
	}

	public String getTargetPolicyXml() {
		return targetPolicyXml;
	}

	public void setTargetPolicyXml(String targetPolicyXml) {
		this.targetPolicyXml = targetPolicyXml;
	}

	public Set<OperationCompareSummary> getOperationCompareSummaries() {
		return operationCompareSummaries;
	}

	public void setOperationCompareSummaries(Set<OperationCompareSummary> operationCompareSummaries) {
		this.operationCompareSummaries = operationCompareSummaries;
	}

	public Set<ApiCompareDetail> getApiCompareDetails() {
		return apiCompareDetails;
	}

	public void setApiCompareDetails(Set<ApiCompareDetail> apiCompareDetails) {
		this.apiCompareDetails = apiCompareDetails;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getReportGuid() {
		return reportGuid;
	}

	public void setReportGuid(String reportGuid) {
		this.reportGuid = reportGuid;
	}

	public boolean isSourceExist() {
		return isSourceExist;
	}

	public void setSourceExist(boolean isSourceExist) {
		this.isSourceExist = isSourceExist;
	}

	//Velocity needs those 3 methods in order to access the boolean attribute
	public boolean getIsTargetExist() {
		return isTargetExist;
	}

	public boolean getIsSourceExist() {
		return isSourceExist;
	}

	public boolean getIsSafePatch() {
		return isSafePatch;
	}
	//End Velocity needs

	public boolean isTargetExist() {
		return isTargetExist;
	}

	public void setTargetExist(boolean isTargetExist) {
		this.isTargetExist = isTargetExist;
	}

	public boolean isSafePatch() {
		return isSafePatch;
	}

	public void setSafePatch(boolean isSafePatch) {
		this.isSafePatch = isSafePatch;
	}

	public Long getIdApiCompareSummary() {
		return idApiCompareSummary;
	}

	public void setIdApiCompareSummary(Long idApiCompareSummary) {
		this.idApiCompareSummary = idApiCompareSummary;
	}

	public TenantCompareSummary getTenantCompareSummary() {
		return tenantCompareSummary;
	}

	public void setTenantCompareSummary(TenantCompareSummary tenantCompareSummary) {
		this.tenantCompareSummary = tenantCompareSummary;
	}

	public AzureApiCatalog getAzureApiCatalogTarget() {
		return azureApiCatalogTarget;
	}

	public void setAzureApiCatalogTarget(AzureApiCatalog azureApiCatalogTarget) {
		this.azureApiCatalogTarget = azureApiCatalogTarget;
	}

	public AzureApiCatalog getAzureApiCatalogSource() {
		return azureApiCatalogSource;
	}

	public void setAzureApiCatalogSource(AzureApiCatalog azureApiCatalogSource) {
		this.azureApiCatalogSource = azureApiCatalogSource;
	}

}
