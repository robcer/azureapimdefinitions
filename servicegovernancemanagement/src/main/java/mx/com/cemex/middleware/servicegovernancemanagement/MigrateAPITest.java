package mx.com.cemex.middleware.servicegovernancemanagement;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import org.springframework.beans.BeansException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureOperationsService;

public class MigrateAPITest {

	public static void main(String[] args) throws BeansException, IOException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException {
		AzureOperationsService azureOperationsService = ((AzureOperationsService) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("azureOperationsService"));
		EnvironmentCatalogDAO environmentCatalogDAO = ((EnvironmentCatalogDAO) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("environmentCatalogDAO"));
		AzureApiOperationCatalogDAO azureApiOperationCatalogDAO = ((AzureApiOperationCatalogDAO) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("azureApiOperationCatalogDAO"));
		Long idEnvironmentCatalog = 35L;
		Long idApiOperationCatalog = 5610L;
		azureOperationsService.migrateOperationIntoAPITargetTenant(environmentCatalogDAO.findEnvironmentCatalog(idEnvironmentCatalog), azureApiOperationCatalogDAO.findAzureApiOperationCatalog(idApiOperationCatalog));
	}

}