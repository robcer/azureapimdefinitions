package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "azurepropertycatalog")
public class AzurePropertyCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_azure_property_catalog")
	private Long idAzurePropertyCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "value")
    private String value;
    
    @Column(name = "secret")
    private boolean secret;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_catalog", nullable = true, updatable = true, insertable = true)
    private EnvironmentCatalog environmentCatalog;

	public Long getIdAzurePropertyCatalog() {
		return idAzurePropertyCatalog;
	}

	public void setIdAzurePropertyCatalog(Long idAzurePropertyCatalog) {
		this.idAzurePropertyCatalog = idAzurePropertyCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public boolean getSecret() {
		return secret;
	}

	public void setSecret(boolean secret) {
		this.secret = secret;
	}

	public EnvironmentCatalog getEnvironmentCatalog() {
		return environmentCatalog;
	}

	public void setEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
		this.environmentCatalog = environmentCatalog;
	}
    
    
    
}
