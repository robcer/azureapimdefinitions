package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCompareSummaryDAO;

@Service("apiCompareSummaryDAO")
public class ApiCompareSummaryDAOImpl implements ApiCompareSummaryDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ApiCompareSummaryDAOImpl()
    {
    }

    ApiCompareSummaryDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ApiCompareSummary create(ApiCompareSummary apiCompareSummary) {
        em.persist(apiCompareSummary);
        return apiCompareSummary;
    }

    @Transactional
    public ApiCompareSummary merge(ApiCompareSummary apiCompareSummary) {
        em.merge(apiCompareSummary);
        return apiCompareSummary;
    }

    public long countApiCompareSummary() {
        return (Long) em.createQuery("select count(o) from ApiCompareSummary o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiCompareSummary> findApiCompareSummary(int firstResult, int maxResults) {
        return em.createQuery("select o from ApiCompareSummary o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ApiCompareSummary findApiCompareSummary(Long id) {
        if (id == null) return null;
        return em.find(ApiCompareSummary.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiCompareSummary> findAll() {
        return em.createQuery("select o from ApiCompareSummary o order by o.idCompareSummary DESC").getResultList();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from ApiCompareSummary");
    	query.executeUpdate();
    }
    
    public List<ApiCompareSummary> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from ApiCompareSummary o where o.devOrgNameEnv = :devOrgNameEnv", ApiCompareSummary.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public List<Object> findTargetFileNameByReportGUID(String reportGuid) {
    	@SuppressWarnings("unchecked")
    	List<Object> results = em.createNativeQuery("select target_file_name from ApiCompareSummary o where o.report_Guid = ?1 order by o.file_Name asc")
        		.setParameter(1, reportGuid).getResultList();
    	return results;
    }
    
    public List<ApiCompareSummary> findAllByReportGUID(String reportGuid) {
        if (reportGuid == null) return null;
        return em.createQuery("select o from ApiCompareSummary o where o.reportGuid = :reportGuid order by o.fileName asc", ApiCompareSummary.class).setParameter("reportGuid", reportGuid).getResultList();
    }
    
    public ApiCompareSummary findApiCompareSummaryById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from ApiCompareSummary o where o.id = :id", ApiCompareSummary.class)
        		.setParameter("id", id).getSingleResult();
    }

}