package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelEnvironmentOperation;

public interface RelEnvironmentOperationRepository extends JpaRepository<RelEnvironmentOperation, Long> {

	List<RelEnvironmentOperation> findAllByEnvironmentCatalogAndApiOperation(EnvironmentCatalog environmentCatalog, ApiOperation apiOperation);

}
