package mx.com.cemex.middleware.servicegovernancemanagement;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;

import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiGovernanceService;

public class APIConnectGovernanceMain {
	
	private static final Logger LOG  = Logger.getLogger(APIConnectGovernanceMain.class);
	
	public static void main(String[] args) throws BeansException, IOException, InterruptedException {
		LOG.info("Starting standalone process on APIConnectGovernanceMain");
		((ApiGovernanceService) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("apiGovernanceService")).runMaitainanceProcess();

	}

}