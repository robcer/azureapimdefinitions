package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "projectcatalog")
public class ProjectCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_project_catalog")
	private Long idProjectCatalog;
    
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "projectCatalog")
    private Set<RelProjectOperation> relProjectOperation = new HashSet<RelProjectOperation>();

	public Long getIdProjectCatalog() {
		return idProjectCatalog;
	}

	public void setIdProjectCatalog(Long idProjectCatalog) {
		this.idProjectCatalog = idProjectCatalog;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public Set<RelProjectOperation> getRelProjectOperation() {
		return relProjectOperation;
	}

	public void setRelProjectOperation(Set<RelProjectOperation> relProjectOperation) {
		this.relProjectOperation = relProjectOperation;
	}
    
}
