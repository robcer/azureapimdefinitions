package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "operationcomparesummary")
public class OperationCompareSummary {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_operation_compare_summary")
	private Long idOperationCompareSummary;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "report_guid")
    private String reportGuid;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
    @JsonIgnore
    @JoinColumn(name = "id_api_compare_summary", nullable = false, updatable = true, insertable = true)
    private ApiCompareSummary apiCompareSummary;
    
    @Column(name = "is_source_exist")
    private boolean isSourceExist;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "operationCompareSummary")
    @OrderBy("sourceStartingLine ASC")
    private Set<OperationCompareDetail> operationCompareDetails = new HashSet<OperationCompareDetail>();

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_azure_api_operation_catalog_target", nullable = true, updatable = true, insertable = true)
    private AzureApiOperationCatalog azureApiOperationCatalogTarget;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_azure_api_operation_catalog_source", nullable = true, updatable = true, insertable = true)
    private AzureApiOperationCatalog azureApiOperationCatalogSource;
    
    @Column(name = "is_target_exist")
    private boolean isTargetExist;

    private boolean isSafePatch;

	@Lob
    @Column(name = "source_policy_xml")
    private String sourcePolicyXml;

	@Lob
    @Column(name = "target_policy_xml")
    private String targetPolicyXml;

	public String getSourcePolicyXml() {
		return sourcePolicyXml;
	}

	public void setSourcePolicyXml(String sourcePolicyXml) {
		this.sourcePolicyXml = sourcePolicyXml;
	}

	public String getTargetPolicyXml() {
		return targetPolicyXml;
	}

	public void setTargetPolicyXml(String targetPolicyXml) {
		this.targetPolicyXml = targetPolicyXml;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getReportGuid() {
		return reportGuid;
	}

	public void setReportGuid(String reportGuid) {
		this.reportGuid = reportGuid;
	}

	public boolean isSourceExist() {
		return isSourceExist;
	}

	public void setSourceExist(boolean isSourceExist) {
		this.isSourceExist = isSourceExist;
	}

	//Velocity needs those 3 methods in order to access the boolean attribute
	public boolean getIsTargetExist() {
		return isTargetExist;
	}

	public boolean getIsSourceExist() {
		return isSourceExist;
	}

	public boolean getIsSafePatch() {
		return isSafePatch;
	}
	//End Velocity needs

	public boolean isTargetExist() {
		return isTargetExist;
	}

	public void setTargetExist(boolean isTargetExist) {
		this.isTargetExist = isTargetExist;
	}

	public boolean isSafePatch() {
		return isSafePatch;
	}

	public void setSafePatch(boolean isSafePatch) {
		this.isSafePatch = isSafePatch;
	}

	public Long getIdOperationCompareSummary() {
		return idOperationCompareSummary;
	}

	public void setIdOperationCompareSummary(Long idOperationCompareSummary) {
		this.idOperationCompareSummary = idOperationCompareSummary;
	}

	public ApiCompareSummary getApiCompareSummary() {
		return apiCompareSummary;
	}

	public void setApiCompareSummary(ApiCompareSummary apiCompareSummary) {
		this.apiCompareSummary = apiCompareSummary;
	}

	public Set<OperationCompareDetail> getOperationCompareDetails() {
		return operationCompareDetails;
	}

	public void setOperationCompareDetails(Set<OperationCompareDetail> operationCompareDetails) {
		this.operationCompareDetails = operationCompareDetails;
	}

	public AzureApiOperationCatalog getAzureApiOperationCatalogTarget() {
		return azureApiOperationCatalogTarget;
	}

	public void setAzureApiOperationCatalogTarget(AzureApiOperationCatalog azureApiOperationCatalogTarget) {
		this.azureApiOperationCatalogTarget = azureApiOperationCatalogTarget;
	}

	public AzureApiOperationCatalog getAzureApiOperationCatalogSource() {
		return azureApiOperationCatalogSource;
	}

	public void setAzureApiOperationCatalogSource(AzureApiOperationCatalog azureApiOperationCatalogSource) {
		this.azureApiOperationCatalogSource = azureApiOperationCatalogSource;
	}
	

}
