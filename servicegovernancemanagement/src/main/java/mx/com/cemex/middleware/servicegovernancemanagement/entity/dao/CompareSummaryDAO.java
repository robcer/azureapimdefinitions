package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareSummary;

public interface CompareSummaryDAO extends GenericEntityDAO<CompareSummary> {

    CompareSummary findCompareSummary(Long id);
    List<CompareSummary> findAllByReportGUID(String reportGuid);
    List<Object> findTargetFileNameByReportGUID(String reportGuid);

}
