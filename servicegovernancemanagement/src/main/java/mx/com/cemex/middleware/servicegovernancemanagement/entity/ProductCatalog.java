package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "productcatalog")
public class ProductCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_product_catalog")
	private Long idProductCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "version")
    private String version;
    
    @Column(name = "product_name")
    private String productName;
    
    @Column(name = "env_id")
    private String envId;
    
    @Column(name = "deployment_state")
    private String deploymentState;
    
    @Column(name = "dev_org_id")
    private String devOrgId;
    
    @Column(name = "dev_org_name")
    private String devOrgName;

    @Column(name = "removed_at")
    @DateTimeFormat(style = "S-")
    private Date removedAt;

    @Column(name = "created_at")
    @DateTimeFormat(style = "S-")
    private Date createdAt;
    
    @Column(name = "created_by")
    private String createdBy;

    @NotNull
    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;
    
    @Column(name = "updated_by")
    private String updatedBy;
    
    @Column(name = "url")
    private String url;
    
    @Column(name = "plan_registrations")
    private Long planRegistrations;

	public Long getIdProductCatalog() {
		return idProductCatalog;
	}

	public void setIdProductCatalog(Long idProductCatalog) {
		this.idProductCatalog = idProductCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getEnvId() {
		return envId;
	}

	public void setEnvId(String envId) {
		this.envId = envId;
	}

	public String getDeploymentState() {
		return deploymentState;
	}

	public void setDeploymentState(String deploymentState) {
		this.deploymentState = deploymentState;
	}

	public String getDevOrgId() {
		return devOrgId;
	}

	public void setDevOrgId(String devOrgId) {
		this.devOrgId = devOrgId;
	}

	public String getDevOrgName() {
		return devOrgName;
	}

	public void setDevOrgName(String devOrgName) {
		this.devOrgName = devOrgName;
	}

	public Date getRemovedAt() {
		return removedAt;
	}

	public void setRemovedAt(Date removedAt) {
		this.removedAt = removedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(String updatedBy) {
		this.updatedBy = updatedBy;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Long getPlanRegistrations() {
		return planRegistrations;
	}

	public void setPlanRegistrations(Long planRegistrations) {
		this.planRegistrations = planRegistrations;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}
    
}
