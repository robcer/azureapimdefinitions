package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.util.Collection;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.Application;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.ApplicationRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApplicationService;

@Named("applicationService")
public class ApplicationServiceImpl extends CrudServiceImpl<Application, Long, ApplicationRepository> implements ApplicationService {

	@Override
	@Inject
	public void setRepository(ApplicationRepository applicationRepository) {
		super.setRepository(applicationRepository);
	}
	
	public List<Application> findByRolesIn(Collection<ApplicationRole> roles) {
		return this.repository.findByRolesIn(roles);
	}
}
