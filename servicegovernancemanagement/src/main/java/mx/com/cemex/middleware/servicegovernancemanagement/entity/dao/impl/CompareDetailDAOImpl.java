package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareDetailDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.GenericEntityDAO;

@Service("compareDetailDAO")
public class CompareDetailDAOImpl extends AbstractEntityDAOImpl<CompareDetail> implements CompareDetailDAO {

	GenericEntityDAO<CompareDetail> genericEntityDAO;

	@Autowired
	public void setDao(GenericEntityDAO<CompareDetail> daoToSet) {
		genericEntityDAO = daoToSet;
		genericEntityDAO.setEntityClass(CompareDetail.class);
	}
 
    public CompareDetail findCompareDetail(Long id) {
        if (id == null) return null;
        return em.find(CompareDetail.class, id);
    }
}