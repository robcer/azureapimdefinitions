package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.sql.Timestamp;
import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiHistoryCall;

public interface ApiHistoryCallDAO {

	ApiHistoryCall create(ApiHistoryCall apiHistoryCall);
    long countApiHistoryCall();
    List<ApiHistoryCall> findApiHistoryCall(int firstResult, int maxResults);
    List<ApiHistoryCall> findAll();
    ApiHistoryCall findApiHistoryCall(Long id);
    Timestamp getLastDateTime(String organization, String catalog);
    Long countByPathResourceAndMethod(String pathresourcemethod);
    String getSchema();
    Timestamp getFirstDateTime(String organization, String catalog);
    Timestamp getFirstDateTime(String organization, String catalog, String devOrgId);
    List<Object[]> analyticsByApiIdAndResourceId(String apiId, String resourceId);
    List<Object[]> analyticsByApiIdAndResourceIdNative(String apiId, String resourceId);
    List<Object[]> analyticsByUriPathLikeOrgIdAndRequestMethodNative(String uriPath, String orgId, String requestMethod);
    List<Object[]> analyticsByPathAndOrgIdAndMethodNative(String path, String orgId, String requestMethod);
    List<Object[]> historicalReportByOrgNameAndCatalog(String devOrgName, String catalogName);
    List<Object[]> historicalReportByOrgNameAndCatalogAndDevOrgId(String devOrgName, String catalogName, String devOrgId);
}
