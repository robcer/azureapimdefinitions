package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.FilterDef;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "azureapioperationcatalog")
@FilterDef(name="notremoved", defaultCondition="removed = 0")
public class AzureApiOperationCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_azure_api_operation_catalog")
	private Long idAzureApiOperationCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_azure_api_catalog", nullable = false, updatable = true, insertable = true)
    private AzureApiCatalog azureApiCatalog;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "type")
    private String type;

    @Column(name = "name", length=1000)
    private String name;
    
    @Column(name = "method")
    private String method;
    
    @Column(name = "url_template")
    private String urlTemplate;
    
    @Column(name = "description", length=3000)
    private String description;

    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;
    
    @Column(name = "removed")
    @NotNull
    private boolean removed;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_catalog", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentCatalog;

	public boolean isRemoved() {
		return removed;
	}

	public boolean getRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public Long getIdAzureApiOperationCatalog() {
		return idAzureApiOperationCatalog;
	}

	public void setIdAzureApiOperationCatalog(Long idAzureApiOperationCatalog) {
		this.idAzureApiOperationCatalog = idAzureApiOperationCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public AzureApiCatalog getAzureApiCatalog() {
		return azureApiCatalog;
	}

	public void setAzureApiCatalog(AzureApiCatalog azureApiCatalog) {
		this.azureApiCatalog = azureApiCatalog;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrlTemplate() {
		return urlTemplate;
	}

	public void setUrlTemplate(String urlTemplate) {
		this.urlTemplate = urlTemplate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EnvironmentCatalog getEnvironmentCatalog() {
		return environmentCatalog;
	}

	public void setEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
		this.environmentCatalog = environmentCatalog;
	}
    
    
}
