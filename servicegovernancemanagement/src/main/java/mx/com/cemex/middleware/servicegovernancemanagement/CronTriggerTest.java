package mx.com.cemex.middleware.servicegovernancemanagement;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.JobKey;
import org.quartz.Scheduler;
import org.quartz.SchedulerFactory;
import org.quartz.SchedulerMetaData;
import org.quartz.impl.StdSchedulerFactory;

public class CronTriggerTest {
	
	private static final Logger LOG  = Logger.getLogger(CronTriggerTest.class);

	public void run() throws Exception {

		LOG.info("------- Inicializando  -------------------");

		//Primero debemos obtener una referencia del Scheduler
		SchedulerFactory schedulerFactory = new StdSchedulerFactory();
		Scheduler scheduler = schedulerFactory.getScheduler();

		LOG.info("------- Inicializacion completa --------");

		LOG.info("------- Programando Jobs ----------------");

		//Los Jobs pueden ser programados antes de que scheduler.start() sea llamado
		
		//Primero eliminamos los jobs de la base de datos si exiten
		scheduler.deleteJob(new JobKey("jobTest","groupTest"));

		//Job 3 correra cada 8 segundos
		JobDetail job = newJob(SimpleJob.class).withIdentity("jobTest", "groupTest").build();
		
		CronTrigger trigger = newTrigger().withIdentity("triggerTest", "groupTest").withSchedule(cronSchedule("0/2 * * * * ?")).build();
		LOG.info("isConcurrent: "+job.isConcurrentExectionDisallowed());
		Date date = scheduler.scheduleJob(job, trigger);
		LOG.info(job.getKey() + " Ha sido programado a correr en: " + date
				+ " y repetirse debido a la expresion: "
				+ trigger.getCronExpression());

//		//Job 1 debe correr cada 20 segundos
//		JobDetail job = newJob(SimpleJob.class).withIdentity("job1", "grupo1").build();
//		CronTrigger trigger = newTrigger().withIdentity("trigger1", "grupo1").withSchedule(cronSchedule("0/20 * * * * ?")).build();
//		
//		Date date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date
//				+ " y repetirse debido a la expresion: " + trigger.getCronExpression());
//
//		//Job 2 correra cada minuto (15 segundos pasados el minuto)
//		job = newJob(SimpleJob.class).withIdentity("job2", "grupo1").build();
//		trigger = newTrigger().withIdentity("trigger2", "grupo1").withSchedule(cronSchedule("15 0/2 * * * ?")).build();
//
//		date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date 
//				+ " y repetirse debido a la expresion: " + trigger.getCronExpression());
//
//		//Job 3 correra cada 2 minutos pero solo entre 8 a.m. y 5 p.m.
//		job = newJob(SimpleJob.class).withIdentity("job3", "grupo1").build();
//		trigger = newTrigger().withIdentity("trigger3", "grupo1").withSchedule(cronSchedule("0 0/2 8-17 * * ?")).build();
//
//		date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date
//				+ " y repetirse debido a la expresion: "
//				+ trigger.getCronExpression());
//
//		//Job 4 correra cada 3 minutos pero solo entre 5 p.m. y 11 p.m.
//		job = newJob(SimpleJob.class).withIdentity("job4", "grupo1").build();
//
//		trigger = newTrigger().withIdentity("trigger4", "grupo1").withSchedule(
//				cronSchedule("0 0/3 17-23 * * ?")).build();
//
//		date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date 
//				+ " y repetirse debido a la expresion: " + trigger.getCronExpression());
//
//		//Job 5 correra a las 10 a.m. el 1ro y 15vo de cada mes.
//		job = newJob(SimpleJob.class).withIdentity("job5", "grupo1").build();
//
//		trigger = newTrigger().withIdentity("trigger5", "grupo1").withSchedule(
//				cronSchedule("0 0 10am 1,15 * ?")).build();
//
//		date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date
//				+ " y repetirse debido a la expresion: " + trigger.getCronExpression());
//
//		//Job 6 correra cada 30 segundos pero solo entre semana (Lunes a Viernes)
//		job = newJob(SimpleJob.class).withIdentity("job6", "grupo1").build();
//
//		trigger = newTrigger().withIdentity("trigger6", "grupo1").withSchedule(
//				cronSchedule("0,30 * * ? * MON-FRI")).build();
//
//		date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date
//				+ " y repetirse debido a la expresion: " + trigger.getCronExpression());
//
//		//Job 7 correra cada 30 segundos pero solo en fines de semana (Sabados y Domindos)
//		job = newJob(SimpleJob.class).withIdentity("job7", "grupo1").build();
//
//		trigger = newTrigger().withIdentity("trigger7", "grupo1").withSchedule(
//				cronSchedule("0,30 * * ? * SAT,SUN")).build();
//
//		date = scheduler.scheduleJob(job, trigger);
//		log.info(job.getKey() + " Ha sido programado a correr en: " + date
//				+ " y repetirse debido a la expresion: " + trigger.getCronExpression());

		LOG.info("------- Iniciando Programador ----------------");

		//Todos los jobs fueron agregados al programador, 
		//pero ninguno de estos jobs correra hasta iniciar el programador
		scheduler.start();

		LOG.info("------- Programador Inicializado -----------------");

		LOG.info("------- Esperamos cinco minutos.. ------------");
		try {
			// esperamos 5 minutos para mostrar los jobs
			Thread.sleep(30000L);
			// ejecutandose
		} catch (Exception e) {
		}

		LOG.info("------- Apagando Programador ---------------------");

		scheduler.shutdown(true);

		LOG.info("------- Programador apagado -----------------");

		SchedulerMetaData metaData = scheduler.getMetaData();
		LOG.info("Ejecutamos " + metaData.getNumberOfJobsExecuted() + " jobs.");

	}

	public static void main(String[] args) throws Exception {

		CronTriggerTest example = new CronTriggerTest();
		example.run();
	}

}
