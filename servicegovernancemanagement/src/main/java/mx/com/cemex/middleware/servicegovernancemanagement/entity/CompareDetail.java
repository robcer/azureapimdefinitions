package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "comparedetail")
public class CompareDetail implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_compare_detail")
	private Long idCompareDetail;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "report_guid")
    private String reportGuid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_source", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentSource;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_target", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentTarget;
    
    @Column(name = "plain_text_diff_insert")
	@Lob
    private String plainTextDiffInsert;
    
    @Column(name = "plain_text_diff_delete")
	@Lob
    private String plainTextDiffDelete;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
    @JoinColumn(name = "id_compare_summary", nullable = false, updatable = true, insertable = true)
    private CompareSummary compareSummary;
    
    @Column(name = "source_starting_line")
	private Long sourceStartingLine;
    
    @Column(name = "target_starting_line")
	private Long targetStartingLine;

	public Long getIdCompareDetail() {
		return idCompareDetail;
	}

	public void setIdCompareDetail(Long idCompareDetail) {
		this.idCompareDetail = idCompareDetail;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getReportGuid() {
		return reportGuid;
	}

	public void setReportGuid(String reportGuid) {
		this.reportGuid = reportGuid;
	}

	public EnvironmentCatalog getEnvironmentSource() {
		return environmentSource;
	}

	public void setEnvironmentSource(EnvironmentCatalog environmentSource) {
		this.environmentSource = environmentSource;
	}

	public EnvironmentCatalog getEnvironmentTarget() {
		return environmentTarget;
	}

	public void setEnvironmentTarget(EnvironmentCatalog environmentTarget) {
		this.environmentTarget = environmentTarget;
	}

	public CompareSummary getCompareSummary() {
		return compareSummary;
	}

	public void setCompareSummary(CompareSummary compareSummary) {
		this.compareSummary = compareSummary;
	}

	public String getPlainTextDiffInsert() {
		return plainTextDiffInsert;
	}

	public void setPlainTextDiffInsert(String plainTextDiffInsert) {
		this.plainTextDiffInsert = plainTextDiffInsert;
	}

	public String getPlainTextDiffDelete() {
		return plainTextDiffDelete;
	}

	public void setPlainTextDiffDelete(String plainTextDiffDelete) {
		this.plainTextDiffDelete = plainTextDiffDelete;
	}

	public Long getSourceStartingLine() {
		return sourceStartingLine;
	}

	public void setSourceStartingLine(Long sourceStartingLine) {
		this.sourceStartingLine = sourceStartingLine;
	}

	public Long getTargetStartingLine() {
		return targetStartingLine;
	}

	public void setTargetStartingLine(Long targetStartingLine) {
		this.targetStartingLine = targetStartingLine;
	}
    

}
