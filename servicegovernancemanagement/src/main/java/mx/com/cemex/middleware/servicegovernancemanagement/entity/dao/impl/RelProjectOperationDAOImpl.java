package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProjectCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelProjectOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.RelProjectOperationDAO;

@Service("relProjectOperationDAO")
public class RelProjectOperationDAOImpl implements RelProjectOperationDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public RelProjectOperationDAOImpl()
    {
    }

    RelProjectOperationDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public RelProjectOperation create(RelProjectOperation relProjectOperation) {
        em.persist(relProjectOperation);
        return relProjectOperation;
    }

    @Transactional
    public RelProjectOperation merge(RelProjectOperation relProjectOperation) {
        em.merge(relProjectOperation);
        return relProjectOperation;
    }

    public long countRelProjectOperation() {
        return (Long) em.createQuery("select count(o) from RelProjectOperation o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<RelProjectOperation> findRelProjectOperation(int firstResult, int maxResults) {
        return em.createQuery("select o from RelProjectOperation o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public RelProjectOperation findRelProjectOperation(Long id) {
        if (id == null) return null;
        return em.find(RelProjectOperation.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<RelProjectOperation> findAll() {
        return em.createQuery("select o from RelProjectOperation o").getResultList();
    }
    
    public List<RelProjectOperation> findAllByProjectAndOperation(ProjectCatalog projectCatalog, ApiOperationCatalog apiOperationCatalog) {
        return em.createQuery("select o from RelProjectOperation o where o.projectCatalog = :projectCatalog and o.apiOperationCatalog = :apiOperationCatalog", RelProjectOperation.class)
        		.setParameter("projectCatalog", projectCatalog)
        		.setParameter("apiOperationCatalog", apiOperationCatalog)
        		.getResultList();
    }
}
