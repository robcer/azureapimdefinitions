package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareSummary;

public interface ApiCompareSummaryDAO {

	ApiCompareSummary create(ApiCompareSummary apiCompareSummary);
	ApiCompareSummary merge(ApiCompareSummary apiCompareSummary);
    long countApiCompareSummary();
    List<ApiCompareSummary> findApiCompareSummary(int firstResult, int maxResults);
    List<ApiCompareSummary> findAll();
    ApiCompareSummary findApiCompareSummary(Long id);
    List<ApiCompareSummary> findAllByReportGUID(String reportGuid);
    List<Object> findTargetFileNameByReportGUID(String reportGuid);
    void deleteAll();

}
