package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "relenvironmentapi")
public class RelEnvironmentApi {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_environment_api")
	private Long idEnvironmentApi;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_catalog", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentCatalog;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_api", nullable = false, updatable = true, insertable = true)
    private Api api;

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}
	
	public EnvironmentCatalog getEnvironmentCatalog() {
		return environmentCatalog;
	}

	public void setEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
		this.environmentCatalog = environmentCatalog;
	}

	public Long getIdEnvironmentApi() {
		return idEnvironmentApi;
	}

	public void setIdEnvironmentApi(Long idEnvironmentApi) {
		this.idEnvironmentApi = idEnvironmentApi;
	}

	public Api getApi() {
		return api;
	}

	public void setApi(Api api) {
		this.api = api;
	}

    
}
