package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;

import org.json.JSONObject;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.BitbucketSrcHeadersBean;

public interface BitbucketOperationsService {
	
	String commitUpdateCreateSourceFile(BitbucketSrcHeadersBean bitbucketSrcHeadersBean) throws IOException;
	JSONObject createPullRequest(BitbucketSrcHeadersBean bitbucketSrcHeadersBean) throws MalformedURLException, IOException, ParseException;
	JSONObject mergePullRequest(BitbucketSrcHeadersBean bitbucketSrcHeadersBean) throws MalformedURLException, IOException, ParseException;
	
}
