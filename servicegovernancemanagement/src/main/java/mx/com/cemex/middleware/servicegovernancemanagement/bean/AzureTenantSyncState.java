package mx.com.cemex.middleware.servicegovernancemanagement.bean;

public class AzureTenantSyncState {

	private String branch;
	private String commitId;
	private boolean isExport;
	private boolean isSynced;
	private boolean isGitEnabled;
	private String syncDate;
	private String configurationChangeDate;
	
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public String getCommitId() {
		return commitId;
	}
	public void setCommitId(String commitId) {
		this.commitId = commitId;
	}
	public boolean isExport() {
		return isExport;
	}
	public void setExport(boolean isExport) {
		this.isExport = isExport;
	}
	public boolean isSynced() {
		return isSynced;
	}
	public void setSynced(boolean isSynced) {
		this.isSynced = isSynced;
	}
	public boolean isGitEnabled() {
		return isGitEnabled;
	}
	public void setGitEnabled(boolean isGitEnabled) {
		this.isGitEnabled = isGitEnabled;
	}
	public String getSyncDate() {
		return syncDate;
	}
	public void setSyncDate(String syncDate) {
		this.syncDate = syncDate;
	}
	public String getConfigurationChangeDate() {
		return configurationChangeDate;
	}
	public void setConfigurationChangeDate(String configurationChangeDate) {
		this.configurationChangeDate = configurationChangeDate;
	}
	
	
}
