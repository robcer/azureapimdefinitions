package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.resthub.web.controller.ServiceBasedRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureApiCatalogService;

@Controller
@RequestMapping(value = "/v1/its/apis", produces = "application/json")
public class AzureApiCatalogController extends ServiceBasedRestController<AzureApiCatalog, Long, AzureApiCatalogService> {
	
	private static final Logger LOG  = Logger.getLogger(AzureApiCatalogController.class);

	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;

    @Inject
    @Named("azureApiCatalogService")
    @Override
    public void setService(AzureApiCatalogService service) {
        this.service = service;
    }

    @RequestMapping(value = "/environments/{idEnvironmentCatalog}")
    @ResponseBody
    public Iterable<AzureApiCatalog> searchBySkillLines(@PathVariable Long idEnvironmentCatalog) {
        return this.service.findAllByEnvironmentCatalog(environmentCatalogDAO.findEnvironmentCatalog(idEnvironmentCatalog));
    }
}