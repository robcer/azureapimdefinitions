package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.io.Serializable;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.GenericEntityDAO;

public abstract class AbstractEntityDAOImpl<T extends Serializable> implements GenericEntityDAO<T> {
	
	private Class<T> entityClass;

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;
    
    public void setEntityClass(Class<T> classToSet) {
       this.entityClass = classToSet;
    }

    public AbstractEntityDAOImpl()
    {
    }

    AbstractEntityDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public T create(T entity) {
        em.persist(entity);
        return entity;
    }

    @Transactional
    public T merge(T entity) {
        em.merge(entity);
        return entity;
    }

    public long countEntity() {
        return (Long) em.createQuery("select count(o) from " + entityClass.getSimpleName() + " o").getSingleResult();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from " + entityClass.getSimpleName());
    	query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    public List<T> findEntity(int firstResult, int maxResults) {
        return em.createQuery("select o from " + entityClass.getSimpleName() + " o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @SuppressWarnings("unchecked")
    public List<T> findAll(String entityIdentifier) {
        return em.createQuery("select o from " + entityClass.getSimpleName() + " o order by o." + entityIdentifier + " DESC").getResultList();
    }

}