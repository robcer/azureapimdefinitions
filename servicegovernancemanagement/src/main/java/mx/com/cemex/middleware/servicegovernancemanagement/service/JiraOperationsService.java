package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;

import org.json.JSONObject;

public interface JiraOperationsService {
	
	JSONObject getJiraIssueByKey(String jiraKey) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;
	JSONObject postJiraIssueComment(String jiraKey, String comment) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException;

}
