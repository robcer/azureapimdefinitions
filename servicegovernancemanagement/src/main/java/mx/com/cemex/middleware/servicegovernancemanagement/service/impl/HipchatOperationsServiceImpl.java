package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.HipchatRoom;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.HipchatRoomDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.HipchatOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

@Service("hipchatOperationsService")
public class HipchatOperationsServiceImpl implements HipchatOperationsService {
	
	private static final Logger LOG  = Logger.getLogger(HipchatOperationsServiceImpl.class);
	
	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	HipchatRoomDAO hipchatRoomDAO;
	
	public void sendNotificationToHipchatRooms(String color, String messageFormat, String message) throws MalformedURLException, IOException, ParseException {
		List<HipchatRoom> hipchatRooms = hipchatRoomDAO.findAll();
		for(HipchatRoom hipchatRoom: hipchatRooms) {
			postHipchatNotification(hipchatRoom, color, messageFormat, message);
		}
	}
	
	public List<HipchatRoom> getHipchatRooms() throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException {
		String apiUrl = IYamlBeansMasterConstants.HIPCHAT_ROOM_BASE_URI + "?auth_token=9ktr5HXt5Z8Gos7cWuEB8Y7pO3cKbYVzjut9wPr2&max-results=1000";
		LOG.info("Fetching Hipchat Rooms... on URL: " + apiUrl);
		JSONObject jsonObjectApis = utilitiesService.getJSONObject(apiUrl, null);
		return fillHipchatRoomBean(jsonObjectApis);
	}
	
	private List<HipchatRoom> fillHipchatRoomBean(JSONObject jsonObjectApis) throws ParseException {
		List<HipchatRoom> hipchatRooms = new ArrayList<HipchatRoom>();
		if(jsonObjectApis.optJSONArray("items") != null &&
				jsonObjectApis.optJSONArray("items").length() > 0) {
			JSONArray jsonArray = jsonObjectApis.optJSONArray("items");
		    for (int i = 0, size = jsonArray.length(); i < size; i++) {
		    	JSONObject jsonObject = jsonArray.getJSONObject(i);
		    	HipchatRoom hipchatRoom = fillHipchatRoomObject(jsonObject);
	    		hipchatRooms.add(hipchatRoom);
		    }
		}
		return hipchatRooms;
	}

	private HipchatRoom fillHipchatRoomObject(JSONObject jsonObject) {
		HipchatRoom hipchatRoom = new HipchatRoom();
		hipchatRoom.setId(jsonObject.optLong("id"));
		hipchatRoom.setName(jsonObject.optString("name"));
		hipchatRoom.setPrivacy(jsonObject.optString("privacy"));
		hipchatRoom.setVersion(jsonObject.optString("version"));
		hipchatRoom.setArchived(new Boolean(jsonObject.optString("is_archived")));
		return hipchatRoom;
	}

	public int postHipchatNotification(HipchatRoom hipchatRoom, String color, String messageFormat, String message)
			throws MalformedURLException, IOException, ParseException {
		String apiUrl = IYamlBeansMasterConstants.HIPCHAT_ROOM_BASE_URI + "/" + hipchatRoom.getId() + "/notification?auth_token=Mwo48EmyowzvGqnRCHAoWFbdfYCkivO1kMN9JjHk&max-results=1000";
		HttpURLConnection connection = (HttpURLConnection) new URL(apiUrl).openConnection();
		try {
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setRequestProperty("Accept", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("color", color);
			jsonObject.put("message_format", messageFormat);
			jsonObject.put("message", message);
			outputStreamWriter.write(jsonObject.toString());
			outputStreamWriter.close();
			return connection.getResponseCode();
		} catch (Exception e) {
			e.printStackTrace();
			return 500;
		} finally {
			connection.disconnect();
		}
	}
	
	
}
