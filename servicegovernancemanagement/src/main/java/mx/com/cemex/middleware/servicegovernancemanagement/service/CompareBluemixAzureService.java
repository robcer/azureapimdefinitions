package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;

import org.json.simple.parser.ParseException;

public interface CompareBluemixAzureService {
	
	void compareBluemixAzure() throws IOException, ParseException, java.text.ParseException;

}
