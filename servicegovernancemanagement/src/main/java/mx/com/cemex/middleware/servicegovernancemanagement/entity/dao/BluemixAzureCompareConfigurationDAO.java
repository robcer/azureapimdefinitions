package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.BluemixAzureCompareConfiguration;

public interface BluemixAzureCompareConfigurationDAO {

	BluemixAzureCompareConfiguration create(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration);
	BluemixAzureCompareConfiguration merge(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration);
    long countBluemixAzureCompareConfiguration();
    List<BluemixAzureCompareConfiguration> findBluemixAzureCompareConfiguration(int firstResult, int maxResults);
    List<BluemixAzureCompareConfiguration> findAll();
    BluemixAzureCompareConfiguration findBluemixAzureCompareConfiguration(Long id);
    List<Object[]> findDifferencesBetweenAzureAndBluemix(Long environmentId, String devOrgId);
}
