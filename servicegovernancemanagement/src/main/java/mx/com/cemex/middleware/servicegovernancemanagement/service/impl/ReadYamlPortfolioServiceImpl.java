package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import javax.persistence.NoResultException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlAppProgress;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlPortfolio;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlProperties;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlAppProgressDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlPortfolioDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ReadYamlPortfolioService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

import com.ebay.xcelite.Xcelite;
import com.ebay.xcelite.reader.SheetReader;
import com.ebay.xcelite.sheet.XceliteSheet;
import com.ebay.xcelite.writer.SheetWriter;

@Service("readYamlPortfolioService")
public class ReadYamlPortfolioServiceImpl implements ReadYamlPortfolioService {
	
	private static final Logger LOG  = Logger.getLogger(ReadYamlPortfolioServiceImpl.class);

	@Autowired
	YamlPortfolioDAO yamlPortfolioDAO;

	@Autowired
	YamlAppProgressDAO yamlAppProgressDAO;
	
	@Autowired
	UtilitiesService utilitiesService;

	private Map<String, String> businessCapabilitiesMap = loadBusinessCapabilitiesMap();
	
	private Map<String, String> developersNamesMap = loadDevelopersNamesMap();
	
	private Map<String, String> loadBusinessCapabilitiesMap() {
		Map<String, String> businessCapabilitiesMap = new HashMap<String, String>();
		businessCapabilitiesMap.put("sm", "Sales Management");
		businessCapabilitiesMap.put("dtm", "Driver and Truck Management");
		businessCapabilitiesMap.put("dm", "Delivery Management");
		businessCapabilitiesMap.put("rm", "Relationship Management");
		businessCapabilitiesMap.put("mm", "Material Management");
		businessCapabilitiesMap.put("arm", "Accounts Receivable Management");
		businessCapabilitiesMap.put("apm", "Accounts Payable Management");
		businessCapabilitiesMap.put("pm", "Production Management");
		businessCapabilitiesMap.put("om", "Opportunity Management");
		businessCapabilitiesMap.put("prc", "Pricing");
		businessCapabilitiesMap.put("qc", "Quotation Creation");
		businessCapabilitiesMap.put("qm", "Quotation Management");
		businessCapabilitiesMap.put("ce", "Configuration Elements ");
		businessCapabilitiesMap.put("secm", "Security Management");
		businessCapabilitiesMap.put("cum", "Customer Management");
		businessCapabilitiesMap.put("gd", "Geodata ");
		businessCapabilitiesMap.put("cm", "Communication Management");
		businessCapabilitiesMap.put("rep", "Reporting");
		businessCapabilitiesMap.put("bi", "Business Intelligence");
		businessCapabilitiesMap.put("ds", "Document Storage");
		businessCapabilitiesMap.put("rc", "Reusable Components");
		businessCapabilitiesMap.put("ld", "Logistics");
		businessCapabilitiesMap.put("legal", "Legal");
		businessCapabilitiesMap.put("sn", "Status Notification");
		businessCapabilitiesMap.put("srva", "Serviceability");
		businessCapabilitiesMap.put("fa", "Financial Accounting");
		businessCapabilitiesMap.put("crm", "Customer Relationship Management");
		businessCapabilitiesMap.put("pe", "Payments Execution");
		businessCapabilitiesMap.put("bl", "Business Line");
		businessCapabilitiesMap.put("ca", "Customer Agreement");
		businessCapabilitiesMap.put("hrm", "Human Resource Management");
		businessCapabilitiesMap.put("gm", "Guest Management");
		businessCapabilitiesMap.put("im", "Information Management");
		businessCapabilitiesMap.put("its", "Information Technology Support");
		//Exceptions
		businessCapabilitiesMap.put("mcx", "My Cemex");
		businessCapabilitiesMap.put("mycemex", "My Cemex");
		businessCapabilitiesMap.put("summary", "Sales Management");
		businessCapabilitiesMap.put("trucklocation", "Driver and Truck Management");
		businessCapabilitiesMap.put("oam", "Security Management");
		businessCapabilitiesMap.put("scm", "Security Management");
		businessCapabilitiesMap.put("detail", "Delivery Management");
		return businessCapabilitiesMap;
	}

	private Map<String, String> loadDevelopersNamesMap() {
		Map<String, String> developersNamesMap = new HashMap<String, String>();
		developersNamesMap.put("Antonio Rafael Robles Delgado", "Antonio Rafael Robles Delgado");
		developersNamesMap.put("Aryan Solanki", "Aryan Solanki");
		developersNamesMap.put("EHVC", "Edgar Humberto Vallejo Cedillo");
		developersNamesMap.put("Edgar Zavala", "Edgar Zavala");
		developersNamesMap.put("Edmundo Campillo", "Edmundo Rene Campillo Flores");
		developersNamesMap.put("Edmundo Rene Campillo", "Edmundo Rene Campillo Flores");
		developersNamesMap.put("Edmundo René Campillo", "Edmundo Rene Campillo Flores");
		developersNamesMap.put("Ferenc Kajtar", "Ferenc Kajtar");
		developersNamesMap.put("Ferenc Kajtár", "Ferenc Kajtar");
		developersNamesMap.put("GAURAV GUPTA", "Gaurav Guptar");
		developersNamesMap.put("German Gutierrez", "German Gutierrez Santillan");
		developersNamesMap.put("German Gutierrez Santillan", "German Gutierrez Santillan");
		developersNamesMap.put("Germán Gutierrez Santillán", "German Gutierrez Santillan");
		developersNamesMap.put("Gilberto Ventura", "Gilberto Ventura Robledo");
		developersNamesMap.put("Gilberto Ventura Robledo", "Gilberto Ventura Robledo");
		developersNamesMap.put("Himanshu Aggarwal", "Himanshu Aggarwal");
		developersNamesMap.put("Isidro Serafin", "Isidro Serafín Hernandez Maldonado");
		developersNamesMap.put("Isidro Serafin Hernandez", "Isidro Serafín Hernandez Maldonado");
		developersNamesMap.put("Jesus Vargas Guerrero", "Jesus Vargas Guerrero");
		developersNamesMap.put("Jesus vargas", "Jesus Vargas Guerrero");
		developersNamesMap.put("Leo BG", "Leonardo Daniel Betanzo Gerardo");
		developersNamesMap.put("Leo Bg", "Leonardo Daniel Betanzo Gerardo");
		developersNamesMap.put("Leonardo Daniel Betanzo Gerardo", "Leonardo Daniel Betanzo Gerardo");
		developersNamesMap.put("Moises Ramos H", "Moises Ramos Hernandez");
		developersNamesMap.put("Moises Ramos Hernandez", "Moises Ramos Hernandez");
		developersNamesMap.put("Nitin Kumar", "Nitin Kumar");
		developersNamesMap.put("René Campillo", "Edmundo Rene Campillo Flores");
		developersNamesMap.put("Rogelio Reyo Cachu", "Rogelio Reyo Cachu");
		developersNamesMap.put("Servando Treviño Leal", "Moises Ramos Hernandez");
		return developersNamesMap;
	}

	public void readYamlPorfolio(String fileName) throws FileNotFoundException, IllegalAccessException, InvocationTargetException {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyyHHmmssZ");
		String fileCreationDate = dateFormatter.format(new Date());
		Map<String, String> directoriesMap = new HashMap<String, String>();
		directoriesMap.put("Development", IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\US-SOUTH-CNX-GBL-ORG-DEVELOPMENT");
		Xcelite xcelite = new Xcelite();
		for (Entry<String, String> entry : directoriesMap.entrySet()) {
			XceliteSheet sheetPorfolio = xcelite.createSheet("Portfolio");
			XceliteSheet sheetAppProgress = xcelite.createSheet("AppProgress");
			SheetWriter<YamlPortfolio> writerPortfolio = sheetPorfolio.getBeanWriter(YamlPortfolio.class);
			SheetWriter<YamlAppProgress> writerAppProgress = sheetAppProgress.getBeanWriter(YamlAppProgress.class);
			File dir = new File(entry.getValue());
			File[] directoryListing = dir.listFiles();
			List<YamlPortfolio> yamlPortfolios = new ArrayList<YamlPortfolio>();
			List<YamlAppProgress> yamlAppProgresses = new ArrayList<YamlAppProgress>();
			if (directoryListing != null) {
				for (File yamlFile : directoryListing) {
					Yaml yaml = new Yaml();
					if(yamlFile.isFile()) {
						InputStream ios = new FileInputStream(yamlFile);
						@SuppressWarnings("unchecked")
						Map<String, Object> map = (Map<String, Object>) yaml.load(ios);
						JSONObject yamlJson = (JSONObject) utilitiesService._convertToJson(map);
						if(!yamlFile.getName().contains("_product_")) {
							yamlPortfolios.addAll(fillYamlPortfolio(yamlJson));
							yamlAppProgresses.addAll(fillYamlAppProgress(yamlJson));
						}
					}
				}
				if(entry.getKey().equals("Development")) {
					Xcelite xceliteOld = new Xcelite(new File(fileName));
					XceliteSheet sheetOld = xceliteOld.getSheet("58e2aa5f0cf26aa41002a447");
					SheetReader<YamlProperties> simpleReader = sheetOld.getBeanReader(YamlProperties.class);
					simpleReader.skipHeaderRow(true);
					Collection<YamlProperties> yamlProperties2 = simpleReader.read();
					for(YamlProperties yamlIndex2: yamlProperties2) {
						for(YamlAppProgress yamlIndex: yamlAppProgresses) {
							if(yamlIndex.getFullPath().trim().toUpperCase().equals(yamlIndex2.getFullPath().trim().toUpperCase())) {
								yamlIndex.setAppName(yamlIndex2.getProject());
								yamlIndex.setDeprecated(yamlIndex2.getDeprecated());
								yamlIndex.setStoreProcedure(yamlIndex2.getStoreProcedure());
							}
						}
					}
				}
				for(YamlAppProgress yamlAppProgress: yamlAppProgresses) {
					try {
						YamlAppProgress persistedYamlAppProgress = yamlAppProgressDAO.findYamlAppProgresByServiceName(yamlAppProgress.getServiceName(), yamlAppProgress.getFullPath());
						Long idYamlAppProgress = persistedYamlAppProgress.getIdYamlAppProgress();
						yamlAppProgress.setUpdatedAt(new Date());
						yamlAppProgress.setCreatedAt(persistedYamlAppProgress.getCreatedAt());
						BeanUtils.copyProperties(persistedYamlAppProgress, yamlAppProgress);
						persistedYamlAppProgress.setIdYamlAppProgress(idYamlAppProgress);
						yamlAppProgressDAO.merge(persistedYamlAppProgress);
					} catch (NoResultException e) {
						yamlAppProgress.setCreatedAt(new Date());
						yamlAppProgressDAO.create(yamlAppProgress);
					}
				}
				for(YamlPortfolio yamlPortfolio: yamlPortfolios) {
					try {
						YamlPortfolio persistedYamlPortfolio = yamlPortfolioDAO.findYamlPortfolioByProductName(yamlPortfolio.getProductName());
						Long idYamlPorfolio = persistedYamlPortfolio.getIdYamlPorfolio();
						yamlPortfolio.setUpdatedAt(new Date());
						yamlPortfolio.setCreatedAt(persistedYamlPortfolio.getCreatedAt());
						BeanUtils.copyProperties(persistedYamlPortfolio, yamlPortfolio);
						persistedYamlPortfolio.setIdYamlPorfolio(idYamlPorfolio);
						yamlPortfolioDAO.merge(persistedYamlPortfolio);
					} catch (NoResultException e) {
						yamlPortfolio.setCreatedAt(new Date());
						yamlPortfolioDAO.create(yamlPortfolio);
					}
				}
				writerPortfolio.write(yamlPortfolios);
				writerAppProgress.write(yamlAppProgresses);
			}
		}
		xcelite.write(new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + fileCreationDate + "_APIDashboard.xlsx"));
	}

	private List<YamlAppProgress> fillYamlAppProgress(JSONObject jsonObject) {
		List<YamlAppProgress> yamlAppProgressList = new ArrayList<YamlAppProgress>();
		if(jsonObject.optJSONObject("paths") != null) {
			Set<String> paths = jsonObject.optJSONObject("paths").keySet();
			Iterator<String> iteratePaths = paths.iterator();
			do {
				String path = iteratePaths.next().toString();
				Set<String> methods = jsonObject.optJSONObject("paths").optJSONObject(path).keySet();
				Iterator<String> iterateMethods = methods.iterator();
				do {
					String method = iterateMethods.next().toString();
					YamlAppProgress yamlAppProgress = new YamlAppProgress();
					yamlAppProgress.setFullPath(method.toUpperCase()+jsonObject.optString("basePath")+path);
					if(jsonObject.optJSONObject("info") != null) {
						String businessCapability = businessCapabilitiesMap.get(jsonObject.optString("basePath").substring(jsonObject.optString("basePath").lastIndexOf("/")+1));
						yamlAppProgress.setServiceName(businessCapability+"-"+jsonObject.optJSONObject("info").optString("title")+"-"+jsonObject.optJSONObject("info").optString("version")+"-"+jsonObject.optString("basePath"));
						yamlAppProgress.setServiceId(jsonObject.optJSONObject("info").optString("title")+"-"+jsonObject.optJSONObject("info").optString("version")+"-"+jsonObject.optString("basePath"));
						yamlAppProgress.setOperation(method.toUpperCase()+jsonObject.optString("basePath")+path);
						if(!method.equals("parameters") && !jsonObject.optJSONObject("info").optString("title").toLowerCase().contains("test")) {
							yamlAppProgressList.add(yamlAppProgress);
						}
					}
				} while (iterateMethods.hasNext());
			} while (iteratePaths.hasNext());
		}
		return yamlAppProgressList;
	}

	private List<YamlPortfolio> fillYamlPortfolio(JSONObject jsonObject) {
		List<YamlPortfolio> yamlPortfolioList = new ArrayList<YamlPortfolio>();
		YamlPortfolio yamlPortfolio = new YamlPortfolio();
		if(jsonObject.optJSONObject("info") != null) {
			String businessCapability = businessCapabilitiesMap.get(jsonObject.optString("basePath").substring(jsonObject.optString("basePath").lastIndexOf("/")+1));
			yamlPortfolio.setProductVersion(jsonObject.optJSONObject("info").optString("version"));
			yamlPortfolio.setProductName(businessCapability+"-"+jsonObject.optJSONObject("info").optString("title")+"-"+jsonObject.optJSONObject("info").optString("version")+"-"+jsonObject.optString("basePath"));
			yamlPortfolio.setId(jsonObject.optJSONObject("info").optString("title")+"-"+jsonObject.optJSONObject("info").optString("version")+"-"+jsonObject.optString("basePath"));
			if(jsonObject.optJSONObject("info").optJSONObject("contact") != null) {
				yamlPortfolio.setDeveloper(developersNamesMap.get(jsonObject.optJSONObject("info").optJSONObject("contact").optString("name")));
			}
			if(yamlPortfolio.getDeveloper() == null || yamlPortfolio.getDeveloper().equals("")) {
				yamlPortfolio.setDeveloper("Moises Ramos Hernandez");
			}
			yamlPortfolio.setBusinessCapability(businessCapability);
			if(!jsonObject.optJSONObject("info").optString("title").toLowerCase().contains("test")) {
				yamlPortfolioList.add(yamlPortfolio);
			}
		}
		return yamlPortfolioList;
	}
}
