package mx.com.cemex.middleware.servicegovernancemanagement.bean;

import java.util.Date;

public class ConfluenceComparissonFilesBean {
	
	private String environmentSource;
	private String environmentTarget;
	private Date registeredAt;
	
	public String getEnvironmentSource() {
		return environmentSource;
	}
	public void setEnvironmentSource(String environmentSource) {
		this.environmentSource = environmentSource;
	}
	public String getEnvironmentTarget() {
		return environmentTarget;
	}
	public void setEnvironmentTarget(String environmentTarget) {
		this.environmentTarget = environmentTarget;
	}
	public Date getRegisteredAt() {
		return registeredAt;
	}
	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}
	

}
