package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.EmailBean;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiHistoryCallDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.PlatformCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiGovernanceService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiHistoryCallService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureApiCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.CompareBluemixAzureService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.CompareFilesService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ConfluenceReportService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.MailManager;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ProductCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ReadYamlsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

@Service("apiGovernanceService")
public class ApiGovernanceServiceImpl implements ApiGovernanceService {
	
	private static final Logger LOG  = Logger.getLogger(ApiGovernanceServiceImpl.class);

	@Autowired
	ApiHistoryCallService apiHistoryCallService;

	@Autowired
	ReadYamlsService readYamlsService;

	@Autowired
	ProductCatalogService productCatalogService;

	@Autowired
	ApiHistoryCallDAO apiHistoryCallDAO;

	@Autowired
	ApiCatalogService apiCatalogService;

	@Autowired
	AzureApiCatalogService azureApiCatalogService;

	@Autowired
	ConfluenceReportService confluenceReportService;

	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;

	@Autowired
	PlatformCatalogDAO platformCatalogDAO;

	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	CompareFilesService compareFilesService;
	
	@Autowired
	CompareBluemixAzureService compareBluemixAzureService;
	
	@Autowired
	MailManager mailManager;

	public void runMaitainanceProcess() throws IOException, InterruptedException {
		List<EnvironmentCatalog> environmentIncludingAzureCatalogs = environmentCatalogDAO.findAllIncludingAzure();
		try {
			for (EnvironmentCatalog environmentCatalog : environmentIncludingAzureCatalogs) {
				try {
					if(environmentCatalog.getPlatformCatalog().getIdentifier().equals(IYamlBeansMasterConstants.AZURE_API_MANAGEMENT_IDENTIFIER)) {
						azureApiCatalogService.mantainAzureApiCatalog(environmentCatalog);
//						confluenceReportService.maintainEnvironmentAzureAPIInventory(environmentCatalog);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
//			compareFilesService.prepareAndCompareVersions();
		} catch (Exception e) {
			e.printStackTrace();
		}
		EmailBean emailBean = new EmailBean();
		emailBean.setEmails(new String[] {"rogelio.reyocachu@ext.cemex.com"});
		emailBean.setExecutedProcess("Maintain Governance Confluence Process");
		emailBean.setFrom("middlewareservices@cemex.com");
		emailBean.setPersona("Middleware Services Team");
		emailBean.setMessage("Maintain Governance Process Confluence Update finished.");
		emailBean.setSubject("Maintain Governance Process Confluence Update finished.");
//		mailManager.maintainGovernanceProcessFinished(emailBean);
	}
}
