package mx.com.cemex.middleware.servicegovernancemanagement.service;

import org.quartz.SchedulerException;

public interface QuartzService {

	void addQuartzJobDetails(String jobName, String jobGroup, String triggerName, String jobExpression) throws SchedulerException;
	void scheduleNewJob(String jobName, String jobGroup, String triggerName, String jobExpression) throws SchedulerException;
}
