package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareDetail;

public interface ApiCompareDetailDAO {

	ApiCompareDetail create(ApiCompareDetail apiCompareDetail);
	ApiCompareDetail merge(ApiCompareDetail apiCompareDetail);
    long countCompareDetail();
    List<ApiCompareDetail> findApiCompareDetail(int firstResult, int maxResults);
    List<ApiCompareDetail> findAll();
    ApiCompareDetail findApiCompareDetail(Long id);
    void deleteAll();
}
