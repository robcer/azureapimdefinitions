package mx.com.cemex.middleware.servicegovernancemanagement;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Base64;

public class TestAuthorizationToken {

	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, IOException {
		// TODO Auto-generated method stub

		final String serviceName = "cemexqas";
		final String url = "https://" + serviceName + ".management.azure-api.net";
		final String identifier = "58c830c1aef6d6007f030003";
		final byte[] primaryKey = "nV6ZeKh9W2U93IeY+RmwQE9Y4EM3HgFOtYfY4Wm2v5SzMiuO/o+aXcdQ2L/uMTo75JKUHR+gaVmWIDePS8hQmw==".getBytes();
//		final String serviceName = "USCLDCNXAPMD01";
//		final String url = "https://" + serviceName + ".management.azure-api.net";
//		final String identifier = "integration";
//		final byte[] primaryKey = "Z7P22z0R5qpyiUvF/YBQozlnTNnHHFhqCSFo9TKVOfmHx5gxL+m4rNH/MWkX8Go2p1eKB1rH/REll/kdXBWyPw==".getBytes();
		
//		final String serviceName = "USCLDCNXAPMPP01";
//		final String url = "https://" + serviceName + ".management.azure-api.net";
//		final String identifier = "integration";
//		final byte[] primaryKey = "PxNn7JoEpAFdMal7VM2mq5Xblmcp7s2ICLAQCn1VjeAlO63lAclaAiVkJQ1ewV21GBWbU4BBG4phn3+eFRcd8w==".getBytes();
//		final String serviceName = "USCLDCNXAPMSA01";
//		final String url = "https://" + serviceName + ".management.azure-api.net";
//		final String identifier = "integration";
//		final byte[] primaryKey = "Y2uZWvl+eVrKkuFLZ2rtU3mEMuUDhYTiMemMabTkQO3eDH3UH02yHV6aNIqflmZTc4UCsLm6n0Ra1D7/3b2EhQ==".getBytes();
//		final String serviceName = "cemex";
//		final String url = "https://" + serviceName + ".management.azure-api.net";
//		final String identifier = "58b5ee943555b70080030003";
//		final byte[] primaryKey = "vYtuYkIpJG5vt0ZszBspxEhzXhpKts+GJEYmszs9TOqeGKILDCIs1tTzoq31qZTHF4xnqvYjPckgmsw9WgIQwg==".getBytes();


		final String expiry = LocalDateTime.now().plusDays(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'0000Z'"));

		Mac hmacSha512 = Mac.getInstance("HmacSHA512");
		hmacSha512.init(new SecretKeySpec(primaryKey, "HmacSHA512"));
		String dataToSign = identifier + "\n" + expiry;
		String signature = new String(Base64.getEncoder().encode(hmacSha512.doFinal(dataToSign.getBytes())));
		String sas = "SharedAccessSignature uid="+identifier+"&ex=" + expiry + "&sn="+signature;
		System.out.println(sas);
	}

}
