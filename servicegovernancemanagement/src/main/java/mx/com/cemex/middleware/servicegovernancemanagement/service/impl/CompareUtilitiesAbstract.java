package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Diff;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Operation;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch.Patch;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCompareDetailDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareConfigurationDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareDetailDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.OperationCompareDetailDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.OperationCompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.TenantCompareDetailDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.TenantCompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ConfluenceReportService;

public abstract class CompareUtilitiesAbstract {
	
	private static final Logger LOG  = Logger.getLogger(CompareUtilitiesAbstract.class);
	
	@Autowired
	CompareDetailDAO compareDetailDAO;
	
	@Autowired
	CompareConfigurationDAO compareConfigurationDAO;
	
	@Autowired
	CompareSummaryDAO compareSummaryDAO;
	
	@Autowired
	ConfluenceReportService confluenceReportService;
	
	@Autowired
	AzureOperationsService azureOperationsService;
	
	@Autowired
	TenantCompareSummaryDAO tenantCompareSummaryDAO;
	
	@Autowired
	TenantCompareDetailDAO tenantCompareDetailDAO;
	
	@Autowired
	ApiCompareSummaryDAO apiCompareSummaryDAO;
	
	@Autowired
	ApiCompareDetailDAO apiCompareDetailDAO;
	
	@Autowired
	OperationCompareSummaryDAO operationCompareSummaryDAO;
	
	@Autowired
	OperationCompareDetailDAO operationCompareDetailDAO;

	@Autowired
	AzureApiCatalogDAO azureApiCatalogDAO;

	protected HashMap<String, List<Integer>> fileLineCount = new HashMap<String, List<Integer>>();
	
	protected String fileContentToString(String fileName) {
		StringBuffer stringBuffer = new StringBuffer();
		String line = "";
		try {
			BufferedReader in = new BufferedReader(new FileReader(fileName));
			int characterCount = 0;
			List<Integer> integers = new ArrayList<Integer>();
			while ((line = in.readLine()) != null) {
				characterCount+=line.length()+2;
				integers.add(characterCount);
				stringBuffer.append(line).append(System.getProperty("line.separator"));
			}
			fileLineCount.put(fileName, integers);
			in.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return stringBuffer.toString();
	}
	
	protected void createTenantCompareDetail(CompareConfiguration compareConfiguration, TenantCompareSummary tenantCompareSummary, UUID guid) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		DiffMatchPatch diffMatchPatch = new DiffMatchPatch();
		tenantCompareSummary.setTargetPolicyXml(azureOperationsService.getTenantPolicy(compareConfiguration.getEnvironmentTarget()).replace("&#xD;", ""));
		tenantCompareSummary.setSourcePolicyXml(azureOperationsService.getTenantPolicy(compareConfiguration.getEnvironmentSource()).replace("&#xD;", ""));
		LinkedList<Diff> diffs = diffMatchPatch.diffMain(tenantCompareSummary.getTargetPolicyXml(), tenantCompareSummary.getSourcePolicyXml(), true);
		diffMatchPatch.diffCleanupEfficiency(diffs);
		tenantCompareSummaryDAO.create(tenantCompareSummary);
		LinkedList<Patch> patchs = diffMatchPatch.patchMake(diffs);
		for (Patch patch : patchs) {
			TenantCompareDetail tenantCompareDetail = getTenantCompareDetail(compareConfiguration, tenantCompareSummary, guid, patch);
			if(tenantCompareDetail.getPlainTextDiffDelete() != null || tenantCompareDetail.getPlainTextDiffInsert() != null) {
				tenantCompareDetailDAO.create(tenantCompareDetail);
			}
		}
	}
	
	protected void createApiOperationCompareDetail(CompareConfiguration compareConfiguration, OperationCompareSummary operationCompareSummary, UUID guid, AzureApiOperationCatalog azureApiOperationCatalog) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		DiffMatchPatch diffMatchPatch = new DiffMatchPatch();
		operationCompareSummary.setTargetPolicyXml(azureOperationsService.getOperationPolicy(compareConfiguration.getEnvironmentTarget(), azureApiOperationCatalog.getId()).replace("&#xD;", ""));
		operationCompareSummary.setSourcePolicyXml(azureOperationsService.getOperationPolicy(compareConfiguration.getEnvironmentSource(), azureApiOperationCatalog.getId()).replace("&#xD;", ""));
		LinkedList<Diff> diffs = diffMatchPatch.diffMain(operationCompareSummary.getTargetPolicyXml(), operationCompareSummary.getSourcePolicyXml(), true);
		diffMatchPatch.diffCleanupEfficiency(diffs);
		operationCompareSummaryDAO.create(operationCompareSummary);
		LinkedList<Patch> patchs = diffMatchPatch.patchMake(diffs);
		for (Patch patch : patchs) {
			OperationCompareDetail operationCompareDetail = getOperationCompareDetail(compareConfiguration, operationCompareSummary, guid, patch);
			if(operationCompareDetail.getPlainTextDiffDelete() != null || operationCompareDetail.getPlainTextDiffInsert() != null) {
				operationCompareDetailDAO.create(operationCompareDetail);
			}
		}
	}
	
	protected void createApiCompareDetail(CompareConfiguration compareConfiguration, ApiCompareSummary apiCompareSummary, UUID guid, AzureApiCatalog azureApiCatalog) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		DiffMatchPatch diffMatchPatch = new DiffMatchPatch();
		apiCompareSummary.setTargetPolicyXml(azureOperationsService.getApiPolicy(compareConfiguration.getEnvironmentTarget(), azureApiCatalog.getId()).replace("&#xD;", ""));
		apiCompareSummary.setSourcePolicyXml(azureOperationsService.getApiPolicy(compareConfiguration.getEnvironmentSource(), azureApiCatalog.getId()).replace("&#xD;", ""));
		LinkedList<Diff> diffs = diffMatchPatch.diffMain(apiCompareSummary.getTargetPolicyXml(), apiCompareSummary.getSourcePolicyXml(), true);
		diffMatchPatch.diffCleanupEfficiency(diffs);
		apiCompareSummaryDAO.create(apiCompareSummary);
		LinkedList<Patch> patchs = diffMatchPatch.patchMake(diffs);
		for (Patch patch : patchs) {
			ApiCompareDetail apiCompareDetail = getApiCompareDetail(compareConfiguration, apiCompareSummary, guid, patch);
			if(apiCompareDetail.getPlainTextDiffDelete() != null || apiCompareDetail.getPlainTextDiffInsert() != null) {
				apiCompareDetailDAO.create(apiCompareDetail);
			}
		}
	}
	
	protected void createCompareDetail(CompareConfiguration compareConfiguration, CompareSummary compareSummary, UUID guid,
			String dirSourcePath, String dirTargetPath) {
		DiffMatchPatch diffMatchPatch = new DiffMatchPatch();
		LinkedList<Diff> diffs = diffMatchPatch.diffMain(fileContentToString(dirTargetPath + File.separator + compareSummary.getTargetFileName()), fileContentToString(dirSourcePath + File.separator + compareSummary.getSourceFileName()), true);
		diffMatchPatch.diffCleanupEfficiency(diffs);
		compareSummaryDAO.create(compareSummary);
		LinkedList<Patch> patchs = diffMatchPatch.patchMake(diffs);
		for (Patch patch : patchs) {
			CompareDetail compareDetail = getCompareDetail(compareConfiguration, compareSummary, guid, dirSourcePath, patch, dirTargetPath);
			if(compareDetail.getPlainTextDiffDelete() != null || compareDetail.getPlainTextDiffInsert() != null) {
				compareDetailDAO.create(compareDetail);
			}
		}
	}

	protected OperationCompareDetail getOperationCompareDetail(CompareConfiguration compareConfiguration, OperationCompareSummary operationCompareSummary, UUID guid, Patch patch) {
		OperationCompareDetail operationCompareDetail = new OperationCompareDetail();
		operationCompareDetail.setOperationCompareSummary(operationCompareSummary);
		operationCompareDetail.setReportGuid(guid.toString());
		operationCompareDetail.setRegisteredAt(new Date());
		for (Diff diff : patch.diffs) {
			if(diff.operation.equals(Operation.DELETE)) {
				if(!diff.text.trim().isEmpty()) {
					operationCompareDetail.setPlainTextDiffDelete(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			} else if(diff.operation.equals(Operation.INSERT)) {
				if(!diff.text.trim().isEmpty()) {
					operationCompareDetail.setPlainTextDiffInsert(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			}
		}
		return operationCompareDetail;
	}

	protected ApiCompareDetail getApiCompareDetail(CompareConfiguration compareConfiguration, ApiCompareSummary apiCompareSummary, UUID guid, Patch patch) {
		ApiCompareDetail apiCompareDetail = new ApiCompareDetail();
		apiCompareDetail.setApiCompareSummary(apiCompareSummary);
		apiCompareDetail.setReportGuid(guid.toString());
		apiCompareDetail.setRegisteredAt(new Date());
		for (Diff diff : patch.diffs) {
			if(diff.operation.equals(Operation.DELETE)) {
				if(!diff.text.trim().isEmpty()) {
					apiCompareDetail.setPlainTextDiffDelete(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			} else if(diff.operation.equals(Operation.INSERT)) {
				if(!diff.text.trim().isEmpty()) {
					apiCompareDetail.setPlainTextDiffInsert(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			}
		}
		return apiCompareDetail;
	}

	protected TenantCompareDetail getTenantCompareDetail(CompareConfiguration compareConfiguration, TenantCompareSummary tenantCompareSummary, UUID guid, Patch patch) {
		TenantCompareDetail tenantCompareDetail = new TenantCompareDetail();
		tenantCompareDetail.setTenantCompareSummary(tenantCompareSummary);
		tenantCompareDetail.setReportGuid(guid.toString());
		tenantCompareDetail.setRegisteredAt(new Date());
		for (Diff diff : patch.diffs) {
			if(diff.operation.equals(Operation.DELETE)) {
				if(!diff.text.trim().isEmpty()) {
					tenantCompareDetail.setPlainTextDiffDelete(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			} else if(diff.operation.equals(Operation.INSERT)) {
				if(!diff.text.trim().isEmpty()) {
					tenantCompareDetail.setPlainTextDiffInsert(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			}
		}
		return tenantCompareDetail;
	}

	protected CompareDetail getCompareDetail(CompareConfiguration compareConfiguration, CompareSummary compareSummary, UUID guid,
			String dirSourcePath, Patch patch, String dirTargetPath) {

		CompareDetail compareDetail = new CompareDetail();
		compareDetail.setEnvironmentSource(compareConfiguration.getEnvironmentSource());
		compareDetail.setEnvironmentTarget(compareConfiguration.getEnvironmentTarget());
		compareDetail.setCompareSummary(compareSummary);
		compareDetail.setReportGuid(guid.toString());
		compareDetail.setRegisteredAt(new Date());
		List<Integer> sourceCountList = fileLineCount.get(dirSourcePath + File.separator + compareSummary.getSourceFileName());
		for (int index = 0; index < sourceCountList.size() ; index++) {
			if(sourceCountList.get(index) > patch.start1) {
				compareDetail.setSourceStartingLine((new Long(index))+1);
				break;
			}
		}
		List<Integer> targetCountList = fileLineCount.get(dirTargetPath + File.separator + compareSummary.getTargetFileName());
		for (int index = 0; index < targetCountList.size() ; index++) {
			if(targetCountList.get(index) > patch.start2) {
				compareDetail.setTargetStartingLine(new Long(index));
				break;
			}
		}

		for (Diff diff : patch.diffs) {
			if(diff.operation.equals(Operation.DELETE)) {
				if(!diff.text.trim().isEmpty()) {
					compareDetail.setPlainTextDiffDelete(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			} else if(diff.operation.equals(Operation.INSERT)) {
				if(!diff.text.trim().isEmpty()) {
					compareDetail.setPlainTextDiffInsert(StringEscapeUtils.escapeHtml3(diff.text).replace("\n", "<br/>").replace(" ", "&nbsp;"));
				}
			}
		}
		return compareDetail;
	}

	protected boolean checkDifferentVersionsExists(String sourceFileName, List<String> fileNameList, CompareSummary compareSummary) {
		for (int i = 0; i < fileNameList.size(); i++) {
			if(sourceFileName.substring(0, sourceFileName.indexOf(".")).toLowerCase().equals(fileNameList.get(i).substring(0, fileNameList.get(i).indexOf(".")).toLowerCase())) {
				compareSummary.setTargetFileName(fileNameList.get(i));
				return true;
			}
		}
		return false;
	}
	
	protected boolean existInTargetSummary(String fileName, UUID guid, List<CompareSummary> compareSummaryList) {
		for(CompareSummary compareSummary: compareSummaryList) {
			if(compareSummary.getTargetFileName() != null && compareSummary.getTargetFileName().equals(fileName)) {
				return true;
			}
		}
		return false;
	}
	
	protected void compareAzureEnvironments(UUID guid, CompareConfiguration compareConfiguration) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, java.text.ParseException, ParseException, InterruptedException {
		tenantCompareSummaryDAO.deleteAll();
		if(compareConfiguration.isEnable()) {
			TenantCompareSummary tenantCompareSummary = new TenantCompareSummary();
			tenantCompareSummary.setEnvironmentSource(compareConfiguration.getEnvironmentSource());
			tenantCompareSummary.setEnvironmentTarget(compareConfiguration.getEnvironmentTarget());
			tenantCompareSummary.setRegisteredAt(new Date());
			tenantCompareSummary.setReportGuid(guid.toString());
			tenantCompareSummary.setSourceExist(true);
			tenantCompareSummary.setTargetExist(true);
			createTenantCompareDetail(compareConfiguration, tenantCompareSummary, guid);
			compareAzureApiCatalogBetweenTwoTenants(guid, compareConfiguration, tenantCompareSummary);
		}
	}
	
	private void compareAzureApiCatalogBetweenTwoTenants(UUID guid, CompareConfiguration compareConfiguration, TenantCompareSummary tenantCompareSummary) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		List<AzureApiCatalog> azureApiCatalogsSource = azureApiCatalogDAO.findAllByEnvironment(tenantCompareSummary.getEnvironmentSource());
		List<AzureApiCatalog> azureApiCatalogsTarget = azureApiCatalogDAO.findAllByEnvironment(tenantCompareSummary.getEnvironmentTarget());
		for(AzureApiCatalog apiCatalog: azureApiCatalogsSource) {
			ApiCompareSummary apiCompareSummary = new ApiCompareSummary();
			apiCompareSummary.setRegisteredAt(new Date());
			apiCompareSummary.setReportGuid(guid.toString());
			AzureApiCatalog apiCatalogTarget = isAzureApiOnAzureApiList(apiCatalog, azureApiCatalogsTarget);
			if(apiCatalogTarget != null) {
				apiCompareSummary.setAzureApiCatalogSource(apiCatalog);
				apiCompareSummary.setAzureApiCatalogTarget(apiCatalogTarget);
				apiCompareSummary.setSourceExist(true);
				apiCompareSummary.setTargetExist(true);
				apiCompareSummary.setTenantCompareSummary(tenantCompareSummary);
				createApiCompareDetail(compareConfiguration, apiCompareSummary, guid, apiCatalog);
			} else {
				apiCompareSummary.setAzureApiCatalogSource(apiCatalog);
				apiCompareSummary.setSourceExist(true);
				apiCompareSummary.setTenantCompareSummary(tenantCompareSummary);
				apiCompareSummaryDAO.create(apiCompareSummary);
			}
			compareAzureApiOperationCatalogInsideTwoAzureApis(apiCatalog, apiCatalogTarget, apiCompareSummary, compareConfiguration, guid);
		}
		for(AzureApiCatalog apiCatalog: azureApiCatalogsTarget) {
			AzureApiCatalog apiCatalogSource = isAzureApiOnAzureApiList(apiCatalog, azureApiCatalogsSource);
			if(apiCatalogSource == null) {
				ApiCompareSummary apiCompareSummary = new ApiCompareSummary();
				apiCompareSummary.setAzureApiCatalogTarget(apiCatalog);
				apiCompareSummary.setRegisteredAt(new Date());
				apiCompareSummary.setReportGuid(guid.toString());
				apiCompareSummary.setTargetExist(true);
				apiCompareSummary.setTenantCompareSummary(tenantCompareSummary);
				apiCompareSummaryDAO.create(apiCompareSummary);
				compareAzureApiOperationCatalogInsideTwoAzureApis(apiCatalogSource, apiCatalog, apiCompareSummary, compareConfiguration, guid);
			}
		}
	}
	
	private void compareAzureApiOperationCatalogInsideTwoAzureApis(AzureApiCatalog apiCatalog, AzureApiCatalog apiCatalogTarget, ApiCompareSummary apiCompareSummary,
			CompareConfiguration compareConfiguration, UUID guid) throws InvalidKeyException, MalformedURLException, UnsupportedEncodingException, NoSuchAlgorithmException, IOException, InterruptedException {
		List<AzureApiOperationCatalog> azureApiOperationCatalogsSource = new ArrayList<AzureApiOperationCatalog>();
		if(apiCatalog != null) {
			azureApiOperationCatalogsSource.addAll(apiCatalog.getApiOperationCatalogs());
		}
		List<AzureApiOperationCatalog> azureApiOperationCatalogsTarget = new ArrayList<AzureApiOperationCatalog>();
		if(apiCatalogTarget != null) {
			azureApiOperationCatalogsTarget.addAll(apiCatalogTarget.getApiOperationCatalogs());
		}
		for(AzureApiOperationCatalog azureApiOperationCatalog: azureApiOperationCatalogsSource) {
//			if(azureApiOperationCatalog.getIdAzureApiOperationCatalog() == 6482L) {
				OperationCompareSummary operationCompareSummary = new OperationCompareSummary();
				operationCompareSummary.setRegisteredAt(new Date());
				operationCompareSummary.setReportGuid(guid.toString());
				AzureApiOperationCatalog azureApiOperationCatalogTarget = isAzureOperationOnAzureOperationList(azureApiOperationCatalog, azureApiOperationCatalogsTarget);
				if(azureApiOperationCatalogTarget != null) {
					operationCompareSummary.setAzureApiOperationCatalogSource(azureApiOperationCatalog);
					operationCompareSummary.setAzureApiOperationCatalogTarget(azureApiOperationCatalogTarget);
					operationCompareSummary.setSourceExist(!azureApiOperationCatalog.isRemoved());
					operationCompareSummary.setTargetExist(!azureApiOperationCatalogTarget.isRemoved());
					operationCompareSummary.setApiCompareSummary(apiCompareSummary);
					createApiOperationCompareDetail(compareConfiguration, operationCompareSummary, guid, azureApiOperationCatalog);
				} else {
					operationCompareSummary.setAzureApiOperationCatalogSource(azureApiOperationCatalog);
					operationCompareSummary.setSourceExist(!azureApiOperationCatalog.isRemoved());
					operationCompareSummary.setApiCompareSummary(apiCompareSummary);
					operationCompareSummaryDAO.create(operationCompareSummary);
				}
//			}
		}
		for(AzureApiOperationCatalog azureApiOperationCatalog: azureApiOperationCatalogsTarget) {
//			if(azureApiOperationCatalog.getIdAzureApiOperationCatalog() == 6482L) {
				AzureApiOperationCatalog azureApiOperationCatalogSource = isAzureOperationOnAzureOperationList(azureApiOperationCatalog, azureApiOperationCatalogsSource);
				if(azureApiOperationCatalogSource == null) {
					OperationCompareSummary operationCompareSummary = new OperationCompareSummary();
					operationCompareSummary.setAzureApiOperationCatalogTarget(azureApiOperationCatalog);
					operationCompareSummary.setRegisteredAt(new Date());
					operationCompareSummary.setReportGuid(guid.toString());
					operationCompareSummary.setTargetExist(!azureApiOperationCatalog.isRemoved());
					operationCompareSummary.setApiCompareSummary(apiCompareSummary);
					operationCompareSummaryDAO.create(operationCompareSummary);
				}
//			}
		}
	}
	
	private AzureApiOperationCatalog isAzureOperationOnAzureOperationList(AzureApiOperationCatalog azureApiOperationCatalog, List<AzureApiOperationCatalog> azureApiOperationCatalogs) {
		for(AzureApiOperationCatalog azureApiOperationCatalogIn: azureApiOperationCatalogs) {
			if(azureApiOperationCatalogIn.getId().equals(azureApiOperationCatalog.getId())) {
				return azureApiOperationCatalogIn;
			}
		}
		return null;
	}
	
	private AzureApiCatalog isAzureApiOnAzureApiList(AzureApiCatalog azureApiCatalog, List<AzureApiCatalog> azureApiCatalogs) {
		for(AzureApiCatalog azureApiCatalogIn: azureApiCatalogs) {
			if(azureApiCatalogIn.getPath().equals(azureApiCatalog.getPath())) {
				return azureApiCatalogIn;
			}
		}
		return null;
	}
	
	protected void compareFolderEnvironments(UUID guid, CompareConfiguration compareConfiguration, String dirSourcePath, String dirTargetPath) throws MalformedURLException, IOException, ParseException, java.text.ParseException {
		if(compareConfiguration.isEnable()) {
			File dirSource = new File(dirSourcePath);
			File dirTarget = new File(dirTargetPath);
			if(dirSource.exists() && dirTarget.exists()) {
				File[] listOfTargetFiles = dirTarget.listFiles();
				File[] listOfSourceFiles = dirSource.listFiles();
				List<String> fileNameTargetList = new ArrayList<String>();
				List<String> fileNameSourceList = new ArrayList<String>();
				for (int i = 0; i < listOfTargetFiles.length; i++) {
					if (listOfTargetFiles[i].isFile()) {
						fileNameTargetList.add(listOfTargetFiles[i].getName().toLowerCase());
					}
				}
				for (int i = 0; i < listOfSourceFiles.length; i++) {
					if (listOfSourceFiles[i].isFile()) {
						fileNameSourceList.add(listOfSourceFiles[i].getName().toLowerCase());
						CompareSummary compareSummary = new CompareSummary();
						compareSummary.setEnvironmentSource(compareConfiguration.getEnvironmentSource());
						compareSummary.setEnvironmentTarget(compareConfiguration.getEnvironmentTarget());
						compareSummary.setFileName(listOfSourceFiles[i].getName());
						compareSummary.setSourceFileName(listOfSourceFiles[i].getName());
						compareSummary.setRegisteredAt(new Date());
						compareSummary.setReportGuid(guid.toString());
						if(fileNameTargetList.contains(listOfSourceFiles[i].getName().toLowerCase())) {
							compareSummary.setSourceExist(true);
							compareSummary.setTargetExist(true);
							compareSummary.setTargetFileName(fileNameTargetList.get(ArrayUtils.indexOf(fileNameTargetList.toArray(), listOfSourceFiles[i].getName().toLowerCase())));
							createCompareDetail(compareConfiguration, compareSummary, guid, dirSourcePath, dirTargetPath);
						} else if(checkDifferentVersionsExists(listOfSourceFiles[i].getName(), fileNameTargetList, compareSummary)) {
							compareSummary.setSourceExist(true);
							compareSummary.setTargetExist(true);
							createCompareDetail(compareConfiguration, compareSummary, guid, dirSourcePath, dirTargetPath);
						} else {
							compareSummary.setSourceExist(true);
							compareSummaryDAO.create(compareSummary);
						}
					}
				}
				
				List<CompareSummary> compareSummaryList =  compareSummaryDAO.findAllByReportGUID(guid.toString());
				for (int i = 0; i < listOfTargetFiles.length; i++) {
					if (listOfTargetFiles[i].isFile()) {
						CompareSummary compareSummary = new CompareSummary();
						compareSummary.setEnvironmentSource(compareConfiguration.getEnvironmentSource());
						compareSummary.setEnvironmentTarget(compareConfiguration.getEnvironmentTarget());
						compareSummary.setFileName(listOfTargetFiles[i].getName());
						compareSummary.setTargetFileName(listOfTargetFiles[i].getName());
						compareSummary.setRegisteredAt(new Date());
						compareSummary.setReportGuid(guid.toString());
						if(ArrayUtils.indexOf(fileNameSourceList.toArray(), listOfTargetFiles[i].getName().toLowerCase()) < 0 && !existInTargetSummary(listOfTargetFiles[i].getName().toLowerCase(), guid, compareSummaryList)) {
							compareSummary.setTargetExist(true);
							compareSummaryDAO.create(compareSummary);
						}
					}
				}
			} else {
				//LOG.error(dirSourcePath + " exists?: " + dirSource.exists());
				//LOG.error(dirTargetPath + " exists?: " + dirTarget.exists());
			}
		}
	}
}
