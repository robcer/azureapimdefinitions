package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "apioperation")
public class ApiOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_api_operation")
	private Long idApiOperation;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_api", nullable = false, updatable = true, insertable = true)
    private Api api;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_business_capability", nullable = true, updatable = true, insertable = true)
    private BusinessCapability businessCapability;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apiOperation")
    @JsonIgnore
    private Set<RelProjectOperation> relProjectOperation = new HashSet<RelProjectOperation>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "apiOperation")
    private Set<RelEnvironmentOperation> relEnvironmentOperation = new HashSet<RelEnvironmentOperation>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "apiOperation")
    @JsonIgnore
    private Set<RelUserOperation> relUserOperation = new HashSet<RelUserOperation>();
    
    @Column(name = "id")
    private String id;

    @Column(name = "name", length=1000)
    private String name;
    
    @Column(name = "method")
    private String method;
    
    @Column(name = "base_path")
    private String basePath;
    
    @Column(name = "full_path")
    private String fullPath;
    
    @Column(name = "url_template")
    private String urlTemplate;
    
    @Column(name = "description", length=3000)
    private String description;
    
    @Column(name = "deprecated")
    @NotNull
    private boolean deprecated;
    
    @Column(name = "critical")
    @NotNull
    private boolean critical;

    @Column(name = "store_procedure")
    private String storeProcedure;

    @Column(name = "azure_class_method")
    private String azureClassMethod;

    @Column(name = "azure_path")
    private String azurePath;

    @Column(name = "azure_full_path")
    private String azureFullPath;

    @Column(name = "azure_method")
    private String azureMethod;

	public String getAzureFullPath() {
		return azureFullPath;
	}

	public void setAzureFullPath(String azureFullPath) {
		this.azureFullPath = azureFullPath;
	}

	public String getStoreProcedure() {
		return storeProcedure;
	}

	public void setStoreProcedure(String storeProcedure) {
		this.storeProcedure = storeProcedure;
	}

	public String getAzureClassMethod() {
		return azureClassMethod;
	}

	public void setAzureClassMethod(String azureClassMethod) {
		this.azureClassMethod = azureClassMethod;
	}

	public String getAzurePath() {
		return azurePath;
	}

	public void setAzurePath(String azurePath) {
		this.azurePath = azurePath;
	}

	public String getAzureMethod() {
		return azureMethod;
	}

	public void setAzureMethod(String azureMethod) {
		this.azureMethod = azureMethod;
	}

	public boolean isCritical() {
		return critical;
	}

	public void setCritical(boolean critical) {
		this.critical = critical;
	}

	public boolean isDeprecated() {
		return deprecated;
	}

	public void setDeprecated(boolean deprecated) {
		this.deprecated = deprecated;
	}

	public Set<RelEnvironmentOperation> getRelEnvironmentOperation() {
		return relEnvironmentOperation;
	}

	public void setRelEnvironmentOperation(Set<RelEnvironmentOperation> relEnvironmentOperation) {
		this.relEnvironmentOperation = relEnvironmentOperation;
	}

	public Set<RelProjectOperation> getRelProjectOperation() {
		return relProjectOperation;
	}

	public void setRelProjectOperation(Set<RelProjectOperation> relProjectOperation) {
		this.relProjectOperation = relProjectOperation;
	}

	public Set<RelUserOperation> getRelUserOperation() {
		return relUserOperation;
	}

	public void setRelUserOperation(Set<RelUserOperation> relUserOperation) {
		this.relUserOperation = relUserOperation;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public Long getIdApiOperation() {
		return idApiOperation;
	}

	public void setIdApiOperation(Long idApiOperation) {
		this.idApiOperation = idApiOperation;
	}

	public Api getApi() {
		return api;
	}

	public void setApi(Api api) {
		this.api = api;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getUrlTemplate() {
		return urlTemplate;
	}

	public void setUrlTemplate(String urlTemplate) {
		this.urlTemplate = urlTemplate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public BusinessCapability getBusinessCapability() {
		return businessCapability;
	}

	public void setBusinessCapability(BusinessCapability businessCapability) {
		this.businessCapability = businessCapability;
	}   
    
}
