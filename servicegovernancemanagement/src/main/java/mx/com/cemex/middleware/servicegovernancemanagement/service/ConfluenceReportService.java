package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.BluemixAzureCompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface ConfluenceReportService {
	
	void maintainEnvironmentProductInventory(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException;
	void maintainEnvironmentAPIInventory(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException;
	void createNewReportAnalytic() throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException;
	void createHistoricalReporteBySpace(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException;
	void createDashboardReport(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException;
	void publishCompareResultConfluence(UUID guid, CompareConfiguration compareConfiguration) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException;
	void maintainEnvironmentAzureAPIInventory(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException;
	void publishBluemixAzureCompare(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException;
	void publishAzureCompareResultConfluence(UUID guid, CompareConfiguration compareConfiguration) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException;
}
