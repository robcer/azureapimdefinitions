package mx.com.cemex.middleware.servicegovernancemanagement.service;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface EnvironmentCatalogService extends CrudService<EnvironmentCatalog, Long> {

}
