package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;


public interface EnvironmentCatalogRepository extends JpaRepository<EnvironmentCatalog, Long> {

}
