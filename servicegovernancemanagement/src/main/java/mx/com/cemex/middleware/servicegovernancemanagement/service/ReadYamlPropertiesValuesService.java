package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;

public interface ReadYamlPropertiesValuesService {
	
	String readPropertiesValues() throws FileNotFoundException, IllegalAccessException, InvocationTargetException;

}
