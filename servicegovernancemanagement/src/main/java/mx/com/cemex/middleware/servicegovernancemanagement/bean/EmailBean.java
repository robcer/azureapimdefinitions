package mx.com.cemex.middleware.servicegovernancemanagement.bean;

public class EmailBean {

	private String[] emails;
	private String[] ccEmails;
	private String executedProcess;
	private String subject;
	private String from;
	private String message;
	private String persona;
	
	public EmailBean() {
	}
	
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String[] getEmails() {
		return emails;
	}
	public void setEmails(String[] emails) {
		this.emails = emails;
	}
	public String[] getCcEmails() {
		return ccEmails;
	}
	public void setCcEmails(String[] ccEmails) {
		this.ccEmails = ccEmails;
	}
	public String getExecutedProcess() {
		return executedProcess;
	}
	public void setExecutedProcess(String executedProcess) {
		this.executedProcess = executedProcess;
	}
	public String getSubject() {
		return subject;
	}
	public void setSubject(String subject) {
		this.subject = subject;
	}
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getPersona() {
		return persona;
	}
	public void setPersona(String persona) {
		this.persona = persona;
	}
}
