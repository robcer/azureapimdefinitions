package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;

public interface ApiCatalogDAO {

	ApiCatalog create(ApiCatalog apiCatalog);
	ApiCatalog merge(ApiCatalog apiCatalog);
    long countApiCatalog();
    List<ApiCatalog> findApiCatalog(int firstResult, int maxResults);
    List<ApiCatalog> findAll();
    ApiCatalog findApiCatalog(Long id);
    ApiCatalog findApiCatalogById(String id);
    List<ApiCatalog> findAllByDevOrgId(String devOrgId);
    List<ApiCatalog> findApiCatalogByDeployedId(String deployedId);
    ApiCatalog findApiCatalogByBasePathAndNameAndEnvIdAndDevOrgId(String basePath, String name, String envId, String devOrgId, String deploymentState);
}
