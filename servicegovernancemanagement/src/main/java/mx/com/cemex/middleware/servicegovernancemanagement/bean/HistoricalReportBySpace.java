package mx.com.cemex.middleware.servicegovernancemanagement.bean;

import java.sql.Timestamp;

public class HistoricalReportBySpace {

	private String apiName;
	private String path;
	private String azureURL;
	private String requestMethod;
	private String devOrgName;
	private String productName;
	private String requestCount;
	private String totalTimeToServeRequest;
	private String averageTimeToServeRequest;
	private String totalBytesSent;
	private String averageBytesSent;
	private String averageButesSentByCall;
	private String apiAncestorId;
	private String operationAncestorId;
	private String serviceEfficiency;
	private String averageServiceEfficiency;
	private Timestamp latestDateTime;
	private String latestEpochDate;
	public String getApiName() {
		return apiName;
	}
	public void setApiName(String apiName) {
		this.apiName = apiName;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getRequestMethod() {
		return requestMethod;
	}
	public void setRequestMethod(String requestMethod) {
		this.requestMethod = requestMethod;
	}
	public String getDevOrgName() {
		return devOrgName;
	}
	public void setDevOrgName(String devOrgName) {
		this.devOrgName = devOrgName;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public String getRequestCount() {
		return requestCount;
	}
	public void setRequestCount(String requestCount) {
		this.requestCount = requestCount;
	}
	public String getTotalTimeToServeRequest() {
		return totalTimeToServeRequest;
	}
	public void setTotalTimeToServeRequest(String totalTimeToServeRequest) {
		this.totalTimeToServeRequest = totalTimeToServeRequest;
	}
	public String getAverageTimeToServeRequest() {
		return averageTimeToServeRequest;
	}
	public void setAverageTimeToServeRequest(String averageTimeToServeRequest) {
		this.averageTimeToServeRequest = averageTimeToServeRequest;
	}
	public String getTotalBytesSent() {
		return totalBytesSent;
	}
	public void setTotalBytesSent(String totalBytesSent) {
		this.totalBytesSent = totalBytesSent;
	}
	public String getAverageBytesSent() {
		return averageBytesSent;
	}
	public void setAverageBytesSent(String averageBytesSent) {
		this.averageBytesSent = averageBytesSent;
	}
	public String getAverageButesSentByCall() {
		return averageButesSentByCall;
	}
	public void setAverageButesSentByCall(String averageButesSentByCall) {
		this.averageButesSentByCall = averageButesSentByCall;
	}
	public String getApiAncestorId() {
		return apiAncestorId;
	}
	public void setApiAncestorId(String apiAncestorId) {
		this.apiAncestorId = apiAncestorId;
	}
	public String getOperationAncestorId() {
		return operationAncestorId;
	}
	public void setOperationAncestorId(String operationAncestorId) {
		this.operationAncestorId = operationAncestorId;
	}
	public String getServiceEfficiency() {
		return serviceEfficiency;
	}
	public void setServiceEfficiency(String serviceEfficiency) {
		this.serviceEfficiency = serviceEfficiency;
	}
	public String getAverageServiceEfficiency() {
		return averageServiceEfficiency;
	}
	public void setAverageServiceEfficiency(String averageServiceEfficiency) {
		this.averageServiceEfficiency = averageServiceEfficiency;
	}
	public String getAzureURL() {
		return azureURL;
	}
	public void setAzureURL(String azureURL) {
		this.azureURL = azureURL;
	}
	public Timestamp getLatestDateTime() {
		return latestDateTime;
	}
	public void setLatestDateTime(Timestamp latestDateTime) {
		this.latestDateTime = latestDateTime;
	}
	public String getLatestEpochDate() {
		return latestEpochDate;
	}
	public void setLatestEpochDate(String latestEpochDate) {
		this.latestEpochDate = latestEpochDate;
	}

	
}
