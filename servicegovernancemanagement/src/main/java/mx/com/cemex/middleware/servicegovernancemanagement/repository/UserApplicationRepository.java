package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.UserApplication;

public interface UserApplicationRepository extends JpaRepository<UserApplication, Long> {

	List<UserApplication> findByUser(User user);

}
