package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface AzureApiOperationCatalogDAO {

	AzureApiOperationCatalog create(AzureApiOperationCatalog azureApiOperationCatalog);
	AzureApiOperationCatalog merge(AzureApiOperationCatalog azureApiOperationCatalog);
    long countAzureApiOperationCatalog();
    List<AzureApiOperationCatalog> findAzureApiOperationCatalog(int firstResult, int maxResults);
    List<AzureApiOperationCatalog> findAll();
    AzureApiOperationCatalog findAzureApiOperationCatalog(Long id);
    AzureApiOperationCatalog findAzureApiOperationCatalogByIdAndEnvironmentCatalog(String id, EnvironmentCatalog environmentCatalog);
    List<AzureApiOperationCatalog> findAllByEnvironment(EnvironmentCatalog environmentCatalog);
    void disableAzureApiCatalogByEnvironment(EnvironmentCatalog environmentCatalog);
}
