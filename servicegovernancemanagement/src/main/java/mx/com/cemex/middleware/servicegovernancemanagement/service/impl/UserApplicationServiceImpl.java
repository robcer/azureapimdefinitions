package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.UserApplication;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.UserApplicationRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UserApplicationService;

@Named("userApplicationService")
public class UserApplicationServiceImpl extends CrudServiceImpl<UserApplication, Long, UserApplicationRepository> implements UserApplicationService {

	@Override
	@Inject
	public void setRepository(UserApplicationRepository userApplicationRepository) {
		super.setRepository(userApplicationRepository);
	}
	
	public List<UserApplication> findByUser(User user) {
		return this.repository.findByUser(user);
	}
}
