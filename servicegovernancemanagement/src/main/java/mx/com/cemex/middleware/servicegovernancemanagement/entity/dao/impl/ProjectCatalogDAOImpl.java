package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProjectCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProjectCatalogDAO;

@Service("projectCatalogDAO")
public class ProjectCatalogDAOImpl implements ProjectCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ProjectCatalogDAOImpl()
    {
    }

    ProjectCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ProjectCatalog create(ProjectCatalog projectCatalog) {
        em.persist(projectCatalog);
        return projectCatalog;
    }

    @Transactional
    public ProjectCatalog merge(ProjectCatalog projectCatalog) {
        em.merge(projectCatalog);
        return projectCatalog;
    }

    public long countProjectCatalog() {
        return (Long) em.createQuery("select count(o) from ProjectCatalog o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<ProjectCatalog> findProjectCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from ProjectCatalog o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ProjectCatalog findProjectCatalog(Long id) {
        if (id == null) return null;
        return em.find(ProjectCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ProjectCatalog> findAll() {
        return em.createQuery("select o from ProjectCatalog o").getResultList();
    }
    
    public ProjectCatalog findProjectCatalogByName(String name) {
        if (name == null) return null;
        return em.createQuery("select o from ProjectCatalog o where o.name = :name", ProjectCatalog.class)
        		.setParameter("name", name).getSingleResult();
    }
    
    public ProjectCatalog findProjectCatalogById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from ProjectCatalog o where o.id = :id", ProjectCatalog.class)
        		.setParameter("id", id).getSingleResult();
    }
}
