package mx.com.cemex.middleware.servicegovernancemanagement.bean;

import java.util.ArrayList;
import java.util.List;

public class LoginApplicationResponse {

	private Long applicationId;

	private String applicationName;
	
	private String applicationCode;

	private String applicationDesc;

	private List<LoginRoleResponse> roles = new ArrayList<LoginRoleResponse>();

	public Long getApplicationId() {
		return applicationId;
	}

	public void setApplicationId(Long applicationId) {
		this.applicationId = applicationId;
	}

	public String getApplicationName() {
		return applicationName;
	}

	public void setApplicationName(String applicationName) {
		this.applicationName = applicationName;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}

	public String getApplicationDesc() {
		return applicationDesc;
	}

	public void setApplicationDesc(String applicationDesc) {
		this.applicationDesc = applicationDesc;
	}

	public List<LoginRoleResponse> getRoles() {
		return roles;
	}

	public void setRoles(List<LoginRoleResponse> roles) {
		this.roles = roles;
	}

	
}