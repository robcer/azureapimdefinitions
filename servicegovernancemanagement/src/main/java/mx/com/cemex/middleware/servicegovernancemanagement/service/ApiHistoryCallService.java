package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;

import org.apache.commons.cli.CommandLine;
import org.json.simple.parser.ParseException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface ApiHistoryCallService {

	void callAndStoreNextApiHistoryCall(EnvironmentCatalog environmentCatalog) throws IOException, ParseException, java.text.ParseException;

	void callAndStoreNextApiHistoryCall(EnvironmentCatalog environmentCatalog, CommandLine commandLine) throws IOException, ParseException, java.text.ParseException;
	
	
}
