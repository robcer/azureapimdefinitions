package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.resthub.web.controller.ServiceBasedRestController;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiOperationService;

@Controller
@RequestMapping(value = "/v1/its/operations", produces = "application/json")
public class ApiOperationController extends ServiceBasedRestController<ApiOperation, Long, ApiOperationService> {
	
	private static final Logger LOG  = Logger.getLogger(ApiOperationController.class);

    @Inject
    @Named("apiOperationService")
    @Override
    public void setService(ApiOperationService service) {
        this.service = service;
    }

	@RequestMapping(value = "/my", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public Page<ApiOperation> myOperations(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
            @RequestParam(value = "size", required = false, defaultValue = "10") Integer size) {
		return this.service.myOperations(new PageRequest(page - 1, size));
	}
}