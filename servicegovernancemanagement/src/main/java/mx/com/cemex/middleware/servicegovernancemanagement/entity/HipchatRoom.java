package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "hipchatroom")
public class HipchatRoom {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_hipchat_room")
	private Long idHipchatRoom;

    @Column(name = "id")
	private Long id;
	
    @Column(name = "is_archived")
	private boolean archived;
    
    @Column(name = "name")
	private String name;
    
    @Column(name = "privacity")
	private String privacy;
    
    @Column(name = "version")
	private String version;
    
    @Column(name = "is_notification_enabled")
	private boolean notificationEnabled;
	
	public Long getIdHipchatRoom() {
		return idHipchatRoom;
	}
	public void setIdHipchatRoom(Long idHipchatRoom) {
		this.idHipchatRoom = idHipchatRoom;
	}
	public boolean isNotificationEnabled() {
		return notificationEnabled;
	}
	public void setNotificationEnabled(boolean notificationEnabled) {
		this.notificationEnabled = notificationEnabled;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public boolean isArchived() {
		return archived;
	}
	public void setArchived(boolean archived) {
		this.archived = archived;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getPrivacy() {
		return privacy;
	}
	public void setPrivacy(String privacy) {
		this.privacy = privacy;
	}
	public String getVersion() {
		return version;
	}
	public void setVersion(String version) {
		this.version = version;
	}
	
}
