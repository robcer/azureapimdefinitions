package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.context.SecurityContextHolder;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.ApiOperationRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiOperationService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.RelUserOperationService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UserService;

@Named("apiOperationService")
public class ApiOperationServiceImpl extends CrudServiceImpl<ApiOperation, Long, ApiOperationRepository> implements ApiOperationService {
	
	@Autowired
	RelUserOperationService relUserOperationService;
	
	@Autowired
	UserService userService;
    
    @Override
    @Inject
    public void setRepository(ApiOperationRepository apiOperationRepository) {
        super.setRepository(apiOperationRepository);
    }
    
    @Cacheable(value = "ehcache")
	public Page<ApiOperation> myOperations(Pageable pageable) {
		return this.repository.findAllByRelUserOperationUser(userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()), pageable);
	}

}