package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.text.ParseException;

public interface HipchatOperationsService {

	void sendNotificationToHipchatRooms(String color, String messageFormat, String message) throws MalformedURLException, IOException, ParseException;
}
