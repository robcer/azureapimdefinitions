package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "comparesummary")
public class CompareSummary implements Serializable {
	
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_compare_summary")
	private Long idCompareSummary;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "report_guid")
    private String reportGuid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_source", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentSource;
    
    @Column(name = "is_source_exist")
    private boolean isSourceExist;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_target", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentTarget;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "compareSummary")
    @OrderBy("sourceStartingLine ASC")
    private Set<CompareDetail> compareDetails = new HashSet<CompareDetail>();
    
    @Column(name = "is_target_exist")
    private boolean isTargetExist;
    
    @Column(name = "file_name")
    private String fileName;
    
    @Column(name = "target_file_name")
    private String targetFileName;
    
    @Column(name = "source_file_name")
    private String sourceFileName;

    private boolean isSafePatch;

	public Long getIdCompareSummary() {
		return idCompareSummary;
	}

	public void setIdCompareSummary(Long idCompareSummary) {
		this.idCompareSummary = idCompareSummary;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getReportGuid() {
		return reportGuid;
	}

	public void setReportGuid(String reportGuid) {
		this.reportGuid = reportGuid;
	}

	public EnvironmentCatalog getEnvironmentSource() {
		return environmentSource;
	}

	public void setEnvironmentSource(EnvironmentCatalog environmentSource) {
		this.environmentSource = environmentSource;
	}

	public boolean isSourceExist() {
		return isSourceExist;
	}

	public void setSourceExist(boolean isSourceExist) {
		this.isSourceExist = isSourceExist;
	}

	public EnvironmentCatalog getEnvironmentTarget() {
		return environmentTarget;
	}

	public void setEnvironmentTarget(EnvironmentCatalog environmentTarget) {
		this.environmentTarget = environmentTarget;
	}

	//Velocity needs those 3 methods in order to access the boolean attribute
	public boolean getIsTargetExist() {
		return isTargetExist;
	}

	public boolean getIsSourceExist() {
		return isSourceExist;
	}

	public boolean getIsSafePatch() {
		return isSafePatch;
	}
	//End Velocity needs

	public boolean isTargetExist() {
		return isTargetExist;
	}

	public void setTargetExist(boolean isTargetExist) {
		this.isTargetExist = isTargetExist;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public Set<CompareDetail> getCompareDetails() {
		return compareDetails;
	}

	public void setCompareDetails(Set<CompareDetail> compareDetails) {
		this.compareDetails = compareDetails;
	}

	public String getTargetFileName() {
		return targetFileName;
	}

	public void setTargetFileName(String targetFileName) {
		this.targetFileName = targetFileName;
	}

	public String getSourceFileName() {
		return sourceFileName;
	}

	public void setSourceFileName(String sourceFileName) {
		this.sourceFileName = sourceFileName;
	}

	public boolean isSafePatch() {
		return isSafePatch;
	}

	public void setSafePatch(boolean isSafePatch) {
		this.isSafePatch = isSafePatch;
	}

}
