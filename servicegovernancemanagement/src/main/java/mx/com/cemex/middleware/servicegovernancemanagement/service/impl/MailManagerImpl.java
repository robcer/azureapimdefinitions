package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.EmailBean;
import mx.com.cemex.middleware.servicegovernancemanagement.service.MailManager;

import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.mail.javamail.MimeMessagePreparator;
import org.springframework.stereotype.Component;
import org.springframework.ui.velocity.VelocityEngineUtils;

import javax.mail.internet.MimeMessage;
import java.util.HashMap;
import java.util.Map;

@Component("mailManager")
public class MailManagerImpl implements MailManager {
	
	private static final Logger LOG  = Logger.getLogger(MailManagerImpl.class);

	@Autowired
	private JavaMailSender mailSender;

	@Autowired
	private VelocityEngine velocityEngine;

    @SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public void maintainGovernanceProcessFinished(final EmailBean emailBean) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) {
	        	try {
		            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		            message.setTo(emailBean.getEmails());
		            message.setCc(emailBean.getCcEmails());
		            message.setFrom(emailBean.getFrom(), emailBean.getPersona());
		            message.setSubject(emailBean.getSubject());
					Map model = new HashMap();
		            model.put("emailBean", emailBean);
					String text = VelocityEngineUtils.mergeTemplateIntoString(
		               velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/maintainprocessfinished.vm", model);
		            message.setText(text, true);
	        	} catch (Exception e) {
	        		e.printStackTrace();
	        		LOG.error(e);
	        	}
	         }
	      };
	      this.mailSender.send(preparator);
	}

    @SuppressWarnings({ "unchecked", "deprecation", "rawtypes" })
	public void publishingNotification(final EmailBean emailBean) {
		MimeMessagePreparator preparator = new MimeMessagePreparator() {
			public void prepare(MimeMessage mimeMessage) {
	        	try {
		            MimeMessageHelper message = new MimeMessageHelper(mimeMessage);
		            message.setTo(emailBean.getEmails());
		            message.setCc(emailBean.getCcEmails());
		            message.setFrom(emailBean.getFrom(), emailBean.getPersona());
		            message.setSubject(emailBean.getSubject());
					Map model = new HashMap();
		            model.put("emailBean", emailBean);
					String text = VelocityEngineUtils.mergeTemplateIntoString(
		               velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/publishingnotification.vm", model);
		            message.setText(text, true);
	        	} catch (Exception e) {
	        		LOG.error(e);
	        	}
	         }
	      };
	      this.mailSender.send(preparator);
	}

}
