package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import static org.quartz.CronScheduleBuilder.cronSchedule;
import static org.quartz.JobBuilder.newJob;
import static org.quartz.TriggerBuilder.newTrigger;

import java.util.Date;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.quartz.CronTrigger;
import org.quartz.JobDetail;
import org.quartz.SchedulerException;
import org.quartz.SchedulerFactory;
import org.quartz.impl.StdSchedulerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.QuartzJobDetails;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.QuartzJobDetailsDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.quartz.jobs.QuartzReviewStatusMonitorJob;
import mx.com.cemex.middleware.servicegovernancemanagement.service.QuartzService;

@Service("quartzService")
public class QuartzServiceImpl implements QuartzService {

	private static final Logger LOG  = Logger.getLogger(QuartzServiceImpl.class);
	
	@Autowired
	QuartzJobDetailsDAO quartzJobDetailsDAO;
	
	SchedulerFactory schedulerFactory = new StdSchedulerFactory();
	
	@Override
	public void addQuartzJobDetails(String jobName, String jobGroup, String triggerName, String jobExpression) throws SchedulerException {
		QuartzJobDetails quartzJobDetails = new QuartzJobDetails();
		quartzJobDetails.setJobGroup(jobGroup);
		quartzJobDetails.setJobName(jobName);
		quartzJobDetails.setTriggerName(triggerName);
		quartzJobDetails.setJobExpression(jobExpression);
		quartzJobDetails.setSchedName(schedulerFactory.getScheduler().getSchedulerName());
		quartzJobDetails.setConfigurationDate(new Date());
		quartzJobDetailsDAO.create(quartzJobDetails);
	}
	
	public void scheduleNewJob(String jobName, String jobGroup, String triggerName, String jobExpression) throws SchedulerException {
		JobDetail job = newJob(QuartzReviewStatusMonitorJob.class).withIdentity(jobName, jobGroup).build();
		CronTrigger trigger = newTrigger().withIdentity(triggerName, jobGroup).withSchedule(cronSchedule(jobExpression)).build();
		Date date = schedulerFactory.getScheduler().scheduleJob(job, trigger);
		LOG.info(job.getKey() + " Ha sido programado a correr en: " + date + " y repetirse debido a la expresion: " + trigger.getCronExpression());
	}
	
	@PostConstruct
	public void startQuartzServer() throws SchedulerException {
		scheduleNewJob("jobMonitor", "globalJobGroup", "reviewStatusMonitor", "0/10 * * * * ?");
		schedulerFactory.getScheduler().start();
		LOG.info("Is scheduler " + schedulerFactory.getScheduler().getSchedulerName() + " started?: " + schedulerFactory.getScheduler().isStarted());
	}
	
}
