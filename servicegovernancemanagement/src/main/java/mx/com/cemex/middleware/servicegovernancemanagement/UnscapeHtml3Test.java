package mx.com.cemex.middleware.servicegovernancemanagement;

import org.apache.commons.lang3.StringEscapeUtils;

class UnscapeHtml3Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(StringEscapeUtils.unescapeHtml3("<set-variable name=\"source\" value=\"@{&#13;&#10;                    try{&#13;&#10;                        var requestBody = context.Request.Body.As&lt;JObject&gt;(preserveContent: true);&#13;&#10;                        return requestBody[&quot;shipment&quot;][&quot;optimalSource&quot;][&quot;address&quot;][&quot;addressId&quot;].ToString();&#13;&#10;                    }&#13;&#10;                    catch{&#13;&#10;                        return null;&#13;&#10;                    }&#13;&#10;                }\"/>"));
	}

}
