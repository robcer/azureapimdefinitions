package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.util.Collection;
import java.util.List;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.Application;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;

public interface ApplicationService extends CrudService<Application, Long> {
	
	List<Application> findByRolesIn(Collection<ApplicationRole> roles);
}
