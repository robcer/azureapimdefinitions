package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.sql.Timestamp;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiHistoryCall;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiHistoryCallDAO;

@Service("apiHistoryCallDAO")
public class ApiHistoryCallDAOImpl implements ApiHistoryCallDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ApiHistoryCallDAOImpl()
    {
    }

    ApiHistoryCallDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ApiHistoryCall create(ApiHistoryCall apiHistoryCall) {
        em.persist(apiHistoryCall);
        return apiHistoryCall;
    }

    public long countApiHistoryCall() {
        return (Long) em.createQuery("select count(o) from ApiHistoryCall o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiHistoryCall> findApiHistoryCall(int firstResult, int maxResults) {
        return em.createQuery("select o from ApiHistoryCall o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ApiHistoryCall findApiHistoryCall(Long id) {
        if (id == null) return null;
        return em.find(ApiHistoryCall.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiHistoryCall> findAll() {
        return em.createQuery("select o from ApiHistoryCall o").getResultList();
    }

    public Timestamp getLastDateTime(String organization, String catalog) {
        return (Timestamp) em.createQuery("select max(o.datetime) from ApiHistoryCall o where o.devOrgName = :organization and o.catalogName = :catalog")
        		.setParameter("organization", organization)
        		.setParameter("catalog", catalog).getSingleResult();
    }

    public Timestamp getFirstDateTime(String organization, String catalog) {
        return (Timestamp) em.createQuery("select min(o.datetime) from ApiHistoryCall o where o.devOrgName = :organization and o.catalogName = :catalog")
        		.setParameter("organization", organization)
        		.setParameter("catalog", catalog).getSingleResult();
    }
    public Timestamp getFirstDateTime(String organization, String catalog, String devOrgId) {
        return (Timestamp) em.createQuery("select min(o.datetime) from ApiHistoryCall o where o.devOrgName = :organization and o.catalogName = :catalog AND o.devOrgId = :devOrgId")
        		.setParameter("organization", organization)
        		.setParameter("catalog", catalog)
        		.setParameter("devOrgId", devOrgId).getSingleResult();
    }
    /*public Long countByPathResourceAndMethod(String pathresourcemethod) {
        return (Long) em.createQuery("select count(o) from ApiHistoryCall o where CONCAT(SUBSTRING_INDEX(o.uriPath, SUBSTRING_INDEX(o.resourceId, '/', 2), 1), o.resourceId, '|' , o.requestMethod) like :pathresourcemethod ")
        		.setParameter("pathresourcemethod", "%" + pathresourcemethod + "%").getSingleResult();
    }*/
    

    public List<Object[]> analyticsByApiIdAndResourceId(String apiId, String resourceId) {
    	List<Object[]> results = em.createQuery("select to_char(o.datetime, 'mm-dd-yyyy'), count(o), max(o.timeToServeRequest), min(o.timeToServeRequest), avg(o.timeToServeRequest) from ApiHistoryCall o where o.apiId = :apiId and o.resourceId = :resourceId group by to_char(o.datetime, 'mm-dd-yyyy') order by 1 asc", Object[].class)
        		.setParameter("apiId", apiId)
        		.setParameter("resourceId", resourceId).getResultList();
    	return results;
    }

    public List<Object[]> analyticsByApiIdAndResourceIdNative(String apiId, String resourceId) {
    	@SuppressWarnings("unchecked")
		List<Object[]> results = em.createNativeQuery("SELECT TO_CHAR(DATETIME, 'MM-DD-YYYY'), COUNT(*), MEDIAN(TIME_TO_SERVE_REQUEST), MAX(TIME_TO_SERVE_REQUEST), MIN(TIME_TO_SERVE_REQUEST), AVG(TIME_TO_SERVE_REQUEST) FROM APIHISTORYCALL WHERE API_ID = ?1 AND RESOURCE_ID = ?2  GROUP BY TO_CHAR(DATETIME, 'MM-DD-YYYY') ORDER BY 1 ASC")
        		.setParameter(1, apiId)
        		.setParameter(2, resourceId).getResultList();
    	return results;
    }

    public List<Object[]> analyticsByPathAndOrgIdAndMethodNative(String path, String orgId, String requestMethod) {
    	@SuppressWarnings("unchecked")
    	List<Object[]> results = em.createNativeQuery("SELECT TO_CHAR(DATETIME, 'MM-DD-YYYY'), COUNT(*), MEDIAN(TIME_TO_SERVE_REQUEST), MAX(TIME_TO_SERVE_REQUEST), MIN(TIME_TO_SERVE_REQUEST), AVG(TIME_TO_SERVE_REQUEST), SUM(BYTES_SENT) FROM APIHISTORYCALL WHERE PATH = ?1 AND ORG_ID = ?2 AND REQUEST_METHOD = ?3  GROUP BY TO_CHAR(DATETIME, 'MM-DD-YYYY') HAVING COUNT(*) > 5 ORDER BY 1 ASC")
        		.setParameter(1, path)
        		.setParameter(2, orgId)
        		.setParameter(3, requestMethod).getResultList();
    	return results;
    }
    
    public List<Object[]> historicalReportByOrgNameAndCatalog(String devOrgName, String catalogName) {
    	@SuppressWarnings("unchecked")
    	List<Object[]> results = em.createNativeQuery("SELECT API_NAME, PATH, REQUEST_METHOD, DEV_ORG_NAME, PRODUCT_NAME, COUNT(*), SUM(TIME_TO_SERVE_REQUEST), AVG(TIME_TO_SERVE_REQUEST), SUM(BYTES_SENT), AVG(BYTES_SENT), DEV_ORG_ID, MAX(DATETIME)  FROM APIHISTORYCALL WHERE PRODUCT_NAME IS NOT NULL AND DEV_ORG_NAME = ?1 AND CATALOG_NAME = ?2 GROUP BY API_NAME, PATH, REQUEST_METHOD, DEV_ORG_NAME, PRODUCT_NAME, DEV_ORG_ID ORDER BY 1, 3 ASC")
        		.setParameter(1, devOrgName)
        		.setParameter(2, catalogName).getResultList();
    	return results;
    }
    
    public List<Object[]> historicalReportByOrgNameAndCatalogAndDevOrgId(String devOrgName, String catalogName, String devOrgId) {
    	@SuppressWarnings("unchecked")
    	List<Object[]> results = em.createNativeQuery("SELECT API_NAME, PATH, REQUEST_METHOD, DEV_ORG_NAME, PRODUCT_NAME, COUNT(*), SUM(TIME_TO_SERVE_REQUEST), AVG(TIME_TO_SERVE_REQUEST), SUM(BYTES_SENT), AVG(BYTES_SENT), DEV_ORG_ID, MAX(DATETIME)  FROM APIHISTORYCALL WHERE PRODUCT_NAME IS NOT NULL AND DEV_ORG_NAME = ?1 AND CATALOG_NAME = ?2 AND DEV_ORG_ID = ?3 GROUP BY API_NAME, PATH, REQUEST_METHOD, DEV_ORG_NAME, PRODUCT_NAME, DEV_ORG_ID ORDER BY 1, 3 ASC")
        		.setParameter(1, devOrgName)
        		.setParameter(2, catalogName)
        		.setParameter(3, devOrgId).getResultList();
    	return results;
    }
    
	public List<Object[]> analyticsByUriPathLikeOrgIdAndRequestMethodNative(String uriPath, String orgId, String requestMethod) {
    	@SuppressWarnings("unchecked")
		List<Object[]> results = em.createNativeQuery("SELECT TO_CHAR(DATETIME, 'MM-DD-YYYY'), COUNT(*), MEDIAN(TIME_TO_SERVE_REQUEST), MAX(TIME_TO_SERVE_REQUEST), MIN(TIME_TO_SERVE_REQUEST), AVG(TIME_TO_SERVE_REQUEST) FROM APIHISTORYCALL WHERE URI_PATH like ?1 AND ORG_ID = ?2 AND REQUEST_METHOD = ?3 GROUP BY TO_CHAR(DATETIME, 'MM-DD-YYYY') ORDER BY 1 ASC")
	    		.setParameter(1, "%" + uriPath + "%")
	    		.setParameter(2, orgId)
	    		.setParameter(3, requestMethod).getResultList();
		return results;
	}
    
    public Long countByPathResourceAndMethod(String pathresourcemethod) {
        return (Long) em.createQuery("select count(o) from ApiHistoryCall o where SUBSTR(o.uriPath, 0, INSTR(o.uriPath, SUBSTR(o.resourceId, 0, INSTR(o.resourceId,'/', 2)-1), 1)-1) || o.resourceId || '|' || o.requestMethod like :pathresourcemethod ")
        		.setParameter("pathresourcemethod", "%" + pathresourcemethod + "%").getSingleResult();
    }
    
    public String getSchema () {
        return (String)em.createNativeQuery("select sys_context( 'userenv', 'current_schema' ) from dual").getSingleResult();
    	
    }
}
