package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

public interface GenericEntityDAO<T> {

	T create(T entity);
	T merge(T entity);
    long countEntity();
    List<T> findEntity(int firstResult, int maxResults);
    List<T> findAll(String entityIdentifier);
    void deleteAll();
    void setEntityClass(Class<T> classToSet);
}
