package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.PlatformCatalogDAO;

@Service("environmentCatalogDAO")
public class EnvironmentCatalogDAOImpl implements EnvironmentCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;
    
    @Autowired
    PlatformCatalogDAO platformCatalogDAO;

    public EnvironmentCatalogDAOImpl()
    {
    }

    EnvironmentCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public EnvironmentCatalog create(EnvironmentCatalog environmentCatalog) {
        em.persist(environmentCatalog);
        return environmentCatalog;
    }

    @Transactional
    public EnvironmentCatalog merge(EnvironmentCatalog environmentCatalog) {
        em.merge(environmentCatalog);
        return environmentCatalog;
    }

    public long countEnvironmentCatalog() {
        return (Long) em.createQuery("select count(o) from EnvironmentCatalog o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<EnvironmentCatalog> findEnvironmentCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from EnvironmentCatalog o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public EnvironmentCatalog findEnvironmentCatalog(Long id) {
        if (id == null) return null;
        return em.find(EnvironmentCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<EnvironmentCatalog> findAll() {
    	
        return em.createQuery("select o from EnvironmentCatalog o where enable = 1 and o.platformCatalog = :platformCatalog order by o.idEnvironmentCatalog DESC")
        		.setParameter("platformCatalog", platformCatalogDAO.findPlatformCatalog(1L)).getResultList();
    }
    
    @SuppressWarnings("unchecked")
    public List<EnvironmentCatalog> findAllIncludingAzure() {
        return em.createQuery("select o from EnvironmentCatalog o where enable = 1 order by o.idEnvironmentCatalog DESC").getResultList();
    }
    
    public List<EnvironmentCatalog> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from EnvironmentCatalog o where o.devOrgNameEnv = :devOrgNameEnv", EnvironmentCatalog.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public EnvironmentCatalog findEnvironmentCatalogById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from EnvironmentCatalog o where o.id = :id", EnvironmentCatalog.class)
        		.setParameter("id", id).getSingleResult();
    }

}