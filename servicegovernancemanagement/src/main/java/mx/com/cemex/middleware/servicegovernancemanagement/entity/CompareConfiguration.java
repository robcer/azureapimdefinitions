package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "compareconfiguration")
public class CompareConfiguration {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_compare_configuration")
	private Long idCompareConfiguration;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_source", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentSource;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_target", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentTarget;
    
    @Column(name = "is_enable")
    private boolean isEnable;
    
    @Column(name = "confluence_page_id")
    private Long confluencePageId;
    
    @Column(name = "private_confluence_page_id")
    private Long privateConfluencePageId;
    
    @Column(name = "local_source_path")
    private String localSourcePath;
    
    @Column(name = "local_target_path")
    private String localTargetPath;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_platform_catalog", nullable = true, updatable = true, insertable = true)
    private PlatformCatalog platformCatalog;

	public Long getPrivateConfluencePageId() {
		return privateConfluencePageId;
	}

	public void setPrivateConfluencePageId(Long privateConfluencePageId) {
		this.privateConfluencePageId = privateConfluencePageId;
	}

	public Long getIdCompareConfiguration() {
		return idCompareConfiguration;
	}

	public void setIdCompareConfiguration(Long idCompareConfiguration) {
		this.idCompareConfiguration = idCompareConfiguration;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public EnvironmentCatalog getEnvironmentSource() {
		return environmentSource;
	}

	public void setEnvironmentSource(EnvironmentCatalog environmentSource) {
		this.environmentSource = environmentSource;
	}

	public EnvironmentCatalog getEnvironmentTarget() {
		return environmentTarget;
	}

	public void setEnvironmentTarget(EnvironmentCatalog environmentTarget) {
		this.environmentTarget = environmentTarget;
	}

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	public Long getConfluencePageId() {
		return confluencePageId;
	}

	public void setConfluencePageId(Long confluencePageId) {
		this.confluencePageId = confluencePageId;
	}

	public String getLocalSourcePath() {
		return localSourcePath;
	}

	public void setLocalSourcePath(String localSourcePath) {
		this.localSourcePath = localSourcePath;
	}

	public String getLocalTargetPath() {
		return localTargetPath;
	}

	public void setLocalTargetPath(String localTargetPath) {
		this.localTargetPath = localTargetPath;
	}

	public PlatformCatalog getPlatformCatalog() {
		return platformCatalog;
	}

	public void setPlatformCatalog(PlatformCatalog platformCatalog) {
		this.platformCatalog = platformCatalog;
	}
    
    

}
