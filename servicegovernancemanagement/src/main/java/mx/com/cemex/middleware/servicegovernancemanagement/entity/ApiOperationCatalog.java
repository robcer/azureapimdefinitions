package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "apioperationcatalog")
public class ApiOperationCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_api_operation_catalog")
	private Long idApiOperationCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_api_catalog", nullable = false, updatable = true, insertable = true)
    private ApiCatalog apiCatalog;
    
    @Column(name = "api_id")
    private String apiId;
    
    @Column(name = "method")
    private String method;
    
    @Column(name = "base_path")
    private String basePath;
    
    @Column(name = "is_integrated_with_net")
    private boolean isIntegratedWithNet;
    
    @Column(name = "is_case_enabled")
    private boolean isCaseEnabled;
    
    @Column(name = "description", length=3000)
    private String description;
    
    @Column(name = "invoke_target_url")
    private String invokeTargetURL;
    
    @Column(name = "operation_id")
    private String operationId;
    
    @Column(name = "path")
    private String path;
    
    @Column(name = "security")
    private String security;
    
    @Column(name = "env_id")
    private String envId;
    
    @Column(name = "dev_org_id")
    private String devOrgId;
    
    @Column(name = "dev_org_name")
    private String devOrgName;

    @Column(name = "removed_at")
    @DateTimeFormat(style = "S-")
    private Date removedAt;
    
    @Column(name = "observation")
    private String observation;
    
    @Column(name = "case_operations")
    private String caseOperations;
    
    @Column(name = "invoke_title")
    private String invokeTitle;
    
    @Column(name = "confluence_page_id")
    private Long confluencePageId;
    
    @Column(name = "performance_attachment_id")
    private String performanceAttachmentId;

	public Long getIdApiOperationCatalog() {
		return idApiOperationCatalog;
	}

	public void setIdApiOperationCatalog(Long idApiOperationCatalog) {
		this.idApiOperationCatalog = idApiOperationCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public ApiCatalog getApiCatalog() {
		return apiCatalog;
	}

	public void setApiCatalog(ApiCatalog apiCatalog) {
		this.apiCatalog = apiCatalog;
	}

	public String getApiId() {
		return apiId;
	}

	public void setApiId(String apiId) {
		this.apiId = apiId;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public boolean isIntegratedWithNet() {
		return isIntegratedWithNet;
	}

	public void setIntegratedWithNet(boolean isIntegratedWithNet) {
		this.isIntegratedWithNet = isIntegratedWithNet;
	}

	public boolean isCaseEnabled() {
		return isCaseEnabled;
	}

	public void setCaseEnabled(boolean isCaseEnabled) {
		this.isCaseEnabled = isCaseEnabled;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getInvokeTargetURL() {
		return invokeTargetURL;
	}

	public void setInvokeTargetURL(String invokeTargetURL) {
		this.invokeTargetURL = invokeTargetURL;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public String getEnvId() {
		return envId;
	}

	public void setEnvId(String envId) {
		this.envId = envId;
	}

	public String getDevOrgId() {
		return devOrgId;
	}

	public void setDevOrgId(String devOrgId) {
		this.devOrgId = devOrgId;
	}

	public String getDevOrgName() {
		return devOrgName;
	}

	public void setDevOrgName(String devOrgName) {
		this.devOrgName = devOrgName;
	}

	public Date getRemovedAt() {
		return removedAt;
	}

	public void setRemovedAt(Date removedAt) {
		this.removedAt = removedAt;
	}

	public String getObservation() {
		return observation;
	}

	public void setObservation(String observation) {
		this.observation = observation;
	}

	public String getCaseOperations() {
		return caseOperations;
	}

	public void setCaseOperations(String caseOperations) {
		this.caseOperations = caseOperations;
	}

	public String getInvokeTitle() {
		return invokeTitle;
	}

	public void setInvokeTitle(String invokeTitle) {
		this.invokeTitle = invokeTitle;
	}

	public Long getConfluencePageId() {
		return confluencePageId;
	}

	public void setConfluencePageId(Long confluencePageId) {
		this.confluencePageId = confluencePageId;
	}

	public String getPerformanceAttachmentId() {
		return performanceAttachmentId;
	}

	public void setPerformanceAttachmentId(String performanceAttachmentId) {
		this.performanceAttachmentId = performanceAttachmentId;
	}
    
    
}
