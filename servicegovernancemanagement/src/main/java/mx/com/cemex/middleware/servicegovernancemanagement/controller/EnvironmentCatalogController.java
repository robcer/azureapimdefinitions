package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.log4j.Logger;
import org.resthub.web.controller.ServiceBasedRestController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.service.EnvironmentCatalogService;

@Controller
@RequestMapping(value = "/v1/its/environments", produces = "application/json")
public class EnvironmentCatalogController extends ServiceBasedRestController<EnvironmentCatalog, Long, EnvironmentCatalogService> {
	
	private static final Logger LOG  = Logger.getLogger(EnvironmentCatalogController.class);

    @Inject
    @Named("environmentCatalogService")
    @Override
    public void setService(EnvironmentCatalogService service) {
        this.service = service;
    }
    
}