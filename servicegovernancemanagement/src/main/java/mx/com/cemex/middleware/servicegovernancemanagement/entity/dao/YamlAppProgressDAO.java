package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlAppProgress;

public interface YamlAppProgressDAO {

	YamlAppProgress create(YamlAppProgress yamlAppProgress);
    long countYamlAppProgress();
    List<YamlAppProgress> findYamlAppProgress(int firstResult, int maxResults);
    List<YamlAppProgress> findAll();
    YamlAppProgress findYamlAppProgress(Long id);
    YamlAppProgress findYamlAppProgresByServiceName(String serviceName, String fullPath);
    YamlAppProgress merge(YamlAppProgress yamlAppProgress);

}
