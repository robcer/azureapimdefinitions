package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.File;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentVariables;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentVariablesDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.CompareFilesService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.HipchatOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.fasterxml.jackson.dataformat.yaml.YAMLGenerator;

@Service("compareFilesService")
public class CompareFilesServiceImpl extends CompareUtilitiesAbstract implements CompareFilesService {
	
	private static final Logger LOG  = Logger.getLogger(CompareFilesServiceImpl.class);
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
	@Autowired
	EnvironmentVariablesDAO environmentVariablesDAO;
	
	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	HipchatOperationsService hipchatOperationsService;
	
	private YAMLFactory yamlFactoryRebuild = new YAMLFactory().disable(YAMLGenerator.Feature.SPLIT_LINES).disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER);
	
	private YAMLFactory yamlFactoryCompare = new YAMLFactory().enable(YAMLGenerator.Feature.MINIMIZE_QUOTES).disable(YAMLGenerator.Feature.SPLIT_LINES).disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER);

	public void prepareAndCompareVersions(String guid) throws IOException, ParseException, java.text.ParseException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException {
		LOG.info("Inside the method: prepareAndCompareVersions");
		List<CompareConfiguration> compareConfigurations = compareConfigurationDAO.findAll();
		for(CompareConfiguration compareConfiguration: compareConfigurations) {
			UUID processGuid = UUID.fromString(guid);
			if(compareConfiguration.getPlatformCatalog().getIdentifier().equals(IYamlBeansMasterConstants.AZURE_API_MANAGEMENT_IDENTIFIER)) {
				compareAzureEnvironments(processGuid, compareConfiguration);
				confluenceReportService.publishAzureCompareResultConfluence(processGuid, compareConfiguration);
			}
		}
	}
	
	public void prepareAndCompareVersions() throws IOException, ParseException, java.text.ParseException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException {
		LOG.info("Inside the method: prepareAndCompareVersions");
		List<CompareConfiguration> compareConfigurations = compareConfigurationDAO.findAll();
		System.out.println("TAMAÑO!!: " + compareConfigurations.size());
		for(CompareConfiguration compareConfiguration: compareConfigurations) {
			UUID guid = UUID.randomUUID();
			if(compareConfiguration.getPlatformCatalog().getIdentifier().equals(IYamlBeansMasterConstants.BLUEMIX_API_MANAGEMENT_IDENTIFIER)) {
			} else if(compareConfiguration.getPlatformCatalog().getIdentifier().equals(IYamlBeansMasterConstants.AZURE_API_MANAGEMENT_IDENTIFIER)) {
				compareAzureEnvironments(guid, compareConfiguration);
				confluenceReportService.publishAzureCompareResultConfluence(guid, compareConfiguration);
				hipchatOperationsService.sendNotificationToHipchatRooms("green", "text", "Publishing on Confluence for Comparison between Azure APIM Environment finished for: " + compareConfiguration.getEnvironmentSource().getName() + " and " + compareConfiguration.getEnvironmentTarget().getName() + " this can be reviewed in: https://cemexpmo.atlassian.net/wiki/spaces/SDTSD/pages/" + compareConfiguration.getConfluencePageId());
			}
		}
	}
	
	public void prepareBluemixYamlFilesForComparison() throws IOException {
		List<EnvironmentCatalog> environmentCatalogs = environmentCatalogDAO.findAll();
		for(EnvironmentCatalog environmentCatalog: environmentCatalogs) {
			File dirSource = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
			File dirDestination = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + IYamlBeansMasterConstants.COMPARE_YAML_RESOURCE_FOLDER + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
			FileUtils.cleanDirectory(dirDestination);
			FileUtils.copyDirectory(dirSource, dirDestination);
			prepareYamlStructureWithOutProperties(environmentCatalog, yamlFactoryRebuild);
			try {
				File dirRebuidDestination = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + IYamlBeansMasterConstants.REBUILD_YAML_RESOURCE_FOLDER + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
				FileUtils.cleanDirectory(dirRebuidDestination);
			} catch (Exception e) {
				
			}
			rebuildYamlFilesWithVariables(environmentCatalog);
			FileUtils.cleanDirectory(dirDestination);
			FileUtils.copyDirectory(dirSource, dirDestination);
			prepareYamlStructureWithOutProperties(environmentCatalog, yamlFactoryCompare);
			removeProductBeforeCompare(environmentCatalog);
		}
	}

	private void persistEnvironmentVariables(String version, String api, String responsible, String email, 
			String environment, String region, String catalog, String type, String path, 
			String value, String fileName, String pathToEnvironment, String id, EnvironmentCatalog environmentCatalog) {
		EnvironmentVariables environmentVariables = new EnvironmentVariables();
		environmentVariables.setApi(api);
		environmentVariables.setCatalog(catalog);
		environmentVariables.setEmail(email);
		environmentVariables.setEnvironment(environment);
		environmentVariables.setEnvironmentCatalog(environmentCatalog);
		environmentVariables.setFileName(fileName);
		environmentVariables.setId(id);
		environmentVariables.setPath(path);
		environmentVariables.setPathToEnvironment(pathToEnvironment);
		environmentVariables.setRegion(region);
		environmentVariables.setRegisteredAt(new Date());
		environmentVariables.setResponsible(responsible);
		environmentVariables.setType(type);
		environmentVariables.setValue(value);
		environmentVariables.setVersion(version);
		environmentVariablesDAO.create(environmentVariables);
	}
	

	protected void rebuildYamlFilesWithVariables(EnvironmentCatalog environmentCatalog) {
		File dir = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + IYamlBeansMasterConstants.COMPARE_YAML_RESOURCE_FOLDER + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File yamlFile : directoryListing) {
				ObjectMapper mapper = new ObjectMapper(new YAMLFactory().disable(YAMLGenerator.Feature.SPLIT_LINES).disable(YAMLGenerator.Feature.WRITE_DOC_START_MARKER)).setSerializationInclusion(Include.NON_NULL);
				ObjectNode root;
				try {
					if(yamlFile.isFile()) {
						root = (ObjectNode) mapper.readTree(yamlFile);
						List<EnvironmentVariables> environmentVariablesList = environmentVariablesDAO.findEnvironmentVariablesByIdAndFileName(environmentCatalog.getId(), yamlFile.getName());
						for(EnvironmentVariables environmentVariables: environmentVariablesList) {
							JsonNode node = root;
							String[] paths = environmentVariables.getPath().split("\\|");
							for (int i = 0; i < paths.length; i++) {
								if(node.findPath(paths[i]).isMissingNode()) {
									if(node instanceof ObjectNode) {
										if(i < (paths.length-1)) {
											ObjectNode jNode = mapper.createObjectNode();
											((ObjectNode)node).set(paths[i], jNode);
										} else {
											if(environmentVariables.getValue() != null) {
												if("true".equals(environmentVariables.getValue().toLowerCase()) || "false".equals(environmentVariables.getValue().toLowerCase())) {
													((ObjectNode)node).put(paths[i], Boolean.parseBoolean(environmentVariables.getValue().toLowerCase()));
												} else {
													((ObjectNode)node).put(paths[i], environmentVariables.getValue());
												}
											}
										}
									}
								}
								node = node.findPath(paths[i]);
							}
						}
//						if(!yamlFile.getName().contains("_product_")) {
//							root.remove("swagger");
//							root.put("swagger", "2.0");
//						}
						//List<JsonNode> typesParents = root.findParents("type");
						File file = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + IYamlBeansMasterConstants.REBUILD_YAML_RESOURCE_FOLDER + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase() + File.separator + yamlFile.getName());
						file.getParentFile().mkdirs();
						mapper.writer().writeValue(file, root);
					}
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	private void prepareYamlStructureWithOutProperties(EnvironmentCatalog environmentCatalog, YAMLFactory yamlFactory) {
		File dir = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + IYamlBeansMasterConstants.COMPARE_YAML_RESOURCE_FOLDER + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File yamlFile : directoryListing) {
				ObjectMapper mapper = new ObjectMapper(yamlFactory).setSerializationInclusion(Include.NON_NULL);
				ObjectNode root;
				try {
					if(yamlFile.isFile() && !yamlFile.getName().contains("_product_")) {
						root = (ObjectNode) mapper.readTree(yamlFile);
						ObjectNode configuration = (ObjectNode)root.get("x-ibm-configuration");
						ObjectNode info = (ObjectNode)root.get("info");
						String apiName = "", version = "", contactName = "", contactEmail = "";
						if(info != null) {
							apiName = info.get("title").asText();
							version = info.get("version").asText();
							if(info.get("contact") != null) {
								if(info.get("contact").get("name") != null) {
									contactName = info.get("contact").get("name").asText();
								}
								if(info.get("contact").get("email") != null) {
									contactEmail = info.get("contact").get("email").asText();
								}
							}
						}
						if(configuration != null) { //x-ibm-configuration.properties.property.propertyName
							if(configuration instanceof ObjectNode) {
								ObjectNode objectNode = (ObjectNode) configuration.get("properties");
								if(objectNode != null) {
									Iterator<String> iterator = objectNode.fieldNames();
									while(iterator.hasNext()) {
										String property = iterator.next();
										ObjectNode objectNode2 = (ObjectNode) objectNode.get(property);
										if(objectNode2 != null) {
											Iterator<String> iterator2 = objectNode2.fieldNames();
											while(iterator2.hasNext()) {
												String propertyName = iterator2.next();
												if(objectNode2.get(propertyName) != null) {
													persistEnvironmentVariables(version, apiName, contactName, contactEmail, environmentCatalog.getName(), environmentCatalog.getRegionHostAcronym(), environmentCatalog.getCatalogEnv(), "properties", "x-ibm-configuration|properties|" + property + "|" + propertyName, objectNode2.get(propertyName).asText(), yamlFile.getName(), environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase(), environmentCatalog.getId(), environmentCatalog);
												}
											}
										}
									}
								}
							}
							if(configuration instanceof ObjectNode) { // + "x-ibm-configuration.catalogs.property.propertyName.subPropertyName"
								ObjectNode objectNode = (ObjectNode) configuration.get("catalogs");
								if(objectNode != null) {
									Iterator<String> iterator = objectNode.fieldNames();
									while(iterator.hasNext()) {
										String property = iterator.next();
										ObjectNode objectNode2 = (ObjectNode) objectNode.get(property);
										if(objectNode2 != null) {
											Iterator<String> iterator2 = objectNode2.fieldNames();
											while(iterator2.hasNext()) {
												String propertyName = iterator2.next();
												if(objectNode2.get(propertyName) != null) {
													ObjectNode objectNode3 = (ObjectNode) objectNode2.get(propertyName);
													if(objectNode3 != null) {
														Iterator<String> iterator3 = objectNode3.fieldNames();
														while(iterator3.hasNext()) {
															String subPropertyName = iterator3.next();
															if(objectNode3.get(subPropertyName) != null) {
																persistEnvironmentVariables(version, apiName, contactName, contactEmail, environmentCatalog.getName(), environmentCatalog.getRegionHostAcronym(), environmentCatalog.getCatalogEnv(), "catalogs", "x-ibm-configuration|catalogs|" + property + "|" + propertyName + "|" + subPropertyName, objectNode3.get(subPropertyName).asText(), yamlFile.getName(), environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase(), environmentCatalog.getId(), environmentCatalog);
															}
														}
													}
												}
											}
										}
									}
								}
							}
							if(configuration instanceof ObjectNode) { // + "x-ibm-configuration.catalogs.property.propertyName.subPropertyName"
								ObjectNode objectNode = (ObjectNode) configuration.get("oauth2");
								if(objectNode != null) {
									Iterator<String> iterator = objectNode.fieldNames();
									while(iterator.hasNext()) {
										String property = iterator.next();
										if(property.equals("access-token")) {
											ObjectNode objectNode2 = (ObjectNode) objectNode.get(property);
											if(objectNode2 != null) {
												Iterator<String> iterator2 = objectNode2.fieldNames();
												while(iterator2.hasNext()) {
													String propertyName = iterator2.next();
													if(objectNode2.get(propertyName) != null) {
														persistEnvironmentVariables(version, apiName, contactName, contactEmail, environmentCatalog.getName(), environmentCatalog.getRegionHostAcronym(), environmentCatalog.getCatalogEnv(), "oauth2", "x-ibm-configuration|oauth2|" + property + "|" + propertyName, objectNode2.get(propertyName).asText(), yamlFile.getName(), environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase(), environmentCatalog.getId(), environmentCatalog);
													}
												}
												objectNode.remove(property);
												break;
											}
										}
									}
								}
								if(objectNode != null) {
									Iterator<String> iterator = objectNode.fieldNames();
									while(iterator.hasNext()) {
										String property = iterator.next();
										if(property.equals("refresh-token")) {
											ObjectNode objectNode2 = (ObjectNode) objectNode.get(property);
											if(objectNode2 != null) {
												Iterator<String> iterator2 = objectNode2.fieldNames();
												while(iterator2.hasNext()) {
													String propertyName = iterator2.next();
													if(objectNode2.get(propertyName) != null) {
														persistEnvironmentVariables(version, apiName, contactName, contactEmail, environmentCatalog.getName(), environmentCatalog.getRegionHostAcronym(), environmentCatalog.getCatalogEnv(), "oauth2", "x-ibm-configuration|oauth2|" + property + "|" + propertyName, objectNode2.get(propertyName).asText(), yamlFile.getName(), environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase(), environmentCatalog.getId(), environmentCatalog);
													}
												}
												objectNode.remove(property);
												break;
											}
										}
									}
								}
							}
							configuration.remove("properties");
							configuration.remove("catalogs");
						}
						JsonNode securityDefinitionsJson = root.path("securityDefinitions");
						if(securityDefinitionsJson instanceof ObjectNode) {
							ObjectNode securityDefinitions = (ObjectNode)root.path("securityDefinitions"); //securityDefinitions.propertyName.subPropertyName
							if(securityDefinitions != null) {
								Iterator<String> iterator2 = securityDefinitions.fieldNames();
								while(iterator2.hasNext()) {
									String propertyName = iterator2.next();
									if(securityDefinitions.get(propertyName) != null) {
										ObjectNode objectNode3 = (ObjectNode) securityDefinitions.get(propertyName);
										if(objectNode3 != null) {
											Iterator<String> iterator3 = objectNode3.fieldNames();
											while(iterator3.hasNext()) {
												String subPropertyName = iterator3.next();
												if(objectNode3.get(subPropertyName) != null) {
													if(subPropertyName.equals("tokenUrl")) {
														persistEnvironmentVariables(version, apiName, contactName, contactEmail, environmentCatalog.getName(), environmentCatalog.getRegionHostAcronym(), environmentCatalog.getCatalogEnv(), "tokenUrl", "securityDefinitions|" + propertyName + "|" + subPropertyName, objectNode3.get("tokenUrl").asText(), yamlFile.getName(), environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase(), environmentCatalog.getId(), environmentCatalog);
														objectNode3.remove("tokenUrl");
													}
												}
											}
										}
									}
								}
							}
							if(securityDefinitions != null) {
								Iterator<String> iterator2 = securityDefinitions.fieldNames();
								while(iterator2.hasNext()) {
									String propertyName = iterator2.next();
									if(securityDefinitions.get(propertyName) != null) {
										ObjectNode objectNode3 = (ObjectNode) securityDefinitions.get(propertyName);
										if(objectNode3 != null) {
											Iterator<String> iterator3 = objectNode3.fieldNames();
											while(iterator3.hasNext()) {
												String subPropertyName = iterator3.next();
												if(objectNode3.get(subPropertyName) != null) {
													if(subPropertyName.equals("authorizationUrl")) {
														persistEnvironmentVariables(version, apiName, contactName, contactEmail, environmentCatalog.getName(), environmentCatalog.getRegionHostAcronym(), environmentCatalog.getCatalogEnv(), "authorizationUrl", "securityDefinitions|" + propertyName + "|" + subPropertyName, objectNode3.get("authorizationUrl").asText(), yamlFile.getName(), environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase(), environmentCatalog.getId(), environmentCatalog);
														objectNode3.remove("authorizationUrl");
														break;
													}
												}
											}
										}
									}
								}
							}
						}
						
						mapper.writer().writeValue(yamlFile, root);
					    @SuppressWarnings("deprecation")
						List<String> lines = FileUtils.readLines(yamlFile);
					    Iterator<String> i = lines.iterator();
					    while (i.hasNext())
					    {
					        String line = i.next();
					        if (line.trim().isEmpty())
					            i.remove();
					    }
					    FileUtils.writeLines(yamlFile, lines);
					}
				} catch (JsonProcessingException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}

	
	private void removeProductBeforeCompare(EnvironmentCatalog environmentCatalog) {
		File dir = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + File.separator + IYamlBeansMasterConstants.COMPARE_YAML_RESOURCE_FOLDER + File.separator + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
		File[] directoryListing = dir.listFiles();
		if (directoryListing != null) {
			for (File yamlFile : directoryListing) {
				try {
					if(yamlFile.getName().contains("_product_")) {
						yamlFile.delete();
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}
	}
}
