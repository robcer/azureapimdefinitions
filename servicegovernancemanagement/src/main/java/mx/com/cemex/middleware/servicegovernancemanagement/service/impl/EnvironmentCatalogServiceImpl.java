package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.EnvironmentCatalogRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.EnvironmentCatalogService;

@Named("environmentCatalogService")
public class EnvironmentCatalogServiceImpl extends CrudServiceImpl<EnvironmentCatalog, Long, EnvironmentCatalogRepository> implements EnvironmentCatalogService {
    
    @Override
    @Inject
    public void setRepository(EnvironmentCatalogRepository environmentCatalogRepository) {
        super.setRepository(environmentCatalogRepository);
    }
}