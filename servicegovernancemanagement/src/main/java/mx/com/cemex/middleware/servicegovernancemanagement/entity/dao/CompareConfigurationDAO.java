package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareConfiguration;

public interface CompareConfigurationDAO {

	CompareConfiguration create(CompareConfiguration compareConfiguration);
	CompareConfiguration merge(CompareConfiguration compareConfiguration);
    long countCompareConfiguration();
    List<CompareConfiguration> findCompareConfiguration(int firstResult, int maxResults);
    List<CompareConfiguration> findAll();
    CompareConfiguration findCompareConfiguration(Long id);

}
