package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;
import mx.com.cemex.middleware.servicegovernancemanagement.exception.CustomException;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.ApplicationRoleRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApplicationRoleService;

@Named("applicationRoleService")
public class ApplicationRoleServiceImpl extends CrudServiceImpl<ApplicationRole, Long, ApplicationRoleRepository> implements ApplicationRoleService {

	@Override
	@Inject
	public void setRepository(ApplicationRoleRepository applicationRoleRepository) {
		super.setRepository(applicationRoleRepository);
	}

	public List<ApplicationRole> findApplicationRolesByAuthorities() {
		System.out.println(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
		System.out.println(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
		List<ApplicationRole> applicationRoles = this.repository.findByNameIn(AuthorityUtils.authorityListToSet(SecurityContextHolder.getContext().getAuthentication().getAuthorities()));
		if (applicationRoles == null) {
			throw new CustomException("UserRole doesn't have linked applications.", HttpStatus.NOT_FOUND);
		}
		return applicationRoles;
	}

}
