package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.BitbucketSrcHeadersBean;
import mx.com.cemex.middleware.servicegovernancemanagement.service.BitbucketOperationsService;

@Service("bitbucketOperationsService")
public class BitbucketOperationsServiceImpl implements BitbucketOperationsService {

	private static final Logger LOG = Logger.getLogger(BitbucketOperationsServiceImpl.class);

	@Override
	public String commitUpdateCreateSourceFile(BitbucketSrcHeadersBean bitbucketSrcHeadersBean) throws IOException {
		LOG.info("Adding a new file: " + bitbucketSrcHeadersBean.getPathToFile() + " to: " + MessageFormat.format(IYamlBeansMasterConstants.BITBUCKET_URI, bitbucketSrcHeadersBean.getUser(), bitbucketSrcHeadersBean.getRepository()));
		MultipartUtility multipart = new MultipartUtility(MessageFormat.format(IYamlBeansMasterConstants.BITBUCKET_URI, bitbucketSrcHeadersBean.getUser(), bitbucketSrcHeadersBean.getRepository()) + "/src");
		multipart.addStringAsBytesPart(bitbucketSrcHeadersBean.getPathToFile(), bitbucketSrcHeadersBean.getXml(), bitbucketSrcHeadersBean.getFileName());
		multipart.addFormField("message", bitbucketSrcHeadersBean.getMessage());
		if(bitbucketSrcHeadersBean.getAuthor() != null) {
			multipart.addFormField("author", bitbucketSrcHeadersBean.getAuthor());
		}
		if(bitbucketSrcHeadersBean.getDestinationBranch() != null) {
			multipart.addFormField("branch", bitbucketSrcHeadersBean.getDestinationBranch());
		}
		return multipart.finish();
	}
	
	@Override
	public JSONObject createPullRequest(BitbucketSrcHeadersBean bitbucketSrcHeadersBean)
			throws MalformedURLException, IOException, ParseException {
		LOG.info("Creating a new pull Request from " + bitbucketSrcHeadersBean.getSourceBranch() + "branch into " + bitbucketSrcHeadersBean.getDestinationBranch() + "branch.");
		String bitBucketUrl = MessageFormat.format(IYamlBeansMasterConstants.BITBUCKET_URI, bitbucketSrcHeadersBean.getUser(), bitbucketSrcHeadersBean.getRepository()) + "/pullrequests";
		HttpURLConnection connection = (HttpURLConnection) new URL(bitBucketUrl).openConnection();
		try {
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", "Basic " + java.util.Base64.getEncoder().encodeToString((IYamlBeansMasterConstants.CONFLUENCE_CREDENTIALS).getBytes(StandardCharsets.UTF_8)));
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			outputStreamWriter.write(buildPostJSON(bitbucketSrcHeadersBean).toString());
			outputStreamWriter.close();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonOutputObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return new org.json.JSONObject(jsonOutputObject.toJSONString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}

	private JSONObject buildPostJSON(BitbucketSrcHeadersBean bitbucketSrcHeadersBean) {
		JSONObject jsonObjectHeader = new JSONObject();
		jsonObjectHeader.put("description", bitbucketSrcHeadersBean.getDescription());
		jsonObjectHeader.put("title", bitbucketSrcHeadersBean.getTitle());
		jsonObjectHeader.put("close_source_branch", "page");
		if (bitbucketSrcHeadersBean.getDestinationBranch() != null) {
			JSONObject jsonObjectDestination = new JSONObject();
			JSONObject jsonObjectDestionationBranch = new JSONObject();
			jsonObjectDestionationBranch.put("name", bitbucketSrcHeadersBean.getDestinationBranch());
			jsonObjectDestination.put("branch", jsonObjectDestionationBranch);
			jsonObjectHeader.put("destination", jsonObjectDestination);
		}
		if (bitbucketSrcHeadersBean.getSourceBranch() != null) {
			JSONObject jsonObjectSource = new JSONObject();
			JSONObject jsonObjectSourceBranch = new JSONObject();
			jsonObjectSourceBranch.put("name", bitbucketSrcHeadersBean.getSourceBranch());
			jsonObjectSource.put("branch", jsonObjectSourceBranch);
			jsonObjectHeader.put("source", jsonObjectSource);
		}
		return jsonObjectHeader;
	}

	@Override
	public JSONObject mergePullRequest(BitbucketSrcHeadersBean bitbucketSrcHeadersBean)
		throws MalformedURLException, IOException, ParseException {
		LOG.info("Merging a new pull Request " + bitbucketSrcHeadersBean.getPullrequestId());
		String bitBucketUrl = MessageFormat.format(IYamlBeansMasterConstants.BITBUCKET_URI, bitbucketSrcHeadersBean.getUser(), bitbucketSrcHeadersBean.getRepository()) + "/pullrequests/" + bitbucketSrcHeadersBean.getPullrequestId() + "merge";
		HttpURLConnection connection = (HttpURLConnection) new URL(bitBucketUrl).openConnection();
		try {
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", "Basic " + java.util.Base64.getEncoder().encodeToString((IYamlBeansMasterConstants.CONFLUENCE_CREDENTIALS).getBytes(StandardCharsets.UTF_8)));
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("title", bitbucketSrcHeadersBean.getTitle());
			outputStreamWriter.write(jsonObject.toString());
			outputStreamWriter.close();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonOutputObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return new org.json.JSONObject(jsonOutputObject.toJSONString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}

}
