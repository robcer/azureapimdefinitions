package mx.com.cemex.middleware.servicegovernancemanagement.bean;

public class ConfluenceIntegrationBean {
	
	private String storageValue;
	private Long versionNumber;
	private String spaceKey;
	private String title;
	private String id;
	private Long ancestorId;
	private String crudOperation;
	
	public String getStorageValue() {
		return storageValue;
	}
	public void setStorageValue(String storageValue) {
		this.storageValue = storageValue;
	}
	public Long getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(Long versionNumber) {
		this.versionNumber = versionNumber;
	}
	public String getSpaceKey() {
		return spaceKey;
	}
	public void setSpaceKey(String spaceKey) {
		this.spaceKey = spaceKey;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Long getAncestorId() {
		return ancestorId;
	}
	public void setAncestorId(Long ancestorId) {
		this.ancestorId = ancestorId;
	}
	public String getCrudOperation() {
		return crudOperation;
	}
	public void setCrudOperation(String crudOperation) {
		this.crudOperation = crudOperation;
	}
	
	
}
