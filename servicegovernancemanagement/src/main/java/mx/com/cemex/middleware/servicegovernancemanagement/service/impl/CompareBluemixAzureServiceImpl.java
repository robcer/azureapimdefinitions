package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.util.List;

import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.BluemixAzureCompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.BluemixAzureCompareConfigurationDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.CompareBluemixAzureService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ConfluenceReportService;

@Service("compareBluemixAzureService")
public class CompareBluemixAzureServiceImpl implements CompareBluemixAzureService {
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
	@Autowired
	ConfluenceReportService confluenceReportService;
	
	@Autowired
	BluemixAzureCompareConfigurationDAO bluemixAzureCompareConfigurationDAO;

	public void compareBluemixAzure() throws IOException, ParseException, java.text.ParseException {
		List<BluemixAzureCompareConfiguration> bluemixAzureCompareConfigurations = bluemixAzureCompareConfigurationDAO.findAll();
		for(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration: bluemixAzureCompareConfigurations) {
			confluenceReportService.publishBluemixAzureCompare(bluemixAzureCompareConfiguration);
		}
	}
}
