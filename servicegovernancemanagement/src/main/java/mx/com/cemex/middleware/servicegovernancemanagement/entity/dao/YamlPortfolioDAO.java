package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlPortfolio;

public interface YamlPortfolioDAO {

	YamlPortfolio create(YamlPortfolio yamlPortfolio);
    long countYamlPortfolio();
    List<YamlPortfolio> findYamlPortfolio(int firstResult, int maxResults);
    List<YamlPortfolio> findAll();
    YamlPortfolio findYamlPortfolio(Long id);
    YamlPortfolio merge(YamlPortfolio yamlPortfolio);
    YamlPortfolio findYamlPortfolioByProductName(String productName);

}
