package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.QuartzJobDetails;

public interface QuartzJobDetailsDAO {

	QuartzJobDetails create(QuartzJobDetails quartzJobDetails);
	QuartzJobDetails merge(QuartzJobDetails quartzJobDetails);
    long countQuartzJobDetails();
    List<QuartzJobDetails> findQuartzJobDetails(int firstResult, int maxResults);
    List<QuartzJobDetails> findAll();
    QuartzJobDetails findQuartzJobDetails(Long id);
    QuartzJobDetails findQuartzJobDetailsById(String id);
    QuartzJobDetails findQuartzJobDetailsByName(String id);

}
