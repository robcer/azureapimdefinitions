package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.PlatformCatalog;

public interface PlatformCatalogDAO {

	PlatformCatalog create(PlatformCatalog platformCatalog);
	PlatformCatalog merge(PlatformCatalog platformCatalog);
    long countPlatformCatalog();
    List<PlatformCatalog> findPlatformCatalog(int firstResult, int maxResults);
    List<PlatformCatalog> findAll();
    PlatformCatalog findPlatformCatalog(Long id);

}
