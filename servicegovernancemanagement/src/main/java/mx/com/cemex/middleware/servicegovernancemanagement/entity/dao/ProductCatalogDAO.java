package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProductCatalog;

public interface ProductCatalogDAO {

	ProductCatalog create(ProductCatalog productCatalog);
	ProductCatalog merge(ProductCatalog productCatalog);
    long countProductCatalog();
    List<ProductCatalog> findProductCatalog(int firstResult, int maxResults);
    List<ProductCatalog> findAll();
    ProductCatalog findProductCatalog(Long id);
    ProductCatalog findProductCatalogById(String id);
    List<ProductCatalog> findAllByDevOrgId(String devOrgId);
    List<ProductCatalog> findAllByDevOrgIdNotRemoved(String devOrgId);
    
}
