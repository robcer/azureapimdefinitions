package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.web.controller.ServiceBasedRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.Application;
import mx.com.cemex.middleware.servicegovernancemanagement.security.JwtTokenProvider;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApplicationRoleService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApplicationService;

@Controller
@RequestMapping(value = "/v1/secm/applications", produces = "application/json")
public class ApplicationController extends ServiceBasedRestController<Application, Long, ApplicationService> {

	@Autowired
	JwtTokenProvider jwtTokenProvider;
	
	@Autowired
	ApplicationRoleService applicationRoleService;

    @Inject
    @Named("applicationService")
    @Override
    public void setService(ApplicationService applicationService) {
        this.service = applicationService;
    }

	@RequestMapping(value = "/menu", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public List<Application> getApplicationMenus() {
		return this.service.findByRolesIn(applicationRoleService.findApplicationRolesByAuthorities());
	}

}
