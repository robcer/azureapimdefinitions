package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "bluemixazurecompareconfiguration")
public class BluemixAzureCompareConfiguration {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_bluemix_azure_compare_configuration")
	private Long idBluemixAzureCompareConfiguration;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_source", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentSource;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_target", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentTarget;
    
    @Column(name = "is_enable")
    private boolean isEnable;
    
    @Column(name = "confluence_page_id")
    private Long confluencePageId;

	public Long getIdBluemixAzureCompareConfiguration() {
		return idBluemixAzureCompareConfiguration;
	}

	public void setIdBluemixAzureCompareConfiguration(Long idBluemixAzureCompareConfiguration) {
		this.idBluemixAzureCompareConfiguration = idBluemixAzureCompareConfiguration;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public EnvironmentCatalog getEnvironmentSource() {
		return environmentSource;
	}

	public void setEnvironmentSource(EnvironmentCatalog environmentSource) {
		this.environmentSource = environmentSource;
	}

	public EnvironmentCatalog getEnvironmentTarget() {
		return environmentTarget;
	}

	public void setEnvironmentTarget(EnvironmentCatalog environmentTarget) {
		this.environmentTarget = environmentTarget;
	}

	public boolean isEnable() {
		return isEnable;
	}

	public void setEnable(boolean isEnable) {
		this.isEnable = isEnable;
	}

	public Long getConfluencePageId() {
		return confluencePageId;
	}

	public void setConfluencePageId(Long confluencePageId) {
		this.confluencePageId = confluencePageId;
	}
    
    

}
