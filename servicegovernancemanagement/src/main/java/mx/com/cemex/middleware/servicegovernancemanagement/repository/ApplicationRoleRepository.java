package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;

public interface ApplicationRoleRepository extends JpaRepository<ApplicationRole, Long> {
	
	List<ApplicationRole> findByNameIn(Collection<String> roles);

}
