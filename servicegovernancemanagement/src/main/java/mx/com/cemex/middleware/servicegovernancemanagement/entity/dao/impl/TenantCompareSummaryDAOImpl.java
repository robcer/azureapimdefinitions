package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.TenantCompareSummaryDAO;

@Service("tenantCompareSummaryDAO")
public class TenantCompareSummaryDAOImpl implements TenantCompareSummaryDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public TenantCompareSummaryDAOImpl()
    {
    }

    TenantCompareSummaryDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public TenantCompareSummary create(TenantCompareSummary tenantCompareSummary) {
        em.persist(tenantCompareSummary);
        return tenantCompareSummary;
    }

    @Transactional
    public TenantCompareSummary merge(TenantCompareSummary tenantCompareSummary) {
        em.merge(tenantCompareSummary);
        return tenantCompareSummary;
    }

    public long countTenantCompareSummary() {
        return (Long) em.createQuery("select count(o) from TenantCompareSummary o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<TenantCompareSummary> findTenantCompareSummary(int firstResult, int maxResults) {
        return em.createQuery("select o from TenantCompareSummary o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public TenantCompareSummary findTenantCompareSummary(Long id) {
        if (id == null) return null;
        return em.find(TenantCompareSummary.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<TenantCompareSummary> findAll() {
        return em.createQuery("select o from TenantCompareSummary o order by o.idCompareSummary DESC").getResultList();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from TenantCompareSummary");
    	query.executeUpdate();
    }
    
    public List<TenantCompareSummary> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from TenantCompareSummary o where o.devOrgNameEnv = :devOrgNameEnv", TenantCompareSummary.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public List<Object> findTargetFileNameByReportGUID(String reportGuid) {
    	@SuppressWarnings("unchecked")
    	List<Object> results = em.createNativeQuery("select target_file_name from TenantCompareSummary o where o.report_Guid = ?1 order by o.file_Name asc")
        		.setParameter(1, reportGuid).getResultList();
    	return results;
    }
    
    public List<TenantCompareSummary> findAllByReportGUID(String reportGuid) {
        if (reportGuid == null) return null;
        return em.createQuery("select o from TenantCompareSummary o where o.reportGuid = :reportGuid", TenantCompareSummary.class).setParameter("reportGuid", reportGuid).getResultList();
    }
    
    public TenantCompareSummary findTenantCompareSummaryById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from TenantCompareSummary o where o.id = :id", TenantCompareSummary.class)
        		.setParameter("id", id).getSingleResult();
    }

}