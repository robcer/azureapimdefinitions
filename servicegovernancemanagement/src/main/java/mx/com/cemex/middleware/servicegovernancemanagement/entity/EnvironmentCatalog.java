package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.sql.Timestamp;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "environmentcatalog")
public class EnvironmentCatalog {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_environment_catalog")
	private Long idEnvironmentCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @Column(name = "latest_operation_call")
    @DateTimeFormat(style = "S-")
    @JsonIgnore
    private Timestamp latestOperationCall;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "dev_org_name_env")
    private String devOrgNameEnv;
    
    @Column(name = "catalog_env")
    private String catalogEnv;
    
    @Column(name = "api_manager_apis_url")
    @JsonIgnore
    private String apiManagerApisURL;
    
    @Column(name = "api_inventory_ancestor_id")
    @JsonIgnore
    private Long apiInventoryAncestorId;
    
    @Column(name = "product_inventory_ancestor_id")
    @JsonIgnore
    private Long productInventoryAncestorId;
    
    @Column(name = "space_inventory_ancestor_id")
    @JsonIgnore
    private Long spaceInventoryAncestorId;
    
    @Column(name = "dashboard_ancestor_id")
    @JsonIgnore
    private Long dashboardAncestorId;
    
    @Column(name = "region")
    private String region;
    
    @Column(name = "region_host_acronym")
    private String regionHostAcronym;
    
    @Column(name = "enable")
    @JsonIgnore
    private boolean enable;
    
    @Column(name = "source_code_repository")
    private String sourceCodeRepository;
    
    @Column(name = "bitbucket_branch")
    private String bitbucketBranch;

    @ManyToOne(fetch = FetchType.EAGER)
    @JsonIgnore
    @JoinColumn(name = "id_platform_catalog", nullable = true, updatable = true, insertable = true)
    private PlatformCatalog platformCatalog;
    
    @Column(name = "identifier")
    @JsonIgnore
    private String identifier;
    
    @Column(name = "primary_key")
    @JsonIgnore
    private String primaryKey;

	public String getBitbucketBranch() {
		return bitbucketBranch;
	}

	public void setBitbucketBranch(String bitbucketBranch) {
		this.bitbucketBranch = bitbucketBranch;
	}

	public PlatformCatalog getPlatformCatalog() {
		return platformCatalog;
	}

	public void setPlatformCatalog(PlatformCatalog platformCatalog) {
		this.platformCatalog = platformCatalog;
	}

	public boolean isEnable() {
		return enable;
	}

	public void setEnable(boolean enable) {
		this.enable = enable;
	}

	public Long getIdEnvironmentCatalog() {
		return idEnvironmentCatalog;
	}

	public void setIdEnvironmentCatalog(Long idEnvironmentCatalog) {
		this.idEnvironmentCatalog = idEnvironmentCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDevOrgNameEnv() {
		return devOrgNameEnv;
	}

	public void setDevOrgNameEnv(String devOrgNameEnv) {
		this.devOrgNameEnv = devOrgNameEnv;
	}

	public String getCatalogEnv() {
		return catalogEnv;
	}

	public void setCatalogEnv(String catalogEnv) {
		this.catalogEnv = catalogEnv;
	}

	public String getApiManagerApisURL() {
		return apiManagerApisURL;
	}

	public void setApiManagerApisURL(String apiManagerApisURL) {
		this.apiManagerApisURL = apiManagerApisURL;
	}

	public Long getApiInventoryAncestorId() {
		return apiInventoryAncestorId;
	}

	public void setApiInventoryAncestorId(Long apiInventoryAncestorId) {
		this.apiInventoryAncestorId = apiInventoryAncestorId;
	}

	public Long getProductInventoryAncestorId() {
		return productInventoryAncestorId;
	}

	public void setProductInventoryAncestorId(Long productInventoryAncestorId) {
		this.productInventoryAncestorId = productInventoryAncestorId;
	}

	public Timestamp getLatestOperationCall() {
		return latestOperationCall;
	}

	public void setLatestOperationCall(Timestamp latestOperationCall) {
		this.latestOperationCall = latestOperationCall;
	}

	public Long getSpaceInventoryAncestorId() {
		return spaceInventoryAncestorId;
	}

	public void setSpaceInventoryAncestorId(Long spaceInventoryAncestorId) {
		this.spaceInventoryAncestorId = spaceInventoryAncestorId;
	}

	public Long getDashboardAncestorId() {
		return dashboardAncestorId;
	}

	public void setDashboardAncestorId(Long dashboardAncestorId) {
		this.dashboardAncestorId = dashboardAncestorId;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getRegionHostAcronym() {
		return regionHostAcronym;
	}

	public void setRegionHostAcronym(String regionHostAcronym) {
		this.regionHostAcronym = regionHostAcronym;
	}

	public String getSourceCodeRepository() {
		return sourceCodeRepository;
	}

	public void setSourceCodeRepository(String sourceCodeRepository) {
		this.sourceCodeRepository = sourceCodeRepository;
	}

	public String getIdentifier() {
		return identifier;
	}

	public void setIdentifier(String identifier) {
		this.identifier = identifier;
	}

	public String getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(String primaryKey) {
		this.primaryKey = primaryKey;
	}
    
    

}
