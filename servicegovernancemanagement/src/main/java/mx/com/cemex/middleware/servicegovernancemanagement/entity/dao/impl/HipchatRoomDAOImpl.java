package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.HipchatRoom;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.HipchatRoomDAO;

@Service("hipchatRoomDAO")
public class HipchatRoomDAOImpl implements HipchatRoomDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public HipchatRoomDAOImpl()
    {
    }

    HipchatRoomDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public HipchatRoom create(HipchatRoom hipchatRoom) {
        em.persist(hipchatRoom);
        return hipchatRoom;
    }

    @Transactional
    public HipchatRoom merge(HipchatRoom hipchatRoom) {
        em.merge(hipchatRoom);
        return hipchatRoom;
    }

    public long countHipchatRoom() {
        return (Long) em.createQuery("select count(o) from HipchatRoom o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<HipchatRoom> findHipchatRoom(int firstResult, int maxResults) {
        return em.createQuery("select o from HipchatRoom o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public HipchatRoom findHipchatRoom(Long id) {
        if (id == null) return null;
        return em.find(HipchatRoom.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<HipchatRoom> findAll() {
    	
        return em.createQuery("select o from HipchatRoom o where o.notificationEnabled = 1 order by o.idHipchatRoom DESC").getResultList();
    }
    
    public HipchatRoom findHipchatRoomById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from HipchatRoom o where o.id = :id", HipchatRoom.class)
        		.setParameter("id", id).getSingleResult();
    }

}