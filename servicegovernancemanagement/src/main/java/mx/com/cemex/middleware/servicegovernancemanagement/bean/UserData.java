package mx.com.cemex.middleware.servicegovernancemanagement.bean;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.Role;

public class UserData {
  
  @ApiModelProperty(position = 0)
  private String username;
  @ApiModelProperty(position = 1)
  private String email;
  @ApiModelProperty(position = 2)
  List<Role> roles;

  public String getUsername() {
    return username;
  }

  public void setUsername(String username) {
    this.username = username;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public List<Role> getRoles() {
    return roles;
  }

  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }

}
