package mx.com.cemex.middleware.servicegovernancemanagement;

public interface IYamlBeansMasterConstants {
	
	static final String LOCAL_RESOURCE_PATH = "D:\\home\\site\\wwwroot\\APICONNECT";
	static final String COMPARE_YAML_RESOURCE_FOLDER = "COMPAREYAMLRESOURCE";
	static final String REBUILD_YAML_RESOURCE_FOLDER = "REBUILDEDYAMLRESOURCE";
	static final String LOCAL_BITBUCKET_REPOSITORY_CNX_GBL_ORG = "D:\\home\\site\\wwwroot\\APICONNECT\\BITBUCKET\\WORKSPACE\\APICONNECT\\{0}\\CNX-GBL-ORG";
	static final String LOCAL_NEW_BITBUCKET_REPOSITORY = "D:\\home\\site\\wwwroot\\APICONNECT\\NEWBITBUCKET";
	static final String LOCAL_BITBUCKET_REPOSITORY = "D:\\home\\site\\wwwroot\\APICONNECT\\BITBUCKET";
	static final String LOCAL_APIM_KUDU_SERVER_PATH = "D:\\home\\site\\wwwroot\\AZUREAPIMANAGEMENT\\GITREPOSITORIES";
	static final String CLOUD_BLUEMIX_HOSTNAME = "{0}.apiconnect.ibmcloud.com";
	static final boolean INITIAL_LOAD = false;
	static final boolean INCREMENTAL_LOAD = true;
	
	static final String BITBUCKET_REPOSITORY = "azureapimdefinitions";
	static final String BITBUCKET_USER = "cemex";
	
	//Confluence Ids
	static final String CONFLUENCE_BASE_URI = "https://cemexpmo.atlassian.net/wiki/rest/api/content/";
	static final String BITBUCKET_URI = "https://api.bitbucket.org/2.0/repositories/{0}/{1}";
	static final String JIRA_URI = "https://cemexpmo.atlassian.net/rest/api/2/issue/{0}";
	static final String TECHNICAL_DEFINITION_SPACE_KEY = "SDTSD";
	static final String PRIVATE_TECHNICAL_DEFINITION_SPACE_KEY = "MSG";
	static final Long API_REPORT_ANALYTICS_ANCESTOR_ID = 21860647L;
	static final String API_CONNECT_REPORT_TITLE = "{0} API Connect Report {1}";
	static final String ENVIRONMENT_REPORT_TITLE = "{0} {1} Environment Report {2}";
	static final String API_TITLE = "{0} API {1} {2} {3}";
	static final String PRODUCT_INVENTORY_TITLE = "{0} {1} Product Inventory";
	static final String API_INVENTORY_TITLE = "{0} {1} APIs Inventory";
	static final String AZURE_API_INVENTORY_TITLE = "{0} {1} APIs Operation Inventory";
	static final String OPERATION_TITLE = "{0} Operation {1} {2}{3} {4}";
	static final String COMPARE_YAML_TITLE = "{0} and {1}";
	
	static final String BITBUCKET_API_POLICIES_PATH = "/api-management/policies/apis/{0}/{1}_{2}.xml";
	static final String BITBUCKET_OPERATIONS_POLICIES_PATH = "/api-management/policies/apis/{0}/{1}_{2}/operations/{3}_{4}_{5}.xml";
	
	//Multipart
	static final String CRLF = "\r\n";
	static final String TWO_HYPHENS = "--";
	static final String BOUNDARY = "----WebKitFormBoundary7MA4YWxkTrZu0gW";
	static final String ATTACHMENT_FILE_KEY = "file";
	static final String ATTACHMENT_COMMENT_KEY = "comment";
	static final String BLUEMIX_CREDENTIALS_USERNAME = "rogelio.reyocachu@ext.cemex.com";
	static final String BLUEMIX_CREDENTIALS_PASSWORD = "c199714c";
	
	static final String CONFLUENCE_CREDENTIALS = "rogelio.reyocachu@ext.cemex.com:rogeLIO185251518";
	static final String BLUEMIX_CREDENTIALS = BLUEMIX_CREDENTIALS_USERNAME + ":" + BLUEMIX_CREDENTIALS_PASSWORD;
	
	static final String AZURE_API_MANAGEMENT_IDENTIFIER = "AZUREAPIM";
	static final String BLUEMIX_API_MANAGEMENT_IDENTIFIER = "BLUEMIXAPIM";
	static final String CLOUD_AZURE_HOSTNAME = "{0}.management.azure-api.net";

	static final String HIPCHAT_ROOM_BASE_URI = "https://cemexpmo.hipchat.com/v2/room";
	
	static final String AZURE_API_MANAGEMENT_ACCESS_TOKEN = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsIng1dCI6IkZTaW11RnJGTm9DMHNKWEdtdjEzbk5aY2VEYyIsImtpZCI6IkZTaW11RnJGTm9DMHNKWEdtdjEzbk5aY2VEYyJ9.eyJhdWQiOiJodHRwczovL21hbmFnZW1lbnQuY29yZS53aW5kb3dzLm5ldC8iLCJpc3MiOiJodHRwczovL3N0cy53aW5kb3dzLm5ldC82ZWUxOTAwMS1kMGM0LTQ1ZjgtYWY4Yi1mZjAwZjE2ZDA3ZTEvIiwiaWF0IjoxNTIzNDYyMDI1LCJuYmYiOjE1MjM0NjIwMjUsImV4cCI6MTUyMzQ2NTkyNSwiYWNyIjoiMSIsImFpbyI6IlkyTmdZSGh3MEdXREpkZDcxbzQ2bTg4cks5WlhGOW13eGgzeFN0UlhsNVNwNGRLdFBBc0EiLCJhbXIiOlsicHdkIl0sImFwcGlkIjoiYzQ0YjQwODMtM2JiMC00OWMxLWI0N2QtOTc0ZTUzY2JkZjNjIiwiYXBwaWRhY3IiOiIyIiwiZV9leHAiOjI2MjgwMCwiZmFtaWx5X25hbWUiOiJTb3RlbGRvIE1hbGF2ZSIsImdpdmVuX25hbWUiOiJNYW51ZWwgQWxlamFuZHJvIiwiZ3JvdXBzIjpbImM1N2ZiNDFjLWEyYmUtNGRhNC1hYTYyLWY3NDExOGE2ZmE2OCIsIjhkZjU2OTAzLTdlOWMtNDJmNC1hNWYzLTY4NWM2MDlmOWIyNyIsIjU2MTNiNDViLWFlNWMtNGQ0MS04MWRlLTkzYWI0MDdiOTViZiIsIjUwMTEzYTVkLWMyN2MtNDIwYS05MmU1LTMyMTQyMjYzMjRkYiIsIjA0NzE0ZGEzLTU4NTMtNDQ0OC1iZDhkLTBjZWVhOGIyNTYzZCIsIjNkNTkwNTI1LTMzMTYtNDkyZS1iYTNhLWEwMGM0MjhiMjk4YyIsImQ1MDQyN2RhLWE5ZDYtNDBiZS1iODJlLWY5OWFlNzI5MmY4OSIsIjBlNjA3YTdjLTE0OTItNGZkYi04MjY0LTQ0NmQ3YTBjODEyNCIsIjkzYjdjOWM5LTNkODAtNDJhNi04NmM4LWRlMGQ4MGRiMjBjYyIsImYwZjY3YzY0LWI5ZWEtNGFhZC05NDk1LTVhOTYwNmYwYjA2NSIsImJkMTkwNmY0LWIyOTAtNDFjMi1hOGMyLWZhOGYxYmFiNDliZiJdLCJpcGFkZHIiOiIyMTIuMjAuMTI2LjIzOCIsIm5hbWUiOiJNYW51ZWwgQWxlamFuZHJvIFNvdGVsZG8gTWFsYXZlIiwib2lkIjoiNGE3OTkyNGYtYjUyZi00ZWY3LWI1M2EtODkwMGM4ZTI0NDM0Iiwib25wcmVtX3NpZCI6IlMtMS01LTIxLTM0MzgxODM5OC0xNDU0NDcxMTY1LTE4MDE2NzQ1MzEtMjU4MTgxIiwicHVpZCI6IjEwMDNCRkZEODlDRDU4RUUiLCJzY3AiOiJ1c2VyX2ltcGVyc29uYXRpb24iLCJzdWIiOiI1RVlBc2ZwWXotZTZnNDJ0SUNrR0RhYWRrbnFiUV85M0pZUUlaRGQ3RzIwIiwidGlkIjoiNmVlMTkwMDEtZDBjNC00NWY4LWFmOGItZmYwMGYxNmQwN2UxIiwidW5pcXVlX25hbWUiOiJtYW51ZWxhbGVqYW5kcm8uc290ZWxkb0BleHQuY2VtZXguY29tIiwidXBuIjoibWFudWVsYWxlamFuZHJvLnNvdGVsZG9AZXh0LmNlbWV4LmNvbSIsInV0aSI6Im5pQkFkNTNfNTBxNFM5OHhFNGtjQUEiLCJ2ZXIiOiIxLjAifQ.mXyrLJq_gJmkPpP9VvWjZLS528axDx7EjH2TRG_HpsPd-ovIh7-UrJq2QbjSUaUgCZP38z5F_-YVxSt89rXDdBHE-Lm3g55ldut9SEPcvBZTRvYiNafZDB0od59OAmtHU46KmSSofxRpAvxJY87pPBIkVEZm9aWxs2NB2MvDLLu_Xqdtnyv81Al7eRYMBxytFartuhcvbbfPtuX8yvBJT7V3eTfdbQcPkF3PSKvVhhUBbktCEvxAqRd5RZg7a9zbRI4NSEGIWJn1wy5GHCaE-7zfeh706SuT9lWi27kX2cEF-Zp2BPpw9EP7JGqLiTu_aqOpQb7HOVcUAtzxUTV6Vg";
	static final String BEARER = "Bearer";
	static final String AZURE_CREDENTIALS = BEARER + " " + AZURE_API_MANAGEMENT_ACCESS_TOKEN;
	
	static final String ADMIN = "ROLE_ADMIN";
	
}
