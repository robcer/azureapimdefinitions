package mx.com.cemex.middleware.servicegovernancemanagement.security;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.UserRole;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.UserRepository;

@Service
public class MsgUserDetails implements UserDetailsService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		final User user = userRepository.findByUsername(username);

		if (user == null) {
			throw new UsernameNotFoundException("User '" + username + "' not found");
		}
		return new org.springframework.security.core.userdetails.User(username, user.getPassword(), true, true, true, true, new ArrayList<UserRole>(user.getRoles()));
//		user2.
//		return org.springframework.security.core.userdetails.User.withUsername(username).password(user.getPassword())
//				.authorities(user.getRoles()).accountExpired(false).accountLocked(false).credentialsExpired(false)
//				.disabled(false).build();
	}

}
