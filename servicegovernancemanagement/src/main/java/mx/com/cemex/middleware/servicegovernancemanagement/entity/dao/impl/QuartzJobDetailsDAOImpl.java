package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.QuartzJobDetails;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.QuartzJobDetailsDAO;

@Service("quartzJobDetailsDAO")
public class QuartzJobDetailsDAOImpl implements QuartzJobDetailsDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public QuartzJobDetailsDAOImpl()
    {
    }

    QuartzJobDetailsDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public QuartzJobDetails create(QuartzJobDetails quartzJobDetails) {
        em.persist(quartzJobDetails);
        return quartzJobDetails;
    }

    @Transactional
    public QuartzJobDetails merge(QuartzJobDetails quartzJobDetails) {
        em.merge(quartzJobDetails);
        return quartzJobDetails;
    }

    public long countQuartzJobDetails() {
        return (Long) em.createQuery("select count(o) from QuartzJobDetails o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<QuartzJobDetails> findQuartzJobDetails(int firstResult, int maxResults) {
        return em.createQuery("select o from QuartzJobDetails o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public QuartzJobDetails findQuartzJobDetails(Long id) {
        if (id == null) return null;
        return em.find(QuartzJobDetails.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<QuartzJobDetails> findAll() {
        return em.createQuery("select o from QuartzJobDetails o").getResultList();
    }
    
    public QuartzJobDetails findQuartzJobDetailsByName(String name) {
        if (name == null) return null;
        return em.createQuery("select o from QuartzJobDetails o where o.name = :name", QuartzJobDetails.class)
        		.setParameter("name", name).getSingleResult();
    }
    
    public QuartzJobDetails findQuartzJobDetailsById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from QuartzJobDetails o where o.id = :id", QuartzJobDetails.class)
        		.setParameter("id", id).getSingleResult();
    }
}
