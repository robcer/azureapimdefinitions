package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface ProductCatalogService {
	
	void mantainProductCatalog(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException;
}
