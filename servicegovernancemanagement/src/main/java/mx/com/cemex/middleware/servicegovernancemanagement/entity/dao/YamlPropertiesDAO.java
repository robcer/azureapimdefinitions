package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlProperties;

public interface YamlPropertiesDAO {

	YamlProperties create(YamlProperties yamlProperties);
    long countYamlProperties();
    List<YamlProperties> findYamlProperties(int firstResult, int maxResults);
    List<YamlProperties> findAll();
    YamlProperties findYamlProperties(Long id);
    YamlProperties merge(YamlProperties yamlProperties);
    YamlProperties findYamlPropertiesByFullPathAndProductNameAndXIbmName(String fullPath, String productName, String xIbmName, String devOrgId, String apiVersion);
    List<YamlProperties> findAllYamlPropertiesByFullPathAndProductNameAndXIbmName(String productName,String xIbmName, String basePath, String devOrgId);
    String findOneAzureHostByDevOrgIdAndApiNameAndApiVersion(String devOrgId, String apiName, String apiVersion);
    void updateLogicalDeletedAll(String devOrgId);
}
