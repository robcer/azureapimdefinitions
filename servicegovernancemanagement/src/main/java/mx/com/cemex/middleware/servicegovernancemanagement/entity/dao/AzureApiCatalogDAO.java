package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface AzureApiCatalogDAO {

	AzureApiCatalog create(AzureApiCatalog azureApiCatalog);
	AzureApiCatalog merge(AzureApiCatalog azureApiCatalog);
    long countAzureApiCatalog();
    List<AzureApiCatalog> findAzureApiCatalog(int firstResult, int maxResults);
    List<AzureApiCatalog> findAll();
    AzureApiCatalog findAzureApiCatalog(Long id);
    AzureApiCatalog findAzureApiCatalogByIdAndEnvironment(String id, EnvironmentCatalog environmentCatalog);
    List<AzureApiCatalog> findAllByEnvironment(EnvironmentCatalog environmentCatalog);
    void disableAzureApiCatalogByEnvironment(EnvironmentCatalog environmentCatalog);
}
