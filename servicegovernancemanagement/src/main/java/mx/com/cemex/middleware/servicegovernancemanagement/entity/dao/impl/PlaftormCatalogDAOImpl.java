package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.PlatformCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.PlatformCatalogDAO;

@Service("platformCatalogDAO")
public class PlaftormCatalogDAOImpl implements PlatformCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public PlaftormCatalogDAOImpl()
    {
    }

    PlaftormCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public PlatformCatalog create(PlatformCatalog platformCatalog) {
        em.persist(platformCatalog);
        return platformCatalog;
    }

    @Transactional
    public PlatformCatalog merge(PlatformCatalog platformCatalog) {
        em.merge(platformCatalog);
        return platformCatalog;
    }

    public long countPlatformCatalog() {
        return (Long) em.createQuery("select count(o) from PlatformCatalog o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<PlatformCatalog> findPlatformCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from PlatformCatalog o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public PlatformCatalog findPlatformCatalog(Long id) {
        if (id == null) return null;
        return em.find(PlatformCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<PlatformCatalog> findAll() {
        return em.createQuery("select o from PlatformCatalog o where enable = 1 order by o.idPlatformCatalog DESC").getResultList();
    }

}