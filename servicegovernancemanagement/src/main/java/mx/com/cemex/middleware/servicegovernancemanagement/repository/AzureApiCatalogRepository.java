package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;


public interface AzureApiCatalogRepository extends JpaRepository<AzureApiCatalog, Long> {

	List<AzureApiCatalog> findAllByEnvironmentCatalog(EnvironmentCatalog environmentCatalog);
}
