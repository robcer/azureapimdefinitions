package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import org.apache.commons.beanutils.BeanUtils;
import org.resthub.web.controller.ServiceBasedRestController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.LoginApplicationResponse;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.LoginResponse;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.LoginRoleResponse;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.UserApplication;
import mx.com.cemex.middleware.servicegovernancemanagement.security.JwtTokenProvider;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UserApplicationService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UserService;

@Controller
@RequestMapping(value = "/v2/secm/oam/oauth2", produces = "application/json")
public class UserController extends ServiceBasedRestController<User, Long, UserService> {
	
	@Autowired
	JwtTokenProvider jwtTokenProvider;

	@Autowired
	UserApplicationService userApplicationService;
	
    @Inject
    @Named("userService")
    @Override
    public void setService(UserService userService) {
        this.service = userService;
    }

	@RequestMapping(value = "/token", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
	public LoginResponse token(@RequestParam Map<String, String> multiValueMap) throws IllegalAccessException, InvocationTargetException {
		if (multiValueMap == null || multiValueMap.get("password") == null) {
			throw new IllegalArgumentException("Password not provided");
		}
		if (multiValueMap == null || multiValueMap.get("username") == null) {
			throw new IllegalArgumentException("Username not provided");
		}
		LoginResponse loginResponse = this.service.signin(multiValueMap.get("username").toString(), multiValueMap.get("password").toString());
		User user = this.service.search(multiValueMap.get("username").toString());
		User userDestination = new User();
		BeanUtils.copyProperties(userDestination, user);
		loginResponse.setUser(userDestination);
		List<UserApplication> userApplications = userApplicationService.findByUser(user);
		List<LoginApplicationResponse> loginApplicationResponses = new ArrayList<LoginApplicationResponse>();
		for(UserApplication userApplication: userApplications) {
			LoginApplicationResponse loginApplicationResponse = new LoginApplicationResponse();
			loginApplicationResponse.setApplicationCode(userApplication.getApplication().getApplicationCode());
			loginApplicationResponse.setApplicationDesc(userApplication.getApplication().getApplicationTitle());
			loginApplicationResponse.setApplicationId(userApplication.getApplication().getApplicationMenuId());
			loginApplicationResponse.setApplicationName(userApplication.getApplication().getApplicationTitle());
			List<LoginRoleResponse> loginRoleResponses = new ArrayList<LoginRoleResponse>();
			for(ApplicationRole applicationRole: userApplication.getApplication().getRoles()) {
				LoginRoleResponse loginRoleResponse = new LoginRoleResponse();
				loginRoleResponse.setRoleCode(applicationRole.getAuthority());
				loginRoleResponse.setRoleId(applicationRole.getRoles());
				loginRoleResponse.setRoleDesc(applicationRole.getName());
				loginRoleResponse.setRoleType("R");
				loginRoleResponses.add(loginRoleResponse);
			}
			loginApplicationResponse.setRoles(loginRoleResponses);
			loginApplicationResponses.add(loginApplicationResponse);
		}
		loginResponse.setApplications(loginApplicationResponses);
		return loginResponse;
	}

//	@RequestMapping(value = "/signup", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
//	@ApiOperation(value = "${UserController.signup}")
//	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 422, message = "Username is already in use"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public String signup(@ApiParam("Signup User") @RequestBody UserData user) {
//		return userService.signup(modelMapper.map(user, User.class));
//	}

//	@RequestMapping(value = "/{username}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
//	@ApiOperation(value = "${UserController.delete}")
//	@ApiResponses(value = { @ApiResponse(code = 400, message = "Something went wrong"),
//			@ApiResponse(code = 403, message = "Access denied"),
//			@ApiResponse(code = 404, message = "The user doesn't exist"),
//			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	public String delete(@ApiParam("Username") @PathVariable String username) {
//		userService.delete(username);
//		return username;
//	}

	@RequestMapping(value = "/users/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@PreAuthorize("hasRole('ROLE_ADMIN')")
    @ResponseBody
	public User search(@PathVariable String username) {
		return this.service.search(username);
	}

//	@RequestMapping(value = "/name/{username}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
////	@PreAuthorize("hasRole('ROLE_ADMIN')")
//    @ResponseBody
//	public User search(@PathVariable String username, @RequestHeader("Authorization") String authorization) {
//		System.out.println(authorization);
//		String bearerToken = authorization;
//		String token = null;
//		if (bearerToken != null && bearerToken.startsWith("Bearer ")) {
//			token = bearerToken.substring(7, bearerToken.length());
//		}
//		System.out.println(token);
//		if (token != null && jwtTokenProvider.validateToken(token)) {
//			Authentication auth = token != null ? jwtTokenProvider.getAuthentication(token) : null;
//			SecurityContextHolder.getContext().setAuthentication(auth);
//		}
//		return this.service.search(username);
//	}
//
//	@RequestMapping(value = "/me", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
//	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
//	public User whoami(HttpServletRequest req) {
//		return this.service.whoami(req);
//	}

}
