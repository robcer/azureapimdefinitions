package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProjectCatalog;

public interface ProjectCatalogDAO {

	ProjectCatalog create(ProjectCatalog projectCatalog);
	ProjectCatalog merge(ProjectCatalog projectCatalog);
    long countProjectCatalog();
    List<ProjectCatalog> findProjectCatalog(int firstResult, int maxResults);
    List<ProjectCatalog> findAll();
    ProjectCatalog findProjectCatalog(Long id);
    ProjectCatalog findProjectCatalogById(String id);
    ProjectCatalog findProjectCatalogByName(String id);

}
