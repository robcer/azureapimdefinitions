package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.MessageFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.apache.velocity.app.VelocityEngine;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.velocity.VelocityEngineUtils;
import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.ConfluenceBluemixAzureComparisonBean;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.HistoricalReportBySpace;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.BluemixAzureCompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareConfiguration;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProductCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiHistoryCallDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.BluemixAzureCompareConfigurationDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProductCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.TenantCompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ConfluenceReportService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

import static org.apache.commons.lang.StringEscapeUtils.escapeHtml;

@Service("confluenceReportService")
public class ConfluenceReportServiceImpl implements ConfluenceReportService {
	
	private static final Logger LOG  = Logger.getLogger(ConfluenceReportServiceImpl.class);

	@Autowired
	private VelocityEngine velocityEngine;
	
	@Autowired
	private UtilitiesService utilitiesService;
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;

	@Autowired
	ProductCatalogDAO productCatalogDAO;

	@Autowired
	ApiCatalogDAO apiCatalogDAO;
	
	@Autowired
	AzureApiCatalogDAO azureApiCatalogDAO;
	
	@Autowired
	AzureApiOperationCatalogDAO azureApiOperationCatalogDAO;

	@Autowired
	ApiOperationCatalogDAO apiOperationCatalogDAO;

	@Autowired
	ApiHistoryCallDAO apiHistoryCallDAO;
	
	@Autowired
	CompareSummaryDAO compareSummaryDAO;
	
	@Autowired
	TenantCompareSummaryDAO tenantCompareSummaryDAO;
	
	@Autowired
	BluemixAzureCompareConfigurationDAO bluemixAzureCompareConfigurationDAO;
	
	public void createNewReportAnalytic() throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException {
		List<EnvironmentCatalog> environmentCatalogs = environmentCatalogDAO.findAll();
		boolean updateOperationPageContent = false;
		for(EnvironmentCatalog environmentCatalog: environmentCatalogs) {
			List<ApiCatalog> apiCatalogList = apiCatalogDAO.findAllByDevOrgId(environmentCatalog.getId());
	        for(ApiCatalog apiCatalog: apiCatalogList) {
	        	Long apiReportAncestorId = null;
				String apiReportPageTitle = MessageFormat.format(IYamlBeansMasterConstants.API_TITLE, environmentCatalog.getRegion(), apiCatalog.getName(), apiCatalog.getVersion(), environmentCatalog.getName());
				
				try {
		        	if(apiCatalog.getConfluencePageId() == null) {
		        		LOG.info("Creating Confluence Page for API: " + apiCatalog.getName() + " " + apiCatalog.getVersion());
				        apiReportAncestorId = utilitiesService.maintainConfluencePage("POST", environmentCatalog.getApiInventoryAncestorId(), apiReportPageTitle, apiReportPageTitle, false, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
				        apiCatalog.setConfluencePageId(apiReportAncestorId);
				        apiCatalogDAO.merge(apiCatalog);
		        	} else {
		        		LOG.info("Updating Confluence Page for API: " + apiCatalog.getName() + " " + apiCatalog.getVersion());
		        		apiReportAncestorId = apiCatalog.getConfluencePageId();
		        	}
			        List<ApiOperationCatalog> apiOperationCatalogList = apiOperationCatalogDAO.findApiOperationCatalogByApiCatalog(apiCatalog);
		        	for(ApiOperationCatalog apiOperationCatalog: apiOperationCatalogList) {
		        		Long apiOperationReportAncestorId = null;
						String apiOperationReportPageTitle = MessageFormat.format(IYamlBeansMasterConstants.OPERATION_TITLE, environmentCatalog.getRegion(), apiOperationCatalog.getMethod(), apiOperationCatalog.getBasePath(), apiOperationCatalog.getPath(), environmentCatalog.getName());
						String storageValue = null, responseFileName = null;
						JSONObject attachJSONResponse = null;
		    			try {
							if(apiOperationCatalog.getConfluencePageId() == null) {
				        		LOG.info("Creating Confluence Page for Operation: " + apiOperationCatalog.getMethod() + " " + apiOperationCatalog.getBasePath() + apiOperationCatalog.getPath());
				        		apiOperationReportAncestorId = utilitiesService.maintainConfluencePage("POST", apiReportAncestorId, apiOperationReportPageTitle, apiOperationReportPageTitle, false, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
				        		apiOperationCatalog.setConfluencePageId(apiOperationReportAncestorId);
				        	}
							List<Object[]> objects = apiHistoryCallDAO.analyticsByPathAndOrgIdAndMethodNative(apiOperationCatalog.getBasePath() + apiOperationCatalog.getPath(), apiOperationCatalog.getDevOrgId(), apiOperationCatalog.getMethod());
							if(objects.size() > 0) {
								String fileName = "Operation-" + apiOperationCatalog.getApiId() + "-" + apiOperationCatalog.getIdApiOperationCatalog();
				        		LOG.info("Updating Confluence Page " + apiOperationCatalog.getConfluencePageId() + " for Operation " + apiOperationCatalog.getIdApiOperationCatalog() + ": " + apiOperationCatalog.getMethod() + " " + apiOperationCatalog.getBasePath() + apiOperationCatalog.getPath());
						        File operationGraphicFile = utilitiesService.createAndStoreSplittedCandlestickChart("API Response Time", fileName, objects, apiOperationReportPageTitle);
						        String performanceAttachmentId = null;
						        if(apiOperationCatalog.getPerformanceAttachmentId() == null) {
						        	performanceAttachmentId = utilitiesService.getConfluenceAttachmentId(operationGraphicFile.getName(), IYamlBeansMasterConstants.CONFLUENCE_BASE_URI + apiOperationCatalog.getConfluencePageId() + "/child/attachment");
							        apiOperationCatalog.setPerformanceAttachmentId(performanceAttachmentId);
						        }
						        if(apiOperationCatalog.getPerformanceAttachmentId() == null) {
						        	attachJSONResponse = utilitiesService.attachFileToConfluencePage(IYamlBeansMasterConstants.CONFLUENCE_BASE_URI + apiOperationCatalog.getConfluencePageId() + "/child/attachment", operationGraphicFile);
									apiOperationCatalog.setPerformanceAttachmentId(attachJSONResponse.optString("id"));
									updateOperationPageContent = true;
						        } else {
						        	attachJSONResponse = utilitiesService.attachFileToConfluencePage(IYamlBeansMasterConstants.CONFLUENCE_BASE_URI + apiOperationCatalog.getConfluencePageId() + "/child/attachment/" + apiOperationCatalog.getPerformanceAttachmentId() + "/data", operationGraphicFile);
						        }
						        responseFileName = attachJSONResponse.optString("title");
				    			operationGraphicFile.deleteOnExit();
							}
							if(updateOperationPageContent) {
						        Map model = new HashMap();
				    	        model.put("operation", apiOperationCatalog);
				    	        if(responseFileName == null) {
				    	        	LOG.info("!No analytics on calls for operation: " + apiOperationCatalog.getIdApiOperationCatalog() + ": " + apiOperationCatalog.getMethod() + " " + apiOperationCatalog.getBasePath() + apiOperationCatalog.getPath());
				    	        }
				    	        model.put("fileName", responseFileName);
				    	        storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/operation.vm", model).replaceAll("\\r|\\n", "");
				        		LOG.info("Updating Confluence Page for Operation: " + apiOperationCatalog.getMethod() + " " + apiOperationCatalog.getBasePath() + apiOperationCatalog.getPath());
				        		utilitiesService.maintainConfluencePage("PUT", apiOperationCatalog.getConfluencePageId(), apiOperationReportPageTitle, storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
				        		apiOperationCatalogDAO.merge(apiOperationCatalog);
							}
		    			} catch (Exception e) {
		    				e.printStackTrace();
						}
		        	}
				} catch (Exception e) {
					e.printStackTrace();
					try {
						//If something happened at this level it means we were enable to create or update an API, then we wait 2 minutes and continue
						Thread.sleep(120000);
					} catch (InterruptedException e1) {
						e1.printStackTrace();
					}
				}
	        }
		}
	}
	
	public void publishBluemixAzureCompare(BluemixAzureCompareConfiguration bluemixAzureCompareConfiguration) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException {
		LOG.info("Publishing Comparisons between " +  bluemixAzureCompareConfiguration.getEnvironmentSource().getRegion() + "-" + bluemixAzureCompareConfiguration.getEnvironmentSource().getName() + " and " +  bluemixAzureCompareConfiguration.getEnvironmentTarget().getRegion() + "-" + bluemixAzureCompareConfiguration.getEnvironmentTarget().getName() + " to Confluence...: " + bluemixAzureCompareConfiguration.getConfluencePageId());
		Map model = new HashMap();
		List<Object[]> objects = bluemixAzureCompareConfigurationDAO.findDifferencesBetweenAzureAndBluemix(bluemixAzureCompareConfiguration.getEnvironmentTarget().getIdEnvironmentCatalog(), bluemixAzureCompareConfiguration.getEnvironmentSource().getId());
		List<ConfluenceBluemixAzureComparisonBean> confluenceBluemixAzureComparisonBeans = new ArrayList<ConfluenceBluemixAzureComparisonBean>();
	    for (int j = 0, sizeJ = objects.size(); j < sizeJ; j++) {
	    	Object[] object = objects.get(j);
			ConfluenceBluemixAzureComparisonBean confluenceBluemixAzureComparisonBean = new ConfluenceBluemixAzureComparisonBean();
			confluenceBluemixAzureComparisonBean.setAzureApiName(escapeHtml((String)object[0]));
			confluenceBluemixAzureComparisonBean.setAzureApiPath(escapeHtml((String)object[1]));
			confluenceBluemixAzureComparisonBean.setAzureServiceUrl((String)object[2]);
			confluenceBluemixAzureComparisonBean.setAzureMethod((String)object[3]);
			confluenceBluemixAzureComparisonBean.setAzureFullPath(escapeHtml((String)object[4]));
			confluenceBluemixAzureComparisonBean.setBluemixApiName((String)object[5]);
			confluenceBluemixAzureComparisonBean.setBluemixApiVersion((String)object[6]);
			confluenceBluemixAzureComparisonBean.setBluemixApiPath((String)object[7]);
			confluenceBluemixAzureComparisonBean.setBluemixFullPath((String)object[8]);
			confluenceBluemixAzureComparisonBean.setBluemixAzureHost((String)object[9]);
			confluenceBluemixAzureComparisonBean.setBluemixMethod((String)object[10]);
			confluenceBluemixAzureComparisonBeans.add(confluenceBluemixAzureComparisonBean);
	    }
        model.put("comparisons", confluenceBluemixAzureComparisonBeans);
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/bluemixazurecompare.vm", model).replaceAll("\\r|\\n", "");
        utilitiesService.maintainConfluencePage("PUT", bluemixAzureCompareConfiguration.getConfluencePageId(), MessageFormat.format(IYamlBeansMasterConstants.COMPARE_YAML_TITLE, bluemixAzureCompareConfiguration.getEnvironmentSource().getRegion() + "-" + bluemixAzureCompareConfiguration.getEnvironmentSource().getName(), bluemixAzureCompareConfiguration.getEnvironmentTarget().getRegion() + "-" + bluemixAzureCompareConfiguration.getEnvironmentTarget().getCatalogEnv()), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);   
	}
	
	public void publishAzureCompareResultConfluence(UUID guid, CompareConfiguration compareConfiguration) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException {
		LOG.info("Publishing Comparisons between " +  compareConfiguration.getEnvironmentSource().getRegion() + "-" + compareConfiguration.getEnvironmentSource().getName() + " and " +  compareConfiguration.getEnvironmentTarget().getRegion() + "-" + compareConfiguration.getEnvironmentTarget().getName() + " to Confluence...: " + compareConfiguration.getConfluencePageId() + " For GUID: " + guid.toString());
		Map model = new HashMap();
		List<TenantCompareSummary> summaries = tenantCompareSummaryDAO.findAllByReportGUID(guid.toString());
		for(TenantCompareSummary tenantCompareSummary: summaries) {
			boolean isSafePatch = true;
			for(TenantCompareDetail tenantCompareDetail: tenantCompareSummary.getTenantCompareDetails()) {
				if(tenantCompareDetail.getPlainTextDiffDelete() != null) {
					isSafePatch = false;
				}
			}
			tenantCompareSummary.setSafePatch(isSafePatch);
			for(ApiCompareSummary apiCompareSummary: tenantCompareSummary.getApiCompareSummaries()) {
				boolean isApiSafePatch = true;
				for(ApiCompareDetail apiCompareDetail: apiCompareSummary.getApiCompareDetails()) {
					if(apiCompareDetail.getPlainTextDiffDelete() != null) {
						isApiSafePatch = false;
					}
				}
				apiCompareSummary.setSafePatch(isApiSafePatch);
				for(OperationCompareSummary operationCompareSummary: apiCompareSummary.getOperationCompareSummaries()) {
					boolean isOperationSafePatch = true;
					for(OperationCompareDetail operationCompareDetail: operationCompareSummary.getOperationCompareDetails()) {
						if(operationCompareDetail.getPlainTextDiffDelete() != null) {
							isOperationSafePatch = false;
						}
					}
					operationCompareSummary.setSafePatch(isOperationSafePatch);
					operationCompareSummary.setTargetPolicyXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(operationCompareSummary.getTargetPolicyXml())));
					operationCompareSummary.setSourcePolicyXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(operationCompareSummary.getSourcePolicyXml())));
				}
				apiCompareSummary.setTargetPolicyXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(apiCompareSummary.getTargetPolicyXml())));
				apiCompareSummary.setSourcePolicyXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(apiCompareSummary.getSourcePolicyXml())));
			}
			tenantCompareSummary.setTargetPolicyXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(tenantCompareSummary.getTargetPolicyXml())));
			tenantCompareSummary.setSourcePolicyXml(StringEscapeUtils.unescapeHtml3(utilitiesService.prettyPrint(tenantCompareSummary.getSourcePolicyXml())));
		}
        model.put("summaries", summaries);
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/azurecompare.vm", model);
        String privateStorageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/privateazurecompare.vm", model);
        utilitiesService.maintainConfluencePage("PUT", compareConfiguration.getConfluencePageId(), MessageFormat.format(IYamlBeansMasterConstants.COMPARE_YAML_TITLE, compareConfiguration.getEnvironmentSource().getRegion() + "-" + compareConfiguration.getEnvironmentSource().getName(), compareConfiguration.getEnvironmentTarget().getRegion() + "-" + compareConfiguration.getEnvironmentTarget().getName()), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
        utilitiesService.maintainConfluencePage("PUT", compareConfiguration.getPrivateConfluencePageId(), MessageFormat.format(IYamlBeansMasterConstants.COMPARE_YAML_TITLE, compareConfiguration.getEnvironmentSource().getRegion() + "-" + compareConfiguration.getEnvironmentSource().getName(), compareConfiguration.getEnvironmentTarget().getRegion() + "-" + compareConfiguration.getEnvironmentTarget().getName()), privateStorageValue, true, IYamlBeansMasterConstants.PRIVATE_TECHNICAL_DEFINITION_SPACE_KEY); 
	}
	
	public void publishCompareResultConfluence(UUID guid, CompareConfiguration compareConfiguration) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException {
		LOG.info("Publishing Comparisons between " +  compareConfiguration.getEnvironmentSource().getRegion() + "-" + compareConfiguration.getEnvironmentSource().getName() + " and " +  compareConfiguration.getEnvironmentTarget().getRegion() + "-" + compareConfiguration.getEnvironmentTarget().getName() + " to Confluence...: " + compareConfiguration.getConfluencePageId());
		Map model = new HashMap();
		List<CompareSummary> summaries = compareSummaryDAO.findAllByReportGUID(guid.toString());
		for(CompareSummary compareSummary: summaries) {
			boolean isSafePatch = true;
			for(CompareDetail compareDetail: compareSummary.getCompareDetails()) {
				if(compareDetail.getPlainTextDiffDelete() != null) {
					isSafePatch = false;
				}
			}
			compareSummary.setSafePatch(isSafePatch);
		}
        model.put("summaries", summaries);
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/yamlcompare.vm", model).replaceAll("\\r|\\n", "");
        utilitiesService.maintainConfluencePage("PUT", compareConfiguration.getConfluencePageId(), MessageFormat.format(IYamlBeansMasterConstants.COMPARE_YAML_TITLE, compareConfiguration.getEnvironmentSource().getRegion() + "-" + compareConfiguration.getEnvironmentSource().getName(), compareConfiguration.getEnvironmentTarget().getRegion() + "-" + compareConfiguration.getEnvironmentTarget().getName()), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);   
	}
	
	public void maintainEnvironmentAPIInventory(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException {
		LOG.info("Updating API Inventory Page on Confluence...");
		Map model = new HashMap();
		List<ApiCatalog> apiCatalogList = apiCatalogDAO.findAllByDevOrgId(environmentCatalog.getId());
        model.put("apiCatalogList", apiCatalogList);
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/apicatalog.vm", model).replaceAll("\\r|\\n", "");
        utilitiesService.maintainConfluencePage("PUT", environmentCatalog.getApiInventoryAncestorId(), MessageFormat.format(IYamlBeansMasterConstants.API_INVENTORY_TITLE, environmentCatalog.getRegion(), environmentCatalog.getName()), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
	}
	
	public void maintainEnvironmentAzureAPIInventory(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException {
		LOG.info("Updating Azure API Inventory Page on Confluence...");
		Map model = new HashMap();
		List<AzureApiOperationCatalog> apiCatalogList = azureApiOperationCatalogDAO.findAllByEnvironment(environmentCatalog);
		for(AzureApiOperationCatalog apiOperationCatalog: apiCatalogList) {
			apiOperationCatalog.setName(escapeHtml(apiOperationCatalog.getName()));
			apiOperationCatalog.setUrlTemplate(escapeHtml(apiOperationCatalog.getUrlTemplate()));
		}
        model.put("apiCatalogList", apiCatalogList);
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/azureapicatalog.vm", model).replaceAll("\\r|\\n", "");
        LOG.info(storageValue);
        utilitiesService.maintainConfluencePage("PUT", environmentCatalog.getApiInventoryAncestorId(), MessageFormat.format(IYamlBeansMasterConstants.AZURE_API_INVENTORY_TITLE, environmentCatalog.getRegion(), environmentCatalog.getName()), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
	}
	
	public void maintainEnvironmentProductInventory(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException {
		LOG.info("Updating Product Inventory Page on Confluence...");
		Map model = new HashMap();
		List<ProductCatalog> productCatalogList = productCatalogDAO.findAllByDevOrgIdNotRemoved(environmentCatalog.getId());
        model.put("productCatalogList", productCatalogList);
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/productcatalog.vm", model).replaceAll("\\r|\\n", "");
        utilitiesService.maintainConfluencePage("PUT", environmentCatalog.getProductInventoryAncestorId(), MessageFormat.format(IYamlBeansMasterConstants.PRODUCT_INVENTORY_TITLE, environmentCatalog.getRegion(), environmentCatalog.getName()), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);   
	}

	public void createHistoricalReporteBySpace(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException {
		LOG.info("Updating Hitorical Report by Space " + environmentCatalog.getName() + " Page on Confluence...");
		List<Object[]> results = apiHistoryCallDAO.historicalReportByOrgNameAndCatalogAndDevOrgId(environmentCatalog.getDevOrgNameEnv(), environmentCatalog.getCatalogEnv(), environmentCatalog.getId());
		Map model = new HashMap(); 
        model.put("historicalReportBySpaces", loadHistoricalReportBySpaceBean(results));
        String storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/spaceanalytics.vm", model).replaceAll("\\r|\\n", "");
        utilitiesService.maintainConfluencePage("PUT", environmentCatalog.getSpaceInventoryAncestorId(), environmentCatalog.getRegion() + " Space " + environmentCatalog.getName(), storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
	}
	
	private List<HistoricalReportBySpace> loadHistoricalReportBySpaceBean(List<Object[]> results) {
		List<HistoricalReportBySpace> historicalReportBySpaces = new ArrayList<HistoricalReportBySpace>();
		DecimalFormat df = new DecimalFormat("#.00");
	    for (int j = 0, sizeJ = results.size(); j < sizeJ; j++) {
	    	Object[] result = results.get(j);
	    	HistoricalReportBySpace historicalReportBySpace = new HistoricalReportBySpace();
	    	historicalReportBySpace.setApiName((String)result[0]);
	    	historicalReportBySpace.setPath((String)result[1]);
	    	historicalReportBySpace.setRequestMethod((String)result[2]);
	    	historicalReportBySpace.setDevOrgName((String)result[3]);
	    	historicalReportBySpace.setProductName((String)result[4]);
	    	historicalReportBySpace.setRequestCount(df.format(((Number)result[5]).doubleValue()));
	    	historicalReportBySpace.setTotalTimeToServeRequest(df.format(((Number)result[6]).doubleValue()/1000D));
	    	historicalReportBySpace.setAverageTimeToServeRequest(df.format(((Number)result[7]).doubleValue()/1000D));
	    	historicalReportBySpace.setTotalBytesSent(df.format(((Number)result[8]).doubleValue()/1024D));
	    	historicalReportBySpace.setAverageBytesSent(df.format(((Number)result[9]).doubleValue()/1024D));
	    	historicalReportBySpace.setServiceEfficiency(df.format(((Number)result[8]).doubleValue()/((Number)result[6]).doubleValue()));
	    	historicalReportBySpace.setAverageServiceEfficiency(df.format(((Number)result[8]).doubleValue()/((Number)result[6]).doubleValue()/((Number)result[5]).doubleValue()));
	    	historicalReportBySpace.setLatestDateTime((Timestamp)result[11]);
	    	historicalReportBySpace.setLatestEpochDate(""+historicalReportBySpace.getLatestDateTime().getTime());
	    	try {
		    	ApiOperationCatalog apiOperationCatalog = apiOperationCatalogDAO.findApiOperationCatalogByApiNameAndPathAndMethodAndDevOrgId((String)result[0], (String)result[1], (String)result[2], (String)result[10]);
		    	historicalReportBySpace.setApiAncestorId(apiOperationCatalog.getApiCatalog().getConfluencePageId().toString());
		    	historicalReportBySpace.setOperationAncestorId(apiOperationCatalog.getConfluencePageId().toString());
		    	historicalReportBySpace.setAzureURL(apiOperationCatalog.getApiCatalog().getAzureHost() + "/" + apiOperationCatalog.getInvokeTargetURL());
	    	} catch (Exception e) {}
	    	historicalReportBySpaces.add(historicalReportBySpace);
	    }
	    return historicalReportBySpaces;
	}

	public void createDashboardReport(EnvironmentCatalog environmentCatalog) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, ParseException {
		LOG.info("Updating Dashboard Graphics " + environmentCatalog.getName() + " Page on Confluence...");
		JSONObject attachJSONResponse = null;
		String storageValue = null, responseFileName = null;
		File operationDashboardReportFile = utilitiesService.createDashboardReport(environmentCatalog);
    	String performanceAttachmentId = utilitiesService.getConfluenceAttachmentId(operationDashboardReportFile.getName(), IYamlBeansMasterConstants.CONFLUENCE_BASE_URI + environmentCatalog.getDashboardAncestorId() + "/child/attachment");
        if(performanceAttachmentId == null) {
        	attachJSONResponse = utilitiesService.attachFileToConfluencePage(IYamlBeansMasterConstants.CONFLUENCE_BASE_URI + environmentCatalog.getDashboardAncestorId() + "/child/attachment", operationDashboardReportFile);
        } else {
        	attachJSONResponse = utilitiesService.attachFileToConfluencePage(IYamlBeansMasterConstants.CONFLUENCE_BASE_URI + environmentCatalog.getDashboardAncestorId() + "/child/attachment/" + performanceAttachmentId + "/data", operationDashboardReportFile);
        }
        responseFileName = attachJSONResponse.optString("title");
        operationDashboardReportFile.deleteOnExit();
        Map model = new HashMap();
        model.put("fileName", responseFileName);
        storageValue = VelocityEngineUtils.mergeTemplateIntoString(velocityEngine, "mx/com/cemex/middleware/servicegovernancemanagement/velocity/dashboard.vm", model).replaceAll("\\r|\\n", "");
		utilitiesService.maintainConfluencePage("PUT", environmentCatalog.getDashboardAncestorId(), environmentCatalog.getName() + " Dashboard", storageValue, true, IYamlBeansMasterConstants.TECHNICAL_DEFINITION_SPACE_KEY);
	}
	
}
