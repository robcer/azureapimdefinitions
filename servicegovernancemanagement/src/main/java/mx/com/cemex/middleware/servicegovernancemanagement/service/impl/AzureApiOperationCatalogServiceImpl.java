package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;


import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.Api;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelEnvironmentOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.ApiOperationRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.ApiRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.RelEnvironmentOperationRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureApiOperationCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;


/**
 * 
 * @author RogelioReyo
 * 
 *
 */

@Service("azureApiOperationCatalogService")
public class AzureApiOperationCatalogServiceImpl implements AzureApiOperationCatalogService {
	
	private static final Logger LOG  = Logger.getLogger(AzureApiOperationCatalogServiceImpl.class);

	@Autowired
	AzureApiOperationCatalogDAO azureApiOperationCatalogDAO;
	
	@Autowired
	ApiOperationRepository apiOperationRepository;
	
	@Autowired
	ApiRepository apiRepository;
	
	@Autowired
	RelEnvironmentOperationRepository relEnvironmentOperationRepository;
	
	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	AzureOperationsService azureOperationsService;
	
	public void mantainAzureApiOperationCatalog(AzureApiCatalog azureApiCatalog, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException {
		LOG.info("Fetching operations from Azure API: " + azureApiCatalog.getId()  + " Environment: " + environmentCatalog.getCatalogEnv());
		List<AzureApiOperationCatalog> deployedAzureApiOperationCatalogs = azureOperationsService.getApiOperations(azureApiCatalog, environmentCatalog);
		AzureApiOperationCatalog persistedApiOperationCatalog = null;
		for(AzureApiOperationCatalog deployedAzureApiOperationCatalog: deployedAzureApiOperationCatalogs) {
			try {
				AzureApiOperationCatalog apiOperationCatalog = azureApiOperationCatalogDAO.findAzureApiOperationCatalogByIdAndEnvironmentCatalog(deployedAzureApiOperationCatalog.getId(), environmentCatalog);
				if(apiOperationCatalog != null) {
					apiOperationCatalog.setUpdatedAt(new Date());
					apiOperationCatalog.setRemoved(false);
					apiOperationCatalog.setName(deployedAzureApiOperationCatalog.getName());
					persistedApiOperationCatalog = azureApiOperationCatalogDAO.merge(apiOperationCatalog);
				}
			} catch (NoResultException e) {
				deployedAzureApiOperationCatalog.setEnvironmentCatalog(environmentCatalog);
				deployedAzureApiOperationCatalog.setAzureApiCatalog(azureApiCatalog);
				deployedAzureApiOperationCatalog.setRegisteredAt(new Date());
				deployedAzureApiOperationCatalog.setRemoved(false);
				persistedApiOperationCatalog = azureApiOperationCatalogDAO.create(deployedAzureApiOperationCatalog);
			}
		}
		try {
			String apiPolicy = azureOperationsService.getOperationPolicy(environmentCatalog, persistedApiOperationCatalog.getId());
				azureOperationsService.commitToBitbucketIntegration(apiPolicy, persistedApiOperationCatalog.getName(), environmentCatalog.getBitbucketBranch(), 
					IYamlBeansMasterConstants.BITBUCKET_REPOSITORY, "&quot;Rogelio Reyo Cachu <rogelio.reyocachu@ext.cemex.com>&quot;", 
					"/api-management/policies" + persistedApiOperationCatalog.getId() + persistedApiOperationCatalog.getId().subSequence(persistedApiOperationCatalog.getId().lastIndexOf("/"), persistedApiOperationCatalog.getId().length()) + ".xml", 
					"Initial version for " + persistedApiOperationCatalog.getName(), IYamlBeansMasterConstants.BITBUCKET_USER);
		} catch (Exception e) {
			e.printStackTrace();
		}
		maintainGenericApiCatalog(persistedApiOperationCatalog);
	}
	
	public void maintainGenericApiCatalog(AzureApiOperationCatalog persistedApiOperationCatalog) {
		if(persistedApiOperationCatalog != null && !persistedApiOperationCatalog.isRemoved()) {
			ApiOperation apiOperation = apiOperationRepository.findById(persistedApiOperationCatalog.getId());
			if(apiOperation == null) {
				Api api = apiRepository.findById(persistedApiOperationCatalog.getAzureApiCatalog().getId());
				if(api == null) {
					api = new Api();
					api.setApiRevision(persistedApiOperationCatalog.getAzureApiCatalog().getApiRevision());
					api.setCreatedAt(new Date());
					api.setDescription(persistedApiOperationCatalog.getAzureApiCatalog().getDescription());
					api.setDisplayName(persistedApiOperationCatalog.getAzureApiCatalog().getDisplayName());
					api.setId(persistedApiOperationCatalog.getAzureApiCatalog().getId());
					api.setName(persistedApiOperationCatalog.getAzureApiCatalog().getName());
					api.setPath(persistedApiOperationCatalog.getAzureApiCatalog().getPath());
					api.setProtocols(persistedApiOperationCatalog.getAzureApiCatalog().getProtocols());
					api = apiRepository.save(api);
				}
				apiOperation = new ApiOperation();
				apiOperation.setApi(api);
				apiOperation.setBasePath(persistedApiOperationCatalog.getAzureApiCatalog().getPath());
				apiOperation.setId(persistedApiOperationCatalog.getId());
				apiOperation.setMethod(persistedApiOperationCatalog.getMethod());
				apiOperation.setName(persistedApiOperationCatalog.getUrlTemplate());
				apiOperation.setRegisteredAt(new Date());
				apiOperation.setUrlTemplate(persistedApiOperationCatalog.getUrlTemplate());
				apiOperation.setFullPath(persistedApiOperationCatalog.getMethod() + "/" + persistedApiOperationCatalog.getAzureApiCatalog().getPath() + persistedApiOperationCatalog.getUrlTemplate());
				apiOperation.setCritical(false);
				apiOperation.setDeprecated(false);
				apiOperation = apiOperationRepository.save(apiOperation);
			}
			List<RelEnvironmentOperation> environmentOperations = relEnvironmentOperationRepository.findAllByEnvironmentCatalogAndApiOperation(persistedApiOperationCatalog.getEnvironmentCatalog(), apiOperation);
			if(environmentOperations.isEmpty()) {
				RelEnvironmentOperation environmentOperation = new RelEnvironmentOperation();
				environmentOperation.setApiOperation(apiOperation);
				environmentOperation.setEnvironmentCatalog(persistedApiOperationCatalog.getEnvironmentCatalog());
				environmentOperation.setRegisteredAt(new Date());
				relEnvironmentOperationRepository.save(environmentOperation);
			}
		}
	}
}
