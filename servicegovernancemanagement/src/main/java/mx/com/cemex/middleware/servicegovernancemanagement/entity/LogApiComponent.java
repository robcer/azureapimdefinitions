package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "locapicomponent")
public class LogApiComponent {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_log_api_component")
	private Long idLogApiComponent;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_api_catalog", nullable = true, updatable = true, insertable = true)
    private ApiCatalog apiCatalog;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_api_operation_catalog", nullable = true, updatable = true, insertable = true)
    private ApiOperationCatalog apiOperationCatalog;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_product_catalog", nullable = true, updatable = true, insertable = true)
    private ProductCatalog productCatalog;
    
    @Column(name = "api_id")
    private String apiId;
    
    @Column(name = "env_id")
    private String envId;
    
    @Column(name = "dev_org_id")
    private String devOrgId;
    
    @Column(name = "description", length=3000)
    private String description;

}
