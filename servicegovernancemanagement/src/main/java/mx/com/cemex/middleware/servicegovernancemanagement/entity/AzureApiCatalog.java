package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Filter;
import org.hibernate.annotations.FilterDef;
import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "azureapicatalog")
@FilterDef(name="notremoved", defaultCondition="removed = 0")
public class AzureApiCatalog {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_azure_api_catalog")
	private Long idAzureApiCatalog;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "name")
    private String name;
    
    @Column(name = "display_name")
    private String displayName;
    
    @Column(name = "api_revision")
    private String apiRevision;

    @Column(name = "description", length=3000)
    private String description;
    
    @Column(name = "service_url")
    private String serviceUrl;
    
    @Column(name = "path")
    private String path;
    
    @Column(name = "protocols")
    private String protocols;

    @Column(name = "removed_at")
    @DateTimeFormat(style = "S-")
    private Date removedAt;

    @Column(name = "created_at")
    @DateTimeFormat(style = "S-")
    private Date createdAt;

    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;
    
    @Column(name = "is_current")
    private boolean isCurrent;
    
    @Column(name = "removed")
    @NotNull
    private boolean removed;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_catalog", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentCatalog;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "azureApiCatalog")
    @Filter(name="notremoved")
    private Set<AzureApiOperationCatalog> apiOperationCatalogs = new HashSet<AzureApiOperationCatalog>();


	public boolean isRemoved() {
		return removed;
	}

	public void setRemoved(boolean removed) {
		this.removed = removed;
	}

	public Long getIdAzureApiCatalog() {
		return idAzureApiCatalog;
	}

	public void setIdAzureApiCatalog(Long idAzureApiCatalog) {
		this.idAzureApiCatalog = idAzureApiCatalog;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDisplayName() {
		return displayName;
	}

	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	public String getApiRevision() {
		return apiRevision;
	}

	public void setApiRevision(String apiRevision) {
		this.apiRevision = apiRevision;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getServiceUrl() {
		return serviceUrl;
	}

	public void setServiceUrl(String serviceUrl) {
		this.serviceUrl = serviceUrl;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public Date getRemovedAt() {
		return removedAt;
	}

	public void setRemovedAt(Date removedAt) {
		this.removedAt = removedAt;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public boolean isCurrent() {
		return isCurrent;
	}

	public void setCurrent(boolean isCurrent) {
		this.isCurrent = isCurrent;
	}

	public EnvironmentCatalog getEnvironmentCatalog() {
		return environmentCatalog;
	}

	public void setEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
		this.environmentCatalog = environmentCatalog;
	}

	public String getProtocols() {
		return protocols;
	}

	public void setProtocols(String protocols) {
		this.protocols = protocols;
	}

	public Set<AzureApiOperationCatalog> getApiOperationCatalogs() {
		return apiOperationCatalogs;
	}

	public void setApiOperationCatalogs(Set<AzureApiOperationCatalog> apiOperationCatalogs) {
		this.apiOperationCatalogs = apiOperationCatalogs;
	}
    
    
    
}
