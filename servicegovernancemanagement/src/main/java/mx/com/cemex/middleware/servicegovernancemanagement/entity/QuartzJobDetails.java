package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

@Entity
@Table(name = "quartzjobdetails")
public class QuartzJobDetails implements Serializable {

	private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_quartz_job_details")
	private Long idQuartzJobDetails;
	
	@Column(name = "sched_name")
    @Size(max = 120)
	private String schedName;

    @Column(name = "job_name")
    @Size(max = 80)
	private String jobName;

    @Column(name = "job_group")
    @Size(max = 80)
	private String jobGroup;

	@Column(name = "description")
    @Size(max = 120)
    private String description;
    
    @Column(name = "application")
    @Size(max = 120)
    private String application;
    
    @Column(name = "url_monitor")
    @Size(max = 120)
    private String urlMonitor;
    
    @Column(name = "reg_exp_monitor")
    @Size(max = 120)
    private String regExpMonitor;
    
    @Column(name = "trigger_name")
    private String triggerName;
    
    @Column(name = "job_expression")
    private String jobExpression;

	@Column(name = "configuration_date")
    private Date configurationDate;

	public String getTriggerName() {
		return triggerName;
	}

	public void setTriggerName(String triggerName) {
		this.triggerName = triggerName;
	}

	public String getJobExpression() {
		return jobExpression;
	}

	public void setJobExpression(String jobExpression) {
		this.jobExpression = jobExpression;
	}

	public Long getIdQuartzJobDetails() {
		return idQuartzJobDetails;
	}

	public void setIdQuartzJobDetails(Long idQuartzJobDetails) {
		this.idQuartzJobDetails = idQuartzJobDetails;
	}

	public String getSchedName() {
		return schedName;
	}

	public void setSchedName(String schedName) {
		this.schedName = schedName;
	}

	public String getJobName() {
		return jobName;
	}

	public void setJobName(String jobName) {
		this.jobName = jobName;
	}

	public String getJobGroup() {
		return jobGroup;
	}

	public void setJobGroup(String jobGroup) {
		this.jobGroup = jobGroup;
	}
    
    public String getRegExpMonitor() {
		return regExpMonitor;
	}

	public void setRegExpMonitor(String regExpMonitor) {
		this.regExpMonitor = regExpMonitor;
	}

	public String getUrlMonitor() {
		return urlMonitor;
	}

	public void setUrlMonitor(String urlMonitor) {
		this.urlMonitor = urlMonitor;
	}	

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getApplication() {
		return application;
	}

	public void setApplication(String application) {
		this.application = application;
	}

	public Date getConfigurationDate() {
		return configurationDate;
	}

	public void setConfigurationDate(Date configurationDate) {
		this.configurationDate = configurationDate;
	}
    
    
}
