package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.context.SecurityContextHolder;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelUserOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.exception.CustomException;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.RelUserOperationRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.RelUserOperationService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UserService;

@Named("relUserOperationService")
public class RelUserOperationServiceImpl extends CrudServiceImpl<RelUserOperation, Long, RelUserOperationRepository> implements RelUserOperationService {
	
	@Autowired
	RelUserOperationRepository relUserOperationRepository;
	
	@Autowired
	UserService userService;

    @Override
    @Inject
    public void setRepository(RelUserOperationRepository relUserOperationRepository) {
        super.setRepository(relUserOperationRepository);
    }

	@Override
	public List<RelUserOperation> findByUser(User user) {
		List<RelUserOperation> relUserOperations = this.relUserOperationRepository.findAllByUser(userService.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName()));
		if (relUserOperations == null) {
			throw new CustomException("UserRole doesn't have linked applications.", HttpStatus.NOT_FOUND);
		}
		return relUserOperations;
	}

}