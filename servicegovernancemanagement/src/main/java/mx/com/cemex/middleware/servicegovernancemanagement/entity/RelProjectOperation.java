package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "relprojectoperation")
public class RelProjectOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_project_operation")
	private Long idProjectOperation;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_api_operation", nullable = false, updatable = true, insertable = true)
    private ApiOperation apiOperation;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_project_catalog", nullable = false, updatable = true, insertable = true)
    private ProjectCatalog projectCatalog;

	public Long getIdProjectOperation() {
		return idProjectOperation;
	}

	public void setIdProjectOperation(Long idProjectOperation) {
		this.idProjectOperation = idProjectOperation;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public ApiOperation getApiOperation() {
		return apiOperation;
	}

	public void setApiOperation(ApiOperation apiOperation) {
		this.apiOperation = apiOperation;
	}

	public ProjectCatalog getProjectCatalog() {
		return projectCatalog;
	}

	public void setProjectCatalog(ProjectCatalog projectCatalog) {
		this.projectCatalog = projectCatalog;
	}
}
