package mx.com.cemex.middleware.servicegovernancemanagement.service;

import javax.servlet.http.HttpServletRequest;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.LoginResponse;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;

public interface UserService extends CrudService<User, Long> {

	LoginResponse signin(String username, String password);
	User search(String username);
	User whoami(HttpServletRequest httpServletRequest);
	User findByUsername(String username);
}
