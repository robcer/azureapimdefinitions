package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "yamlappprogress")
public class YamlAppProgress {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Column(name = "id_yaml_app_progress")
	private Long idYamlAppProgress;

	@com.ebay.xcelite.annotations.Column(name = "AppName")
    @javax.persistence.Column(name = "app_name")
	private String appName;

	@com.ebay.xcelite.annotations.Column(name = "Deprecated")
    @javax.persistence.Column(name = "deprecated")
	private String deprecated;

	@com.ebay.xcelite.annotations.Column(name = "StoreProcedure")
    @javax.persistence.Column(name = "store_procedure")
	private String storeProcedure;

    @NotNull
	@com.ebay.xcelite.annotations.Column(name = "ServiceName")
    @javax.persistence.Column(name = "service_name")
	private String serviceName;

	@com.ebay.xcelite.annotations.Column(name = "ServiceId")
    @javax.persistence.Column(name = "service_id")
	private String serviceId;

	@com.ebay.xcelite.annotations.Column(name = "Operation")
    @javax.persistence.Column(name = "operation")
	private String operation;

	private String fullPath;

    @Column(name = "created_at")
    @DateTimeFormat(style = "S-")
    private Date createdAt;

    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getServiceName() {
		return serviceName;
	}

	public void setServiceName(String serviceName) {
		this.serviceName = serviceName;
	}

	public String getServiceId() {
		return serviceId;
	}

	public void setServiceId(String serviceId) {
		this.serviceId = serviceId;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getOperation() {
		return operation;
	}

	public void setOperation(String operation) {
		this.operation = operation;
	}

	public Long getIdYamlAppProgress() {
		return idYamlAppProgress;
	}

	public void setIdYamlAppProgress(Long idYamlAppProgress) {
		this.idYamlAppProgress = idYamlAppProgress;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getDeprecated() {
		return deprecated;
	}

	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}

	public String getStoreProcedure() {
		return storeProcedure;
	}

	public void setStoreProcedure(String storeProcedure) {
		this.storeProcedure = storeProcedure;
	}
}
