package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

@Entity
@Table(name = "usercatalog")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Integer userId;

	@Size(min = 4, max = 255, message = "Minimum username length: 4 characters")
	@Column(name = "username", unique = true, nullable = false)
	@JsonProperty(value = "userAccount")
	private String username;

	@Column(name = "email", unique = true, nullable = false)
	private String email;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "phone_number")
    @JsonIgnore
	private String phoneNumber;

	@Column(name = "status")
	private String status;

	@Column(name = "user_type")
	private String userType;

	@Column(name = "user_position")
	private String userPosition;

	@Column(name = "allow_information_share")
	private Integer allowInformationShare;

	@Column(name = "allow_email_updates")
	private Integer allowEmailUpdates;

	@Column(name = "full_name")
	private String fullName;

	@Size(min = 8, message = "Minimum password length: 8 characters")
    @JsonIgnore
	private String password;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
	private Set<UserRole> roles = new HashSet<UserRole>();

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Set<UserRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<UserRole> roles) {
		this.roles = roles;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public String getUserPosition() {
		return userPosition;
	}

	public void setUserPosition(String userPosition) {
		this.userPosition = userPosition;
	}

	public Integer getAllowInformationShare() {
		return allowInformationShare;
	}

	public void setAllowInformationShare(Integer allowInformationShare) {
		this.allowInformationShare = allowInformationShare;
	}

	public Integer getAllowEmailUpdates() {
		return allowEmailUpdates;
	}

	public void setAllowEmailUpdates(Integer allowEmailUpdates) {
		this.allowEmailUpdates = allowEmailUpdates;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

}
