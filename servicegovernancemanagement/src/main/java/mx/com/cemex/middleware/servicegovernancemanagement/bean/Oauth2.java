package mx.com.cemex.middleware.servicegovernancemanagement.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Oauth2 {
	
	@JsonProperty(value="access_token")
	private String accessToken;
	
	@JsonProperty(value="expires_in")
	private Long expiresIn;
	
	@JsonProperty(value="scope")
	private String scope;
	
	@JsonProperty(value="refresh_token")
	private String refreshToken;
	
	@JsonProperty(value="region")
	private String region;

	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

	public Long getExpiresIn() {
		return expiresIn;
	}

	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}

	public String getScope() {
		return scope;
	}

	public void setScope(String scope) {
		this.scope = scope;
	}

	public String getRefreshToken() {
		return refreshToken;
	}

	public void setRefreshToken(String refreshToken) {
		this.refreshToken = refreshToken;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}
	

}
