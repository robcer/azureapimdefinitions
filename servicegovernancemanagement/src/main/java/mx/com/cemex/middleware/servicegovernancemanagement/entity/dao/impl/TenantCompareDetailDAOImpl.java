package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.TenantCompareDetailDAO;

@Service("tenantCompareDetailDAO")
public class TenantCompareDetailDAOImpl implements TenantCompareDetailDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public TenantCompareDetailDAOImpl()
    {
    }

    TenantCompareDetailDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public TenantCompareDetail create(TenantCompareDetail tenantCompareDetail) {
        em.persist(tenantCompareDetail);
        return tenantCompareDetail;
    }

    @Transactional
    public TenantCompareDetail merge(TenantCompareDetail tenantCompareDetail) {
        em.merge(tenantCompareDetail);
        return tenantCompareDetail;
    }

    public long countCompareDetail() {
        return (Long) em.createQuery("select count(o) from TenantCompareDetail o").getSingleResult();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from TenantCompareDetail");
    	query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    public List<TenantCompareDetail> findTenantCompareDetail(int firstResult, int maxResults) {
        return em.createQuery("select o from TenantCompareDetail o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public TenantCompareDetail findTenantCompareDetail(Long id) {
        if (id == null) return null;
        return em.find(TenantCompareDetail.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<TenantCompareDetail> findAll() {
        return em.createQuery("select o from TenantCompareDetail o order by o.idTenantCompareDetail DESC").getResultList();
    }
    
    public List<TenantCompareDetail> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from TenantCompareDetail o where o.devOrgNameEnv = :devOrgNameEnv", TenantCompareDetail.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public TenantCompareDetail findTenantCompareDetailById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from TenantCompareDetail o where o.id = :id", TenantCompareDetail.class)
        		.setParameter("id", id).getSingleResult();
    }

}