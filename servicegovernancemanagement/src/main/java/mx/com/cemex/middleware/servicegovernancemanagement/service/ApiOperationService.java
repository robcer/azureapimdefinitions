package mx.com.cemex.middleware.servicegovernancemanagement.service;

import org.resthub.common.service.CrudService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperation;

public interface ApiOperationService extends CrudService<ApiOperation, Long> {
	
	Page<ApiOperation> myOperations(Pageable pageable);

}
