package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureOperationsService;

@Controller
@RequestMapping(value = "/v1/its")
public class AzureOperationsController {
	
	private static final Logger LOG  = Logger.getLogger(ApiGovernanceController.class);
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
	@Autowired
	AzureApiOperationCatalogDAO azureApiOperationCatalogDAO;
	
	@Autowired
	AzureApiCatalogDAO azureApiCatalogDAO;
	
	@Autowired
	AzureOperationsService azureOperationsService;
	
    @RequestMapping(value = "/operations/{idApiOperationCatalog}/environments/{idEnvironmentCatalog}/deployed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AzureApiOperationCatalog operationMigrateToEnvironment(@PathVariable("idApiOperationCatalog") Long idApiOperationCatalog, @PathVariable("idEnvironmentCatalog") Long idEnvironmentCatalog) throws JsonParseException, JsonMappingException, RestClientException, IOException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException {
    	LOG.info("Starting the execution of operationMigrateToEnvironment();");
    	return azureOperationsService.migrateOperationIntoAPITargetTenant(environmentCatalogDAO.findEnvironmentCatalog(idEnvironmentCatalog), azureApiOperationCatalogDAO.findAzureApiOperationCatalog(idApiOperationCatalog));
    }
	
    @RequestMapping(value = "/apis/{idAzureApiCatalog}/environments/{idEnvironmentCatalog}/deployed", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AzureApiCatalog migrateAPIIntoTargetTenant(@PathVariable("idAzureApiCatalog") Long idAzureApiCatalog, @PathVariable("idEnvironmentCatalog") Long idEnvironmentCatalog) throws JsonParseException, JsonMappingException, RestClientException, IOException, InterruptedException, InvalidKeyException, NoSuchAlgorithmException, ParseException, org.json.simple.parser.ParseException {
    	LOG.info("Starting the execution of operationMigrateAPIPolicy();");
    	return azureOperationsService.migrateAPIIntoTargetTenant(environmentCatalogDAO.findEnvironmentCatalog(idEnvironmentCatalog), azureApiCatalogDAO.findAzureApiCatalog(idAzureApiCatalog));
    }
    
    

}
