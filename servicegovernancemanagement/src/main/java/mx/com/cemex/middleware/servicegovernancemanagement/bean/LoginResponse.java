package mx.com.cemex.middleware.servicegovernancemanagement.bean;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;

public class LoginResponse {
	
	@JsonProperty(value="oauth2")
	private Oauth2 oauth2;
	
	@JsonProperty(value="jwt")
	private String jwt;
	
	@JsonProperty(value="powerUser")
	private String powerUser = "true";
	
	@JsonProperty(value="edit")
	private String edit = "true";
	
	@JsonProperty(value="customerId")
	private Integer customerId = 1;
	
	@JsonProperty(value="sessionId")
	private Integer sessionId = 1;
	
	@JsonProperty(value="role")
	private String role = "E";
	
	@JsonProperty(value="country")
	private String country = "MX";

	@JsonProperty(value="applications")
	private List<LoginApplicationResponse> applications;

	@JsonProperty(value="profile")
	private User user;

	@JsonProperty(value="customer")
	private Customer customer = new Customer();
	
	public Oauth2 getOauth2() {
		return oauth2;
	}

	public void setOauth2(Oauth2 oauth2) {
		this.oauth2 = oauth2;
	}

	public String getJwt() {
		return jwt;
	}

	public void setJwt(String jwt) {
		this.jwt = jwt;
	}

	public String getPowerUser() {
		return powerUser;
	}

	public void setPowerUser(String powerUser) {
		this.powerUser = powerUser;
	}

	public String getEdit() {
		return edit;
	}

	public void setEdit(String edit) {
		this.edit = edit;
	}

	public Integer getCustomerId() {
		return customerId;
	}

	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public List<LoginApplicationResponse> getApplications() {
		return applications;
	}

	public void setApplications(List<LoginApplicationResponse> applications) {
		this.applications = applications;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

}

class Customer {
	
	private Integer customerId = 1;
	private String customerDesc = "Customer Information Description";
	private Integer userId = 1;
	private String customerCode = "CUSTOMERCODE001";
	public Integer getCustomerId() {
		return customerId;
	}
	public void setCustomerId(Integer customerId) {
		this.customerId = customerId;
	}
	public String getCustomerDesc() {
		return customerDesc;
	}
	public void setCustomerDesc(String customerDesc) {
		this.customerDesc = customerDesc;
	}
	public Integer getUserId() {
		return userId;
	}
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	public String getCustomerCode() {
		return customerCode;
	}
	public void setCustomerCode(String customerCode) {
		this.customerCode = customerCode;
	}
	
	
}