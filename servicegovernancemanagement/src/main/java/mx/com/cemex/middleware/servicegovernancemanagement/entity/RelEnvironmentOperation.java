package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "relenvironmentoperation")
public class RelEnvironmentOperation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_environment_operation")
	private Long idEnvironmentOperation;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_catalog", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentCatalog;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonIgnore
    @JoinColumn(name = "id_api_operation", nullable = false, updatable = true, insertable = true)
    private ApiOperation apiOperation;

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}
	
	public ApiOperation getApiOperation() {
		return apiOperation;
	}

	public void setApiOperation(ApiOperation apiOperation) {
		this.apiOperation = apiOperation;
	}

	public Long getIdEnvironmentOperation() {
		return idEnvironmentOperation;
	}

	public void setIdEnvironmentOperation(Long idEnvironmentOperation) {
		this.idEnvironmentOperation = idEnvironmentOperation;
	}

	public EnvironmentCatalog getEnvironmentCatalog() {
		return environmentCatalog;
	}

	public void setEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
		this.environmentCatalog = environmentCatalog;
	}

    
}
