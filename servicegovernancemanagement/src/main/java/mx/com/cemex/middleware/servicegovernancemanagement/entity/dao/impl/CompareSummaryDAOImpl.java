package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.CompareSummaryDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.GenericEntityDAO;

@Service("compareSummaryDAO")
public class CompareSummaryDAOImpl extends AbstractEntityDAOImpl<CompareSummary> implements CompareSummaryDAO {

	GenericEntityDAO<CompareSummary> genericEntityDAO;

	@Autowired
	public void setDao(GenericEntityDAO<CompareSummary> daoToSet) {
		genericEntityDAO = daoToSet;
		genericEntityDAO.setEntityClass(CompareSummary.class);
	}

    
    public CompareSummary findCompareSummary(Long id) {
        if (id == null) return null;
        return em.find(CompareSummary.class, id);
    }
    
    public List<Object> findTargetFileNameByReportGUID(String reportGuid) {
    	@SuppressWarnings("unchecked")
    	List<Object> results = em.createNativeQuery("select target_file_name from CompareSummary o where o.report_Guid = ?1 order by o.file_Name asc")
        		.setParameter(1, reportGuid).getResultList();
    	return results;
    }
    
    public List<CompareSummary> findAllByReportGUID(String reportGuid) {
        if (reportGuid == null) return null;
        return em.createQuery("select o from CompareSummary o where o.reportGuid = :reportGuid order by o.fileName asc", CompareSummary.class).setParameter("reportGuid", reportGuid).getResultList();
    }

}