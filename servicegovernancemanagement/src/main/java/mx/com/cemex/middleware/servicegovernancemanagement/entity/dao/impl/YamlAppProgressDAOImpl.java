package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlAppProgress;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlAppProgressDAO;

@Service("yamlAppProgressDAO")
public class YamlAppProgressDAOImpl implements YamlAppProgressDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public YamlAppProgressDAOImpl()
    {
    }

    YamlAppProgressDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public YamlAppProgress create(YamlAppProgress yamlAppProgress) {
        em.persist(yamlAppProgress);
        return yamlAppProgress;
    }

    @Transactional
    public YamlAppProgress merge(YamlAppProgress yamlAppProgress) {
        em.merge(yamlAppProgress);
        return yamlAppProgress;
    }

    public long countYamlAppProgress() {
        return (Long) em.createQuery("select count(o) from YamlAppProgress o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<YamlAppProgress> findYamlAppProgress(int firstResult, int maxResults) {
        return em.createQuery("select o from YamlAppProgress o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public YamlAppProgress findYamlAppProgress(Long id) {
        if (id == null) return null;
        return em.find(YamlAppProgress.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<YamlAppProgress> findAll() {
        return em.createQuery("select o from YamlAppProgress o").getResultList();
    }
    
    public YamlAppProgress findYamlAppProgresByServiceName(String serviceName, String fullPath) {
        if (serviceName == null) return null;
        return em.createQuery("select o from YamlAppProgress o where o.serviceName = :serviceName and o.fullPath = :fullPath", YamlAppProgress.class)
        		.setParameter("serviceName", serviceName).setParameter("fullPath", fullPath).getSingleResult();
    }
}
