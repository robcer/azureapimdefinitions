package mx.com.cemex.middleware.servicegovernancemanagement;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.BeansException;

import mx.com.cemex.middleware.servicegovernancemanagement.service.ApplicationRoleService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApplicationService;

public class TestApplicationMenuList {
	
	private static final Logger LOG  = Logger.getLogger(APIConnectGovernanceMain.class);
	
	public static void main(String[] args) throws BeansException, IOException, InterruptedException {
		LOG.info("Starting standalone process on APIConnectGovernanceMain");
		ApplicationRoleService applicationRoleService = ((ApplicationRoleService) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("applicationRoleService"));
		ApplicationService applicationService = ((ApplicationService) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("applicationService"));
		((ApplicationRoleService) IConstantApplicationContext.APPLICATION_CONTEXT.getBean("applicationRoleService")).findApplicationRolesByAuthorities();
		System.out.println(applicationRoleService.findApplicationRolesByAuthorities());
		applicationService.findByRolesIn(applicationRoleService.findApplicationRolesByAuthorities());

	}

}
