package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import org.springframework.security.core.GrantedAuthority;

public enum Role implements GrantedAuthority {
  ROLE_ADMIN, ROLE_DEVELOPER, ROLE_CONFIGURATIONMANAGER;

  public String getAuthority() {
    return name();
  }

}
