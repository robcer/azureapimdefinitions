package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
@Table(name = "tenantcomparedetail")
public class TenantCompareDetail {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_tenant_compare_detail")
	private Long idTenantCompareDetail;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "report_guid")
    private String reportGuid;
    
    @Column(name = "plain_text_diff_insert")
	@Lob
    private String plainTextDiffInsert;
    
    @Column(name = "plain_text_diff_delete")
	@Lob
    private String plainTextDiffDelete;

    @ManyToOne(fetch = FetchType.LAZY, cascade=CascadeType.REMOVE)
    @JsonIgnore
    @JoinColumn(name = "id_tenant_compare_summary", nullable = false, updatable = true, insertable = true)
    private TenantCompareSummary tenantCompareSummary;
    
    @Column(name = "source_starting_line")
	private Long sourceStartingLine;
    
    @Column(name = "target_starting_line")
	private Long targetStartingLine;


	public Long getIdTenantCompareDetail() {
		return idTenantCompareDetail;
	}

	public void setIdTenantCompareDetail(Long idTenantCompareDetail) {
		this.idTenantCompareDetail = idTenantCompareDetail;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getReportGuid() {
		return reportGuid;
	}

	public void setReportGuid(String reportGuid) {
		this.reportGuid = reportGuid;
	}

	public TenantCompareSummary getTenantCompareSummary() {
		return tenantCompareSummary;
	}

	public void setTenantCompareSummary(TenantCompareSummary tenantCompareSummary) {
		this.tenantCompareSummary = tenantCompareSummary;
	}

	public String getPlainTextDiffInsert() {
		return plainTextDiffInsert;
	}

	public void setPlainTextDiffInsert(String plainTextDiffInsert) {
		this.plainTextDiffInsert = plainTextDiffInsert;
	}

	public String getPlainTextDiffDelete() {
		return plainTextDiffDelete;
	}

	public void setPlainTextDiffDelete(String plainTextDiffDelete) {
		this.plainTextDiffDelete = plainTextDiffDelete;
	}

	public Long getSourceStartingLine() {
		return sourceStartingLine;
	}

	public void setSourceStartingLine(Long sourceStartingLine) {
		this.sourceStartingLine = sourceStartingLine;
	}

	public Long getTargetStartingLine() {
		return targetStartingLine;
	}

	public void setTargetStartingLine(Long targetStartingLine) {
		this.targetStartingLine = targetStartingLine;
	}
    

}
