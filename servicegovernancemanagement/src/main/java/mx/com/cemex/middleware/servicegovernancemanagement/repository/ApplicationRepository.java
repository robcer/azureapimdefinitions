package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import java.util.Collection;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.Application;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;

public interface ApplicationRepository extends JpaRepository<Application, Long> {

	List<Application> findByRolesIn(Collection<ApplicationRole> roles);

}
