package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "yamlproperties")
public class YamlProperties {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Column(name = "id_yaml_properties")
	private Long idYamlProperties;

	@com.ebay.xcelite.annotations.Column(name = "Product")
    @javax.persistence.Column(name = "product")
	private String product;

	@com.ebay.xcelite.annotations.Column(name = "ProductVersion")
    @javax.persistence.Column(name = "product_version")
	private String productVersion;

	@com.ebay.xcelite.annotations.Column(name = "APIName")
    @javax.persistence.Column(name = "api_name")
	private String apiName;

	@com.ebay.xcelite.annotations.Column(name = "APIVersion")
    @javax.persistence.Column(name = "api_version")
	private String apiVersion;

	@com.ebay.xcelite.annotations.Column(name = "BasePath")
    @javax.persistence.Column(name = "base_path")
	private String basePath;

	@com.ebay.xcelite.annotations.Column(name = "Path")
    @javax.persistence.Column(name = "path")
	private String path;

	@com.ebay.xcelite.annotations.Column(name = "Method")
    @javax.persistence.Column(name = "method")
	private String method;

	@com.ebay.xcelite.annotations.Column(name = "FullPath")
    @javax.persistence.Column(name = "full_path")
	private String fullPath;

	@com.ebay.xcelite.annotations.Column(name = "OperationId")
    @javax.persistence.Column(name = "operation_id")
	private String operationId;

	@com.ebay.xcelite.annotations.Column(name = "OperationDescription")
	@Lob
    @javax.persistence.Column(name = "operation_description")
	private String operationDescription;

	@com.ebay.xcelite.annotations.Column(name = "Responsible")
    @javax.persistence.Column(name = "responsible")
	private String responsible;

	@com.ebay.xcelite.annotations.Column(name = "Security")
    @javax.persistence.Column(name = "security")
	private String security;

	@com.ebay.xcelite.annotations.Column(name = "Email")
    @javax.persistence.Column(name = "email")
	private String email;

	@com.ebay.xcelite.annotations.Column(name = "Description")
	@Lob
    @javax.persistence.Column(name = "description")
	private String description;

	@com.ebay.xcelite.annotations.Column(name = "x-ibm-name")
    @javax.persistence.Column(name = "x_ibm_name")
	private String xIbmName;

	@com.ebay.xcelite.annotations.Column(name = "ProductName")
    @javax.persistence.Column(name = "product_name")
	private String productName;

	@com.ebay.xcelite.annotations.Column(name = "Project")
    @javax.persistence.Column(name = "project")
	private String project;

	@com.ebay.xcelite.annotations.Column(name = "BC")
    @javax.persistence.Column(name = "bc")
	private String bc;

	@com.ebay.xcelite.annotations.Column(name = "NETPath")
    @javax.persistence.Column(name = "neth_path")
	private String netPath;

	@com.ebay.xcelite.annotations.Column(name = "StoredProcedure")
    @javax.persistence.Column(name = "store_procedure")
	private String storeProcedure;

	@com.ebay.xcelite.annotations.Column(name = "StoredProcedureAssigned")
    @javax.persistence.Column(name = "stored_procedure_assigned")
	private String storeProcedureAssigned;

	@com.ebay.xcelite.annotations.Column(name = "ReportedResponsible")
    @javax.persistence.Column(name = "reported_responsible")
	private String reportedResponsible;

	@com.ebay.xcelite.annotations.Column(name = "IsIntegratedWithNET")
    @javax.persistence.Column(name = "is_integrated_with_net")
	private String isIntegratedWithNET;

	@com.ebay.xcelite.annotations.Column(name = "AzureHost")
    @javax.persistence.Column(name = "azure_host")
	private String azureHost;

	@com.ebay.xcelite.annotations.Column(name = "IsCasePrepared")
    @javax.persistence.Column(name = "is_case_prepared")
	private String isCasePrepared;

	@com.ebay.xcelite.annotations.Column(name = "InvokeTargetURL")
    @javax.persistence.Column(name = "invoke_target_url")
	private String invokeTargetURL;

	@com.ebay.xcelite.annotations.Column(name = "CountPathResourceMethod")
    @javax.persistence.Column(name = "count_path_resource_method")
	private String countPathResourceMethod;

    @javax.persistence.Column(name = "dev_org_id")
	private String devOrgId;

    @javax.persistence.Column(name = "environment")
	private String environment;

	@com.ebay.xcelite.annotations.Column(name = "Deprecated")
    @javax.persistence.Column(name = "Deprecated")
	private String deprecated;

    @Column(name = "created_at")
    @DateTimeFormat(style = "S-")
    private Date createdAt;

    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;

    @Column(name = "deleted_at")
    @DateTimeFormat(style = "S-")
    private Date deletedAt;

	public String getProduct() {
		return product;
	}

	public void setProduct(String product) {
		this.product = product;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getApiName() {
		return apiName;
	}

	public void setApiName(String apiName) {
		this.apiName = apiName;
	}

	public String getApiVersion() {
		return apiVersion;
	}

	public void setApiVersion(String apiVersion) {
		this.apiVersion = apiVersion;
	}

	public String getBasePath() {
		return basePath;
	}

	public void setBasePath(String basePath) {
		this.basePath = basePath;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getMethod() {
		return method;
	}

	public void setMethod(String method) {
		this.method = method;
	}

	public String getOperationId() {
		return operationId;
	}

	public void setOperationId(String operationId) {
		this.operationId = operationId;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getSecurity() {
		return security;
	}

	public void setSecurity(String security) {
		this.security = security;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getxIbmName() {
		return xIbmName;
	}

	public void setxIbmName(String xIbmName) {
		this.xIbmName = xIbmName;
	}

	public String getOperationDescription() {
		return operationDescription;
	}

	public void setOperationDescription(String operationDescription) {
		this.operationDescription = operationDescription;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getFullPath() {
		return fullPath;
	}

	public void setFullPath(String fullPath) {
		this.fullPath = fullPath;
	}

	public String getProject() {
		return project;
	}

	public void setProject(String project) {
		this.project = project;
	}

	public String getBc() {
		return bc;
	}

	public void setBc(String bc) {
		this.bc = bc;
	}

	public String getNetPath() {
		return netPath;
	}

	public void setNetPath(String netPath) {
		this.netPath = netPath;
	}

	public String getStoreProcedure() {
		return storeProcedure;
	}

	public void setStoreProcedure(String storeProcedure) {
		this.storeProcedure = storeProcedure;
	}

	public String getStoreProcedureAssigned() {
		return storeProcedureAssigned;
	}

	public void setStoreProcedureAssigned(String storeProcedureAssigned) {
		this.storeProcedureAssigned = storeProcedureAssigned;
	}

	public String getReportedResponsible() {
		return reportedResponsible;
	}

	public void setReportedResponsible(String reportedResponsible) {
		this.reportedResponsible = reportedResponsible;
	}

	public String getIsIntegratedWithNET() {
		return isIntegratedWithNET;
	}

	public void setIsIntegratedWithNET(String isIntegratedWithNET) {
		this.isIntegratedWithNET = isIntegratedWithNET;
	}

	public String getAzureHost() {
		return azureHost;
	}

	public void setAzureHost(String azureHost) {
		this.azureHost = azureHost;
	}

	public String getIsCasePrepared() {
		return isCasePrepared;
	}

	public void setIsCasePrepared(String isCasePrepared) {
		this.isCasePrepared = isCasePrepared;
	}

	public String getInvokeTargetURL() {
		return invokeTargetURL;
	}

	public void setInvokeTargetURL(String invokeTargetURL) {
		this.invokeTargetURL = invokeTargetURL;
	}

	public Long getIdYamlProperties() {
		return idYamlProperties;
	}

	public void setIdYamlProperties(Long idYamlProperties) {
		this.idYamlProperties = idYamlProperties;
	}

	public String getCountPathResourceMethod() {
		return countPathResourceMethod;
	}

	public void setCountPathResourceMethod(String countPathResourceMethod) {
		this.countPathResourceMethod = countPathResourceMethod;
	}

	public String getDevOrgId() {
		return devOrgId;
	}

	public void setDevOrgId(String devOrgId) {
		this.devOrgId = devOrgId;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}

	public String getDeprecated() {
		return deprecated;
	}

	public void setDeprecated(String deprecated) {
		this.deprecated = deprecated;
	}

	public Date getDeletedAt() {
		return deletedAt;
	}

	public void setDeletedAt(Date deletedAt) {
		this.deletedAt = deletedAt;
	}

}
