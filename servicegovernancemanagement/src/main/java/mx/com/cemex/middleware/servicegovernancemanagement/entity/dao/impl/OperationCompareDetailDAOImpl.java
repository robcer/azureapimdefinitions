package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.OperationCompareDetailDAO;

@Service("operationCompareDetailDAO")
public class OperationCompareDetailDAOImpl implements OperationCompareDetailDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public OperationCompareDetailDAOImpl()
    {
    }

    OperationCompareDetailDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public OperationCompareDetail create(OperationCompareDetail operationCompareDetail) {
        em.persist(operationCompareDetail);
        return operationCompareDetail;
    }

    @Transactional
    public OperationCompareDetail merge(OperationCompareDetail operationCompareDetail) {
        em.merge(operationCompareDetail);
        return operationCompareDetail;
    }

    public long countCompareDetail() {
        return (Long) em.createQuery("select count(o) from OperationCompareDetail o").getSingleResult();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from OperationCompareDetail");
    	query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    public List<OperationCompareDetail> findOperationCompareDetail(int firstResult, int maxResults) {
        return em.createQuery("select o from OperationCompareDetail o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public OperationCompareDetail findOperationCompareDetail(Long id) {
        if (id == null) return null;
        return em.find(OperationCompareDetail.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<OperationCompareDetail> findAll() {
        return em.createQuery("select o from OperationCompareDetail o order by o.idOperationCompareDetail DESC").getResultList();
    }
    
    public List<OperationCompareDetail> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from OperationCompareDetail o where o.devOrgNameEnv = :devOrgNameEnv", OperationCompareDetail.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public OperationCompareDetail findOperationCompareDetailById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from OperationCompareDetail o where o.id = :id", OperationCompareDetail.class)
        		.setParameter("id", id).getSingleResult();
    }

}