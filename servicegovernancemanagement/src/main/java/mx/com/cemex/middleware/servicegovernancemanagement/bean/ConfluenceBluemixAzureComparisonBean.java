package mx.com.cemex.middleware.servicegovernancemanagement.bean;

public class ConfluenceBluemixAzureComparisonBean {
	
	private String azureApiName;
	private String azureApiPath;
	private String azureServiceUrl;
	private String azureMethod;
	private String azureFullPath;
	private String bluemixApiName;
	private String bluemixApiVersion;
	private String bluemixApiPath;
	private String bluemixFullPath;
	private String bluemixAzureHost;
	private String bluemixMethod;
	public String getAzureApiName() {
		return azureApiName;
	}
	public void setAzureApiName(String azureApiName) {
		this.azureApiName = azureApiName;
	}
	public String getAzureApiPath() {
		return azureApiPath;
	}
	public void setAzureApiPath(String azureApiPath) {
		this.azureApiPath = azureApiPath;
	}
	public String getAzureServiceUrl() {
		return azureServiceUrl;
	}
	public void setAzureServiceUrl(String azureServiceUrl) {
		this.azureServiceUrl = azureServiceUrl;
	}
	public String getAzureMethod() {
		return azureMethod;
	}
	public void setAzureMethod(String azureMethod) {
		this.azureMethod = azureMethod;
	}
	public String getAzureFullPath() {
		return azureFullPath;
	}
	public void setAzureFullPath(String azureFullPath) {
		this.azureFullPath = azureFullPath;
	}
	public String getBluemixApiName() {
		return bluemixApiName;
	}
	public void setBluemixApiName(String bluemixApiName) {
		this.bluemixApiName = bluemixApiName;
	}
	public String getBluemixApiVersion() {
		return bluemixApiVersion;
	}
	public void setBluemixApiVersion(String bluemixApiVersion) {
		this.bluemixApiVersion = bluemixApiVersion;
	}
	public String getBluemixApiPath() {
		return bluemixApiPath;
	}
	public void setBluemixApiPath(String bluemixApiPath) {
		this.bluemixApiPath = bluemixApiPath;
	}
	public String getBluemixFullPath() {
		return bluemixFullPath;
	}
	public void setBluemixFullPath(String bluemixFullPath) {
		this.bluemixFullPath = bluemixFullPath;
	}
	public String getBluemixAzureHost() {
		return bluemixAzureHost;
	}
	public void setBluemixAzureHost(String bluemixAzureHost) {
		this.bluemixAzureHost = bluemixAzureHost;
	}
	public String getBluemixMethod() {
		return bluemixMethod;
	}
	public void setBluemixMethod(String bluemixMethod) {
		this.bluemixMethod = bluemixMethod;
	}
	

}
