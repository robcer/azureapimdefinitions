package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.CompareDetail;

public interface CompareDetailDAO extends GenericEntityDAO<CompareDetail> {

    CompareDetail findCompareDetail(Long id);
}
