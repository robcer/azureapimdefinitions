package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;


public interface ApiOperationRepository extends JpaRepository<ApiOperation, Long> {
	
	Page<ApiOperation> findAllByRelUserOperationUser(User user, Pageable pageable);
	ApiOperation findById(String id);
}
