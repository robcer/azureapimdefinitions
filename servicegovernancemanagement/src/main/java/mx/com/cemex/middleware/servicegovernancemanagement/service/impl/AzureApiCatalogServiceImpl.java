package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.commons.lang3.StringEscapeUtils;
import org.apache.log4j.Logger;
import org.resthub.common.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.BitbucketSrcHeadersBean;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.AzureApiCatalogRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureApiCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureApiOperationCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.BitbucketOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;


@Service("azureApiCatalogService")
public class AzureApiCatalogServiceImpl extends CrudServiceImpl<AzureApiCatalog, Long, AzureApiCatalogRepository> implements AzureApiCatalogService {
	
	private static final Logger LOG  = Logger.getLogger(AzureApiCatalogServiceImpl.class);

	@Autowired
	AzureApiCatalogDAO azureApiCatalogDAO;

	@Autowired
	AzureApiOperationCatalogDAO azureApiOperationCatalogDAO;

	@Autowired
	AzureApiOperationCatalogService azureApiOperationCatalogService;
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
//	@Autowired
//	BitbucketOperationsService bitbucketOperationsService;
	
	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	AzureOperationsService azureOperationsService;
	
	public void mantainAzureApiCatalog(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException {
		LOG.info("Inside mantainAzureApiCatalog method.");
		azureApiCatalogDAO.disableAzureApiCatalogByEnvironment(environmentCatalog);
		azureApiOperationCatalogDAO.disableAzureApiCatalogByEnvironment(environmentCatalog);
		List<AzureApiCatalog> azureApiCatalogs = azureOperationsService.getApiList(environmentCatalog);
		for(AzureApiCatalog publishedAzureApiCatalog: azureApiCatalogs) {
			try {
				AzureApiCatalog azureApiCatalog = azureApiCatalogDAO.findAzureApiCatalogByIdAndEnvironment(publishedAzureApiCatalog.getId(), environmentCatalog);
				if(azureApiCatalog != null) {
					azureApiCatalog.setUpdatedAt(new Date());
					azureApiCatalog.setRemoved(false);
					azureApiCatalog = azureApiCatalogDAO.merge(azureApiCatalog);
					try {
						String apiPolicy = azureOperationsService.getApiPolicy(environmentCatalog, azureApiCatalog.getId());
						azureOperationsService.commitToBitbucketIntegration(apiPolicy, azureApiCatalog.getName(), environmentCatalog.getBitbucketBranch(), 
							IYamlBeansMasterConstants.BITBUCKET_REPOSITORY, "&quot;Rogelio Reyo Cachu <rogelio.reyocachu@ext.cemex.com>&quot;", 
							"/api-management/policies" + azureApiCatalog.getId() + azureApiCatalog.getId().replace("/apis", "") + ".xml", 
							"Initial version for " + azureApiCatalog.getName(), IYamlBeansMasterConstants.BITBUCKET_USER);
					} catch (Exception e) {
						e.printStackTrace();
					}
					azureApiOperationCatalogService.mantainAzureApiOperationCatalog(azureApiCatalog, environmentCatalog);
				}
			} catch (NoResultException e) {
				publishedAzureApiCatalog.setEnvironmentCatalog(environmentCatalog);
				publishedAzureApiCatalog.setCreatedAt(new Date());
				publishedAzureApiCatalog.setRemoved(false);
				publishedAzureApiCatalog.setRegisteredAt(new Date());
				publishedAzureApiCatalog = azureApiCatalogDAO.create(publishedAzureApiCatalog);
				azureApiOperationCatalogService.mantainAzureApiOperationCatalog(publishedAzureApiCatalog, environmentCatalog);
				String apiPolicy;
				try {
					apiPolicy = azureOperationsService.getApiPolicy(environmentCatalog, publishedAzureApiCatalog.getId());
					azureOperationsService.commitToBitbucketIntegration(apiPolicy, publishedAzureApiCatalog.getName(), environmentCatalog.getBitbucketBranch(), 
							"rogelioreyorepository", "&quot;Rogelio Reyo Cachu <rogelio.reyocachu@ext.cemex.com>&quot;", 
							//MessageFormat.format(IYamlBeansMasterConstants.BITBUCKET_API_POLICIES_PATH, publishedAzureApiCatalog.getName().replace(" ", "_"), publishedAzureApiCatalog.getName().replace(" ", "_"), publishedAzureApiCatalog.getId().replace("/apis", "")),
							"/api-management/policies" + publishedAzureApiCatalog.getId() + publishedAzureApiCatalog.getId().replace("/apis", "") + ".xml", 
							"Initial version for " + publishedAzureApiCatalog.getName(), IYamlBeansMasterConstants.BITBUCKET_USER);
				} catch (Exception e1) {
					e1.printStackTrace();
				}
			}
		}
	}
	
	
    @Override
    @Inject
    public void setRepository(AzureApiCatalogRepository azureApiCatalogRepository) {
        super.setRepository(azureApiCatalogRepository);
    }
    
    public List<AzureApiCatalog> findAllByEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
    	return azureApiCatalogDAO.findAllByEnvironment(environmentCatalog);
    }
}
