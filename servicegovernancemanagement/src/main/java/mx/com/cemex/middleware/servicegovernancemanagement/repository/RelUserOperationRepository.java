package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelUserOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;

public interface RelUserOperationRepository extends JpaRepository<RelUserOperation, Long> {

	List<RelUserOperation> findAllByUser(User user);

}
