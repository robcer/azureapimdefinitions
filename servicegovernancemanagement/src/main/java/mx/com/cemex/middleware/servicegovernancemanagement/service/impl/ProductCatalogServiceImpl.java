package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.MessageFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProductCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProductCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ProductCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;


/**
 * 
 * 
 * @author RogelioReyo
 *
 */

@Service("productCatalogService")
public class ProductCatalogServiceImpl implements ProductCatalogService {
	
	private static final Logger LOG  = Logger.getLogger(ProductCatalogServiceImpl.class);

	@Autowired
	ProductCatalogDAO productCatalogDAO;
	
	@Autowired
	UtilitiesService utilitiesService;
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
	private SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS");
	
	public void mantainProductCatalog(EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException {
		String apiUrl = "https://"+MessageFormat.format(IYamlBeansMasterConstants.CLOUD_BLUEMIX_HOSTNAME, environmentCatalog.getRegionHostAcronym())+"/v1/orgs/{0}/environments/{1}/products";
		
		LOG.info("Fetching information from Bluemix Product on: " + environmentCatalog.getName() + " Environment...: " + apiUrl);
		String cUrl = MessageFormat.format(apiUrl, environmentCatalog.getId(), environmentCatalog.getCatalogEnv());
		JSONArray jsonArrayProducts = utilitiesService.getJSONArray(cUrl, utilitiesService.getEncodedAuthorization(IYamlBeansMasterConstants.BLUEMIX_CREDENTIALS));
		List<ProductCatalog> productCatalogs = fillProductCatalogBean(jsonArrayProducts, environmentCatalog.getCatalogEnv());
		for(ProductCatalog productCatalog: productCatalogs) {
			try {
				ProductCatalog databaseProductCatalog = productCatalogDAO.findProductCatalogById(productCatalog.getId());
				databaseProductCatalog.setUpdatedAt(productCatalog.getUpdatedAt());
				databaseProductCatalog.setUpdatedBy(productCatalog.getUpdatedBy());
				databaseProductCatalog.setDeploymentState(productCatalog.getDeploymentState());
				databaseProductCatalog.setPlanRegistrations(productCatalog.getPlanRegistrations());
	    		productCatalogDAO.merge(databaseProductCatalog);
			} catch (NoResultException e) {
				productCatalogDAO.create(productCatalog);
			}
		}
		for(ProductCatalog databaseProductCatalog: productCatalogDAO.findAllByDevOrgId(environmentCatalog.getId())) {
			boolean isStillPresent = false;
			for(ProductCatalog productCatalog: productCatalogs) {
				if(databaseProductCatalog.getId().equals(productCatalog.getId())) {
					isStillPresent = true;
				}
			}
			if(!isStillPresent) {
				databaseProductCatalog.setRemovedAt(new Date());
	    		databaseProductCatalog.setDeploymentState("removed");
	    		productCatalogDAO.merge(databaseProductCatalog);
			}
		}
	}
	
	private List<ProductCatalog> fillProductCatalogBean(JSONArray jsonArray, String environment) throws ParseException {
		List<ProductCatalog> productCatalogs = new ArrayList<ProductCatalog>();
	    for (int i = 0, size = jsonArray.length(); i < size; i++) {
	    	JSONObject jsonObject = jsonArray.getJSONObject(i);
	    	ProductCatalog productCatalog = new ProductCatalog();
	    	productCatalog.setId(jsonObject.optString("id"));
	    	productCatalog.setProductName(jsonObject.optString("productName"));
    		productCatalog.setUrl(jsonObject.optString("url"));
    		productCatalog.setUpdatedAt(dateFormatter.parse(jsonObject.optString("updatedAt")));
    		productCatalog.setUpdatedBy(jsonObject.optString("updatedBy"));
    		if(jsonObject.optJSONObject("document").optJSONObject("info") != null) {
        		productCatalog.setName(jsonObject.optJSONObject("document").optJSONObject("info").optString("title"));
        		productCatalog.setVersion(jsonObject.optJSONObject("document").optJSONObject("info").optString("version"));
    		}
    		productCatalog.setDevOrgId(jsonObject.optString("orgId"));
    		productCatalog.setCreatedAt(dateFormatter.parse(jsonObject.optString("createdAt")));
    		productCatalog.setCreatedBy(jsonObject.optString("createdBy"));
    		productCatalog.setEnvId(jsonObject.optString("envId"));
    		productCatalog.setDeploymentState(jsonObject.optString("deploymentState"));
    		productCatalog.setPlanRegistrations(jsonObject.optLong("planRegistrations"));
    		productCatalog.setRegisteredAt(new Date());
    		productCatalogs.add(productCatalog);
	    }
		return productCatalogs;
	}
}
