package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.text.ParseException;

import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.service.JiraOperationsService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

@Service("jiraOperationsService")
public class JiraOperationsServiceImpl implements JiraOperationsService {

	private static final Logger LOG = Logger.getLogger(JiraOperationsServiceImpl.class);
	
	@Autowired
	UtilitiesService utilitiesService;

	@Override
	public JSONObject getJiraIssueByKey(String jiraKey) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException {
		String jiraUrl = MessageFormat.format(IYamlBeansMasterConstants.JIRA_URI, jiraKey);
		LOG.info("Fetching JIRA ticket information: " + jiraKey + "... on URL: " + jiraUrl);
		JSONObject jsonObjectApi = utilitiesService.getJSONObject(jiraUrl, utilitiesService.getEncodedAuthorization(IYamlBeansMasterConstants.CONFLUENCE_CREDENTIALS));
		LOG.info(jsonObjectApi);
		if(jsonObjectApi != null) {
			return jsonObjectApi;
		}
		return null;
	}

	@Override
	public JSONObject postJiraIssueComment(String jiraKey, String comment) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException {
		LOG.info("Adding a Comment: " + comment + " to JIRA ticket: " + jiraKey);
		String bitBucketUrl = MessageFormat.format(IYamlBeansMasterConstants.JIRA_URI, jiraKey) + "/comment";
		HttpURLConnection connection = (HttpURLConnection) new URL(bitBucketUrl).openConnection();
		try {
			connection.setRequestMethod("POST");
			connection.setRequestProperty("Authorization", "Basic " + java.util.Base64.getEncoder().encodeToString((IYamlBeansMasterConstants.CONFLUENCE_CREDENTIALS).getBytes(StandardCharsets.UTF_8)));
			connection.setRequestProperty("Content-Type", "application/json");
			connection.setUseCaches(false);
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.connect();
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(connection.getOutputStream());
			JSONObject jsonObject = new JSONObject();
			jsonObject.put("body", comment);
			outputStreamWriter.write(jsonObject.toString());
			outputStreamWriter.close();
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonOutputObject = (org.json.simple.JSONObject) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
			return new org.json.JSONObject(jsonOutputObject.toJSONString());
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
		return null;
	}

}
