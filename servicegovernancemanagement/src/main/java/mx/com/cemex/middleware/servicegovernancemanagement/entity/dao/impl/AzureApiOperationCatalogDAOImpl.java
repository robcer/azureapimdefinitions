package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiOperationCatalogDAO;

@Service("azureApiOperationCatalogDAO")
public class AzureApiOperationCatalogDAOImpl implements AzureApiOperationCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public AzureApiOperationCatalogDAOImpl()
    {
    }

    AzureApiOperationCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public AzureApiOperationCatalog create(AzureApiOperationCatalog apiOperationCatalog) {
        em.persist(apiOperationCatalog);
        return apiOperationCatalog;
    }

    @Transactional
    public AzureApiOperationCatalog merge(AzureApiOperationCatalog apiOperationCatalog) {
        em.merge(apiOperationCatalog);
        return apiOperationCatalog;
    }

    public long countAzureApiOperationCatalog() {
        return (Long) em.createQuery("select count(o) from AzureApiOperationCatalog o WHERE o.removed = 0").getSingleResult();
    }

    @Transactional
    public void disableAzureApiCatalogByEnvironment(EnvironmentCatalog environmentCatalog) {
    	Query query = em.createNativeQuery("UPDATE AzureApiOperationCatalog SET removed = 1 WHERE id_environment_catalog = :environmentCatalog")
        	.setParameter("environmentCatalog", environmentCatalog.getIdEnvironmentCatalog());
    	query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    public List<AzureApiOperationCatalog> findAzureApiOperationCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from AzureApiOperationCatalog o WHERE o.removed = 0").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public AzureApiOperationCatalog findAzureApiOperationCatalog(Long id) {
        if (id == null) return null;
        return em.find(AzureApiOperationCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<AzureApiOperationCatalog> findAll() {
        return em.createQuery("select o from AzureApiOperationCatalog o WHERE o.removed = 0").getResultList();
    }
    
    public AzureApiOperationCatalog findAzureApiOperationCatalogByIdAndEnvironmentCatalog(String id, EnvironmentCatalog environmentCatalog) {
        if (id == null) return null;
        return em.createQuery("select o from AzureApiOperationCatalog o where o.id = :id and o.environmentCatalog = :environmentCatalog", AzureApiOperationCatalog.class)
        		.setParameter("id", id)
        		.setParameter("environmentCatalog", environmentCatalog)
        		.getSingleResult();
    }
    
    public List<AzureApiOperationCatalog> findAllByEnvironment(EnvironmentCatalog environmentCatalog) {
        if (environmentCatalog == null) return null;
        return em.createQuery("select o from AzureApiOperationCatalog o where o.environmentCatalog = :environmentCatalog AND o.removed = 0", AzureApiOperationCatalog.class)
        		.setParameter("environmentCatalog", environmentCatalog).getResultList();
    }

}