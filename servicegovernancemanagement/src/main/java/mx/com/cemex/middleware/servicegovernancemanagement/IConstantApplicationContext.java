package mx.com.cemex.middleware.servicegovernancemanagement;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public interface IConstantApplicationContext {
    
    static final ApplicationContext APPLICATION_CONTEXT = new ClassPathXmlApplicationContext("classpath*:applicationContext*.xml");
    
}
