package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "yamlporfolio")
public class YamlPortfolio {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @javax.persistence.Column(name = "id_yaml_portfolio")
	private Long idYamlPorfolio;

	@com.ebay.xcelite.annotations.Column(name = "ProductName")
    @javax.persistence.Column(name = "product_name")
	private String productName;

	@com.ebay.xcelite.annotations.Column(name = "ProductVersion")
    @javax.persistence.Column(name = "product_version")
	private String productVersion;

	@com.ebay.xcelite.annotations.Column(name = "Developer")
    @javax.persistence.Column(name = "developer")
	private String developer;

	@com.ebay.xcelite.annotations.Column(name = "Id")
    @javax.persistence.Column(name = "id")
	private String id;

	@com.ebay.xcelite.annotations.Column(name = "BusinessCapability")
    @javax.persistence.Column(name = "business_capability")
	private String businessCapability;

    @Column(name = "created_at")
    @DateTimeFormat(style = "S-")
    private Date createdAt;

    @Column(name = "updated_at")
    @DateTimeFormat(style = "S-")
    private Date updatedAt;

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductVersion() {
		return productVersion;
	}

	public void setProductVersion(String productVersion) {
		this.productVersion = productVersion;
	}

	public String getDeveloper() {
		return developer;
	}

	public void setDeveloper(String developer) {
		this.developer = developer;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getBusinessCapability() {
		return businessCapability;
	}

	public void setBusinessCapability(String businessCapability) {
		this.businessCapability = businessCapability;
	}

	public Long getIdYamlPorfolio() {
		return idYamlPorfolio;
	}

	public void setIdYamlPorfolio(Long idYamlPorfolio) {
		this.idYamlPorfolio = idYamlPorfolio;
	}

	public Date getCreatedAt() {
		return createdAt;
	}

	public void setCreatedAt(Date createdAt) {
		this.createdAt = createdAt;
	}

	public Date getUpdatedAt() {
		return updatedAt;
	}

	public void setUpdatedAt(Date updatedAt) {
		this.updatedAt = updatedAt;
	}
	
}
