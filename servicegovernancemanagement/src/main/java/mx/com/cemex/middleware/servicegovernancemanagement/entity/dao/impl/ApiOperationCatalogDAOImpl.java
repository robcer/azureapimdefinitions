package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiOperationCatalogDAO;

@Service("apiOperationCatalogDAO")
public class ApiOperationCatalogDAOImpl implements ApiOperationCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ApiOperationCatalogDAOImpl()
    {
    }

    ApiOperationCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ApiOperationCatalog create(ApiOperationCatalog apiOperationCatalog) {
        em.persist(apiOperationCatalog);
        return apiOperationCatalog;
    }

    @Transactional
    public ApiOperationCatalog merge(ApiOperationCatalog apiOperationCatalog) {
        em.merge(apiOperationCatalog);
        return apiOperationCatalog;
    }

    public long countApiOperationCatalog() {
        return (Long) em.createQuery("select count(o) from ApiOperationCatalog o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiOperationCatalog> findApiOperationCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from ApiOperationCatalog o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ApiOperationCatalog findApiOperationCatalog(Long id) {
        if (id == null) return null;
        return em.find(ApiOperationCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiOperationCatalog> findAll() {
        return em.createQuery("select o from ApiOperationCatalog o").getResultList();
    }
    
    public List<ApiOperationCatalog> findAllByDevOrgId(String devOrgId) {
        if (devOrgId == null) return null;
        return em.createQuery("select o from ApiOperationCatalog o where o.devOrgId = :devOrgId", ApiOperationCatalog.class).setParameter("devOrgId", devOrgId).getResultList();
    }
    
    public List<ApiOperationCatalog> findApiOperationCatalogByApiCatalog(ApiCatalog apiCatalog) {
        if (apiCatalog == null) return null;
        return em.createQuery("select o from ApiOperationCatalog o where o.apiCatalog = :apiCatalog", ApiOperationCatalog.class).setParameter("apiCatalog", apiCatalog).getResultList();
    }

	public ApiOperationCatalog findApiOperationCatalogByPathAndMethodAndEnvId(String envId, String path,
			String method, String basePath) {
//		LOG.info.println(envId + "|" + path + "|" + method + "|" + basePath);
        return em.createQuery("select o from ApiOperationCatalog o where o.envId = :envId AND o.path = :path AND o.method = :method AND o.basePath = :basePath", ApiOperationCatalog.class)
        		.setParameter("envId", envId)
        		.setParameter("path", path)
        		.setParameter("method", method)
        		.setParameter("basePath", basePath)
        		.getSingleResult();
	}

	public ApiOperationCatalog findApiOperationCatalogByApiIdAndPathAndMethod(String apiId, String path,
			String method) {
        return em.createQuery("select o from ApiOperationCatalog o where o.apiId = :apiId AND o.path = :path AND o.method = :method", ApiOperationCatalog.class)
        		.setParameter("apiId", apiId)
        		.setParameter("path", path)
        		.setParameter("method", method).getSingleResult();
	}

	public ApiOperationCatalog findApiOperationCatalogByApiNameAndPathAndMethodAndDevOrgId(String apiName, String path,
			String method, String devOrgId) {
        return em.createQuery("select o from ApiOperationCatalog o where lower(o.apiCatalog.name) = :apiName AND (o.basePath || o.path) = :path AND o.method = :method AND o.devOrgId = :devOrgId AND o.confluencePageId IS NOT NULL", ApiOperationCatalog.class)
        		.setParameter("apiName", apiName)
        		.setParameter("path", path)
        		.setParameter("method", method)
        		.setParameter("devOrgId", devOrgId).getSingleResult();
	}
    
    public List<ApiOperationCatalog> findApiOperationCatalogByFullPathConcatenated(String fullPath) {
        if (fullPath == null) return null;
        return em.createQuery("select o from ApiOperationCatalog o where upper(o.method || o.basePath || o.path ) = :fullPath", ApiOperationCatalog.class)
        		.setParameter("fullPath", fullPath)
        		.getResultList();
    }
    
    public List<Object[]> findProjectOperationsCountByDevOrgId(String devOrgId) {
    	@SuppressWarnings("unchecked")
    	List<Object[]> results = em.createNativeQuery("SELECT P.NAME, COUNT(*) FROM RELPROJECTOPERATION PO INNER JOIN APIOPERATIONCATALOG OP ON OP.ID_API_OPERATION_CATALOG = PO.ID_API_OPERATION_CATALOG INNER JOIN PROJECTCATALOG P ON P.ID_PROJECT_CATALOG = PO.ID_PROJECT_CATALOG INNER JOIN APICATALOG A ON A.ID_API_CATALOG = OP.ID_API_CATALOG WHERE OP.DEV_ORG_ID = ?1 AND OP.INVOKE_TITLE IS NOT NULL AND A.DEPLOYMENT_STATE = 'running' GROUP BY P.NAME ORDER BY P.NAME DESC")
    		.setParameter(1, devOrgId).getResultList();
    	return results;
    }

}