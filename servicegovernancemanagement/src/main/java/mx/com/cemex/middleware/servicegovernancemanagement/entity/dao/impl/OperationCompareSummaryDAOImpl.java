package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.OperationCompareSummaryDAO;

@Service("operationCompareSummaryDAO")
public class OperationCompareSummaryDAOImpl implements OperationCompareSummaryDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public OperationCompareSummaryDAOImpl()
    {
    }

    OperationCompareSummaryDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public OperationCompareSummary create(OperationCompareSummary operationCompareSummary) {
        em.persist(operationCompareSummary);
        return operationCompareSummary;
    }

    @Transactional
    public OperationCompareSummary merge(OperationCompareSummary operationCompareSummary) {
        em.merge(operationCompareSummary);
        return operationCompareSummary;
    }

    public long countOperationCompareSummary() {
        return (Long) em.createQuery("select count(o) from OperationCompareSummary o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<OperationCompareSummary> findOperationCompareSummary(int firstResult, int maxResults) {
        return em.createQuery("select o from OperationCompareSummary o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public OperationCompareSummary findOperationCompareSummary(Long id) {
        if (id == null) return null;
        return em.find(OperationCompareSummary.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<OperationCompareSummary> findAll() {
        return em.createQuery("select o from OperationCompareSummary o order by o.idCompareSummary DESC").getResultList();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from OperationCompareSummary");
    	query.executeUpdate();
    }
    
    public List<OperationCompareSummary> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from OperationCompareSummary o where o.devOrgNameEnv = :devOrgNameEnv", OperationCompareSummary.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public List<Object> findTargetFileNameByReportGUID(String reportGuid) {
    	@SuppressWarnings("unchecked")
    	List<Object> results = em.createNativeQuery("select target_file_name from OperationCompareSummary o where o.report_Guid = ?1 order by o.file_Name asc")
        		.setParameter(1, reportGuid).getResultList();
    	return results;
    }
    
    public List<OperationCompareSummary> findAllByReportGUID(String reportGuid) {
        if (reportGuid == null) return null;
        return em.createQuery("select o from OperationCompareSummary o where o.reportGuid = :reportGuid order by o.fileName asc", OperationCompareSummary.class).setParameter("reportGuid", reportGuid).getResultList();
    }
    
    public OperationCompareSummary findOperationCompareSummaryById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from OperationCompareSummary o where o.id = :id", OperationCompareSummary.class)
        		.setParameter("id", id).getSingleResult();
    }

}