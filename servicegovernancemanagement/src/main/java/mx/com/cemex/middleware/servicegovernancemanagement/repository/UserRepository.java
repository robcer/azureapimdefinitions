package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
//import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;

public interface UserRepository extends JpaRepository<User, Long> {

//  boolean existsByUsername(String username);

  User findByUsername(String username);

//  @Transactional
//  void deleteByUsername(String username);

}
