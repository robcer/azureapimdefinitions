package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;

public interface ReadYamlsService {

	void readYamlInformation() throws FileNotFoundException, IllegalAccessException, InvocationTargetException;
	
}
