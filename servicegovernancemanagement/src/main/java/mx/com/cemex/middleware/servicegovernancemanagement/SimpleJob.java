package mx.com.cemex.middleware.servicegovernancemanagement;

import java.util.Date;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;

public class SimpleJob implements Job {
	
	private static final Logger LOG  = Logger.getLogger(SimpleJob.class);

	public SimpleJob() {
	}

	public void execute(JobExecutionContext context)
			throws JobExecutionException {
		JobKey jobKey = context.getJobDetail().getKey();
		LOG.info("SimpleJob dice: " + jobKey + " ejecutandose en: " + new Date());
		LOG.info("Name: " + jobKey.getName());
		LOG.info("Group: " + jobKey.getGroup());
		LOG.info("Termino de ejecutarse mi job.");
	}

}
