package mx.com.cemex.middleware.servicegovernancemanagement;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLEncoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import javax.net.ssl.HttpsURLConnection;

public class TestCallServiceOData {

	public static void main(String[] args) throws NoSuchAlgorithmException, InvalidKeyException, IOException {
		// TODO Auto-generated method stub
		String encodedFilter = URLEncoder.encode("$filter=path eq 'v7/dm'", "UTF-8");
		HttpsURLConnection connection = (HttpsURLConnection) new URL("https://cemexqas.management.azure-api.net/apis?"+encodedFilter+"&api-version=2017-03-01").openConnection();
		try {
			connection.setRequestMethod("GET");
			connection.setRequestProperty("Connection", "keep-alive");
			org.json.simple.parser.JSONParser jsonParser = new org.json.simple.parser.JSONParser();
			org.json.simple.JSONObject jsonObject = (org.json.simple.JSONObject) jsonParser
					.parse(new InputStreamReader((InputStream) connection.getContent(), "UTF-8"));
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			connection.disconnect();
		}
	}

}

