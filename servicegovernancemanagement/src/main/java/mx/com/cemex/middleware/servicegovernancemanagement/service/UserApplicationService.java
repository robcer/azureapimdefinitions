package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.util.List;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.UserApplication;

public interface UserApplicationService extends CrudService<UserApplication, Long> {
	
	List<UserApplication> findByUser(User user);
}
