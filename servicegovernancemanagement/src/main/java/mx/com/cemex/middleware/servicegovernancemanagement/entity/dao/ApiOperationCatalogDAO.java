package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;

public interface ApiOperationCatalogDAO {

	ApiOperationCatalog create(ApiOperationCatalog apiOperationCatalog);
	ApiOperationCatalog merge(ApiOperationCatalog apiOperationCatalog);
    long countApiOperationCatalog();
    List<ApiOperationCatalog> findApiOperationCatalog(int firstResult, int maxResults);
    List<ApiOperationCatalog> findAll();
    ApiOperationCatalog findApiOperationCatalog(Long id);
    List<ApiOperationCatalog> findApiOperationCatalogByApiCatalog(ApiCatalog apiCatalog);
    ApiOperationCatalog findApiOperationCatalogByApiIdAndPathAndMethod(String apiId, String path, String method);
    ApiOperationCatalog findApiOperationCatalogByApiNameAndPathAndMethodAndDevOrgId(String apiName, String path, String method, String devOrgId);
    List<ApiOperationCatalog> findApiOperationCatalogByFullPathConcatenated(String fullPath);
    List<Object[]> findProjectOperationsCountByDevOrgId(String devOrgId);
    ApiOperationCatalog findApiOperationCatalogByPathAndMethodAndEnvId(String envId, String path, String method, String basePath);
}
