package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.HipchatRoom;

public interface HipchatRoomDAO {

	HipchatRoom create(HipchatRoom hipchatRoom);
	HipchatRoom merge(HipchatRoom hipchatRoom);
    long countHipchatRoom();
    List<HipchatRoom> findHipchatRoom(int firstResult, int maxResults);
    List<HipchatRoom> findAll();
    HipchatRoom findHipchatRoom(Long id);
    HipchatRoom findHipchatRoomById(String id);

}
