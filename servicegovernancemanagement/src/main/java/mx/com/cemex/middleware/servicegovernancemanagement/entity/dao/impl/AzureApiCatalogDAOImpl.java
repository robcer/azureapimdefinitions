package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.AzureApiCatalogDAO;

@Service("azureApiCatalogDAO")
public class AzureApiCatalogDAOImpl implements AzureApiCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public AzureApiCatalogDAOImpl()
    {
    }

    AzureApiCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public AzureApiCatalog create(AzureApiCatalog azureAzureApiCatalog) {
        em.persist(azureAzureApiCatalog);
        return azureAzureApiCatalog;
    }

    @Transactional
    public AzureApiCatalog merge(AzureApiCatalog azureAzureApiCatalog) {
        em.merge(azureAzureApiCatalog);
        return azureAzureApiCatalog;
    }

    public long countAzureApiCatalog() {
        return (Long) em.createQuery("select count(o) from AzureApiCatalog o WHERE o.removed = 0").getSingleResult();
    }

    @Transactional
    public void disableAzureApiCatalogByEnvironment(EnvironmentCatalog environmentCatalog) {
    	Query query = em.createNativeQuery("UPDATE AzureApiCatalog SET removed = 1 WHERE id_environment_catalog = :environmentCatalog")
        		.setParameter("environmentCatalog", environmentCatalog.getIdEnvironmentCatalog());
    	query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    public List<AzureApiCatalog> findAzureApiCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from AzureApiCatalog o WHERE o.removed = 0").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public AzureApiCatalog findAzureApiCatalog(Long id) {
        if (id == null) return null;
        return em.find(AzureApiCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<AzureApiCatalog> findAll() {
        return em.createQuery("select o from AzureApiCatalog o WHERE o.removed = 0").getResultList();
    }
    
    public AzureApiCatalog findAzureApiCatalogByIdAndEnvironment(String id, EnvironmentCatalog environmentCatalog) {
        if (id == null) return null;
        return em.createQuery("select o from AzureApiCatalog o where o.id = :id and o.environmentCatalog = :environmentCatalog", AzureApiCatalog.class)
        		.setParameter("id", id).setParameter("environmentCatalog", environmentCatalog).getSingleResult();
    }
    
    public List<AzureApiCatalog> findAllByEnvironment(EnvironmentCatalog environmentCatalog) {
        if (environmentCatalog == null) return null;
        return em.createQuery("select o from AzureApiCatalog o where o.environmentCatalog = :environmentCatalog and o.removed = 0", AzureApiCatalog.class)
        		.setParameter("environmentCatalog", environmentCatalog).getResultList();
    }
    
    
}