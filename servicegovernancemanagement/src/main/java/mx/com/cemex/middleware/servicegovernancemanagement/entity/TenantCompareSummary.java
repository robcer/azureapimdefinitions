package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "tenantcomparesummary")
public class TenantCompareSummary {
	
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_tenant_compare_summary")
	private Long idTenantCompareSummary;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "report_guid")
    private String reportGuid;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_source", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentSource;
    
    @Column(name = "is_source_exist")
    private boolean isSourceExist;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment_target", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentTarget;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "tenantCompareSummary")
    private Set<ApiCompareSummary> apiCompareSummaries = new HashSet<ApiCompareSummary>();

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "tenantCompareSummary")
    @OrderBy("sourceStartingLine ASC")
    private Set<TenantCompareDetail> tenantCompareDetails = new HashSet<TenantCompareDetail>();
    
    @Column(name = "is_target_exist")
    private boolean isTargetExist;

    private boolean isSafePatch;

	@Lob
    @Column(name = "source_policy_xml")
    private String sourcePolicyXml;

	@Lob
    @Column(name = "target_policy_xml")
    private String targetPolicyXml;

	public String getSourcePolicyXml() {
		return sourcePolicyXml;
	}

	public void setSourcePolicyXml(String sourcePolicyXml) {
		this.sourcePolicyXml = sourcePolicyXml;
	}

	public String getTargetPolicyXml() {
		return targetPolicyXml;
	}

	public void setTargetPolicyXml(String targetPolicyXml) {
		this.targetPolicyXml = targetPolicyXml;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getReportGuid() {
		return reportGuid;
	}

	public void setReportGuid(String reportGuid) {
		this.reportGuid = reportGuid;
	}

	public EnvironmentCatalog getEnvironmentSource() {
		return environmentSource;
	}

	public void setEnvironmentSource(EnvironmentCatalog environmentSource) {
		this.environmentSource = environmentSource;
	}

	public boolean isSourceExist() {
		return isSourceExist;
	}

	public void setSourceExist(boolean isSourceExist) {
		this.isSourceExist = isSourceExist;
	}

	public EnvironmentCatalog getEnvironmentTarget() {
		return environmentTarget;
	}

	public void setEnvironmentTarget(EnvironmentCatalog environmentTarget) {
		this.environmentTarget = environmentTarget;
	}

	//Velocity needs those 3 methods in order to access the boolean attribute
	public boolean getIsTargetExist() {
		return isTargetExist;
	}

	public boolean getIsSourceExist() {
		return isSourceExist;
	}

	public boolean getIsSafePatch() {
		return isSafePatch;
	}
	//End Velocity needs

	public boolean isTargetExist() {
		return isTargetExist;
	}

	public void setTargetExist(boolean isTargetExist) {
		this.isTargetExist = isTargetExist;
	}

	public Set<ApiCompareSummary> getApiCompareSummaries() {
		return apiCompareSummaries;
	}

	public void setApiCompareSummaries(Set<ApiCompareSummary> apiCompareSummaries) {
		this.apiCompareSummaries = apiCompareSummaries;
	}

	public Set<TenantCompareDetail> getTenantCompareDetails() {
		return tenantCompareDetails;
	}

	public void setTenantCompareDetails(Set<TenantCompareDetail> tenantCompareDetails) {
		this.tenantCompareDetails = tenantCompareDetails;
	}

	public boolean isSafePatch() {
		return isSafePatch;
	}

	public void setSafePatch(boolean isSafePatch) {
		this.isSafePatch = isSafePatch;
	}

	public Long getIdTenantCompareSummary() {
		return idTenantCompareSummary;
	}

	public void setIdTenantCompareSummary(Long idTenantCompareSummary) {
		this.idTenantCompareSummary = idTenantCompareSummary;
	}

}
