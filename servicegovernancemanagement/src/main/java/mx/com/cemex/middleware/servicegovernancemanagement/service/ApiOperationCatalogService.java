package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;

public interface ApiOperationCatalogService {

	void mantainApiOperationCatalog(ApiCatalog apiCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException;
}
