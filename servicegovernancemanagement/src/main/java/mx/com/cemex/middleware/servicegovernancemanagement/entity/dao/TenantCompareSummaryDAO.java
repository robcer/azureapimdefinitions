package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;

public interface TenantCompareSummaryDAO {

	TenantCompareSummary create(TenantCompareSummary tenantCompareSummary);
	TenantCompareSummary merge(TenantCompareSummary tenantCompareSummary);
    long countTenantCompareSummary();
    List<TenantCompareSummary> findTenantCompareSummary(int firstResult, int maxResults);
    List<TenantCompareSummary> findAll();
    TenantCompareSummary findTenantCompareSummary(Long id);
    List<TenantCompareSummary> findAllByReportGUID(String reportGuid);
    List<Object> findTargetFileNameByReportGUID(String reportGuid);
    void deleteAll();

}
