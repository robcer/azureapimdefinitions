package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import javax.inject.Inject;
import javax.inject.Named;

import org.resthub.common.service.CrudServiceImpl;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.TenantCompareSummaryRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.service.TenantCompareSummaryService;

@Named("tenantCompareSummaryService")
public class TenantCompareSummaryServiceImpl extends CrudServiceImpl<TenantCompareSummary, Long, TenantCompareSummaryRepository> implements TenantCompareSummaryService {
    
    @Override
    @Inject
    public void setRepository(TenantCompareSummaryRepository tenantCompareSummaryRepository) {
        super.setRepository(tenantCompareSummaryRepository);
    }
}