package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.TenantCompareSummary;

public interface TenantCompareSummaryRepository extends JpaRepository<TenantCompareSummary, Long> {

}
