package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.text.ParseException;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.AzureApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface AzureApiOperationCatalogService {

	void mantainAzureApiOperationCatalog(AzureApiCatalog azureApiCatalog, EnvironmentCatalog environmentCatalog) throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException;
}
