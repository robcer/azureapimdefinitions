package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentVariables;

public interface EnvironmentVariablesDAO {

	EnvironmentVariables create(EnvironmentVariables environmentVariables);
	EnvironmentVariables merge(EnvironmentVariables environmentVariables);
    long countEnvironmentVariables();
    List<EnvironmentVariables> findEnvironmentVariables(int firstResult, int maxResults);
    List<EnvironmentVariables> findAll();
    EnvironmentVariables findEnvironmentVariables(Long id);
    EnvironmentVariables findEnvironmentVariablesById(String id);
    void deleteAll();
    List<EnvironmentVariables> findEnvironmentVariablesByIdAndFileName(String id, String fileName);
}
