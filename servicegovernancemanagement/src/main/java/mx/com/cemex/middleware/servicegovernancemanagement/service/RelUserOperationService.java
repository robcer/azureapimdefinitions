package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.util.List;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelUserOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;

public interface RelUserOperationService extends CrudService<RelUserOperation, Long> {
	
	List<RelUserOperation> findByUser(User user);

}
