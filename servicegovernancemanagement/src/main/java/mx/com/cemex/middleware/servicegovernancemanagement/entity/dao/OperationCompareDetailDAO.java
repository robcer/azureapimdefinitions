package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao;

import java.util.List;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.OperationCompareDetail;

public interface OperationCompareDetailDAO {

	OperationCompareDetail create(OperationCompareDetail operationCompareDetail);
	OperationCompareDetail merge(OperationCompareDetail operationCompareDetail);
    long countCompareDetail();
    List<OperationCompareDetail> findOperationCompareDetail(int firstResult, int maxResults);
    List<OperationCompareDetail> findAll();
    OperationCompareDetail findOperationCompareDetail(Long id);
    void deleteAll();
}
