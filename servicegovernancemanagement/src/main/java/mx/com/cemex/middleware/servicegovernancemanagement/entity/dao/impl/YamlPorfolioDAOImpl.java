package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlPortfolio;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlPortfolioDAO;

@Service("yamlPortfolioDAO")
public class YamlPorfolioDAOImpl implements YamlPortfolioDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public YamlPorfolioDAOImpl()
    {
    }

    YamlPorfolioDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public YamlPortfolio create(YamlPortfolio yamlPortfolio) {
        em.persist(yamlPortfolio);
        return yamlPortfolio;
    }

    @Transactional
    public YamlPortfolio merge(YamlPortfolio yamlPortfolio) {
        em.merge(yamlPortfolio);
        return yamlPortfolio;
    }

    public long countYamlPortfolio() {
        return (Long) em.createQuery("select count(o) from YamlPortfolio o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<YamlPortfolio> findYamlPortfolio(int firstResult, int maxResults) {
        return em.createQuery("select o from YamlPortfolio o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public YamlPortfolio findYamlPortfolio(Long id) {
        if (id == null) return null;
        return em.find(YamlPortfolio.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<YamlPortfolio> findAll() {
        return em.createQuery("select o from YamlPortfolio o").getResultList();
    }
    
    public YamlPortfolio findYamlPortfolioByProductName(String productName) {
        if (productName == null) return null;
        return em.createQuery("select o from YamlPortfolio o where o.productName = :productName", YamlPortfolio.class)
        		.setParameter("productName", productName).getSingleResult();
    }
    
    
}
