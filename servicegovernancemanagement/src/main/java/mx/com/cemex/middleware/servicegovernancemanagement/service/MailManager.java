package mx.com.cemex.middleware.servicegovernancemanagement.service;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.EmailBean;

public interface MailManager {

	void maintainGovernanceProcessFinished(final EmailBean emailBean);
	void publishingNotification(final EmailBean emailBean);
}
