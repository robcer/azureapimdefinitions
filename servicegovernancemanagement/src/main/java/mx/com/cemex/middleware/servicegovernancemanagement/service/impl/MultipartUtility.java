package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

import org.apache.log4j.Logger;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;

public class MultipartUtility {
	
	private static final Logger LOG  = Logger.getLogger(MultipartUtility.class);
	
	private HttpURLConnection httpConn;
	private DataOutputStream request;

	/**
	 * This constructor initializes a new HTTP POST request with content type is
	 * set to multipart/form-data
	 *
	 * @param requestURL
	 * @throws IOException
	 */
	public MultipartUtility(String requestURL) throws IOException {
		String encoded = java.util.Base64.getEncoder().encodeToString((IYamlBeansMasterConstants.CONFLUENCE_CREDENTIALS).getBytes(StandardCharsets.UTF_8));
		URL url = new URL(requestURL);
		httpConn = (HttpURLConnection) url.openConnection();
		httpConn.setUseCaches(false);
		httpConn.setDoOutput(true);
		httpConn.setDoInput(true);
		httpConn.setRequestMethod("POST");
		httpConn.setRequestProperty("Authorization", "Basic " + encoded);
		httpConn.setRequestProperty("Cache-Control", "no-cache");
		httpConn.setRequestProperty("X-Atlassian-Token", "no-check");
		httpConn.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + IYamlBeansMasterConstants.BOUNDARY);
		request = new DataOutputStream(httpConn.getOutputStream());
		
		//request = new DataOutputStream(new FileOutputStream("resources/operationcharts/request3.txt"));
	}

	/**
	 * Adds a form field to the request
	 *
	 * @param name
	 *            field name
	 * @param value
	 *            field value
	 */
	public void addFormField(String name, String value) throws IOException {
		request.writeBytes(IYamlBeansMasterConstants.CRLF + IYamlBeansMasterConstants.CRLF + IYamlBeansMasterConstants.TWO_HYPHENS + IYamlBeansMasterConstants.BOUNDARY + IYamlBeansMasterConstants.CRLF);
		request.writeBytes("Content-Disposition: form-data; name=\"" + name + "\"" + IYamlBeansMasterConstants.CRLF);
		request.writeBytes(IYamlBeansMasterConstants.CRLF);
		request.writeBytes(value + IYamlBeansMasterConstants.CRLF);
		request.flush();
	}

	/**
	 * Adds a upload file section to the request
	 *
	 * @param fieldName
	 *            name attribute in <input type="file" name="..." />
	 * @param uploadFile
	 *            a File to be uploaded
	 * @throws IOException
	 */
	public void addFilePart(String fieldName, File uploadFile) throws IOException {
		String fileName = uploadFile.getName();
		request.writeBytes(IYamlBeansMasterConstants.TWO_HYPHENS + IYamlBeansMasterConstants.BOUNDARY + IYamlBeansMasterConstants.CRLF);
		request.writeBytes("Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"" + IYamlBeansMasterConstants.CRLF);
		request.writeBytes("Content-Type: " + Files.probeContentType(uploadFile.toPath()) + IYamlBeansMasterConstants.CRLF);
		request.writeBytes(IYamlBeansMasterConstants.CRLF);
		byte[] bytes = Files.readAllBytes(uploadFile.toPath());
		request.write(bytes);
	}

	/**
	 * Adds a upload file section to the request
	 *
	 * @param fieldName
	 *            name attribute in <input type="file" name="..." />
	 * @param uploadFile
	 *            a File to be uploaded
	 * @throws IOException
	 */
	public void addStringAsBytesPart(String filePath, String str, String fileName) throws IOException {
		request.writeBytes(IYamlBeansMasterConstants.TWO_HYPHENS + IYamlBeansMasterConstants.BOUNDARY + IYamlBeansMasterConstants.CRLF);
		request.writeBytes("Content-Disposition: form-data; name=\"" + filePath + "\"; filename=\"" + fileName + "\"" + IYamlBeansMasterConstants.CRLF);
		request.writeBytes("Content-Type: text/plain" + IYamlBeansMasterConstants.CRLF);
		request.writeBytes(IYamlBeansMasterConstants.CRLF);
		byte[] bytes = str.getBytes(Charset.forName("UTF-8"));
		request.write(bytes);
	}

	/**
	 * Completes the request and receives response from the server.
	 *
	 * @return a list of Strings as response in case the server returned status
	 *         OK, otherwise an exception is thrown.
	 * @throws IOException
	 */
	public String finish() throws IOException {
		String response = "";
		request.writeBytes(IYamlBeansMasterConstants.TWO_HYPHENS + IYamlBeansMasterConstants.BOUNDARY + IYamlBeansMasterConstants.TWO_HYPHENS + IYamlBeansMasterConstants.CRLF);
		request.flush();
		request.close();
		int status = httpConn.getResponseCode();
		if (status == HttpURLConnection.HTTP_OK || status == HttpURLConnection.HTTP_CREATED) {
			InputStream responseStream = new BufferedInputStream(httpConn.getInputStream());
			BufferedReader responseStreamReader = new BufferedReader(new InputStreamReader(responseStream));
			String line = "";
			StringBuilder stringBuilder = new StringBuilder();
			while ((line = responseStreamReader.readLine()) != null) {
				stringBuilder.append(line).append("\n");
			}
			responseStreamReader.close();
			response = stringBuilder.toString();
			httpConn.disconnect();
		} else {
			LOG.info("Server returned non-OK status: " + status);
			throw new IOException("Server returned non-OK status: " + status);
		}
		return response;
	}

}
