package mx.com.cemex.middleware.servicegovernancemanagement.quartz.jobs;

import java.util.List;

import org.apache.log4j.Logger;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.JobKey;
import org.quartz.SchedulerException;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.WebApplicationContextUtils;
import org.quartz.impl.matchers.GroupMatcher;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.EmailBean;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.QuartzJobDetails;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.QuartzJobDetailsDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiGovernanceService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.MailManager;
import mx.com.cemex.middleware.servicegovernancemanagement.service.QuartzService;

@DisallowConcurrentExecution
public class QuartzReviewStatusMonitorJob implements Job  {

	private static final Logger LOG  = Logger.getLogger(QuartzReviewStatusMonitorJob.class);
	
	QuartzJobDetailsDAO quartzJobDetailsDAO;
	
	QuartzService quartzService;
	
	MailManager mailManager;
	
	ApiGovernanceService apiGovernanceService;
	
	public void execute(JobExecutionContext context) throws JobExecutionException {
		LOG.info("Inside execute method in QuartzReviewStatusMonitor");
		ApplicationContext springContext = WebApplicationContextUtils.getWebApplicationContext(ContextLoaderListener.getCurrentWebApplicationContext().getServletContext());
		quartzJobDetailsDAO = (QuartzJobDetailsDAO) springContext.getBean("quartzJobDetailsDAO");
		quartzService = (QuartzService) springContext.getBean("quartzService");
		apiGovernanceService = (ApiGovernanceService) springContext.getBean("apiGovernanceService");
//		mailManager = (MailManager) springContext.getBean("mailManager");
//		EmailBean emailBean = new EmailBean();
//		emailBean.setEmails(new String[] {"rogelio.reyocachu@ext.cemex.com"});
//		emailBean.setCcEmails(new String[] {"rogelio.reyo@gmail.com","rogelio.reyo@hotmail.com"});
//		emailBean.setExecutedProcess("Maintain Governance Confluence Process");
//		emailBean.setFrom("middlewareservices@cemex.com");
//		emailBean.setPersona("Middleware Services Team");
//		emailBean.setMessage("Maintain Governance Process Confluence Update finished.");
//		emailBean.setSubject("Maintain Governance Process Confluence Update finished.");
//		mailManager.maintainGovernanceProcessFinished(emailBean);
		//Uncoment the following code in order to enable Quartz Jobs
		try {
			quartzJobDetailsDAO.findAll();
			List<QuartzJobDetails> quartzJobDetailsList = quartzJobDetailsDAO.findAll();
	    	for(QuartzJobDetails quartzJobDetails: quartzJobDetailsList) {
	    		if(!isAlreadyOnJobStore(quartzJobDetails, context)) {
	    			LOG.info("About to add the job: " + quartzJobDetails.getJobName() + " to the Quartz Context.");
    				quartzService.scheduleNewJob(quartzJobDetails.getJobName(), quartzJobDetails.getJobGroup(), quartzJobDetails.getTriggerName(), quartzJobDetails.getJobExpression());
	    		} else {
	    			if(quartzJobDetails.getApplication().equals("GOVERNANCEMAINTAINMAITAINANCEPROCESS")) {
		    			LOG.info(quartzJobDetails.getJobName() + " is on execution?: " + isJobRunning(quartzJobDetails.getJobName(), quartzJobDetails.getJobGroup(), context));
		    			if(!isJobRunning(quartzJobDetails.getJobName(), quartzJobDetails.getJobGroup(), context)) {
			    			LOG.info("About to execute the job: " + quartzJobDetails.getJobName() + ".");
		    				apiGovernanceService.runMaitainanceProcess();
		    				System.exit(0);
		    			}
	    			}
	    		}
	    	}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
	}

	public boolean isJobRunning(String jobName, String groupName, JobExecutionContext context) {
		try {

			List<JobExecutionContext> currentJobs = context.getScheduler().getCurrentlyExecutingJobs();
			if(currentJobs!=null){
				for (JobExecutionContext jobExecutionContext : currentJobs) {
					if (jobName.equalsIgnoreCase(jobExecutionContext.getJobDetail().getKey().getName()) && groupName.equalsIgnoreCase(jobExecutionContext.getJobDetail().getKey().getGroup())) {
						return true;
					}
				}
			}
		} catch (SchedulerException e) {
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	private boolean isAlreadyOnJobStore(QuartzJobDetails quartzJobDetails, JobExecutionContext context) throws SchedulerException {
		for (String groupName : context.getScheduler().getJobGroupNames()) {
			for (JobKey jobKey : context.getScheduler().getJobKeys(GroupMatcher.jobGroupEquals(groupName))) {
				if((jobKey.getName() + "." + jobKey.getGroup()).equals(quartzJobDetails.getJobName() + "." + quartzJobDetails.getJobGroup())) {
					return true;
				}
			}
		}
		return false;
	}
}
