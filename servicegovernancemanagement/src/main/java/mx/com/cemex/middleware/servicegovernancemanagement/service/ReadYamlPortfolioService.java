package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.FileNotFoundException;
import java.lang.reflect.InvocationTargetException;

public interface ReadYamlPortfolioService {

	void readYamlPorfolio(String fileName) throws FileNotFoundException, IllegalAccessException, InvocationTargetException;
}
