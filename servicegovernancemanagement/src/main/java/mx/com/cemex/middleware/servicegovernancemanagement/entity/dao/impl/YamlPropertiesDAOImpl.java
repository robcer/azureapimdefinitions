package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlProperties;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlPropertiesDAO;

@Service("yamlPropertiesDAO")
public class YamlPropertiesDAOImpl implements YamlPropertiesDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public YamlPropertiesDAOImpl()
    {
    }

    YamlPropertiesDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public YamlProperties create(YamlProperties yamlProperties) {
        em.persist(yamlProperties);
        return yamlProperties;
    }

    @Transactional
    public void updateLogicalDeletedAll(String devOrgId) {
    	Query query = em.createNativeQuery("UPDATE YamlProperties SET deleted_At = SYSDATE WHERE dev_Org_Id = :devOrgId")
        		.setParameter("devOrgId", devOrgId);
    	query.executeUpdate();
    }

    @Transactional
    public YamlProperties merge(YamlProperties yamlProperties) {
        em.merge(yamlProperties);
        return yamlProperties;
    }

    public long countYamlProperties() {
        return (Long) em.createQuery("select count(o) from YamlProperties o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<YamlProperties> findYamlProperties(int firstResult, int maxResults) {
        return em.createQuery("select o from YamlProperties o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public YamlProperties findYamlProperties(Long id) {
        if (id == null) return null;
        return em.find(YamlProperties.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<YamlProperties> findAll() {
        return em.createQuery("select o from YamlProperties o").getResultList();
    }
    
    public YamlProperties findYamlPropertiesByFullPathAndProductNameAndXIbmName(String fullPath, String productName, String xIbmName, String devOrgId, String apiVersion) {
        return em.createQuery("select o from YamlProperties o where o.fullPath = :fullPath and o.productName = :productName and o.xIbmName = :xIbmName and o.devOrgId = :devOrgId and o.apiVersion = :apiVersion", YamlProperties.class)
        		.setParameter("fullPath", fullPath)
        		.setParameter("productName", productName)
        		.setParameter("xIbmName", xIbmName)
        		.setParameter("devOrgId", devOrgId)
        		.setParameter("apiVersion", apiVersion)
        		.getSingleResult();
    }
    
    public List<YamlProperties> findAllYamlPropertiesByFullPathAndProductNameAndXIbmName(String productName,String xIbmName, String basePath, String devOrgId) {
        return em.createQuery("select o from YamlProperties o where o.basePath = :basePath and o.productName = :productName and o.xIbmName = :xIbmName and o.devOrgId = :devOrgId", YamlProperties.class)
        		.setParameter("basePath", basePath)
        		.setParameter("productName", productName)
        		.setParameter("xIbmName", xIbmName)
        		.setParameter("devOrgId", devOrgId)
        		.getResultList();
    }
    
    public String findOneAzureHostByDevOrgIdAndApiNameAndApiVersion(String devOrgId, String apiName, String apiVersion) {
        return (String) em.createQuery("SELECT DISTINCT o.azureHost FROM YamlProperties o WHERE o.devOrgId = :devOrgId AND o.apiName = :apiName AND o.apiVersion = :apiVersion AND ROWNUM < 2")
        		.setParameter("devOrgId", devOrgId)
        		.setParameter("apiName", apiName)
        		.setParameter("apiVersion", apiVersion)
        		.getSingleResult();
    }
}
