package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentVariables;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentVariablesDAO;

@Service("environmentVariablesDAO")
public class EnvironmentVariablesDAOImpl implements EnvironmentVariablesDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public EnvironmentVariablesDAOImpl()
    {
    }

    EnvironmentVariablesDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public EnvironmentVariables create(EnvironmentVariables environmentVariables) {
        em.persist(environmentVariables);
        return environmentVariables;
    }

    @Transactional
    public EnvironmentVariables merge(EnvironmentVariables environmentVariables) {
        em.merge(environmentVariables);
        return environmentVariables;
    }

    public long countEnvironmentVariables() {
        return (Long) em.createQuery("select count(o) from EnvironmentVariables o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<EnvironmentVariables> findEnvironmentVariables(int firstResult, int maxResults) {
        return em.createQuery("select o from EnvironmentVariables o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public EnvironmentVariables findEnvironmentVariables(Long id) {
        if (id == null) return null;
        return em.find(EnvironmentVariables.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<EnvironmentVariables> findAll() {
        return em.createQuery("select o from EnvironmentVariables o").getResultList();
    }
    
    public EnvironmentVariables findEnvironmentVariablesById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from EnvironmentVariables o where o.id = :id", EnvironmentVariables.class)
        		.setParameter("id", id).getSingleResult();
    }
    
    public List<EnvironmentVariables> findEnvironmentVariablesByIdAndFileName(String id, String fileName) {
        return em.createQuery("select o from EnvironmentVariables o where o.id = :id and o.fileName = :fileName", EnvironmentVariables.class)
        		.setParameter("id", id)
        		.setParameter("fileName", fileName)
        		.getResultList();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from EnvironmentVariables");
    	query.executeUpdate();
    }

}