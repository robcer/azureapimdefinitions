package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "application")
public class Application {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id_application")
	private Long applicationMenuId;

    @Column(name = "application_title")
	private String applicationTitle;

    @Column(name = "application_icon")
	private String applicationIcon;

    @Column(name = "application_url")
	private String applicationUrl;

    @Column(name = "application_code")
	private String applicationCode;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_menu", nullable = false, updatable = true, insertable = true)
    private Menu menu;

	@OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "application")
	private Set<ApplicationRole> roles = new HashSet<ApplicationRole>();

	public Long getApplicationMenuId() {
		return applicationMenuId;
	}

	public void setApplicationMenuId(Long applicationMenuId) {
		this.applicationMenuId = applicationMenuId;
	}

	public String getApplicationTitle() {
		return applicationTitle;
	}

	public void setApplicationTitle(String applicationTitle) {
		this.applicationTitle = applicationTitle;
	}

	public String getApplicationIcon() {
		return applicationIcon;
	}

	public void setApplicationIcon(String applicationIcon) {
		this.applicationIcon = applicationIcon;
	}

	public String getApplicationUrl() {
		return applicationUrl;
	}

	public void setApplicationUrl(String applicationUrl) {
		this.applicationUrl = applicationUrl;
	}

	public Menu getMenu() {
		return menu;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Set<ApplicationRole> getRoles() {
		return roles;
	}

	public void setRoles(Set<ApplicationRole> roles) {
		this.roles = roles;
	}

	public String getApplicationCode() {
		return applicationCode;
	}

	public void setApplicationCode(String applicationCode) {
		this.applicationCode = applicationCode;
	}
    
}
