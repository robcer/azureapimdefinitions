package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.util.List;

import org.resthub.common.service.CrudService;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApplicationRole;

public interface ApplicationRoleService extends CrudService<ApplicationRole, Long> {

	List<ApplicationRole> findApplicationRolesByAuthorities();
}
