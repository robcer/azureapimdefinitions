package mx.com.cemex.middleware.servicegovernancemanagement.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.Api;


public interface ApiRepository extends JpaRepository<Api, Long> {
	
	Api findById(String id);
}
