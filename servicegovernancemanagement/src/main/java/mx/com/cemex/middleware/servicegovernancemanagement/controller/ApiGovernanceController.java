package mx.com.cemex.middleware.servicegovernancemanagement.controller;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.text.ParseException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestClientException;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ApiGovernanceService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.AzureApiCatalogService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.CompareBluemixAzureService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.CompareFilesService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ConfluenceReportService;

@Controller
@RequestMapping(value = "/v1/its", produces = "application/json")
public class ApiGovernanceController {
	
	private static final Logger LOG  = Logger.getLogger(ApiGovernanceController.class);
	
	@Autowired
	ApiGovernanceService apiGovernanceService;
	
	@Autowired
	CompareFilesService compareFilesService;
	
	@Autowired
	CompareBluemixAzureService compareBluemixAzureService;

	@Autowired
	ConfluenceReportService confluenceReportService;

	@Autowired
	AzureApiCatalogService azureApiCatalogService;

	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;

    @RequestMapping(value = "/processes")
    @ResponseBody
    public void governanceProcesses() throws JsonParseException, JsonMappingException, RestClientException, IOException, InterruptedException {
    	LOG.info("Starting the execution of governanceProcesses();" + System.getProperty("APPINSIGHTS_INSTRUMENTATIONKEY"));
    	apiGovernanceService.runMaitainanceProcess();
    }

    @RequestMapping(value = "/azureapicatalogs", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void mantainAzureApiCatalog() throws MalformedURLException, ParseException, IOException, org.json.simple.parser.ParseException, URISyntaxException {
		List<EnvironmentCatalog> environmentIncludingAzureCatalogs = environmentCatalogDAO.findAllIncludingAzure();
		for (EnvironmentCatalog environmentCatalog : environmentIncludingAzureCatalogs) {
			if(environmentCatalog.getPlatformCatalog().getIdentifier().equals(IYamlBeansMasterConstants.AZURE_API_MANAGEMENT_IDENTIFIER)) {
				azureApiCatalogService.mantainAzureApiCatalog(environmentCatalog);
			}
		}
    }

    @RequestMapping(value = "/apiinventories", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void maintainEnvironmentAzureAPIInventory() throws MalformedURLException, IOException, ParseException, org.json.simple.parser.ParseException {
		List<EnvironmentCatalog> environmentIncludingAzureCatalogs = environmentCatalogDAO.findAllIncludingAzure();
		for (EnvironmentCatalog environmentCatalog : environmentIncludingAzureCatalogs) {
				if(environmentCatalog.getPlatformCatalog().getIdentifier().equals(IYamlBeansMasterConstants.AZURE_API_MANAGEMENT_IDENTIFIER)) {
					confluenceReportService.maintainEnvironmentAzureAPIInventory(environmentCatalog);
				}
		}
    }

    @RequestMapping(value = "/comparesummaries/{guid}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public void prepareAndCompareVersions(@PathVariable("guid") String guid) throws InvalidKeyException, NoSuchAlgorithmException, IOException, org.json.simple.parser.ParseException, ParseException, InterruptedException, ParserConfigurationException, SAXException, TransformerFactoryConfigurationError, TransformerException {
		compareFilesService.prepareAndCompareVersions(guid);
    }
}
