package mx.com.cemex.middleware.servicegovernancemanagement.bean;

public class BitbucketSrcHeadersBean {
	
	private String pathToFile;
	private String message;
	private String author;
	private String destinationBranch;
	private String sourceBranch;
	private boolean closeSourceBranch;
	private String title;
	private String description;
	private Long pullrequestId;
	private String user;
	private String repository;
	private String xml;
	private String fileName;
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getUser() {
		return user;
	}
	public String getXml() {
		return xml;
	}
	public void setXml(String xml) {
		this.xml = xml;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getRepository() {
		return repository;
	}
	public void setRepository(String repository) {
		this.repository = repository;
	}
	public Long getPullrequestId() {
		return pullrequestId;
	}
	public void setPullrequestId(Long pullrequestId) {
		this.pullrequestId = pullrequestId;
	}
	public String getPathToFile() {
		return pathToFile;
	}
	public void setPathToFile(String pathToFile) {
		this.pathToFile = pathToFile;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getAuthor() {
		return author;
	}
	public void setAuthor(String author) {
		this.author = author;
	}
	public String getDestinationBranch() {
		return destinationBranch;
	}
	public void setDestinationBranch(String destinationBranch) {
		this.destinationBranch = destinationBranch;
	}
	public String getSourceBranch() {
		return sourceBranch;
	}
	public void setSourceBranch(String sourceBranch) {
		this.sourceBranch = sourceBranch;
	}
	public boolean isCloseSourceBranch() {
		return closeSourceBranch;
	}
	public void setCloseSourceBranch(boolean closeSourceBranch) {
		this.closeSourceBranch = closeSourceBranch;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	

}
