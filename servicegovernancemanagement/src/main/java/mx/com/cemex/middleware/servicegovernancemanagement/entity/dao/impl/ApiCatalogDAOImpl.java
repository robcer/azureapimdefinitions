package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCatalogDAO;

@Service("apiCatalogDAO")
public class ApiCatalogDAOImpl implements ApiCatalogDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ApiCatalogDAOImpl()
    {
    }

    ApiCatalogDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ApiCatalog create(ApiCatalog apiCatalog) {
        em.persist(apiCatalog);
        return apiCatalog;
    }

    @Transactional
    public ApiCatalog merge(ApiCatalog apiCatalog) {
        em.merge(apiCatalog);
        return apiCatalog;
    }

    public long countApiCatalog() {
        return (Long) em.createQuery("select count(o) from ApiCatalog o").getSingleResult();
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiCatalog> findApiCatalog(int firstResult, int maxResults) {
        return em.createQuery("select o from ApiCatalog o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ApiCatalog findApiCatalog(Long id) {
        if (id == null) return null;
        return em.find(ApiCatalog.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiCatalog> findAll() {
        return em.createQuery("select o from ApiCatalog o").getResultList();
    }
    
    public List<ApiCatalog> findAllByDevOrgId(String devOrgId) {
        if (devOrgId == null) return null;
        return em.createQuery("select o from ApiCatalog o where o.devOrgId = :devOrgId AND o.deploymentState = 'running'", ApiCatalog.class).setParameter("devOrgId", devOrgId).getResultList();
    }
    
    public ApiCatalog findApiCatalogById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from ApiCatalog o where o.id = :id", ApiCatalog.class)
        		.setParameter("id", id).getSingleResult();
    }
    
    public List<ApiCatalog> findApiCatalogByDeployedId(String deploymentId) {
        if (deploymentId == null) return null;
        return em.createQuery("select o from ApiCatalog o where o.deploymentId = :deploymentId", ApiCatalog.class)
        		.setParameter("deploymentId", deploymentId).getResultList();
    }
    
    public ApiCatalog findApiCatalogByBasePathAndNameAndEnvIdAndDevOrgId(String basePath, String name, String envId, String devOrgId, String deploymentState) {
//    	LOG.info.println(basePath + "|" + name + "|" + envId + "|" + devOrgId + "|" + deploymentState);
        return em.createQuery("select o from ApiCatalog o where o.basePath = :basePath AND o.name = :name AND o.envId = :envId AND o.devOrgId = :devOrgId AND o.deploymentState = :deploymentState", ApiCatalog.class)
        		.setParameter("basePath", basePath)
        		.setParameter("name", name)
        		.setParameter("envId", envId)
        		.setParameter("deploymentState", deploymentState)
        		.setParameter("devOrgId", devOrgId)
        		.getSingleResult();
    }

}