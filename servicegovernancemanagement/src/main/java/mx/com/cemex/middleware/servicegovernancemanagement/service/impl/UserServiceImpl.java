package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;

import org.resthub.common.service.CrudServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.LoginResponse;
import mx.com.cemex.middleware.servicegovernancemanagement.bean.Oauth2;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.User;
import mx.com.cemex.middleware.servicegovernancemanagement.exception.CustomException;
import mx.com.cemex.middleware.servicegovernancemanagement.repository.UserRepository;
import mx.com.cemex.middleware.servicegovernancemanagement.security.JwtTokenProvider;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UserService;

@Named("userService")
public class UserServiceImpl extends CrudServiceImpl<User, Long, UserRepository> implements UserService {

	@Override
	@Inject
	public void setRepository(UserRepository userRepository) {
		super.setRepository(userRepository);
	}

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private JwtTokenProvider jwtTokenProvider;

	@Autowired
	private AuthenticationManager authenticationManager;

	public LoginResponse signin(String username, String password) {
		try {
			authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
			String jwt = jwtTokenProvider.createToken(username, this.repository.findByUsername(username).getRoles());
			LoginResponse loginResponseDTO = new LoginResponse();
			loginResponseDTO.setJwt(jwt);
			Oauth2 oauth2 = new Oauth2();
			oauth2.setAccessToken(jwt);
			oauth2.setRefreshToken(jwt);
			oauth2.setRegion("US");
			oauth2.setScope("security");
			oauth2.setExpiresIn(14400L);
			loginResponseDTO.setOauth2(oauth2);
			return loginResponseDTO;
		} catch (AuthenticationException e) {
			e.printStackTrace();
			throw new CustomException("Invalid username/password supplied", HttpStatus.UNPROCESSABLE_ENTITY);
		}
	}

	// public String signup(User user) {
	// if (!userRepository.existsByUsername(user.getUsername())) {
	// user.setPassword(passwordEncoder.encode(user.getPassword()));
	// userRepository.save(user);
	// return jwtTokenProvider.createToken(user.getUsername(), user.getRoles());
	// } else {
	// throw new CustomException("Username is already in use",
	// HttpStatus.UNPROCESSABLE_ENTITY);
	// }
	// }

	// public void delete(String username) {
	// userRepository.deleteByUsername(username);
	// }

	public User search(String username) {
		User user = this.repository.findByUsername(username);
		if (user == null) {
			throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
		}
		return user;
	}

	public User whoami(HttpServletRequest httpServletRequest) {
		return this.repository.findByUsername(jwtTokenProvider.getUsername(jwtTokenProvider.resolveToken(httpServletRequest)));
	}

	@Override
	public User findByUsername(String username) {
		User user = this.repository.findByUsername(username);
		if (user == null) {
			throw new CustomException("The user doesn't exist", HttpStatus.NOT_FOUND);
		}
		return user;
	}

}
