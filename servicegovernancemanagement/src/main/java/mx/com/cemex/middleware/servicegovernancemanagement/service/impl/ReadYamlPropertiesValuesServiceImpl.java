package mx.com.cemex.middleware.servicegovernancemanagement.service.impl;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.lang.reflect.InvocationTargetException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.persistence.NoResultException;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.yaml.snakeyaml.Yaml;

import mx.com.cemex.middleware.servicegovernancemanagement.IYamlBeansMasterConstants;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiOperationCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.ProjectCatalog;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.RelProjectOperation;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.YamlProperties;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiHistoryCallDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiOperationCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.EnvironmentCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ProjectCatalogDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.RelProjectOperationDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.YamlPropertiesDAO;
import mx.com.cemex.middleware.servicegovernancemanagement.service.ReadYamlPropertiesValuesService;
import mx.com.cemex.middleware.servicegovernancemanagement.service.UtilitiesService;

import com.ebay.xcelite.Xcelite;
import com.ebay.xcelite.reader.SheetReader;
import com.ebay.xcelite.sheet.XceliteSheet;
import com.ebay.xcelite.writer.SheetWriter;

@Service("readYamlPropertiesValuesService")
public class ReadYamlPropertiesValuesServiceImpl implements ReadYamlPropertiesValuesService {
	
	private static final Logger LOG  = Logger.getLogger(ReadYamlPropertiesValuesServiceImpl.class);

	@Autowired
	YamlPropertiesDAO yamlPropertiesDAO;
	
	@Autowired
	ApiHistoryCallDAO apiHistoryCallDAO;
	
	@Autowired
	EnvironmentCatalogDAO environmentCatalogDAO;
	
	@Autowired
	ApiOperationCatalogDAO apiOperationCatalogDAO;
	
	@Autowired
	ProjectCatalogDAO projectCatalogDAO;
	
	@Autowired
	RelProjectOperationDAO relProjectOperationDAO;
	
	@Autowired
	UtilitiesService utilitiesService;

	public String readPropertiesValues() throws FileNotFoundException, IllegalAccessException, InvocationTargetException {
		SimpleDateFormat dateFormatter = new SimpleDateFormat("ddMMyyyyHHmmssZ");
		String fileCreationDate = dateFormatter.format(new Date());
		List<EnvironmentCatalog> environmentCatalogs = environmentCatalogDAO.findAll();
		Xcelite xcelite = new Xcelite();
		Xcelite xceliteJustDev = new Xcelite();
		for(EnvironmentCatalog environmentCatalog: environmentCatalogs) {
			yamlPropertiesDAO.updateLogicalDeletedAll(environmentCatalog.getId());
			XceliteSheet sheet = xcelite.createSheet(environmentCatalog.getId());
			SheetWriter<YamlProperties> writer = sheet.getBeanWriter(YamlProperties.class);
			SheetWriter<YamlProperties> writerJustDev = null;
			File dir = new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase());
			File[] directoryListing = dir.listFiles();
			List<YamlProperties> yamlProperties = new ArrayList<YamlProperties>();
			if (directoryListing != null) {
				for (File yamlFile : directoryListing) {
					Yaml yaml = new Yaml();
					if (yamlFile.getName().contains("_product_")) {
						InputStream ios = new FileInputStream(yamlFile);
						Map<String, Object> map = (Map<String, Object>) yaml.load(ios);
						JSONObject productJson = (JSONObject) utilitiesService._convertToJson(map);
						Set<String> productApis = productJson.optJSONObject("apis").keySet();
						Iterator<String> iterateProductApis = productApis.iterator();
						do {
							String yamlFileName = IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + environmentCatalog.getRegion().toUpperCase() + "-" + environmentCatalog.getDevOrgNameEnv().toUpperCase() + "\\" + productJson.optJSONObject("apis")
									.optJSONObject(iterateProductApis.next().toString()).optString("$ref");
							LOG.info("Reading information from YAML file: " + yamlFileName);
							InputStream iosProductApi = new FileInputStream(new File(yamlFileName));
							Map<String, Object> mapProductApi = (Map<String, Object>) yaml.load(iosProductApi);
							yamlProperties.addAll(
									fillYamlProperties((JSONObject) utilitiesService._convertToJson(mapProductApi), productJson, environmentCatalog.getName()));
						} while (iterateProductApis.hasNext());
					}
				}
				if (environmentCatalog.getName().equals("Development") && environmentCatalog.getId().equals("58e2aa5f0cf26aa41002a447")) {
					XceliteSheet sheetJustDev = xceliteJustDev.createSheet(environmentCatalog.getId());
					writerJustDev = sheetJustDev.getBeanWriter(YamlProperties.class);
					Xcelite xceliteOld = new Xcelite(lastFileModified(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH));
					XceliteSheet sheetOld = xceliteOld.getSheet("58e2aa5f0cf26aa41002a447");
					SheetReader<YamlProperties> simpleReader = sheetOld.getBeanReader(YamlProperties.class);
					simpleReader.skipHeaderRow(true);
					Collection<YamlProperties> yamlProperties2 = simpleReader.read();
					for (YamlProperties yamlIndex2 : yamlProperties2) {
						for (YamlProperties yamlIndex : yamlProperties) {
							if (yamlIndex.getFullPath().trim().toUpperCase()
									.equals(yamlIndex2.getFullPath().trim().toUpperCase())) {
								maintainProjectOperationRelationship(yamlIndex2.getFullPath().trim().toUpperCase(), yamlIndex2.getProject());
								yamlIndex.setProject(yamlIndex2.getProject());
								yamlIndex.setDeprecated(yamlIndex2.getDeprecated());
								yamlIndex.setBc(yamlIndex2.getBc());
								yamlIndex.setReportedResponsible(yamlIndex2.getReportedResponsible());
								yamlIndex.setNetPath(yamlIndex2.getNetPath());
								yamlIndex.setStoreProcedure(yamlIndex2.getStoreProcedure());
								yamlIndex.setStoreProcedureAssigned(yamlIndex2.getStoreProcedureAssigned());
							}
						}
					}
				}
				for(YamlProperties yamlProperties1: yamlProperties) {
					try {
						YamlProperties persistedYamlProperties = yamlPropertiesDAO.findYamlPropertiesByFullPathAndProductNameAndXIbmName(yamlProperties1.getFullPath(), 
								yamlProperties1.getProductName(), yamlProperties1.getxIbmName(), environmentCatalog.getId(), yamlProperties1.getApiVersion());
						persistedYamlProperties.setUpdatedAt(new Date());
						persistedYamlProperties.setApiVersion(yamlProperties1.getApiVersion());
						persistedYamlProperties.setAzureHost(yamlProperties1.getAzureHost());
						persistedYamlProperties.setDescription(yamlProperties1.getDescription());
						persistedYamlProperties.setInvokeTargetURL(yamlProperties1.getInvokeTargetURL());
						persistedYamlProperties.setIsCasePrepared(yamlProperties1.getIsCasePrepared());
						persistedYamlProperties.setIsIntegratedWithNET(yamlProperties1.getIsIntegratedWithNET());
						persistedYamlProperties.setOperationDescription(yamlProperties1.getOperationDescription());
						persistedYamlProperties.setSecurity(yamlProperties1.getSecurity());
						persistedYamlProperties.setProject(yamlProperties1.getProject());
						persistedYamlProperties.setDeprecated(yamlProperties1.getDeprecated());
						persistedYamlProperties.setStoreProcedure(yamlProperties1.getStoreProcedure());
						persistedYamlProperties.setProduct(yamlProperties1.getProduct());
						persistedYamlProperties.setProductName(yamlProperties1.getProductName());
						persistedYamlProperties.setDeletedAt(null);
						persistedYamlProperties.setProductVersion(yamlProperties1.getProductVersion());
						yamlPropertiesDAO.merge(persistedYamlProperties);
					} catch (NoResultException e) {
						yamlProperties1.setEnvironment(environmentCatalog.getName());
						yamlProperties1.setDevOrgId(environmentCatalog.getId());
						yamlProperties1.setCreatedAt(new Date());
						try {
							yamlPropertiesDAO.create(yamlProperties1);			
						} catch (Exception e1) {
						}
					}
				}
				writer.write(yamlProperties);
				if (environmentCatalog.getName().equals("Development") && environmentCatalog.getId().equals("58e2aa5f0cf26aa41002a447")) {
					writerJustDev.write(yamlProperties);
				}
			}
		}
		xcelite.write(
				new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + fileCreationDate + "_APIInventory_AllEnvironments.xlsx"));
		xceliteJustDev.write(
				new File(IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + fileCreationDate + "_APIInventory_AllEnvironmentsJustDev.xlsx"));
		return IYamlBeansMasterConstants.LOCAL_RESOURCE_PATH + "\\" + fileCreationDate + "_APIInventory_AllEnvironmentsJustDev.xlsx";
	}
	
	private void maintainProjectOperationRelationship(String fullPath, String projectName) {
		try {
			List<ApiOperationCatalog> apiOperationCatalogs = apiOperationCatalogDAO.findApiOperationCatalogByFullPathConcatenated(fullPath);
			for(ApiOperationCatalog apiOperationCatalog: apiOperationCatalogs) {
				ProjectCatalog projectCatalog = projectCatalogDAO.findProjectCatalogByName(projectName);
				if(relProjectOperationDAO.findAllByProjectAndOperation(projectCatalog, apiOperationCatalog).isEmpty()) {
					RelProjectOperation relProjectOperation = new RelProjectOperation();
//					relProjectOperation.setApiOperationCatalog(apiOperationCatalog);
					relProjectOperation.setProjectCatalog(projectCatalog);
					relProjectOperation.setRegisteredAt(new Date());
					relProjectOperationDAO.create(relProjectOperation);
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
		}
	}

	public File lastFileModified(String dir) {
		File fl = new File(dir);
		File[] files = fl.listFiles(new FileFilter() {
			public boolean accept(File file) {
				return file.isFile();
			}
		});
		long lastMod = Long.MIN_VALUE;
		File choice = null;
		for (File file : files) {
			if (file.lastModified() > lastMod && file.getName().contains("APIInventory_AllEnvironments")) {
				choice = file;
				lastMod = file.lastModified();
			}
		}
		return choice;
	}

	private List<YamlProperties> fillYamlProperties(JSONObject jsonObject, JSONObject productJson, String environment) {
		List<YamlProperties> yamlPropertiesList = new ArrayList<YamlProperties>();
		if (jsonObject.optJSONObject("paths") != null) {
			Set<String> paths = jsonObject.optJSONObject("paths").keySet();
			Iterator<String> iteratePaths = paths.iterator();
			do {
				String path = iteratePaths.next().toString();
				Set<String> methods = jsonObject.optJSONObject("paths").optJSONObject(path).keySet();
				Iterator<String> iterateMethods = methods.iterator();
				do {
					String method = iterateMethods.next().toString();
					YamlProperties yamlProperties = new YamlProperties();
					yamlProperties.setPath(path);
					yamlProperties.setBasePath(jsonObject.optString("basePath"));
					if (jsonObject.optJSONObject("info") != null) {
						yamlProperties.setApiName(jsonObject.optJSONObject("info").optString("title"));
						yamlProperties.setApiVersion(jsonObject.optJSONObject("info").optString("version"));
						yamlProperties.setDescription(jsonObject.optJSONObject("info").optString("description"));
						if (jsonObject.optJSONObject("info").optJSONObject("contact") != null) {
							yamlProperties.setEmail(
									jsonObject.optJSONObject("info").optJSONObject("contact").optString("email"));
							yamlProperties.setResponsible(
									jsonObject.optJSONObject("info").optJSONObject("contact").optString("name"));
						}
					}
					yamlProperties.setProduct(productJson.optJSONObject("info").optString("title"));
					yamlProperties.setProductVersion(productJson.optJSONObject("info").optString("version"));
					yamlProperties.setProductName(productJson.optJSONObject("info").optString("name"));
					if (jsonObject.optJSONArray("security") != null) {
						yamlProperties.setSecurity(jsonObject.optJSONArray("security").toString());
					}
					yamlProperties.setxIbmName(jsonObject.optJSONObject("info").optString("x-ibm-name"));
					yamlProperties.setMethod(method.toUpperCase());
					yamlProperties.setFullPath(method.toUpperCase() + jsonObject.optString("basePath") + path);
					//yamlProperties.setCountPathResourceMethod(apiHistoryCallDAO.countByPathResourceAndMethod(jsonObject.optString("basePath") + path + "|" + method.toUpperCase()).toString());
					if (jsonObject.optJSONObject("paths").optJSONObject(path).optJSONObject(method) != null) {
						yamlProperties.setOperationId(jsonObject.optJSONObject("paths").optJSONObject(path).optJSONObject(method).optString("operationId"));
						yamlProperties.setOperationDescription(jsonObject.optJSONObject("paths").optJSONObject(path).optJSONObject(method).optString("description"));
					}
					yamlProperties.setIsIntegratedWithNET("NO");
					yamlProperties.setIsCasePrepared("NO");
					if (jsonObject.optJSONObject("x-ibm-configuration") == null) {
						yamlProperties.setIsIntegratedWithNET("MISSINGXIBMCONFIGURATION");
						yamlProperties.setIsCasePrepared("MISSINGXIBMCONFIGURATION");
					} else {
						String azureHost = "";
						if (jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("properties") != null) {
							Set<String> variables = jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("properties").keySet();
							Iterator<String> iterateVariables = variables.iterator();
							do {
								String variable = iterateVariables.next().toString();
								String variableValue = jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("properties").optJSONObject(variable).optString("value");
								if (variableValue.contains("azurewebsites.net") || variableValue.contains("tm.cemex.com")) {
									azureHost = jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("properties").optJSONObject(variable).optString("value");
								}
							} while (iterateVariables.hasNext());
						}
						if (jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("catalogs") != null) {
							if(jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("catalogs").optJSONObject(environment) != null) {
								Set<String> properties = jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("catalogs").optJSONObject(environment).optJSONObject("properties").keySet();
								Iterator<String> iterateProperties = properties.iterator();
								do {
									String property = iterateProperties.next().toString();
									String propertyValue = jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("catalogs").optJSONObject(environment).optJSONObject("properties").optString(property);
									if (propertyValue.contains("azurewebsites.net") || propertyValue.contains("https://api01.tm.cemex.com")) {
										azureHost = propertyValue;
									}
								} while (iterateProperties.hasNext());
							}
						}
						yamlProperties.setAzureHost(azureHost);
						if (jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("assembly") != null) {
							if (jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("assembly").optJSONArray("execute") != null) {
								JSONArray jsonArrayAssemblyExecute = jsonObject.optJSONObject("x-ibm-configuration").optJSONObject("assembly").optJSONArray("execute");
							    for (int i = 0, sizeI = jsonArrayAssemblyExecute.length(); i < sizeI; i++) {
							    	JSONObject assemblyExecute = jsonArrayAssemblyExecute.getJSONObject(i);
									String[] assemblyExecutesNames = JSONObject.getNames(assemblyExecute);
									for (String assemblyExecuteName : assemblyExecutesNames) {
										if(assemblyExecuteName.equals("operation-switch")) {
											JSONObject jsonObjectSwitch = assemblyExecute.optJSONObject("operation-switch");
											if(jsonObjectSwitch.optJSONArray("case") != null) {
												JSONArray jsonArraySwitchExecute = jsonObjectSwitch.optJSONArray("case");
											    for (int j = 0, sizeJ = jsonArraySwitchExecute.length(); j < sizeJ; j++) {
											    	JSONObject switchCase = jsonArraySwitchExecute.getJSONObject(j);
											    	if(switchCase.optJSONArray("operations").toString().contains(method) || switchCase.optJSONArray("operations").toString().contains(path)) {
														yamlProperties.setIsCasePrepared(switchCase.optJSONArray("operations").toString());
												    	if(switchCase.optJSONArray("execute") != null) {
															JSONArray switchCaseExecutes = switchCase.optJSONArray("execute");
														    for (int k = 0, sizeK = switchCaseExecutes.length(); k < sizeK; k++) {
														    	JSONObject switchCaseExecute = switchCaseExecutes.getJSONObject(k);
																String[] switchCaseExecuteNames = JSONObject.getNames(switchCaseExecute);
																for (String switchCaseExecuteName : switchCaseExecuteNames) {
																	if(switchCaseExecuteName.equals("invoke")) {
																		yamlProperties.setIsIntegratedWithNET(switchCaseExecute.getJSONObject("invoke").optString("title"));
																		yamlProperties.setInvokeTargetURL(switchCaseExecute.getJSONObject("invoke").optString("target-url"));
																	}
																}
														    }
												    	}
											    	}
											    }
											}
										}
									}
							    }
							}
						}
						
					}
					if (!method.equals("parameters") && !yamlProperties.getApiName().toLowerCase().contains("test")) {
						yamlPropertiesList.add(yamlProperties);
					}
				} while (iterateMethods.hasNext());
			} while (iteratePaths.hasNext());
		}
		if (jsonObject.optJSONObject("paths") != null) {
			
		}
		return yamlPropertiesList;
	}

}
