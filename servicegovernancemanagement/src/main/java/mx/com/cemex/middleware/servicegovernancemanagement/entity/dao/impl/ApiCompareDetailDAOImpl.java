package mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import mx.com.cemex.middleware.servicegovernancemanagement.entity.ApiCompareDetail;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.dao.ApiCompareDetailDAO;

@Service("apiCompareDetailDAO")
public class ApiCompareDetailDAOImpl implements ApiCompareDetailDAO {

    @PersistenceContext(unitName="persistenceUnitSQLServer")
    transient EntityManager em;

    public ApiCompareDetailDAOImpl()
    {
    }

    ApiCompareDetailDAOImpl(EntityManager em)
    {
        this.em = em;
    }

    @Transactional
    public ApiCompareDetail create(ApiCompareDetail apiCompareDetail) {
        em.persist(apiCompareDetail);
        return apiCompareDetail;
    }

    @Transactional
    public ApiCompareDetail merge(ApiCompareDetail apiCompareDetail) {
        em.merge(apiCompareDetail);
        return apiCompareDetail;
    }

    public long countCompareDetail() {
        return (Long) em.createQuery("select count(o) from ApiCompareDetail o").getSingleResult();
    }

    @Transactional
    public void deleteAll() {
    	Query query = em.createNativeQuery("DELETE from ApiCompareDetail");
    	query.executeUpdate();
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiCompareDetail> findApiCompareDetail(int firstResult, int maxResults) {
        return em.createQuery("select o from ApiCompareDetail o").setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public ApiCompareDetail findApiCompareDetail(Long id) {
        if (id == null) return null;
        return em.find(ApiCompareDetail.class, id);
    }
    
    @SuppressWarnings("unchecked")
    public List<ApiCompareDetail> findAll() {
        return em.createQuery("select o from ApiCompareDetail o order by o.idApiCompareDetail DESC").getResultList();
    }
    
    public List<ApiCompareDetail> findAllByDevOrgId(String devOrgNameEnv) {
        if (devOrgNameEnv == null) return null;
        return em.createQuery("select o from ApiCompareDetail o where o.devOrgNameEnv = :devOrgNameEnv", ApiCompareDetail.class).setParameter("devOrgNameEnv", devOrgNameEnv).getResultList();
    }
    
    public ApiCompareDetail findApiCompareDetailById(String id) {
        if (id == null) return null;
        return em.createQuery("select o from ApiCompareDetail o where o.id = :id", ApiCompareDetail.class)
        		.setParameter("id", id).getSingleResult();
    }

}