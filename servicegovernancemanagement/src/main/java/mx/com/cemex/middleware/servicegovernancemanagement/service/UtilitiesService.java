package mx.com.cemex.middleware.servicegovernancemanagement.service;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.util.List;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactoryConfigurationError;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.ParseException;
import org.xml.sax.SAXException;

import mx.com.cemex.middleware.servicegovernancemanagement.bean.ConfluenceIntegrationBean;
import mx.com.cemex.middleware.servicegovernancemanagement.entity.EnvironmentCatalog;

public interface UtilitiesService {
	
	Object _convertToJson(Object o) throws JSONException;
	JSONObject getJSONObject(String cUrl, String authorization) throws java.text.ParseException, MalformedURLException, IOException, ParseException;
	String getEncodedAuthorization(String credentials);
	JSONArray getJSONArray(String cUrl, String authorization) throws java.text.ParseException, MalformedURLException, IOException, ParseException;
	JSONObject crudConfluencePage(ConfluenceIntegrationBean confluenceIntegrationBean, String cUrl) throws MalformedURLException, IOException, ParseException;
	Long getConfluencePageVersionNumber(String cUrl) throws java.text.ParseException, MalformedURLException, IOException, ParseException;
	Long maintainConfluencePage(String method, Long ancestorId, String pageTitle, String storageValue, boolean isUpdate, String spaceKey) throws MalformedURLException, IOException, org.json.simple.parser.ParseException, java.text.ParseException;
	void zipIt(String zipFile);
	void generateFileList(File node);
	void maintainLocalYamlFiles(EnvironmentCatalog environmentCatalog);
	void cleanYamlDirectoryFiles();
	File createAndStoreCandlestickChart(String serieName, String fileName, List<Object[]> results, String operationIdentifier) throws java.text.ParseException;
	JSONObject attachFileToConfluencePage(String cUrl, File attachment) throws IOException, ParseException;
	String getConfluenceAttachmentId(String fileName, String cUrl) throws java.text.ParseException, MalformedURLException, IOException, ParseException;
	File createAndStoreSplittedCandlestickChart(String serieName, String fileName, List<Object[]> results, String operationIdentifier) throws java.text.ParseException;
	Runtime getRuntime();
	void setRuntime(Runtime runtime);
	File createDashboardReport(EnvironmentCatalog environmentCatalog);
	void maintainBitbucketRepository(EnvironmentCatalog environmentCatalog);
	void executeCommand(String command, File workingDirectory) throws IOException, InterruptedException;
	void executeCommandError(String command, File workingDirectory) throws IOException, InterruptedException;
	void setupBluemixApicRuntime(String regionHostAcronym) throws IOException, InterruptedException;
	void logoutBluemixApicRuntime(String regionHostAcronym) throws IOException, InterruptedException;
	String getXMLString(String cUrl, String authorization) throws MalformedURLException, IOException, InterruptedException;
	String prettyPrint(String xml) throws ParserConfigurationException, SAXException, IOException, TransformerFactoryConfigurationError, TransformerException;
}
