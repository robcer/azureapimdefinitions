package mx.com.cemex.middleware.servicegovernancemanagement.entity;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "environmentvariables")
public class EnvironmentVariables {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_environment_variable")
	private Long idEnvironmentVariable;

    @NotNull
    @Column(name = "registered_at")
    @DateTimeFormat(style = "S-")
    private Date registeredAt;
    
    @Column(name = "id")
    private String id;
    
    @Column(name = "version")
    private String version;
    
    @Column(name = "api")
    private String api;
    
    @Column(name = "responsible")
    private String responsible;
    
    @Column(name = "email")
    private String email;
    
    @Column(name = "environment")
    private String environment;
    
    @Column(name = "region")
    private String region;
    
    @Column(name = "catalog")
    private String catalog;
    
    @Column(name = "type")
    private String type;
    
    @Column(name = "path")
    private String path;
    
    @Column(name = "value")
    private String value;
    
    @Column(name = "file_name")
    private String fileName;
    
    @Column(name = "path_to_environment")
    private String pathToEnvironment;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "id_environment", nullable = false, updatable = true, insertable = true)
    private EnvironmentCatalog environmentCatalog;

	public Long getIdEnvironmentVariable() {
		return idEnvironmentVariable;
	}

	public void setIdEnvironmentVariable(Long idEnvironmentVariable) {
		this.idEnvironmentVariable = idEnvironmentVariable;
	}

	public Date getRegisteredAt() {
		return registeredAt;
	}

	public void setRegisteredAt(Date registeredAt) {
		this.registeredAt = registeredAt;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getApi() {
		return api;
	}

	public void setApi(String api) {
		this.api = api;
	}

	public String getResponsible() {
		return responsible;
	}

	public void setResponsible(String responsible) {
		this.responsible = responsible;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getEnvironment() {
		return environment;
	}

	public void setEnvironment(String environment) {
		this.environment = environment;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getCatalog() {
		return catalog;
	}

	public void setCatalog(String catalog) {
		this.catalog = catalog;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getPath() {
		return path;
	}

	public void setPath(String path) {
		this.path = path;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getPathToEnvironment() {
		return pathToEnvironment;
	}

	public void setPathToEnvironment(String pathToEnvironment) {
		this.pathToEnvironment = pathToEnvironment;
	}

	public EnvironmentCatalog getEnvironmentCatalog() {
		return environmentCatalog;
	}

	public void setEnvironmentCatalog(EnvironmentCatalog environmentCatalog) {
		this.environmentCatalog = environmentCatalog;
	}
    
    
    
}
