-- this script is for SQL Server and Azure SQL

USE [USCLDCNXDBMSGD01]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS]') AND OBJECTPROPERTY(id, N'ISFOREIGNKEY') = 1)
ALTER TABLE [dbo].[qrtz_triggers] DROP CONSTRAINT FK_QRTZ_TRIGGERS_QRTZ_JOB_DETAILS
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS]') AND OBJECTPROPERTY(id, N'ISFOREIGNKEY') = 1)
ALTER TABLE [dbo].[qrtz_cron_triggers] DROP CONSTRAINT FK_QRTZ_CRON_TRIGGERS_QRTZ_TRIGGERS
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS]') AND OBJECTPROPERTY(id, N'ISFOREIGNKEY') = 1)
ALTER TABLE [dbo].[qrtz_simple_triggers] DROP CONSTRAINT FK_QRTZ_SIMPLE_TRIGGERS_QRTZ_TRIGGERS
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS]') AND OBJECTPROPERTY(id, N'ISFOREIGNKEY') = 1)
ALTER TABLE [dbo].[qrtz_simprop_triggers] DROP CONSTRAINT FK_QRTZ_SIMPROP_TRIGGERS_QRTZ_TRIGGERS
GO

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QRTZ_JOB_LISTENERS_QRTZ_JOB_DETAILS]') AND parent_object_id = OBJECT_ID(N'[dbo].[QRTZ_JOB_LISTENERS]'))
ALTER TABLE [dbo].[qrtz_job_listeners] DROP CONSTRAINT [FK_QRTZ_JOB_LISTENERS_QRTZ_JOB_DETAILS]

IF  EXISTS (SELECT * FROM sys.foreign_keys WHERE object_id = OBJECT_ID(N'[dbo].[FK_QRTZ_TRIGGER_LISTENERS_QRTZ_TRIGGERS]') AND parent_object_id = OBJECT_ID(N'[dbo].[QRTZ_TRIGGER_LISTENERS]'))
ALTER TABLE [dbo].[qrtz_trigger_listeners] DROP CONSTRAINT [FK_QRTZ_TRIGGER_LISTENERS_QRTZ_TRIGGERS]


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_calendars]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_calendars]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_cron_triggers]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_cron_triggers]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_blob_triggers]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_blob_triggers]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_fired_triggers]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_fired_triggers]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_paused_trigger_grps]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_paused_trigger_grps]
GO

IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[qrtz_job_listeners]') AND type in (N'U'))
DROP TABLE [dbo].[qrtz_job_listeners]

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_scheduler_state]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_scheduler_state]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_locks]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_locks]
GO
IF  EXISTS (SELECT * FROM sys.objects WHERE object_id = OBJECT_ID(N'[dbo].[qrtz_trigger_listeners]') AND type in (N'U'))
DROP TABLE [dbo].[qrtz_trigger_listeners]


IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_job_details]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_job_details]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_simple_triggers]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_simple_triggers]
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_simprop_triggers]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].qrtz_simprop_triggers
GO

IF EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[qrtz_triggers]') AND OBJECTPROPERTY(id, N'ISUSERTABLE') = 1)
DROP TABLE [dbo].[qrtz_triggers]
GO

create table [dbo].[qrtz_calendars] (
  [sched_name] [nvarchar] (120)  not null ,
  [calendar_name] [nvarchar] (200)  not null ,
  [calendar] [varbinary](max) not null
)
go

create table [dbo].[qrtz_cron_triggers] (
  [sched_name] [nvarchar] (120)  not null ,
  [trigger_name] [nvarchar] (150)  not null ,
  [trigger_group] [nvarchar] (150)  not null ,
  [cron_expression] [nvarchar] (120)  not null ,
  [time_zone_id] [nvarchar] (80) 
)
go

create table [dbo].[qrtz_fired_triggers] (
  [sched_name] [nvarchar] (120)  not null ,
  [entry_id] [nvarchar] (140)  not null ,
  [trigger_name] [nvarchar] (150)  not null ,
  [trigger_group] [nvarchar] (150)  not null ,
  [instance_name] [nvarchar] (200)  not null ,
  [fired_time] [bigint] not null ,
  [sched_time] [bigint] not null ,
  [priority] [integer] not null ,
  [state] [nvarchar] (16)  not null,
  [job_name] [nvarchar] (150)  null ,
  [job_group] [nvarchar] (150)  null ,
  [is_nonconcurrent] bit  null ,
  [requests_recovery] bit  null 
)
go

create table [dbo].[qrtz_paused_trigger_grps] (
  [sched_name] [nvarchar] (120)  not null ,
  [trigger_group] [nvarchar] (150)  not null 
)
go

create table [dbo].[qrtz_scheduler_state] (
  [sched_name] [nvarchar] (120)  not null ,
  [instance_name] [nvarchar] (200)  not null ,
  [last_checkin_time] [bigint] not null ,
  [checkin_interval] [bigint] not null
)
go

create table [dbo].[qrtz_locks] (
  [sched_name] [nvarchar] (120)  not null ,
  [lock_name] [nvarchar] (40)  not null 
)
go

create table [dbo].[qrtz_job_details] (
  [sched_name] [nvarchar] (120)  not null ,
  [job_name] [nvarchar] (150)  not null ,
  [job_group] [nvarchar] (150)  not null ,
  [description] [nvarchar] (250) null ,
  [job_class_name] [nvarchar] (250)  not null ,
  [is_durable] bit  not null ,
  [is_nonconcurrent] bit  not null ,
  [is_update_data] bit  not null ,
  [requests_recovery] bit  not null ,
  [job_data] [varbinary](max) null
)
go

create table [dbo].[qrtz_simple_triggers] (
  [sched_name] [nvarchar] (120)  not null ,
  [trigger_name] [nvarchar] (150)  not null ,
  [trigger_group] [nvarchar] (150)  not null ,
  [repeat_count] [integer] not null ,
  [repeat_interval] [bigint] not null ,
  [times_triggered] [integer] not null
)
go

create table [dbo].[qrtz_simprop_triggers] (
  [sched_name] [nvarchar] (120)  not null ,
  [trigger_name] [nvarchar] (150)  not null ,
  [trigger_group] [nvarchar] (150)  not null ,
  [str_prop_1] [nvarchar] (512) null,
  [str_prop_2] [nvarchar] (512) null,
  [str_prop_3] [nvarchar] (512) null,
  [int_prop_1] [int] null,
  [int_prop_2] [int] null,
  [long_prop_1] [bigint] null,
  [long_prop_2] [bigint] null,
  [dec_prop_1] [numeric] (13,4) null,
  [dec_prop_2] [numeric] (13,4) null,
  [bool_prop_1] bit null,
  [bool_prop_2] bit null,
  [time_zone_id] [nvarchar] (80) null 
)
go

create table [dbo].[qrtz_blob_triggers] (
  [sched_name] [nvarchar] (120)  not null ,
  [trigger_name] [nvarchar] (150)  not null ,
  [trigger_group] [nvarchar] (150)  not null ,
  [blob_data] [varbinary](max) null
)
go

create table [dbo].[qrtz_triggers] (
  [sched_name] [nvarchar] (120)  not null ,
  [trigger_name] [nvarchar] (150)  not null ,
  [trigger_group] [nvarchar] (150)  not null ,
  [job_name] [nvarchar] (150)  not null ,
  [job_group] [nvarchar] (150)  not null ,
  [description] [nvarchar] (250) null ,
  [next_fire_time] [bigint] null ,
  [prev_fire_time] [bigint] null ,
  [priority] [integer] null ,
  [trigger_state] [nvarchar] (16)  not null ,
  [trigger_type] [nvarchar] (8)  not null ,
  [start_time] [bigint] not null ,
  [end_time] [bigint] null ,
  [calendar_name] [nvarchar] (200)  null ,
  [misfire_instr] [integer] null ,
  [job_data] [varbinary](max) null
)
go

alter table [dbo].[qrtz_calendars] with nocheck add
  constraint [pk_qrtz_calendars] primary key  clustered
  (
    [sched_name],
    [calendar_name]
  ) 
go

alter table [dbo].[qrtz_cron_triggers] with nocheck add
  constraint [pk_qrtz_cron_triggers] primary key  clustered
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) 
go

alter table [dbo].[qrtz_fired_triggers] with nocheck add
  constraint [pk_qrtz_fired_triggers] primary key  clustered
  (
    [sched_name],
    [entry_id]
  ) 
go

alter table [dbo].[qrtz_paused_trigger_grps] with nocheck add
  constraint [pk_qrtz_paused_trigger_grps] primary key  clustered
  (
    [sched_name],
    [trigger_group]
  ) 
go

alter table [dbo].[qrtz_scheduler_state] with nocheck add
  constraint [pk_qrtz_scheduler_state] primary key  clustered
  (
    [sched_name],
    [instance_name]
  ) 
go

alter table [dbo].[qrtz_locks] with nocheck add
  constraint [pk_qrtz_locks] primary key  clustered
  (
    [sched_name],
    [lock_name]
  ) 
go

alter table [dbo].[qrtz_job_details] with nocheck add
  constraint [pk_qrtz_job_details] primary key  clustered
  (
    [sched_name],
    [job_name],
    [job_group]
  ) 
go

alter table [dbo].[qrtz_simple_triggers] with nocheck add
  constraint [pk_qrtz_simple_triggers] primary key  clustered
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) 
go

alter table [dbo].[qrtz_simprop_triggers] with nocheck add
  constraint [pk_qrtz_simprop_triggers] primary key  clustered
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) 
go

alter table [dbo].[qrtz_triggers] with nocheck add
  constraint [pk_qrtz_triggers] primary key  clustered
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) 
go

alter table [dbo].qrtz_blob_triggers with nocheck add
  constraint [pk_qrtz_blob_triggers] primary key  clustered
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) 
go

alter table [dbo].[qrtz_cron_triggers] add
  constraint [fk_qrtz_cron_triggers_qrtz_triggers] foreign key
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) references [dbo].[qrtz_triggers] (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) on delete cascade
go

alter table [dbo].[qrtz_simple_triggers] add
  constraint [fk_qrtz_simple_triggers_qrtz_triggers] foreign key
  (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) references [dbo].[qrtz_triggers] (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) on delete cascade
go

alter table [dbo].[qrtz_simprop_triggers] add
  constraint [fk_qrtz_simprop_triggers_qrtz_triggers] foreign key
  (
	[sched_name],
    [trigger_name],
    [trigger_group]
  ) references [dbo].[qrtz_triggers] (
    [sched_name],
    [trigger_name],
    [trigger_group]
  ) on delete cascade
go

alter table [dbo].[qrtz_triggers] add
  constraint [fk_qrtz_triggers_qrtz_job_details] foreign key
  (
    [sched_name],
    [job_name],
    [job_group]
  ) references [dbo].[qrtz_job_details] (
    [sched_name],
    [job_name],
    [job_group]
  )
go

create index idx_qrtz_t_j on qrtz_triggers(sched_name,job_name,job_group)
create index idx_qrtz_t_jg on qrtz_triggers(sched_name,job_group)
create index idx_qrtz_t_c on qrtz_triggers(sched_name,calendar_name)
create index idx_qrtz_t_g on qrtz_triggers(sched_name,trigger_group)
create index idx_qrtz_t_state on qrtz_triggers(sched_name,trigger_state)
create index idx_qrtz_t_n_state on qrtz_triggers(sched_name,trigger_name,trigger_group,trigger_state)
create index idx_qrtz_t_n_g_state on qrtz_triggers(sched_name,trigger_group,trigger_state)
create index idx_qrtz_t_next_fire_time on qrtz_triggers(sched_name,next_fire_time)
create index idx_qrtz_t_nft_st on qrtz_triggers(sched_name,trigger_state,next_fire_time)
create index idx_qrtz_t_nft_misfire on qrtz_triggers(sched_name,misfire_instr,next_fire_time)
create index idx_qrtz_t_nft_st_misfire on qrtz_triggers(sched_name,misfire_instr,next_fire_time,trigger_state)
create index idx_qrtz_t_nft_st_misfire_grp on qrtz_triggers(sched_name,misfire_instr,next_fire_time,trigger_group,trigger_state)

create index idx_qrtz_ft_trig_inst_name on qrtz_fired_triggers(sched_name,instance_name)
create index idx_qrtz_ft_inst_job_req_rcvry on qrtz_fired_triggers(sched_name,instance_name,requests_recovery)
create index idx_qrtz_ft_j_g on qrtz_fired_triggers(sched_name,job_name,job_group)
create index idx_qrtz_ft_jg on qrtz_fired_triggers(sched_name,job_group)
create index idx_qrtz_ft_t_g on qrtz_fired_triggers(sched_name,trigger_name,trigger_group)
create index idx_qrtz_ft_tg on qrtz_fired_triggers(sched_name,trigger_group)
go