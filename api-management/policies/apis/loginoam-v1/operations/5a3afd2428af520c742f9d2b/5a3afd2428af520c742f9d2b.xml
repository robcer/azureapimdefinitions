<!--
    IMPORTANT:
    - Policy elements can appear only within the <inbound>, <outbound>, <backend> section elements.
    - Only the <forward-request> policy element can appear within the <backend> section element.
    - To apply a policy to the incoming request (before it is forwarded to the backend service), place a corresponding policy element within the <inbound> section element.
    - To apply a policy to the outgoing response (before it is sent back to the caller), place a corresponding policy element within the <outbound> section element.
    - To add a policy position the cursor at the desired insertion point and click on the round button associated with the policy.
    - To remove a policy, delete the corresponding policy statement from the policy document.
    - Position the <base> element within a section element to inherit all policies from the corresponding section element in the enclosing scope.
    - Remove the <base> element to prevent inheriting policies from the corresponding section element in the enclosing scope.
    - Policies are applied in the order of their appearance, from the top down.
--><policies>
	<inbound>
		<set-variable name="dpAuthenticationOAMUser" value="{{dpAuthenticationOAMUser}}"/>
		<set-variable name="dpAuthenticationOAMPassword" value="{{dpAuthenticationOAMPassword}}"/>
		<cors>
			<allowed-origins>
				<origin>*</origin>
				<!-- allow any -->
			</allowed-origins>
			<allowed-methods>
				<!-- allow any -->
				<method>*</method>
			</allowed-methods>
			<allowed-headers>
				<!-- allow any -->
				<header>*</header>
			</allowed-headers>
		</cors>
		<set-variable name="grant" value="@(context.Request.Body.As<string>(preserveContent: true).Contains("grant_type=")? System.Uri.UnescapeDataString(context.Request.Body.As<string>(preserveContent: true).Split('&').Single(s => s.StartsWith("grant_type=")).Substring(11)): null )"/>
		<choose>
			<!-- evaluates if username parameter exists -->
			<when condition="@(((string)context.Variables["grant"]) =="refresh_token")">
				<send-request ignore-error="false" mode="new" response-variable-name="fnoauth2_refresh" timeout="1000">
					<!-- <set-url>{{OAM_Auth}}</set-url> -->
					<set-url>{{OAM_Auth}}</set-url>
					<set-method>POST</set-method>
					<set-header exists-action="override" name="Content-Type">
						<value>application/json</value>
					</set-header>
					<set-body>@{
                var answer = new JObject(
                    new JProperty("user", "refresh"),
                    new JProperty("countrycode", "us")
                );
                return answer.ToString();
            }</set-body>
				</send-request>
				<return-response>
					<set-status code="200" reason="Success"/>
					<set-header exists-action="override" name="Content-Type">
						<value>application/json</value>
					</set-header>
					<set-body>@(((IResponse)context.Variables["fnoauth2_refresh"]).Body.As<string>())</set-body>
				</return-response>
			</when>
		</choose>
		<set-variable name="usrnm" value="@(context.Request.Body.As<string>(preserveContent: true).Contains("username=")? System.Uri.UnescapeDataString(context.Request.Body.As<string>(preserveContent: true).Split('&').Single(s => s.StartsWith("username=")).Substring(9)): null )"/>
		<set-variable name="pwd" value="@(context.Request.Body.As<string>(preserveContent: true).Contains("password=")? System.Uri.UnescapeDataString(context.Request.Body.As<string>(preserveContent: true).Split('&').Single(s => s.StartsWith("password=")).Substring(9)): null )"/>
		<set-variable name="appCodeHeader" value="@(context.Request.Headers.ContainsKey("App-Code")?context.Request.Headers["App-Code"].Last():null)"/>
		<choose>
			<!-- evaluates if username parameter exists -->
			<when condition="@(context.Variables["usrnm"] != null)">
				<!-- creates base64 token -->
				<set-variable name="authbasic" value="@(Convert.ToBase64String(Encoding.UTF8.GetBytes(context.Variables["usrnm"]+":"+context.Variables["pwd"])))"/>
				<!-- calls datapower -->
				<send-request ignore-error="true" mode="new" response-variable-name="datapower_resp" timeout="20">
					<set-url>{{dp_autenticationoam}}/oam_authentication/profile</set-url>
					<set-method>GET</set-method>
					<set-header exists-action="override" name="Authorization">
						<value>@{
                            return ("Basic " + context.Variables["authbasic"]);
                        }</value>
					</set-header>
				</send-request>
				<choose>
					<when condition="@(((IResponse)context.Variables["datapower_resp"]).StatusCode == 401 || ((IResponse)context.Variables["datapower_resp"]).StatusReason.ToLower().Contains("unauthorized"))">
						<return-response>
							<set-status code="401" reason="Unauthorized"/>
							<set-header exists-action="override" name="Content-Type">
								<value>application/json</value>
							</set-header>
							<set-body>@{  
                                     
                                            var response =  "{\"httpCode\": \"401\",\"httpMessage\": \"No oauth information provided\",\"moreInformation\": \"unauthorized user for oauth\"}";
                                        return response.ToString();  
                                         
                                         
                                        }</set-body>
						</return-response>
					</when>
				</choose>
				<!-- saves token in variable -->
				<set-variable name="datapowerToken" value="@(((IResponse)context.Variables["datapower_resp"]).Body.As<string>(preserveContent: true).Contains("token")?((IResponse)context.Variables["datapower_resp"]).Body.As<JObject>(preserveContent: true)["token"]:null)"/>
				<set-variable name="countrycode" value="@(((IResponse)context.Variables["datapower_resp"]).Body.As<string>(preserveContent: true).Contains("countrycode")?((IResponse)context.Variables["datapower_resp"]).Body.As<JObject>(preserveContent: true)["countrycode"]:null)"/>
				<send-request ignore-error="false" mode="new" response-variable-name="fnoauth2" timeout="1000">
					<set-url>{{OAM_Auth}}</set-url>
					<set-method>POST</set-method>
					<set-header exists-action="override" name="Content-Type">
						<value>application/json</value>
					</set-header>
					<set-body>@{
                        var answer = new JObject(
                        new JProperty("user", (string)context.Variables["usrnm"]),
                        new JProperty("countrycode", context.Variables["countrycode"])
                        );
                        return answer.ToString();
                    }</set-body>
				</send-request>
				<set-variable name="region" value="@(((IResponse)context.Variables["fnoauth2"]).Body.As<string>(preserveContent: true).Contains("region")?((IResponse)context.Variables["fnoauth2"]).Body.As<JObject>(preserveContent: true)["region"]:"US")"/>
				<choose>
					<when condition="@(context.Variables["region"]+""=="UK")">
						<set-variable name="api_backend" value="{{uk_backend}}"/>
					</when>
					<otherwise>
						<set-variable name="api_backend" value="{{us_backend}}"/>
					</otherwise>
				</choose>
				<!-- calls .NET -->
				<send-request ignore-error="true" mode="new" response-variable-name="dot_net_resp" timeout="20">
					<set-url>@(context.Variables["api_backend"]+"/v5/secm/user?include=application")</set-url>
					<set-method>GET</set-method>
					<set-header exists-action="override" name="Authorization">
						<value>@{
                            return ("Bearer " + context.Variables["datapowerToken"]);
                        }</value>
					</set-header>
					<set-header exists-action="override" name="Jwt">
						<value>@{
                            return ""+context.Variables["datapowerToken"];
                        }</value>
					</set-header>
				</send-request>
				<choose>
					<!-- evaluates some part of the json response -->
					<when condition="@((string)((IResponse)context.Variables["dot_net_resp"]).Body.As<JObject>(preserveContent: true)["httpCode"] != null)">
						<!-- creates the response -->
						<!--Start Activity Log -->
						<set-variable name="authbasicDP" value="@(Convert.ToBase64String(Encoding.UTF8.GetBytes(((string)context.Variables["dpAuthenticationOAMUser"]) + ":" + ((string)context.Variables["dpAuthenticationOAMPassword"]))))"/>
						<send-request ignore-error="true" mode="new" response-variable-name="datapower_respdp" timeout="20">
							<set-url>{{dp_autenticationoam}}/oam_authentication/profile</set-url>
							<set-method>GET</set-method>
							<set-header exists-action="override" name="Authorization">
								<value>@("Basic " + context.Variables["authbasicDP"])</value>
							</set-header>
						</send-request>
						<choose>
							<when condition="@{
                                var response = ((IResponse)context.Variables["datapower_respdp"]).Body.As<string>(preserveContent: true);
 
                                if (response.Contains("message")) {
                                    return true;
                                }
 
                                return false;
                            }">
								<set-variable name="detailErr" value="@{
                                var response = ((IResponse)context.Variables["datapower_respdp"]).Body.As<JObject>(preserveContent: true);
                                var detail = response["message"].ToString();
                                return "Authentication Service error could not be generated JWT -- " + detail;
                                }"/>
								<return-response>
									<set-status code="500" reason="Internal Server Error"/>
									<set-body>@{
                                    return @"{  ""httpCode"":""500"",
                                    ""httpMessage"": ""*Internal Server Error"",
                                    ""reasonCode"":""2006"",
                                    ""moreInformation"":""" + context.Variables["detailErr"] + @"""}";
                                }</set-body>
								</return-response>
							</when>
							<when condition="@(((IResponse)context.Variables["datapower_respdp"]).StatusCode != 200)">
								<return-response>
									<set-status code="500" reason="Internal Server Error"/>
									<set-body>@{
                                    return @"{  ""httpCode"":""500"",
                                    ""httpMessage"": ""/Internal Server Error"",
                                    ""reasonCode"":""2006"",
                                    ""moreInformation"":""Authentication Service error could not be generated JWT""}";
                                    }</set-body>
								</return-response>
							</when>
							<otherwise>
								<send-request ignore-error="true" mode="new" response-variable-name="Log" timeout="20">
									<set-url>@(context.Variables["api_backend"]+"/v5/secm/audit/log")</set-url>
									<set-method>POST</set-method>
									<set-header exists-action="override" name="Authorization">
										<value>@{
                                            var x = ((IResponse)context.Variables["datapower_respdp"]).Body.As<JObject>(preserveContent: true);
                                            return "Bearer " + x["token"].ToString();
                                        }</value>
									</set-header>
									<set-header exists-action="override" name="Jwt">
										<value>@{
                                        var x = ((IResponse)context.Variables["datapower_respdp"]).Body.As<JObject>(preserveContent: true);
                                        return x["token"].ToString();
                                        }</value>
									</set-header>
									<set-header exists-action="override" name="Content-Type">
										<value>application/json</value>
									</set-header>
									<set-body>@{
                                        var response =  new JObject(
                                                                new JProperty("executedBy", (string)context.Variables["usrnm"]),
                                                                new JProperty("changeType", "LOGIN"),
                                                                new JProperty("changeEntityId", ""),
                                                                new JProperty("changeEntityTable", "AuditLog"),
                                                                new JProperty("action", "httpCode: 401 | httpMessage: unauthorized, unauthorized user oauth"),
                                                                new JProperty("notes", ""),
                                                                new JProperty("endPoint", "")
                                                            );
                                                             
                                            return response.ToString();
                                    }</set-body>
								</send-request>
							</otherwise>
						</choose>
						<!--End Activity Log -->
						<return-response>
							<set-status code="401" reason="Unauthorized"/>
							<set-header exists-action="override" name="Content-Type">
								<value>application/json</value>
							</set-header>
							<set-body>@{ 
                                    //var a = ((IResponse)context.Variables["dot_net_resp"]).StatusCode;
                                    var response =  "{\"httpCode\": \"401\",\"httpMessage\": \"No oauth information provided\",\"moreInformation\": \"unauthorized user for oauth\"}";
                                    return response.ToString();  
                                }</set-body>
						</return-response>
					</when>
					<!-- evaluates statusCode != 201 -->
					<when condition="@(((IResponse)context.Variables["dot_net_resp"]).StatusCode != 200)">
						<!--Start Activity Log -->
						<set-variable name="authbasicDP" value="@(Convert.ToBase64String(Encoding.UTF8.GetBytes(((string)context.Variables["dpAuthenticationOAMUser"]) + ":" + ((string)context.Variables["dpAuthenticationOAMPassword"]))))"/>
						<send-request ignore-error="true" mode="new" response-variable-name="datapower_respdp" timeout="20">
							<set-url>{{dp_autenticationoam}}/oam_authentication/profile</set-url>
							<set-method>GET</set-method>
							<set-header exists-action="override" name="Authorization">
								<value>@("Basic " + context.Variables["authbasicDP"])</value>
							</set-header>
						</send-request>
						<choose>
							<when condition="@{
                                var response = ((IResponse)context.Variables["datapower_respdp"]).Body.As<string>(preserveContent: true);
 
                                if (response.Contains("message")) {
                                    return true;
                                }
 
                                return false;
                            }">
								<set-variable name="detailErr" value="@{
                                var response = ((IResponse)context.Variables["datapower_respdp"]).Body.As<JObject>(preserveContent: true);
                                var detail = response["message"].ToString();
                                return "Authentication Service error could not be generated JWT -- " + detail;
                                }"/>
								<return-response>
									<set-status code="500" reason="Internal Server Error"/>
									<set-body>@{
                                    return @"{  ""httpCode"":""500"",
                                    ""httpMessage"": ""*Internal Server Error"",
                                    ""reasonCode"":""2006"",
                                    ""moreInformation"":""" + context.Variables["detailErr"] + @"""}";
                                }</set-body>
								</return-response>
							</when>
							<when condition="@(((IResponse)context.Variables["datapower_respdp"]).StatusCode != 200)">
								<return-response>
									<set-status code="500" reason="Internal Server Error"/>
									<set-body>@{
                                    return @"{  ""httpCode"":""500"",
                                    ""httpMessage"": ""/Internal Server Error"",
                                    ""reasonCode"":""2006"",
                                    ""moreInformation"":""Authentication Service error could not be generated JWT""}";
                                    }</set-body>
								</return-response>
							</when>
							<otherwise>
								<send-request ignore-error="true" mode="new" response-variable-name="Log" timeout="20">
									<set-url>@(context.Variables["api_backend"]+"/v5/secm/audit/log")</set-url>
									<set-method>POST</set-method>
									<set-header exists-action="override" name="Authorization">
										<value>@{
                                            var x = ((IResponse)context.Variables["datapower_respdp"]).Body.As<JObject>(preserveContent: true);
                                            return "Bearer " + x["token"].ToString();
                                        }</value>
									</set-header>
									<set-header exists-action="override" name="Jwt">
										<value>@{
                                        var x = ((IResponse)context.Variables["datapower_respdp"]).Body.As<JObject>(preserveContent: true);
                                        return x["token"].ToString();
                                        }</value>
									</set-header>
									<set-header exists-action="override" name="Content-Type">
										<value>application/json</value>
									</set-header>
									<set-body>@{
                                        var response =  new JObject(
                                                                new JProperty("executedBy", (string)context.Variables["usrnm"]),
                                                                new JProperty("changeType", "LOGIN"),
                                                                new JProperty("changeEntityId", ""),
                                                                new JProperty("changeEntityTable", "AuditLog"),
                                                                new JProperty("action", "httpCode: 401 | httpMessage: statuscode different to 200"),
                                                                new JProperty("notes", ""),
                                                                new JProperty("endPoint", "")
                                                            );
                                                             
                                            return response.ToString();
                                    }</set-body>
								</send-request>
							</otherwise>
						</choose>
						<!--End Activity Log -->
						<!-- creates the response -->
						<return-response>
							<set-status code="401" reason="Unauthorized"/>
							<set-header exists-action="override" name="Content-Type">
								<value>application/json</value>
							</set-header>
							<set-body>@{  
                                    var response =  "{\"httpCode\": \"401\",\"httpMessage\": \"No oauth information provided\",\"moreInformation\": \"unauthorized user for oauth\"}";
                                    return response.ToString();  
                                }</set-body>
						</return-response>
					</when>
					<!-- correct response from .NET -->
					<otherwise>
						<set-variable name="isAuthorized" value="@{
                                                                    var netResponse = ((IResponse)context.Variables["dot_net_resp"]).Body.As<JObject>(preserveContent: true);
                                                                     
                                                                    var apps =  (JArray)netResponse["applications"];
                                                                    var editAuth = "";
                                                                     
                                                                    if (apps.HasValues){
                                                                         editAuth = (string)(apps.First()["userAuth"]);
                                                                    }
                                                                     
                                                                    var returned = "N";
                                                                     
                                                                    if ((string)netResponse["status"]=="A"){
                                                                     
                                                                        if ((string)netResponse["powerUser"]=="N"){
                                                                            if( editAuth != ""){
                                                                                returned= "Y";
                                                                            }
                                                                        }else{
                                                                            returned= "Y";
                                                                        }
                                                                         
                                                                    }
                                                                     
                                                                     
                                                                    return returned;
                                                                    
                                                                }"/>
						<choose>
							<when condition="@(context.Variables["isAuthorized"] == "Y")">
								<!--Start Activity Log -->
								<send-request ignore-error="true" mode="new" response-variable-name="Log" timeout="20">
									<set-url>@(context.Variables["api_backend"]+"/v5/secm/audit/log")</set-url>
									<set-method>POST</set-method>
									<set-header exists-action="override" name="Authorization">
										<value>@{
                                            return "Bearer " + context.Variables["datapowerToken"];
                                        }</value>
									</set-header>
									<set-header exists-action="override" name="Jwt">
										<value>@{
                                                    return "" + context.Variables["datapowerToken"];
                                                }</value>
									</set-header>
									<set-header exists-action="override" name="Content-Type">
										<value>application/json</value>
									</set-header>
									<set-body>@{
                                                var response =  new JObject(
                                                                        new JProperty("executedBy", (string)context.Variables["usrnm"]),
                                                                        new JProperty("changeType", "LOGIN"),
                                                                        new JProperty("changeEntityId", ""),
                                                                        new JProperty("changeEntityTable", "AuditLog"),
                                                                        new JProperty("action", "httpCode: 200 | httpMessage: Login successful"),
                                                                        new JProperty("notes", ""),
                                                                        new JProperty("endPoint", "")
                                                                    );
                                                                     
                                                    return response.ToString();
                                            }</set-body>
								</send-request>
								<!--End Activity Log -->
								<!-- <set-variable name="validEmails" value="MX_PRAGA_CERT@MAILINATOR.COM,US_PRAGA_CERT@MAILINATOR.COM,CO_PRAGA_CERT@MAILINATOR.COM,GB_PRAGA_CERT@MAILINATOR.COM" />
                                <choose>
                                    <when condition="@(((IResponse)context.Variables["dot_net_resp"]).Body.As<JObject>(preserveContent: true)["profile"]["userType"]+"" =="C" && ((string)context.Variables["usrnm"]).ToUpper().IndexOf(("MAILINATOR.COM").ToUpper())<0)">
                                        <set-variable name="esValido" value="false" />
                                        <return-response>
                                            <set-status code="401" reason="Unauthorized" />
                                            <set-header name="Content-Type" exists-action="override">
                                                <value>application/json</value>
                                            </set-header>
                                            <set-body>@{  
                                                       
                                                            var response =  "{\"httpCode\": \"401\",\"httpMessage\": \"No oauth information provided\",\"moreInformation\": \"unauthorized user for oauth..\"}";
                                                        return response.ToString();  
                                                         
                                                         
                                                        }</set-body>
                                        </return-response>
                                    </when>
                                </choose> -->
								<return-response>
									<set-status code="200" reason="OK"/>
									<set-header exists-action="override" name="Content-Encoding">
										<value>gzip</value>
									</set-header>
									<set-header exists-action="override" name="Content-Type">
										<value>application/json</value>
									</set-header>
									<set-body>@{ 
                                            var a =((IResponse)context.Variables["datapower_resp"]).Body.As<JObject>();
                                            var b = ((IResponse)context.Variables["dot_net_resp"]).Body.As<JObject>();
                                             
                                            var oauth2 = ((IResponse)context.Variables["fnoauth2"]).Body.As<JObject>();
                                             
                                            /*new JObject(
                                                                new JProperty("access_token",""),
                                                                new JProperty("expires_in",""),
                                                                new JProperty("scope",""),
                                                                new JProperty("refresh_token","")
                                                            );*/
                                             
                                             
                                            var powerUser = "Y";
                                            var edit = "Y";
                                            var customerId = 0;
                                             
                                            if ( (string)b["powerUser"] =="S"){
                                                powerUser = "S";
                                                edit = "N";
                                            } else if ((string)b["powerUser"] =="N") {
                                             
                                                powerUser = "N";
                                                var editAuth = (string)((JArray)(b["applications"]).First()["userAuth"]);
                                                 
                                                if (editAuth != "E"){
                                                    edit = "N";
                                                }
                                            }
                                             
                                            var customer = (JObject)b["customer"];
                                             
                                            if ( customer != null){
                                                customerId = customer["customerId"].ToObject<int>();
                                            }
                                 
                                            //new JProperty("oauth2",oauth2 ),
                                            var response =  new JObject(
 
                                                                new JProperty("oauth2",oauth2 ),
                                                                new JProperty("jwt", a["token"]),
                                                                new JProperty("powerUser", powerUser),
                                                                new JProperty("edit", edit),
                                                                new JProperty("customerId", customerId),
                                                                new JProperty("sessionId", b["userId"]),
                                                                new JProperty("role", b["profile"]["userType"]),
                                                                new JProperty("country", b["countryCode"]),
                                                                new JProperty("customer", (JObject)b["customer"]),
                                                                new JProperty("profile", (JObject)b["profile"]),
                                                                new JProperty("applications", (JArray)b["applications"])
                                                 
                                                            );
                                                             
                                             
                                                         
                                            return response.ToString(); 
                                                 
                                        }</set-body>
								</return-response>
							</when>
							<otherwise>
								<!--Start Activity Log -->
								<set-variable name="authbasicDP" value="@(Convert.ToBase64String(Encoding.UTF8.GetBytes(((string)context.Variables["dpAuthenticationOAMUser"]) + ":" + ((string)context.Variables["dpAuthenticationOAMPassword"]))))"/>
								<send-request ignore-error="true" mode="new" response-variable-name="datapower_resp" timeout="20">
									<set-url>{{dp_autenticationoam}}/oam_authentication/profile</set-url>
									<set-method>GET</set-method>
									<set-header exists-action="override" name="Authorization">
										<value>@("Basic " + context.Variables["authbasicDP"])</value>
									</set-header>
								</send-request>
								<choose>
									<when condition="@{
                                var response = ((IResponse)context.Variables["datapower_resp"]).Body.As<string>(preserveContent: true);
 
                                if (response.Contains("message")) {
                                    return true;
                                }
 
                                return false;
                            }">
										<set-variable name="detailErr" value="@{
                                var response = ((IResponse)context.Variables["datapower_resp"]).Body.As<JObject>(preserveContent: true);
                                var detail = response["message"].ToString();
                                return "Authentication Service error could not be generated JWT -- " + detail;
                                }"/>
										<return-response>
											<set-status code="500" reason="Internal Server Error"/>
											<set-body>@{
                                    return @"{  ""httpCode"":""500"",
                                    ""httpMessage"": ""*Internal Server Error"",
                                    ""reasonCode"":""2006"",
                                    ""moreInformation"":""" + context.Variables["detailErr"] + @"""}";
                                }</set-body>
										</return-response>
									</when>
									<when condition="@(((IResponse)context.Variables["datapower_resp"]).StatusCode != 200)">
										<return-response>
											<set-status code="500" reason="Internal Server Error"/>
											<set-body>@{
                                    return @"{  ""httpCode"":""500"",
                                    ""httpMessage"": ""/Internal Server Error"",
                                    ""reasonCode"":""2006"",
                                    ""moreInformation"":""Authentication Service error could not be generated JWT""}";
                                    }</set-body>
										</return-response>
									</when>
									<otherwise>
										<send-request ignore-error="true" mode="new" response-variable-name="Log" timeout="20">
											<set-url>{{us_backend}}/v5/secm/audit/log</set-url>
											<set-method>POST</set-method>
											<set-header exists-action="override" name="Authorization">
												<value>@{
                                            var x = ((IResponse)context.Variables["datapower_resp"]).Body.As<JObject>(preserveContent: true);
                                            return "Bearer " + x["token"].ToString();
                                        }</value>
											</set-header>
											<set-header exists-action="override" name="Jwt">
												<value>@{
                                        var x = ((IResponse)context.Variables["datapower_resp"]).Body.As<JObject>(preserveContent: true);
                                        return x["token"].ToString();
                                        }</value>
											</set-header>
											<set-header exists-action="override" name="Content-Type">
												<value>application/json</value>
											</set-header>
											<set-body>@{
                                        var response =  new JObject(
                                                                new JProperty("executedBy", (string)context.Variables["usrnm"]),
                                                                new JProperty("changeType", "LOGIN"),
                                                                new JProperty("changeEntityId", ""),
                                                                new JProperty("changeEntityTable", "AuditLog"),
                                                                new JProperty("action", "httpCode: 401 | httpMessage: unauthorized, user status invalid"),
                                                                new JProperty("notes", ""),
                                                                new JProperty("endPoint", "")
                                                            );
                                                             
                                            return response.ToString();
                                    }</set-body>
										</send-request>
									</otherwise>
								</choose>
								<!--End Activity Log -->
								<return-response>
									<set-status code="401" reason="Unauthorized"/>
									<set-header exists-action="override" name="Content-Type">
										<value>application/json</value>
									</set-header>
									<set-body>@{  
                                                       
                                                            var response =  "{\"httpCode\": \"401\",\"httpMessage\": \"No oauth information provided\",\"moreInformation\": \"unauthorized user for oauth\"}";
                                                        return response.ToString();  
                                                         
                                                         
                                                        }</set-body>
								</return-response>
							</otherwise>
						</choose>
					</otherwise>
				</choose>
			</when>
			<otherwise>
				<return-response>
					<set-status code="401" reason="Unauthorized"/>
					<set-header exists-action="override" name="Content-Type">
						<value>application/json</value>
					</set-header>
					<set-body>@{  
                                   
                                        var response =  "{\"httpCode\": \"401\",\"httpMessage\": \"No oauth information provided\",\"moreInformation\": \"unauthorized user for oauth\"}";
                                    return response.ToString();  
                                     
                                     
                                    }</set-body>
				</return-response>
			</otherwise>
		</choose>
	</inbound>
	<backend>
		<base/>
	</backend>
	<outbound>
		<base/>
	</outbound>
	<on-error>
		<base/>
	</on-error>
</policies>

